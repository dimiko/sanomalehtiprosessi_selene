<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_karttuvat.php");
include("global_variables.php");
?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <meta http-equiv="refresh" content="180" >
    <title>Sanomalehtiprosessin tila</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
</head>
<body>

<?php
directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);
docIdSearchForDocPage(basename(__FILE__));
echo"<div class='dailyReportFrame'>";
  echo"<h3>Tänään</h3>";
  
  $export_sivuja = dailyReport($db, $queryExportted);
  $accepted_sivuja = dailyReport($db, $queryAccepted);
  $rejected_sivuja = dailyReport($db, $queryRejected);
  $filmattu_sivuja = dailyReport($db, $queryFilmed);
  $importoitu_sivuja = dailyReport($db, $queryImported);
  $deletoitu_sivuja = dailyReport($db, $queryDeleted);
 
  echo"<table class='dailyTable'>";
  echo"<tr'>";
    echo "<th>Exportoitu&nbsp;</th>";
    echo "<th>Tarkastettu&nbsp;</th>";
    echo "<th> Tulostettu&nbsp;</th>";
    echo "<th>Importoitu&nbsp;</th>";
    echo "<th>Poistettu</th>";
  echo"<tr>";
    echo "<td>$export_sivuja</td>";
    echo "<td>$accepted_sivuja</td>";
    echo "<td>$filmattu_sivuja</td>";
    echo "<td>$importoitu_sivuja</td>";
    echo "<td>$deletoitu_sivuja</td>";
  echo"</tr>";
  echo "</table>";
   
echo"</div>";

naviHeader();

echo"<div class='dailyStaticFrame'>";
 
  include("acknowledgment_messages.php");
  
  $message = $_SESSION['message'];
  echo "<div id='okMessage'>$message</div>";
  
  $docId = $_GET["docId"];
  $page = $_GET["page"];

  doc_info_page($db, $page, $docId);

echo"</div>";

close_sql($db);

?>
</body>
</html>

