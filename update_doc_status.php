<?php
include("sessio.php");
include("functions.php");

$column = $_POST["submit"];
$docId = $_POST["docId"];
$page = $_POST["page"];

$processRestarOrRemove = true;

$isRemoveProcess = $column == 'Remove';
if( !$isRemoveProcess ) {

    $identifier = GetOneColumnFromSlstatusByDocid($docId, "path");
    $handle = fopen("log/log.txt", "r");
    if ($handle) {
    
         while (($line = fgets($handle)) !== false) {
            $line = str_replace(array("\r", "\n"), '', $line);
             $lineToList = explode(" ", $line);
             $lastElement = end($lineToList);
             if($identifier == $lastElement) {
                $processRestarOrRemove = false;
             }
         }
    
    }
    fclose($handle);

}

if($processRestarOrRemove) {
    
    if($column == "Remove from Debosit") {
        $result = deleteIssueFromDepositDatabase($db, $conn, $docId);
        $result = 1;
        if( !$result ) {
            $_SESSION['displayErrorMessage'] = "DocId : $docId, DepositBrowserin poistossa tapahtui virhe.";
        } else {
            $_SESSION['displayOkMessage'] = "DocId : $docId poistettu Deposit Browserin tietokannasta onnistuneesti.";
        }       
    } else {
        updateDocumentStatus($db, $docId, $column);
        writeLog("Changing DocID : $docId $column status = 1 $identifier");
        if( $isRemoveProcess ) {
            
            $result = deleteIssueFromDepositDatabase($db, $conn, $docId);
            $result = 1;
            if( !$result ) {
                $_SESSION['displayErrorMessage'] = "DocId : $docId, DepositBrowserin poistossa tapahtui virhe.";
            } else {
                $_SESSION['displayOkMessage'] = "DocId : $docId laitettu onnistuneesti odottamaan ajastettua poistoa.";
            }
        
        } else {
            $result = deleteIssueFromDepositDatabase($db, $conn, $docId);
            $result = 1;
            if( !$result ) {
                $_SESSION['displayErrorMessage'] = "DocId : $docId, DepositBrowserin poistossa tapahtui virhe.";
            }
            
            $_SESSION['displayOkMessage'] = "DocId : $docId laitettu onnistuneesti odottamaan ajastettua uudellenkäynnistystä.";
            
        }
    }
   
} else {
    $_SESSION['displayErrorMessage'] = "$identifier <br>on uudelleenkäynnistetty jo kerran. <br>Ei voida suorittaa Restart toimintoa uudestaan!!!";    
}

header("Location: $page");

?>

