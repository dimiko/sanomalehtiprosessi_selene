<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_karttuvat.php");
include("global_variables.php");

   if (isset($_POST["nextid"])) {
        $nideid=$_POST["nextid"];
   } 
   if (isset($_POST["nideid"])) {
        $nideid=$_POST["nideid"];
    } elseif (isset($_GET["nideid"])) {
        $nideid=$_GET["nideid"];
    } else {
     echo "ei nideid:tä";
     exit;
}


?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>Sanomalehtin tarkistu sivu</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">

</head>
<style>
.grid-container {
  display: grid;
}
.grid-item {
 height:400px;
 width:400px;
}



    .image-grid {
      display: grid;
      grid-template-columns: repeat(auto-fill, minmax(20%, 1fr));
      grid-gap: 20px;
      background-color:grey;
      padding: 20px;
    }

    .image-item {
      position: relative;
      overflow: hidden;
    }

    .image-item img {
      width: 100%;
      height: auto;
      display: block;
    }
    


.overlay {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    text-align: center;
    background-color: rgba(0, 0, 0, 0.7);
    color: #fff;
    padding: 10px;
    border-radius: 5px;
}

        .image-item:hover .overlay {
            opacity: 1;
        }

        .overlay h2 {
            margin: 0;
            font-size: 1.5rem;
        }

        .overlay p {
            margin: 0;
            margin-top: 10px;
            font-size: 1rem;
	    color:darkgrey;
        }

        form {
            width: 800px; /* Adjust the width as needed */
            padding: 20px;
        }


        form input,
        form button {
            margin-bottom: 0px;
            padding: 20px;
        }

</style>
<body>



<?php



naviHeader();
$ora_db = oci_connect('diona', 'diona', 'pythia/dionapdb.docworks.lib.helsinki.fi','AL32UTF8');


function showPages ($nideid,$db) {

$select="select kl.paanimeke,kl.issn,sn.no,to_char(n.julkaisu_pvm,'DD.MM.YYYY') as PVM,n.aineistoyleismaare
from kausi_lehti kl, sarjajulkaisu_nide sn, nide n
where kl.issn=sn.issn
and sn.sarjanide_id=n.nide_id
and n.nide_id=".$nideid;

$stid = oci_parse($db, $select);
$res=oci_execute($stid);
        if (!$res) {
                echo "error:".oci_error()."--";
                exit(9);
        }

$row = oci_fetch_assoc($stid);

if ($row) {
    $paanimeke = $row['PAANIMEKE'];
    $pvm = $row['PVM'];
    $issn = $row['ISSN'];
    $no = $row['NO'];
    $aineistoyleismaare=$row['AINEISTOYLEISMAARE'];
} else {
    echo "No rows found";
}

switch ($aineistoyleismaare) {

	case "SAN" :
		$urlAYM="sanomalehti";
		$topImage=True;
		break;

	case "AIK" :
		$urlAYM="aikakausi";
		$topImage=False;
		break;

	case "pie" :
		$urlAYM="pienpainate";
		$topImage=False;
		break;

	default:
		$urlAYM="teos";
		$topImage=False;
		break;


}

$select2="select 'https://digi.kansalliskirjasto.fi/".$urlAYM."/binding/'||nide_id||'/image/'||juoksevanumero||'?tracking=null' as URL
,juoksevanumero
,sivunumero
from sivu 
where nide_id=".$nideid."
order by juoksevanumero";

$stid2 = oci_parse($db, $select2);
$res2=oci_execute($stid2);
        if (!$res2) {
                echo "error:".oci_error()."--";
                exit(9);
        }

if ($topImage) {
    echo "<div><img style=\"width:100%\" src=\"http://selene.docworks.lib.helsinki.fi/sl-prosessi/imagetop.php?nideid=".$nideid."\"></div>";
}
echo "<div style=\"width:100%; padding-top:20px; background-color:grey;\"> 
<h1 style=\"text-align: center; color:white;\">$paanimeke (todo) no $no $pvm</h1> 
</div>";
echo "<div class=\"image-grid\">";

while (($row = oci_fetch_array($stid2, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
        $imageUrl=$row['URL'];
        $sivunumero=$row['SIVUNUMERO'];
        $juoksevanumero=$row['JUOKSEVANUMERO'];
        echo "<div class=\"image-item\"><img style=\"width:100%;\"src=\"http://selene.docworks.lib.helsinki.fi/sl-prosessi/display_images.php\">
        <div class=\"overlay\">
            <h2>".$sivunumero."</h2>
            <p>". $juoksevanumero."</p>
        </div>
</div>";
}
echo "</div>";
$nideid=$nideid+1;
echo "<div style=\"background-color:grey;\">
<form id=\"lomake\" style=\"margin-left:auto;margin-right:auto; background-color:grey;\" method=\"post\" action=\"sivutarkistus.php\">
  <input type=\"submit\" style=\"background-color:red; font-size:2rem;\" value=\"Hylkää\">
  <textarea cols=\"40\" rows=\"4\"id=\"kommentti\" name=\"kommentti\" placeholder=\"kommentti\"></textarea>
  <input type=\"submit\" style=\"background-color:green; font-size:2rem;\" value=\"Hyväksy\">
  <input type=\"hidden\" name=\"nideid\" value=\"".$nideid."\">
</form>
</div>
";
}

showPages($nideid,$ora_db);

?>
</body>
</html>

