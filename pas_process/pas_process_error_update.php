<?php
include("../sessio.php");
include("../functions.php");

$column = $_POST["submit"];
$zipPath = $_POST["zipPath"];
$zipName = $_POST["zipName"];
$errMessageFile = $_POST["errMessageFile"];
$errFile = $_POST["errFile"];
$page = $_POST["page"];

$isBypassProcess = $column == 'Ohita';
if( $isBypassProcess ) {
    updatePasZipToBypass($zipName, "Ohitettu manuaalisesti");
    unlink($zipPath);
    unlink($errFile);
    unlink($errMessageFile);
    $_SESSION['displayOkMessage'] = "Paketti : $zipPath ohitettu.";
} else {
    $newFile = str_replace(".err","", $errMessageFile);
    //rename($errMessageFile, $newFile);
	updatePasZipInprocessingToNull($zipName);
	shell_exec("mv $errMessageFile $newFile");
    $_SESSION['displayOkMessage'] = "Paketti : $zipPath käynnistetty uudestaan.";
}

close_sql($db);
oci_close($conn);
oci_close($conn_diona);
header("Location: ../$page");

?>

