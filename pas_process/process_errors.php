<?php
include("../sessio.php");
include("../functions.php");
include("../global_variables.php");



$column = $_POST["submit"];
$page = $_POST["page"];
$errorMessage = $_POST["errorMsg"];
$zipList = $_POST['zipList'];
$array = $pieces = explode(",", $zipList);

$isBypassProcess = $column == 'Ohita';
if( $isBypassProcess ) {
    
    foreach($array as $zipName) {
        $zipPath = "$PAS_transfer_folder/$zipName";
        unlink($zipPath);
        updatePasZipToBypass($zipName, "$errorMessage");
        $extrataskFiles = GetExtrataskFileForZip($zipName, $PAS_error_folder);
        foreach($extrataskFiles as $file) {
            $filePath = "$PAS_error_folder/$file";
            unlink($filePath);
        }
    } 
    $_SESSION['displayOkMessage'] = "Paketit : $zipList OHITETTU.";
    
} else {
    foreach($array as $zipName) {
    updatePasZipInprocessingToNull($zipName);
    CreateTaskTclFile($zipName);
}
    $_SESSION['displayOkMessage'] = "Paketit : $zipList käynnistetty uudestaan. \nHOX!! Paketit poistuvan virhelistalta viiveellä.";
}

close_sql($db);
oci_close($conn);
oci_close($conn_diona);

header("Location: ../$page");

?>