<?php
include("../sessio.php");

$onOff = $_GET["onOff"];
$arc = $_GET["arc"];
$zip = $_GET["zip"];
$minZip = $_GET["minZip"];

$systemOnOff = ($onOff == 'ON') ? 1 : 0;
$sql = "update PAS_PROCESS_CONF set processing_arc = '$arc', process_on_off = $systemOnOff,
        processing_zip_in_sequence = $zip, min_zip_in_process = $minZip";
        
$result = mssql_query($sql) or die(mssql_errors());
if( !$result ) {
    $_SESSION['displayErrorMessage'] = "Järjestelmän päivityksessä tapahtui virhe.";
} else {
    $_SESSION['displayOkMessage'] = "Järjestelmä päivitetty onnistuneesti.";
}
mssql_free_result($result);

close_sql($db);
oci_close($conn);
oci_close($conn_diona);
header("Location: ../pas_process.php");

?>