<?php
include("../sessio.php");
include("../functions.php");
include("../global_variables.php");

$zipName = $_POST["zip_name"];
$pas_zip = $_POST["pas_zip_name"];
$page = $_POST["page"];
$uri = $_POST["uri"];

$biaFolder = "/mnt/export";
$zipPath = $biaFolder . "$uri";
$zipDestinationPath = "$PAS_transfer_folder/$zipName";
if( file_exists($zipPath) ) {
    updatePasZipInprocessingToNull($zipName);
    updatePasZipStatus($zipName, 'kopioitu muunnettavaksi'); 
    shell_exec("cp $zipPath $zipDestinationPath");
    //echo "cp $zipPath $zipDestinationPath";
    CreateTaskTclFile($zipName);
    $_SESSION['displayOkMessage'] = "Paketti : $zipName käynnistetty uudestaan.";
} else {
    $_SESSION['displayErrorMessage'] = "$zipPath : ei löytynyt.";
}

close_sql($db);
oci_close($conn);
oci_close($conn_diona);


header("Location: ../pas_process_rejected.php");
//header("Location: ../$page?zip=$pas_zip");

?>