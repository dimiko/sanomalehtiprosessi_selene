<?php
include("../sessio.php");
include("../functions.php");
include("../global_variables.php");

$biaFolder = "/mnt/export";

$comment = $_POST["comment"];

$query = "select uri, zip_name from dw_nlf_custom.dbo.lehtipas where comment = '$comment'";

$result = mssql_query($query);
$okCounter = 0;
$errorCounter = 0;

while($row = mssql_fetch_array($result)) {
    $uri = $row["uri"];
    $zipName = $row["zip_name"];
    $zipPath = $biaFolder . "$uri";
    // FIXME
    //$PAS_transfer_folder = "/mnt/akte/pas_process/transfer_temp";
    $zipDestinationPath = "$PAS_transfer_folder/$zipName";
    if( file_exists($zipPath) ) {
        $okCounter++;
        updatePassedRestartedZip($zipName);
        shell_exec("cp $zipPath $zipDestinationPath");
        CreateTaskTclFile($zipName);
    } else {
        $errorCounter++;
        updatePasZipToBypass($zipName, "Ohitettua zippiä ei löytyny restartissa. -> $comment");
	}
}

mssql_free_result($result);

close_sql($db);
oci_close($conn);
oci_close($conn_diona);
if($okCounter > 0) {
    $_SESSION['displayOkMessage'] = "$okCounter Ohitettua paketia on käynnistetty uudelleen.";   
}
if($errorCounter > 0) {
    $_SESSION['displayErrorMessage'] = "$errorCounter päätyi virheeseen.";    
}

header("Location: ../pas_process_passed.php");

?>