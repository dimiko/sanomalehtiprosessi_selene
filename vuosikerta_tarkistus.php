

<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_karttuvat.php");
include("global_variables.php");
$dbUsername = getenv('DB_USERNAME');
$dbPassword = getenv('DB_PASSWORD');
$connectionString = getenv('DB_CONNECTION_STRING');

if (!$dbUsername || !$dbPassword || !$connectionString) {
    #die("Error: Database credentials are not properly set in environment variables.\n");
}

#$conn = oci_connect($dbUsername, $dbPassword, $connectionString, 'AL32UTF8');
$conn=oci_connect('diona', 'diona', '//192.168.10.10:1521/dionapdb.docworks.lib.helsinki.fi', 'AL32UTF8');

if (!$conn) {
    $e = oci_error();
    file_put_contents('db_errors.log', date('[Y-m-d H:i:s] ') . $e['message'], FILE_APPEND);
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    exit("Database connection error, execution stopped.\n");
}
?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>Vuosikertojen tarkistus</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
</head>
<style>
table tr:nth-child(even) {background-color: #f9f9f9;}
table tr:nth-child(odd) {background-color: #ffffff;}

</style>
<body>

<?php
directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);
docIdSearch(basename(__FILE__), $db);

        $eraStyle="";
        $eraFileCount=countFiles("/mnt/sl_import/era/*.zip");
        if ($eraFileCount>40) {
         $eraStyle="background-color:red; color: white;";
         $era="$eraFileCount Zip";

        } elseif ($eraFileCount==0) {
          $era="";
        } else {
        $era="$eraFileCount Zip";
        }

        echo"<div class='dailyReportFrame'>";
        echo"<h3>Tänään</h3>";
        $export_sivuja = dailyReport($db, $queryExportted);
        $accepted_sivuja = dailyReport($db, $queryAccepted);
        $rejected_sivuja = dailyReport($db, $queryRejected);
        $importoitu_sivuja = dailyReport($db, $queryImported);
        $deletoitu_sivuja = dailyReport($db, $queryDeleted);

        echo"<table class='dailyTable'>";
        echo"<tr style='text-align: center;'>";
        echo "<th>Exportoitu&nbsp;</th>";
        echo "<th>Tarkastettu&nbsp;</th>";
        echo "<th>Importoitumassa&nbsp;</th>";
        echo "<th>Importoitu&nbsp;</th>";
        echo "<th>Poistettu</th>";
        echo"<tr style='text-align: center;'>";
        echo "<td>$export_sivuja</td>";
        echo "<td>$accepted_sivuja</td>";
        echo "<td style='$eraStyle'>$era</td>";
        echo "<td>$importoitu_sivuja</td>";
        echo "<td>$deletoitu_sivuja</td>";
        echo"</tr>";
        echo "</table>";

echo"</div>";

naviHeader();
?>

<?php
function signalHandler($signal) {
    switch ($signal) {
        case SIGINT:
            echo "Script execution stopped by user.\n";
            exit;
    }
}

if (function_exists('pcntl_signal')) {
    pcntl_signal(SIGINT, "signalHandler");
} else {
    #echo "PCNTL functions are not available on this system.\n";
}

// Function to extract numeric parts from an issue number and to handle textual parts
function extractNumeric($issueNum) {
   # if (preg_match('/(\d+)/', $issueNum, $matches))
    if (preg_match('/(?<=^|\s|\p{P})(\d{1,3})(?![\s\p{P}])\b/', $issueNum, $matches)) {
        return (int)$matches[0];
    }
    return 0;  // Return 0 if no numeric part is found
}

function extractNumericSort($issueNum) {
   # if (preg_match('/(\d+)/', $issueNum, $matches))
    if (preg_match('/(\d+)/', $issueNum, $matches)) {
        return (int)$matches[0];
    }
    return 0;  // Return 0 if no numeric part is found
}

// Function to parse issue number ranges into individual numbers
function expandIssueRange($issueRange) {
    $expanded = [];
    foreach (explode(',', $issueRange) as $part) {
        if (strpos($part, '-') !== false) {
            list($start, $end) = explode('-', $part);
            if ($end!="") {
                $expanded = array_merge($expanded, range($start, $end));
            } else {
                $expanded = array_merge($expanded, range($start, $start));
            }
        } else {
            $expanded[] = $part;
        }
    }
    return $expanded;
}

function sortIssuesWithText($a, $b) {
    $numA = extractNumericSort($a['issuenum']);
    $numB = extractNumericSort($b['issuenum']);
    if ($numA === $numB) {
        return strcmp($a['issuenum'], $b['issuenum']); // Sort textually if numbers are the same
    } else if ($numA < $numB) {
        return -1;  // Return -1 if $numA is less than $numB
    } else {
        return 1;   // Return 1 if $numA is greater than $numB
    }
}

function expandIssues($issues) {
    $expandedIssues = [];
    foreach ($issues as $issue) {
        if (strpos($issue, '-') !== false) {
            list($start, $end) = explode('-', $issue);
            if ($end) {
                $expandedIssues = array_merge($expandedIssues, range($start, $end));
            } else {
                $expandedIssues = array_merge($expandedIssues, $start);
            }
        } else {
            $expandedIssues[] = $issue;
        }
    }
    return $expandedIssues;
}

function containsNumberStart($array) {
    foreach ($array as $item) {
        // Check if the item starts with a digit
        if (preg_match('/^\d/', $item)) {
            return true;
        }
    }
    return false;
}


function containsNumericIssues($tempIssues) {
    foreach ($tempIssues as $item) {
        // Check if the 'issuenum' starts with a digit or is entirely numeric
        if (preg_match('/^\d+/', $item['issuenum'])) {
            return true;
        }
    }
    return false;
}


function issueExistsInTempIssues($issue, $tempIssues) {
    foreach ($tempIssues as $tempIssue) {
        // Expand the range if the current issuenum is in a range format
        $currentIssueNumbers = explode('-', $tempIssue['issuenum']);
        if (count($currentIssueNumbers) > 1) {
            $range = range($currentIssueNumbers[0], $currentIssueNumbers[1]);
            if (in_array($issue, $range)) {
                return true;
            }
        } elseif ($tempIssue['issuenum'] == $issue) {
            // Direct comparison if not a range
            return true;
        }
    }
    return false;
}




if (isset($_GET['type'])) {
    $type = $_GET['type'];
} else {
    $type = 'SAN';  // Default value if 'type' is not set
}

$queryissn = "SELECT TITLE, ISSN, START_YEAR, END_YEAR FROM nimeke_vuosikerta_tarkastus WHERE type='{$type}' ORDER BY lower(title), issn";
$stid2 = oci_parse($conn, $queryissn);
oci_execute($stid2);

// Store ISSNs and Titles
$issnList = [];
while ($row = oci_fetch_assoc($stid2)) {
    $issnList[] = $row;
}

// Find current ISSN index
$currentIndex = 0;
if (!empty($_GET['issn'])) {
    foreach ($issnList as $index => $item) {
        if ($item['ISSN'] == $_GET['issn']) {
            $currentIndex = $index;
            break;
        }
    }
}

// Calculate next and previous ISSNs
$nextIndex = $currentIndex + 1 < count($issnList) ? $currentIndex + 1 : 0;
$prevIndex = $currentIndex - 1 >= 0 ? $currentIndex - 1 : count($issnList) - 1;
?>

<script>
window.onload = function() {
    document.getElementById('loadingSpinner').style.display = 'none';
};

function showLoadingSpinner() {
    document.getElementById('loadingSpinner').style.display = 'block';
}

function submitFormWithISSN(newISSN) {
    showLoadingSpinner();
    var form = document.getElementById('issnForm');
    var select = document.getElementById('issnSelect');
    select.value = newISSN;
    form.submit();
}
</script>

<style>
.spinner {
    border: 8px solid #f3f3f3; /* Light grey background */
    border-top: 8px solid #3498db; /* Blue spinner */
    border-radius: 50%;
    width: 150px;
    height: 150px;
    animation: spin 2s linear infinite;
    position: fixed;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    z-index: 1050; /* Make sure it's above most other items */
}

@keyframes spin {
    0% { transform: translate(-50%, -50%) rotate(0deg); }
    100% { transform: translate(-50%, -50%) rotate(360deg); }
}
</style>

<div style="text-align: center; margin-top:20px;">
     <h1>Sanomalehtien vuosikertojen tarkistus</h1>
</div>
<div style="text-align: center; position: sticky; top: 0; background-color:white; padding: 10px;">
   <div id="loadingSpinner" style="display: none; position: fixed; left: 50%; top: 50%; transform: translate(-50%, -50%); z-index: 1050;">
    <div class="spinner"></div>
</div>

    <form id="issnForm" action="vuosikerta_tarkistus.php" method="get" onsubmit="showLoadingSpinner()">
        <label><input type="radio" name="type" value="SAN" <?= (!isset($_GET['type']) || $_GET['type'] === 'SAN') ? 'checked' : '' ?> onchange="this.form.submit()"> SAN</label>
        <label><input type="radio" name="type" value="AIK" <?= (isset($_GET['type']) && $_GET['type'] === 'AIK') ? 'checked' : '' ?> onchange="this.form.submit()"> AIK</label>

        <select id="issnSelect" name="issn" style="font-size:16px;" onchange="submitFormWithISSN(this.value)">
            <option value="">Valitse nimeke</option>
            <?php foreach ($issnList as $item): ?>
                <?php $isSelected = ($item['ISSN'] === $_GET['issn']) ? ' selected' : '';
                $title = htmlspecialchars($item['TITLE']);
                $trimmedTitle = (strlen($title) > 60) ? substr($title, 0, 60) . '...' : $title;
                ?>
                
                <option value="<?= htmlspecialchars($item['ISSN']); ?>"<?= $isSelected; ?>>
                    <?= $trimmedTitle; ?> (<?= $item['ISSN']; ?>) <?= $item['START_YEAR']; ?>-<?= $item['END_YEAR']; ?>
                </option>
            <?php endforeach; ?>
        </select>
        <button type="button" onclick="submitFormWithISSN('<?= $issnList[$prevIndex]['ISSN']; ?>')">Edellinen</button>
        <button type="button" onclick="submitFormWithISSN('<?= $issnList[$nextIndex]['ISSN']; ?>')">Seuraava</button>
        <input type="checkbox" name="shownOnlyProblems" value="1" <?= isset($_GET['shownOnlyProblems']) ? 'checked' : ''; ?> onchange="this.form.submit()">
        <label for="shownOnlyProblems">Näytä vain ongelmavuodet</label>
    </form>

<?php

oci_free_statement($stid2);

echo "<p style='margin-top:10px;'>
<span style='color:grey;'>Numero</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span>Tekstiä numerossa.</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:red; font-weight:bold;'>Aukko numeroinnissa.</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:gold; font-weight:bold;'>Kaksi samaa.</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:DarkMagenta; font-weight:bold;'>Puuttuu kokoelmista</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:SeaGreen; font-weight:bold;'>Ei julkaistu</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:SaddleBrown; font-weight:bold;'>Puuttuu</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:Blue; font-weight:bold;'>Ei painettuna</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:BurlyWoode; font-weight:bold;'>Digitointipoikkeama</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:lightgreen; font-weight:bold;'>Numerointipoikkeama</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:DarkOrange; font-weight:bold;'>Digitoitava alkuperäisestä</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:DarkTurquoise; font-weight:bold;'>Muu</span>
</p></div>";

$selectedIssn = isset($_GET['issn']) ? $_GET['issn'] : null;
$shownOnlyProblems = isset($_GET['shownOnlyProblems']) ? $_GET['shownOnlyProblems'] : 0;
if ($selectedIssn=="") {
    echo "</body></html>";
    exit;
}

$missingIssuesQuery = "SELECT issn, TO_CHAR(pvm,'YYYY') AS year, REPLACE(no,' - ','-') AS issuenum, tyyppi AS type, selitys as descr, to_char(pvm,'DD.MM.YYYY') as issuedate
FROM nidepuutteet WHERE issn = :issn
and not (tyyppi='NOT_PUBLISHED' and (no is null))";
    $missingStid = oci_parse($conn, $missingIssuesQuery);
    oci_bind_by_name($missingStid, ':issn', $selectedIssn);
    oci_execute($missingStid);

    $manualMissing = [];
    while ($row = oci_fetch_assoc($missingStid)) {
        $key = $row['ISSN'] . '|' . $row['YEAR'];
        foreach (expandIssueRange($row['ISSUENUM']) as $issue) {
            #$manualMissing[$key][$issue] = true;
            $manualMissing[$key][$issue] = [
            'type' => $row['TYPE'],
            'descr' => $row['DESCR'],
            'issuedate' => $row['ISSUEDATE'],
            'rawIssueNum' => $row['ISSUENUM']
        ];
        }
    }

    oci_free_statement($missingStid);


                    
 $query_yearRange = "SELECT ISSN,START_YEAR,END_YEAR
from nimeke_vuosikerta_tarkastus
where type='SAN'
order by title, issn";                   

$stid_yearRange = oci_parse($conn, $query_yearRange);
oci_execute($stid_yearRange);

$issnYearRange = [];
while ($row = oci_fetch_assoc($stid_yearRange)) {
    $issn = $row['ISSN'];
    $issnYearRange[$issn] = ["start" => $row['START_YEAR'], "end" => $row['END_YEAR']];
}

// Main query to fetch publication data
$query = "SELECT kl.paanimeke AS title, sn.issn AS issn, TO_CHAR(julkaisu_pvm,'YYYY') AS year, nvl(no,'##') AS issuenum, julkaisu_pvm as issuedate
FROM nide n
JOIN sarjajulkaisu_nide sn ON n.nide_id = sn.sarjanide_id
JOIN kausi_lehti kl ON kl.issn = sn.issn
JOIN niteet_joilla_sivuja njs on njs.nide_id=n.nide_id
WHERE kl.issn='".$selectedIssn."'
ORDER BY title, issn, year, julkaisu_pvm, issuenum";

$stid = oci_parse($conn, $query);
oci_execute($stid);

$results = [];
$allYears = [];
$issnTitles = [];

// Collect all data and prepare a structure to verify years per ISSN
while ($row = oci_fetch_assoc($stid)) {
    $key = $row['TITLE'] . '|' . $row['YEAR'] . '|' . $row['ISSN'];
    $results[$key][] = $row['ISSUENUM'];
    $allYears[$row['ISSN']][$row['YEAR']] = true;  // Track years we have data for each ISSN
    $issnTitles[$row['ISSN']] = $row['TITLE']; 
}
// Iterate through each ISSN year range
foreach ($issnYearRange as $issn => $years) {
    $start = $years["start"];
    $end = $years["end"];
    
    for ($year = $start; $year <= $end; $year++) {
        if (!isset($allYears[$issn][$year])) {  // Check if the year is missing in data
            #$title = ) ? $issnTitles[$issn] : "Unknown Title";
            if (isset($issnTitles[$issn])) {
                $title = $issnTitles[$issn];
                $key = "$title|$year|$issn";  // Using a placeholder for title or determine a method to find the title
                $results[$key][] = "Vuosikerta puuttuu";  // Notate the missing volume
            } else {
                continue;
            }
            
        }
    }
}
ksort($results);

$lastIssues = [];
foreach ($results as $key => $issues) {
    list($title, $year, $issn) = explode('|', $key);
    if (!isset($lastIssues[$issn])) {
        $lastIssues[$issn] = [];
    }
    $lastIssues[$issn][$year] = end($issues); // Assuming $issues is already sorted
}



echo "<table border='1' style='margin-left:auto; margin-right:auto;margin-top:20px'>";
echo "<tr><th>Vuosi</th><th>Numerot</th></tr>";

foreach ($results as $key => $issues) {
    list($title, $year, $issn) = explode('|', $key);

    $expandedIssues = expandIssues($issues);
    $issueComponents = array_map('extractNumeric', $issues);
    $issueComponents2 = array_map('extractNumeric', $expandedIssues);
    $uniqueIssues = array_unique($issueComponents);
    
    // Check if only contains "Vuosikerta puuttuu"
    if (count($issues) == 1 && in_array("Vuosikerta puuttuu", $issues)) {
        $missingYearDescr ="";
        // Add manually missing issues
        if (isset($manualMissing[$issn . '|' . $year])) {
            foreach ($manualMissing[$issn . '|' . $year] as $issue => $data) {
        // Extracting type and descr from $data array
        $type = $data['type'];
        $descr = $data['descr'];
        
        // Append issuenumber, status, type, and descr to $missingYearDescr string
        $missingYearDescr .= "$issue : $type : $descr\n";
        }
            }
      
        
        
        echo "<tr><td>" . htmlspecialchars($year) . "</td><td style='color:orange; font-weight:bold;'>Vuosikerta digitoimatta. $missingYearDescr</td></tr>";
        continue; // Skip to the next iteration of the foreach loop
    }
    
    sort($uniqueIssues);

    $previousYear = $year - 1;
    $prevLastIssue = isset($lastIssues[$issn][$previousYear]) ? $lastIssues[$issn][$previousYear] : 0;
    $firstIssueCurrentYear = reset($uniqueIssues);
    
    $rangeStart = $prevLastIssue + 1 == $firstIssueCurrentYear ? $firstIssueCurrentYear : 1;
    $rangeEnd = max($uniqueIssues);
    $completeRange = range($rangeStart, $rangeEnd);
    
    $tempIssues = [];
    
     // Add manually missing issues
    if (isset($manualMissing[$issn . '|' . $year])) {
        foreach ($manualMissing[$issn . '|' . $year] as $issue => $data) {
            if (!in_array($issue, $uniqueIssues)) {
            $tempIssues[] = ['issuenum' => $issue, 'status' => 'manuallymissing', 'type' => $data['type'],'descr' => $data['descr'],'rawIssueNum' => $data['rawIssueNum'], 'issueDate' => $data['issuedate']];
            }
        }
    } 

    // Include existing issues first
    foreach ($issues as $issue) {
        $tempIssues[] = ['issuenum' => $issue, 'status' => 'ok'];
    }


    #$numericIssues = array_filter($issueComponents, 'is_numeric');
    if (containsNumericIssues($tempIssues)) {
            // Identify and add detected missing issues
           foreach ($completeRange as $issue) {
              // Ensure that manualMissing array for the specific ISSN and year is checked properly
              $isManuallyMissing = isset($manualMissing[$issn . '|' . $year]) && array_key_exists((string)$issue, $manualMissing[$issn . '|' . $year]);
              if (!issueExistsInTempIssues($issue, $tempIssues) && !$isManuallyMissing ) {
                  $tempIssues[] = ['issuenum' => $issue, 'status' => 'detectedmissing','type'=>'Merkitsemätön puute vuosikerran numeroinnissa!'];
              }
          }
    }

    // Sort all issues by numeric and textual content
    usort($tempIssues, 'sortIssuesWithText');

    // Generate HTML output for the issue list
    $issueList = [];
    $prevIssueDisplay = null;  // Initialize to ensure it is set before use.
    $problemYear=false;
foreach ($tempIssues as $tempIssue) {
    $issueDisplay = htmlspecialchars($tempIssue['issuenum']);
    switch ($tempIssue['status']) {
        case 'ok':
            if (is_numeric($issueDisplay)) {
                if ($prevIssueDisplay != $issueDisplay) {
                    $issueList[] = "<span style='color:LightGrey;'><a style='all:unset;' title='".htmlspecialchars($tempIssue['type'])."' href='https://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".htmlspecialchars($year)."' target='_blank'>".$issueDisplay."</a></span>";
                } else {
                    $issueList[] = "<span style='color:gold; font-weight:bold;'><a style='all:unset;' title='".htmlspecialchars($tempIssue['type'])."' href='https://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".htmlspecialchars($year)."' target='_blank'>".$issueDisplay."</a></span>";
                    $problemYear=true;
                }
            } else {
                if ($prevIssueDisplay != $issueDisplay) {
                    $issueList[] = "<span style='color:Grey;'>$issueDisplay</span>";
                } else {
                    $issueList[] = "<span style='color:gold; font-weight:bold;'>$issueDisplay</span>";
                    $problemYear=true;
                }
            }
            break;
        case 'detectedmissing':
            $issueList[] = "<span style='color:red; font-weight:bold;'><a style='all:unset; cursor:pointer;' title='".htmlspecialchars($tempIssue['type'])."' href='https://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".htmlspecialchars($year)."' target='_blank'>".$issueDisplay."</a></span>";
            $problemYear=true;
            break;
        case 'manuallymissing':
        switch ($tempIssue['type']){
            case TO_BE_SCANNED_FROM_ORIGINAL: {
                $style="color:DarkOrange; font-weight:bold;";
                break;
            }
            case NOT_IN_COLLECTIONS: {
                $style="color:DarkMagenta; font-weight:bold;";
                break;
            }
            case OTHER: {
                $style="color:DarkTurquoise; font-weight:bold;";
                break;
            }
            case DIGITISATION_ANOMALY: {
                $style="color:BurlyWood; font-weight:bold;";
                break;
            }
            case MISSING: {
                $style="color:SaddleBrown; font-weight:bold;";
                break;
            }
            case NOT_PUBLISHED_IN_PRINT: {
                $style="color:Blue; font-weight:bold;";
                break;
            }
            case NOT_PUBLISHED: {
                $style="color:SeaGreen; font-weight:bold;";
                break;
            }
            case ISSUE_NUMBERING_ANOMALY: {
                $style="color:lightgreen; font-weight:bold;";
                break;
            }
            default: {
                $style="color:SteelBlue; font-weight:bold;";
                break;
                
            }
        }
            
            if ($issueDisplay != "") {
                $issueList[] = "<span style='$style'><a style='all:unset; cursor:pointer;' title='".htmlspecialchars($tempIssue['rawIssueNum'])." : ".htmlspecialchars($tempIssue['issueDate'])." : ".htmlspecialchars($tempIssue['type']).": ".htmlspecialchars($tempIssue['descr'])."' href='https://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".htmlspecialchars($year)."' target='_blank'>".$issueDisplay."</a></span>";
                $problemYear=true;
            } else {
                $issueList[] = "<span style='color:Blue; font-weight:bold;'>??</span>";
                $problemYear=true;
            }
            break;
        default:
            $issueList[] = "<span style='color:blue;'>??</span>"; // For unexpected or empty cases
            $problemYear=true;
            break;
    }
    $prevIssueDisplay = $issueDisplay; // Update the previous display for the next iteration.
}
  if ($shownOnlyProblems==1 && $problemYear==true ) {
      echo "<tr><td><a href='https://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".htmlspecialchars($year)."' target='_blank'>" . htmlspecialchars($year) . "</a></td>";
      echo "<td>". implode(', ', $issueList) . "</td></tr>";
  } elseif ($shownOnlyProblems==0) {
          echo "<tr><td><a href='https://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".htmlspecialchars($year)."' target='_blank'>" . htmlspecialchars($year) . "</a></td>";
      echo "<td>" . implode(', ', $issueList) . "</td></tr>";

  }
   
}

echo "</table>";
echo "<br/>";
echo "</body></html>";

oci_free_statement($stid);
oci_free_statement($missingStid);
oci_close($conn);

?>
