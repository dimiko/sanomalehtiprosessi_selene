<?php

    if (isset($_POST["file"])) {
        $file=$_POST["file"];
    } elseif (isset($_GET["file"])) {
        $file=$_GET["file"];
    }
$replacement = "/mnt/akte/pas_process";
$newfile = str_replace("\\", "/", $file); // Replace backslashes with slashes
$newfile = str_replace("//akte/pas_process", $replacement,$newfile);
$file=$newfile;
    header('Content-Description: File Transfer');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Type: text/plain');

$xml = simplexml_load_file($file);
// Iterate through each premis:event node
foreach ($xml->xpath('//premis:event') as $event) {
    // Check if the eventOutcome is 'failure'
    $eventOutcome = (string) $event->{'premis:eventOutcomeInformation'}->{'premis:eventOutcome'};
    
    if ($eventOutcome == 'failure') {
        // Echo the indented XML for the matching node
        echo formatXml($event->asXML()) . "\n";
    }
}

// Function to format XML with proper indentation
function formatXml($xml) {
    $dom = new DOMDocument('1.0');
    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;
    $dom->loadXML($xml);
    return $dom->saveXML();
}

?>

