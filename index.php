<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_takautuva.php");
include("global_variables.php");
//include("javascript/testi.php");
?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <meta http-equiv="refresh" content="180" >
    <title>Takautuvan sanomalehtiprosessin tila</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css?v1">
    <script type="text/javascript" src="chart/Chart.bundle.js"></script>
    <script type="text/javascript" src="javascript/javaScripts.js?v1"></script>
</head>
<body>

<?php
directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);
docIdSearch(basename(__FILE__), $db);

echo"<div class='dailyReportFrame'>";
  echo"<h3>Tänään</h3>";
      
  $export_sivuja = dailyReport($db, $query_export);
  $accepted_sivuja = dailyReport($db, $query_accepted);
  $rejected_sivuja = dailyReport($db, $query_rejected);
  $importoitu_sivuja = dailyReport($db, $query_imported);
  $deletoitu_sivuja = dailyReport($db, $query_deleted);
  
  echo"<table class='dailyTable'>";
    echo"<tr>";
      echo "<th>Exportoitu&nbsp;</th>";
      echo "<th>Tarkastettu&nbsp;</th>";
      echo "<th>Importoitu&nbsp;</th>";
      echo "<th>Poistettu</th>";
    echo"<tr>";
      echo "<td>$export_sivuja</td>";
      echo "<td>$accepted_sivuja</td>";
      echo "<td>$importoitu_sivuja</td>";
      echo "<td>$deletoitu_sivuja</td>";
    echo"</tr>";
  echo "</table>";
echo"</div>";

naviHeader();

include("acknowledgment_messages.php");
$message = $_SESSION['message'];
echo "<div id='okMessage'>$message</div>";

echo "<div class='container'>";
  echo "<div class='frame errorFrame withRightBorder '>";
    echo "<button class='toggleButton'>NÄYTÄ VIRHEELLISET</button>";
    rescan_docs($db, $query_rescans, basename(__FILE__));
    status_error_table($db, $query_dw_error,"Virhe- ja hylkäystilassa olevat työt !");
echo "</div>";
  echo "<div class='frame processingFrame withRightBorder '>";
    status_table_yearly_steps($db, htmlspecialchars($query_importing_process), "Käsittelyssä");
  echo "</div>";

  echo "<div class='frame inspectionFrame withRightBorder '>";
    InspectionListing($db, htmlspecialchars($query_inspection_state), "Tarkastettavat");
echo "</div>";

  echo "<div class='frame importFrame'>";
    status_table_yearly_steps($db, htmlspecialchars($query_importoitavat), "Importoitavat");
  echo "</div>";
echo "</div>";

close_sql($db);
oci_close($conn);
oci_close($conn_diona);
echo $_SESSION['getYearByTitle'];
?>


</body>
</html>

