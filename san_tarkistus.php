

<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_karttuvat.php");
include("global_variables.php");

?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta http-equiv="refresh" content="180" >
    <title>SLS puutteet</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
</head>
<body>

<?php
directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);
docIdSearch(basename(__FILE__), $db);

        $eraStyle="";
        $eraFileCount=countFiles("/mnt/sl_import/era/*.zip");
        if ($eraFileCount>40) {
         $eraStyle="background-color:red; color: white;";
         $era="$eraFileCount Zip";

        } elseif ($eraFileCount==0) {
          $era="";
        } else {
        $era="$eraFileCount Zip";
        }

        echo"<div class='dailyReportFrame'>";
        echo"<h3>Tänään</h3>";
        $export_sivuja = dailyReport($db, $queryExportted);
        $accepted_sivuja = dailyReport($db, $queryAccepted);
        $rejected_sivuja = dailyReport($db, $queryRejected);
        $importoitu_sivuja = dailyReport($db, $queryImported);
        $deletoitu_sivuja = dailyReport($db, $queryDeleted);

        echo"<table class='dailyTable'>";
        echo"<tr style='text-align: center;'>";
        echo "<th>Exportoitu&nbsp;</th>";
        echo "<th>Tarkastettu&nbsp;</th>";
        echo "<th>Importoitumassa&nbsp;</th>";
        echo "<th>Importoitu&nbsp;</th>";
        echo "<th>Poistettu</th>";
        echo"<tr style='text-align: center;'>";
        echo "<td>$export_sivuja</td>";
        echo "<td>$accepted_sivuja</td>";
        echo "<td style='$eraStyle'>$era</td>";
        echo "<td>$importoitu_sivuja</td>";
        echo "<td>$deletoitu_sivuja</td>";
        echo"</tr>";
        echo "</table>";

echo"</div>";

naviHeader();


function signalHandler($signal) {
    switch ($signal) {
        case SIGINT:
            echo "Script execution stopped by user.\n";
            exit;
    }
}

if (function_exists('pcntl_signal')) {
    pcntl_signal(SIGINT, "signalHandler");
} else {
    #echo "PCNTL functions are not available on this system.\n";
}

$dbUsername = getenv('DB_USERNAME');
$dbPassword = getenv('DB_PASSWORD');
$connectionString = getenv('DB_CONNECTION_STRING');

if (!$dbUsername || !$dbPassword || !$connectionString) {
    #die("Error: Database credentials are not properly set in environment variables.\n");
}

#$conn = oci_connect($dbUsername, $dbPassword, $connectionString, 'AL32UTF8');
$conn=oci_connect('diona', 'diona', '//192.168.10.10:1521/dionapdb.docworks.lib.helsinki.fi', 'AL32UTF8');

if (!$conn) {
    $e = oci_error();
    file_put_contents('db_errors.log', date('[Y-m-d H:i:s] ') . $e['message'], FILE_APPEND);
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    exit("Database connection error, execution stopped.\n");
}



// Function to extract numeric parts from an issue number and to handle textual parts
function extractNumeric($issueNum) {
    if (preg_match('/(\d+)/', $issueNum, $matches)) {
        return (int)$matches[0];
    }
    return 0;  // Return 0 if no numeric part is found
}




// Function to parse issue number ranges into individual numbers
function expandIssueRange($issueRange) {
    $expanded = [];
    foreach (explode(',', $issueRange) as $part) {
        if (strpos($part, '-') !== false) {
            list($start, $end) = explode('-', $part);
            $expanded = array_merge($expanded, range($start, $end));
        } else {
            $expanded[] = $part;
        }
    }
    return $expanded;
}

// Fetch the second list of manually detected missing issues
$missingIssuesQuery = "SELECT issn, TO_CHAR(pvm,'YYYY') AS year, REPLACE(no,' ','') AS issuenum FROM nidepuutteet";
$missingStid = oci_parse($conn, $missingIssuesQuery);
oci_execute($missingStid);

$manualMissing = [];
while ($row = oci_fetch_assoc($missingStid)) {
    $key = $row['ISSN'] . '|' . $row['YEAR'];
    foreach (expandIssueRange($row['ISSUENUM']) as $issue) {
        $manualMissing[$key][$issue] = true;
    }
}

// Main query to fetch publication data
$query = "SELECT kl.paanimeke AS title, sn.issn AS issn, TO_CHAR(julkaisu_pvm,'YYYY') AS year, no AS issuenum FROM nide n JOIN sarjajulkaisu_nide sn ON n.nide_id = sn.sarjanide_id JOIN kausi_lehti kl ON kl.issn = sn.issn
where paanimeke like 'A%' and aineistoyleismaare='SAN' ORDER BY title, issn, year, julkaisu_pvm, issuenum";

$stid = oci_parse($conn, $query);
oci_execute($stid);

$results = [];
while ($row = oci_fetch_assoc($stid)) {
    $key = $row['TITLE'] . '|' . $row['YEAR'] . '|' . $row['ISSN'];
    $results[$key][] = $row['ISSUENUM'];
}

function sortIssuesWithText($a, $b) {
    $numA = extractNumeric($a['issuenum']);
    $numB = extractNumeric($b['issuenum']);
    if ($numA === $numB) {
        return strcmp($a['issuenum'], $b['issuenum']); // Sort textually if numbers are the same
    } else if ($numA < $numB) {
        return -1;  // Return -1 if $numA is less than $numB
    } else {
        return 1;   // Return 1 if $numA is greater than $numB
    }
}
echo "<div style=\"text-align: center;\"><h1>Ameriikassa julkaistut lehdet digissä</h1>";
echo "<h2><span>Nide digissä</span>&nbsp;&nbsp;<span style='color:Indigo;font-weight:bold;'>Käsin merkitty puute/puuteväli</span>&nbsp;&nbsp;<span style='color:red;'>Havaittu puute</span></h2></div>";
echo "<table border='1'>";
echo "<tr><th>Nimeke</th><th>ISSN</th><th>Vuosi</th><th>Numerot</th></tr>";

foreach ($results as $key => $issues) {
    list($title, $year, $issn) = explode('|', $key);
    
    $tempIssues = [];
    $issueComponents = array_map('extractNumeric', $issues);
    $uniqueIssues = array_unique($issueComponents);

    // Check for missing issues
    $completeRange = range(1, end($uniqueIssues));
    $missing = array_diff($completeRange, $uniqueIssues);

    // Detect missing issues and manually missing issues
    foreach ($completeRange as $issue) {
        if (in_array($issue, $missing)) {
            $manualMissingKey = $issn . '|' . $year;
            if (isset($manualMissing[$manualMissingKey]) && array_key_exists((string)$issue, $manualMissing[$manualMissingKey])) {
                $tempIssues[] = ['issuenum' => $issue, 'status' => 'manuallymissing'];
            } else {
                $tempIssues[] = ['issuenum' => $issue, 'status' => 'detectedmissing'];
            }
        }
    }

    // Add existing issues
    foreach ($issues as $issue) {
        $status = 'ok'; // Default status
        $tempIssues[] = ['issuenum' => $issue, 'status' => $status];
    }

    // Sort issues by numeric value and text parts
    usort($tempIssues, 'sortIssuesWithText');

    // Generate HTML output for issue list
    $issueList = [];
    foreach ($tempIssues as $tempIssue) {
        $issueDisplay = htmlspecialchars($tempIssue['issuenum']);
        switch ($tempIssue['status']) {
            case 'ok':
                $issueList[] = $issueDisplay;
                break;
            case 'detectedmissing':
                $issueList[] = "<span style='color:red;'>$issueDisplay</span>";
                break;
            case 'manuallymissing':
                $issueList[] = "<span style='color:Indigo;font-weight:bold;'>$issueDisplay</span>";
                break;
        }
    }

    echo "<tr><td>" . htmlspecialchars($title) . "</td><td>" . htmlspecialchars($issn) . "</td><td><a href=\"https://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".$year."\" target=\"_new\">$year</td><td>" . implode(', ', $issueList) . "</td></tr>";
}

echo "</table>";

echo "</body></html>";

oci_free_statement($stid);
oci_free_statement($missingStid);
oci_close($conn);

?>
