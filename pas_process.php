<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_karttuvat.php");
include("sql_querys/query_diona.php");
include("global_variables.php");
?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <meta http-equiv="refresh" content="30" >
    <title>PAS prosessin tila</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
</head>
<body>

<?php
#$execstring='ps -u pas x --format cmd | grep -v sendmail | grep -v  "gdm-session-worker" | grep -v  "/usr/sbin/postdrop" | grep -v "grep" | grep -v "CMD" | grep -v "/usr/sbin/httpd" | grep -v "ps -u" |  sort 2>&1';
#$psOutput="";
#exec($execstring, $psOutput);

$execstring='pstree -c -a -A -l pas > /var/www/html/sl-prosessi/pstree.txt';
$psOutput="";
exec($execstring, $psOutput);

#directorysFreeSpaceIndicatory($DW_directorys, $DW_directorys_alert_space);
directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);
#docIdSearch(basename(__FILE__), $db);

pasProcessStatusTable ();

naviHeader();

echo"<div class='dailyStaticFrame'>";
 
  include("acknowledgment_messages.php");
  
  $message = $_SESSION['message'];
  echo "<div id='okMessage'>$message</div>";
  
     echo "<div id = 'docIdPageFrame'>";
      echo "<div id = 'docInfo'>";
         echo "<h3 style='padding-bottom: 10'>Järjestelmän status</a></h3>";
         
         echo "<table id = 'tableDoc'>";
            $query_form_pool_main = "select IIF(process_on_off = 1, 'ON', 'OFF') as 'ON/OFF', arc_folders,
              processing_arc as 'Käsiteltä acr-kansio', processing_zip_in_sequence as 'Käsiteltävä zippi määrä',
              min_zip_in_process from dw_nlf_custom.dbo.PAS_PROCESS_CONF";
            $result = mssql_query($query_form_pool_main);
            while($row = mssql_fetch_assoc($result)) {
              $onOff = $row['ON/OFF'];
              $arcProcessing = $row['Käsiteltä acr-kansio'];
              $zipToProcess = $row['Käsiteltävä zippi määrä'];
              $minZipInProcess = $row['min_zip_in_process'];
              $arcFolders = $row['arc_folders'];
              $arcFolders = explode(",",$arcFolders);
              $arcFolders = \array_diff($arcFolders, [$arcProcessing]);
              sort($arcFolders);
              array_unshift($arcFolders, $arcProcessing);
              $onOffSelect = ($onOff == 'ON') ? ['ON', 'OFF'] : ['OFF', 'ON'];
              $arcSelect = $arcFolders;
              $zipSelect = [$zipToProcess, 1, 2, 5, 10, 50, 100, 200, 500, 1000, 10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000];
              $minZipSelect = [$minZipInProcess, 10, 100, 1000, 2000, 3000, 4000, 5000, 6000];
              echo"<tr>";
                echo "<th>ON/OFF</th><th>Käsiteltä acr-kansio</th><th>Käsiteltävä zippi määrä</th><th>Min zip limit</th><th>Nappi</th>";
              echo "</tr>";
              
              echo "<form action='pas_process/pas_process_status_update.php' method='GET'>";
              echo"<tr>";
                echo "<td>";
                  echo"<select name='onOff'>";
                    foreach ($onOffSelect as $values) {
                        echo"<option >$values</option>";
                    }
                  echo "</select>";
                echo "</td>";
                
                echo "<td>";
                  echo"<select name='arc'>";
                    foreach ($arcSelect as $values) {
                        echo"<option >$values</option>";
                    }
                  echo "</select>";
                echo "</td>";
                
                echo "<td>";
                  echo"<select name='zip'>";
                    foreach ($zipSelect as $values) {
                        echo"<option >$values</option>";
                    }
                  echo "</select>";
                echo "</td>";
                
                echo "<td>";
                  echo"<select name='minZip'>";
                    foreach ($minZipSelect as $values) {
                        echo"<option >$values</option>";
                    }
                  echo "</select>";
                echo "</td>";
                
                echo "<td>";
                  echo "<input type='submit' value='Päivitä järjestelmä'>";
                echo "</td>";
              echo "</tr>";
              echo "</form>";

            }

         echo"</table>";
      echo "</div>";
      
  echo "</div>";
  
  echo "<div id = 'docIdPageFrame'>";
  
    echo "<div id = 'docInfo'>";

      echo "<h3 style='padding-bottom: 10'>Prosessin status</h3>";
            
      echo "<table id = 'tableDoc'>";
        $query_form_pool_main = "
            SELECT status, COUNT(*) AS summa 
            FROM dw_nlf_custom.dbo.lehtipas 
            GROUP BY status";
        $skippingKeys = array("sailytyksessa PASsissa");
        $result = mssql_query($query_form_pool_main);
        
        // Initialize an associative array to hold counts for each status
        $statusCounts = array(
            'kopioitu muunnettavaksi' => 0,
            'zip not found' => 0,
            'virhe muunnossa' => 0,
            'virhe kopioinnissa' => 0,
            'muunnettavana' => 0,
            'muunnettu PAS-kelpoiseksi' => 0,
            'sftp siirrossa' => 0,
            'virhe sftp siirrossa' => 0,
            'lahetetty PASsiin' => 0,
            'hylatty PASsissa' => 0,
            'sailytyksessa PASsissa' => 0,
            'Ohitettu' => 0
        );
        while ($row = mssql_fetch_assoc($result)) {
            $status = $row['status'];
            $summa = $row['summa'];
            if (array_key_exists($status, $statusCounts)) {
                $statusCounts[$status] = $summa;
            }
        }
        echo "<tr>";
        foreach ($statusCounts as $key => $value) {
            if (!in_array($key, $skippingKeys)) {
                echo "<th>$key</th>";
            }
        }
        echo "<tr>";
        foreach ($statusCounts as $key => $value) {
            if (!in_array($key, $skippingKeys)) {
                if ($key == 'Ohitettu') {
                    echo "<td><h3 style='padding-bottom: 10px;'><a href='pas_process_passed.php'>$value</a></h3></td>";
                } elseif ($key == 'hylatty PASsissa') {
                    echo "<td><h3 style='padding-bottom: 10px;'><a href='pas_process_rejected.php'>$value</a></h3></td>";
                } else {
                    echo "<td>$value</td>";  
                }
            }
        }
        echo "</tr>";
        $zipInConservation = $statusCounts['sailytyksessa PASsissa'];
        $allProcessingZip = getAllProcessingZip( $conn_diona, $query_all_processing_zip );
        $donePercentage = round(($zipInConservation / $allProcessingZip * 100),2);
        $usedSpace = (usedSpaceInPreservation($db) * 1.35); 
        $freespace = 174080;
        
        
        $json = file_get_contents('pas_static.json');
        $obj = json_decode($json);
        $totalCapacity = $obj->{'data'}->{'capacity'}->{'total'}; // 12345
        $usedCapasity = $obj->{'data'}->{'capacity'}->{'used'}; // 12345
        $freeCapasity = $obj->{'data'}->{'capacity'}->{'available'}; // 12345
        $usedSpacePercentage = round(($usedSpace / $totalCapacity * 100),2);
        
        $systemStyle = ($usedSpacePercentage > 80) ? "background-color:red; color:white;" : "background-color:green; color:white;";
      echo"</table>";
      
      echo "<table id = 'tableDoc' style='width: 700px'>";
          echo"<tr>";
            echo "<th>Käsiteltävät</th>";
            echo "<th>PASsissa</th>";
            echo "<th>Tehty %</th>";
            echo "<th>PAS tila TB</th>";
            echo "<th>Käytetty TB</th>";
            echo "<th>Käytetty  %</th>";
          echo "</tr>";
          echo"<tr>";
            echo "<td>$allProcessingZip</td>";
            echo "<td>$zipInConservation</td>";
            echo "<td>$donePercentage</td>";
            echo "<td>$totalCapacity</td>";
            echo "<td>$usedSpace</td>";
            echo "<td style='$systemStyle'>$usedSpacePercentage</td>";
          echo "</tr>";
      echo"</table>";

    echo "</div>";
  
  echo "</div>";
  

  echo "<div id = 'docIdPageFrame'>";
    echo "<div id = 'docInfo'>";
          $jobsInprogress = processingJobs($PAS_error_folder);
          $jobsTransfer = transferFolder($PAS_transfer_folder);
          $jobsInFailed = failedJobs($PAS_failde_folder);
          //$errors = processErrors($PAS_error_folder);
          $errors = "?";
          
          echo "<table id = 'tableDoc'>";
            echo"<tr>";
              echo "<th>Työt käynnissä kansiossa : $PAS_error_folder</th>";
              echo "<th>Paketteja transfer kansiossa : $PAS_transfer_folder</th>";
              echo "<th>Hylätyt työt : $PAS_failde_folder</th>";
              echo "<th>Dw errors kansiossa : $PAS_error_folder</th>";
            echo "</tr>";
            echo"<tr>";
              echo "<td><h3 style='padding-bottom: 10';><a href=pas_process_processing.php>$jobsInprogress</a></h3></td>";
              echo "<td>$jobsTransfer</td>";
              echo "<td><h3 style='padding-bottom: 10';><a href=pas_process_failed.php>$jobsInFailed</a></h3></td>";
              //echo "<td><h3 style='padding-bottom: 10; color: red';><a href=pas_process_errors.php>$errors</a></h3></td>";
              echo "<td><h3 style='padding-bottom: 10; color: red';>$errors</h3></td>";
            echo "</tr>";
          echo"</table>";   
    echo "</div>";
  echo "</div>";


    echo "<div style='display: flex; margin: 10px;'>";
	echo "<div style='flex: 1;'>";
		echo "<div>";
			echo "<pre><h2>Selene PAS prosessit</h2><br>";
			echo file_get_contents('/var/www/html/sl-prosessi/pstree.txt');
			echo "<br><h2>Selene dstat</h2><br>";

			echo file_get_contents('selene_dstat.txt');
			echo "</pre></div>";
      			echo "<div>";
			echo "<pre><br><h2>Selene PAS loki</h2><br>";
			exec("tail -500 /var/www/html/sl-prosessi/pas_master.log | tac", $masterLog);
			echo implode("\n",$masterLog);
			echo "</pre>";
		echo "</div>";
      		echo "<div>";
			echo "<pre><br><h2>PAS crontab</h2><br>";
			echo file_get_contents('/var/www/html/sl-prosessi/pas_crontab.txt');
			echo "</pre>";
			echo "</div>";
		echo "</div>";
//	echo "<div style='flex: 1;'>";              
//        	echo "<table id = 'tableDoc'>";
//          $query_from_pool_main = "select SUBSTRING(uri, 2, 3) as arc, aineistoyleismaare, jp2_lossy,count(*) as nide,
//              FORMAT (min(julkaisu_pvm),'yyyy-MM-dd') as min_julkaisu, FORMAT (max(julkaisu_pvm),'yyyy-MM-dd') as max_julkaisu,
//              FORMAT (min(tuonti_pvm),'yyyy-MM-dd') as min_tuonti, FORMAT (max(tuonti_pvm),'yyyy-MM-dd') as max_tuonti 
//	      FROM [dw_nlf_custom].[dbo].[lehtipas] 
//	      where pas_zip_md5_sum is not null and process_time is not null 
//	      group by  SUBSTRING(uri, 2, 3),aineistoyleismaare,jp2_lossy 
//	      order by 8 desc,2,3;";
//		
//          $query_form_pool_main = "SELECT replace(SUBSTRING(uri,2,3),'/','') as arc, aineistoyleismaare, jp2_lossy,count(*) as nide,
//              FORMAT (min(julkaisu_pvm),'yyyy-MM-dd') as min_julkaisu, FORMAT (max(julkaisu_pvm),'yyyy-MM-dd') as max_julkaisu,
//              FORMAT (min(tuonti_pvm),'yyyy-MM-dd') as min_tuonti, FORMAT (max(tuonti_pvm),'yyyy-MM-dd') as max_tuonti FROM [dw_nlf_custom].[dbo].[lehtipas] where pas_zip_md5_sum is not null
//              and process_time is not null group by replace(SUBSTRING(uri,2,3),'/',''),aineistoyleismaare,jp2_lossy order by 8 desc,2,3;";
//
//
//          $skippingKeys = array("sailytyksessa PASsissa");
//          $result = mssql_query($query_form_pool_main);
//          echo"<tr>";
//            echo "<th style='text-align: center;'>arc</th>";
//            echo "<th style='text-align: center;'>laji</th>";
//            echo "<th style='text-align: center;'>lossy</th>";
//            echo "<th style='text-align: center;'>niteitä</th>";
//            echo "<th>min_julkaisu</th>";
//            echo "<th>max_julkaisu</th>";
//            echo "<th>min_tuonti</th>";
//            echo "<th>max_tuonti</th>";
//          echo "</tr>";
//          while($row = mssql_fetch_array($result)) {
//            $arc = $row["arc"];
//            $aineistoyleismaare = $row["aineistoyleismaare"];
//            $jp2_lossy = $row["jp2_lossy"];
//            $nide = $row["nide"];
//            $min_julkaisu = $row["min_julkaisu"];
//            $max_julkaisu = $row["max_julkaisu"];
//            $min_tuonti = $row["min_tuonti"];
//            $max_tuonti = $row["max_tuonti"];
//  
//            echo"<tr>";
//                  echo "<td style='text-align: center;'>$arc</td>";
//                  echo "<td>$aineistoyleismaare</td>";
//                  echo "<td style='text-align: right;'>$jp2_lossy</td>";
//                  echo "<td style='text-align: right;'>$nide</td>";
//                  echo "<td>$min_julkaisu</td>";
//                  echo "<td>$max_julkaisu</td>";
//                  echo "<td>$min_tuonti</td>";
//                  echo "<td>$max_tuonti</td>";
//            echo "</tr>";
//  
//          }
//  
//        echo"</table>";      
// 	 echo "</div";
      echo "</div>";
  
    echo "</div>";
  
close_sql($db);
oci_close($conn);
oci_close($conn_diona);

?>
</body>
</html>

