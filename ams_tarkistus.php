

<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_karttuvat.php");
include("global_variables.php");

?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>AMS tarkistus</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
</head>
<body>

<?php
directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);
docIdSearch(basename(__FILE__), $db);

        $eraStyle="";
        $eraFileCount=countFiles("/mnt/sl_import/era/*.zip");
        if ($eraFileCount>40) {
         $eraStyle="background-color:red; color: white;";
         $era="$eraFileCount Zip";

        } elseif ($eraFileCount==0) {
          $era="";
        } else {
        $era="$eraFileCount Zip";
        }

        echo"<div class='dailyReportFrame'>";
        echo"<h3>Tänään</h3>";
        $export_sivuja = dailyReport($db, $queryExportted);
        $accepted_sivuja = dailyReport($db, $queryAccepted);
        $rejected_sivuja = dailyReport($db, $queryRejected);
        $importoitu_sivuja = dailyReport($db, $queryImported);
        $deletoitu_sivuja = dailyReport($db, $queryDeleted);

        echo"<table class='dailyTable'>";
        echo"<tr style='text-align: center;'>";
        echo "<th>Exportoitu&nbsp;</th>";
        echo "<th>Tarkastettu&nbsp;</th>";
        echo "<th>Importoitumassa&nbsp;</th>";
        echo "<th>Importoitu&nbsp;</th>";
        echo "<th>Poistettu</th>";
        echo"<tr style='text-align: center;'>";
        echo "<td>$export_sivuja</td>";
        echo "<td>$accepted_sivuja</td>";
        echo "<td style='$eraStyle'>$era</td>";
        echo "<td>$importoitu_sivuja</td>";
        echo "<td>$deletoitu_sivuja</td>";
        echo"</tr>";
        echo "</table>";

echo"</div>";

naviHeader();


function signalHandler($signal) {
    switch ($signal) {
        case SIGINT:
            echo "Script execution stopped by user.\n";
            exit;
    }
}

if (function_exists('pcntl_signal')) {
    pcntl_signal(SIGINT, "signalHandler");
} else {
    #echo "PCNTL functions are not available on this system.\n";
}

// Function to extract numeric parts from an issue number and to handle textual parts
function extractNumeric($issueNum) {
    if (preg_match('/(\d+)/', $issueNum, $matches)) {
        return (int)$matches[0];
    }
    return 0;  // Return 0 if no numeric part is found
}

// Function to parse issue number ranges into individual numbers
function expandIssueRange($issueRange) {
    $expanded = [];
    foreach (explode(',', $issueRange) as $part) {
        if (strpos($part, '-') !== false) {
            list($start, $end) = explode('-', $part);
            $expanded = array_merge($expanded, range($start, $end));
        } else {
            $expanded[] = $part;
        }
    }
    return $expanded;
}

function sortIssuesWithText($a, $b) {
    $numA = extractNumeric($a['issuenum']);
    $numB = extractNumeric($b['issuenum']);
    if ($numA === $numB) {
        return strcmp($a['issuenum'], $b['issuenum']); // Sort textually if numbers are the same
    } else if ($numA < $numB) {
        return -1;  // Return -1 if $numA is less than $numB
    } else {
        return 1;   // Return 1 if $numA is greater than $numB
    }
}

function expandIssues($issues) {
    $expandedIssues = [];
    foreach ($issues as $issue) {
        if (strpos($issue, '-') !== false) {
            list($start, $end) = explode('-', $issue);
            $expandedIssues = array_merge($expandedIssues, range($start, $end));
        } else {
            $expandedIssues[] = $issue;
        }
    }
    return $expandedIssues;
}

function containsNumberStart($array) {
    foreach ($array as $item) {
        // Check if the item starts with a digit
        if (preg_match('/^\d/', $item)) {
            return true;
        }
    }
    return false;
}


$dbUsername = getenv('DB_USERNAME');
$dbPassword = getenv('DB_PASSWORD');
$connectionString = getenv('DB_CONNECTION_STRING');

if (!$dbUsername || !$dbPassword || !$connectionString) {
    #die("Error: Database credentials are not properly set in environment variables.\n");
}

#$conn = oci_connect($dbUsername, $dbPassword, $connectionString, 'AL32UTF8');
$conn=oci_connect('diona', 'diona', '//192.168.10.10:1521/dionapdb.docworks.lib.helsinki.fi', 'AL32UTF8');

if (!$conn) {
    $e = oci_error();
    file_put_contents('db_errors.log', date('[Y-m-d H:i:s] ') . $e['message'], FILE_APPEND);
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    exit("Database connection error, execution stopped.\n");
}




// Fetch the second list of manually detected missing issues
$missingIssuesQuery = "SELECT issn, TO_CHAR(pvm,'YYYY') AS year, REPLACE(no,' ','') AS issuenum FROM nidepuutteet WHERE issn IN (SELECT issn FROM kausi_lehti
WHERE julkaisumaa IN ('US', 'XX', 'CA','us','USA','usa','XX','xx','xx ','xxc','xxk','xxu'))";
$missingStid = oci_parse($conn, $missingIssuesQuery);
oci_execute($missingStid);

$manualMissing = [];
while ($row = oci_fetch_assoc($missingStid)) {
    $key = $row['ISSN'] . '|' . $row['YEAR'];
    foreach (expandIssueRange($row['ISSUENUM']) as $issue) {
        $manualMissing[$key][$issue] = true;
    }
}




$query_yearRange = "SELECT n.tunnus AS ISSN, 
                    MIN(TO_CHAR(julkaisu_pvm, 'YYYY')) AS START_YEAR,
                    MAX(TO_CHAR(julkaisu_pvm, 'YYYY')) AS END_YEAR
                    FROM nide n
                    WHERE aineistoyleismaare in ('SAN','AIK')
                    GROUP BY n.tunnus
                    ORDER BY n.tunnus";

$stid_yearRange = oci_parse($conn, $query_yearRange);
oci_execute($stid_yearRange);

$issnYearRange = [];
while ($row = oci_fetch_assoc($stid_yearRange)) {
    $issn = $row['ISSN'];
    $issnYearRange[$issn] = ["start" => $row['START_YEAR'], "end" => $row['END_YEAR']];
}

// Main query to fetch publication data
$query = "SELECT kl.paanimeke AS title, sn.issn AS issn, TO_CHAR(julkaisu_pvm,'YYYY') AS year, nvl(no,'##') AS issuenum FROM nide n
JOIN sarjajulkaisu_nide sn ON n.nide_id = sn.sarjanide_id
JOIN kausi_lehti kl ON kl.issn = sn.issn
WHERE kl.issn IN (select issn from kausi_lehti WHERE julkaisumaa IN ('US', 'XX', 'CA','us','USA','usa','XX','xx','xx ','xxc','xxk','xxu'))
ORDER BY title, issn, year, julkaisu_pvm, issuenum";

$query2 = "SELECT kl.paanimeke AS title, sn.issn AS issn, TO_CHAR(julkaisu_pvm,'YYYY') AS year, nvl(no,'##') AS issuenum FROM nide n
JOIN sarjajulkaisu_nide sn ON n.nide_id = sn.sarjanide_id
JOIN kausi_lehti kl ON kl.issn = sn.issn
WHERE kl.issn IN ('fk32000'
,'fk32001'
,'fk32003'
,'fk32004'
,'fk32006'
,'fk16566'
,'fk32016'
,'0008-2775'
,'fk24823'
,'fk32017'
,'fk32019'
,'fk11168'
,'fk32021'
,'fk20220775'
,'fk32025'
,'fk32026'
,'fk31211'
,'fk32028'
,'fk11651'
,'fk32029
,'fk32030'
,'fk27509'
,'fk32032'
,'fk32033'
,'fk32034'
,'1059-4779'
,'fk32035'
,'fk32044'
,'fk32037'
,'fk32036'
,'fk32039'
,'fk32998'
,'fk24873'
,'fk32041'
,'fk32042'
,'fk32045')
ORDER BY title, issn, year, julkaisu_pvm, issuenum";

$stid = oci_parse($conn, $query);
oci_execute($stid);

$results = [];
$allYears = [];
$issnTitles = [];

// Collect all data and prepare a structure to verify years per ISSN
while ($row = oci_fetch_assoc($stid)) {
    $key = $row['TITLE'] . '|' . $row['YEAR'] . '|' . $row['ISSN'];
    $results[$key][] = $row['ISSUENUM'];
    $allYears[$row['ISSN']][$row['YEAR']] = true;  // Track years we have data for each ISSN
    $issnTitles[$row['ISSN']] = $row['TITLE']; 
}
// Iterate through each ISSN year range
foreach ($issnYearRange as $issn => $years) {
    $start = $years["start"];
    $end = $years["end"];
    
    for ($year = $start; $year <= $end; $year++) {
        if (!isset($allYears[$issn][$year])) {  // Check if the year is missing in data
            #$title = ) ? $issnTitles[$issn] : "Unknown Title";
            if (isset($issnTitles[$issn])) {
                $title = $issnTitles[$issn];
                $key = "$title|$year|$issn";  // Using a placeholder for title or determine a method to find the title
                $results[$key][] = "Vuosikerta puuttuu";  // Notate the missing volume
            } else {
                continue;
            }
            
        }
    }
}
ksort($results);

$lastIssues = [];
foreach ($results as $key => $issues) {
    list($title, $year, $issn) = explode('|', $key);
    if (!isset($lastIssues[$issn])) {
        $lastIssues[$issn] = [];
    }
    $lastIssues[$issn][$year] = end($issues); // Assuming $issues is already sorted
}


echo "<div style=\"text-align: center;\"><h1>Amerikassa julkaistut puutteita sisältävät vuosikerrat</h1>";
echo "<h2><span style='color:grey;'>Nide digissä</span>&nbsp;&nbsp;<span style='color:Indigo;font-weight:bold;'>Käsin merkitty puute/puuteväli</span>&nbsp;&nbsp;<span style='color:red;'>Havaittu puute</span></h2></div>";
echo "<table border='1'>";
echo "<tr><th>Nimeke</th><th>ISSN</th><th>Vuosi</th><th>Numerot</th></tr>";

foreach ($results as $key => $issues) {
    list($title, $year, $issn) = explode('|', $key);
       #     if ($issues[0]=="Vuosikerta puuttuu") {
       #             echo "<tr style=\"background-color:LavenderBlush;\"><td>" . htmlspecialchars($title) . "</td><td>" . htmlspecialchars($issn) . "</td><td>".$year."</td><td>".$issues[0]."</span></td></tr>";
       #             continue;
        #}
    
    // Expand any ranges in issues to individual issues
    $allIssues = expandIssues($issues);
    $issueComponents = array_map('extractNumeric', $allIssues);
    $uniqueIssues = array_unique($issueComponents);
    $previousYear = $year - 1;
    $prevLastIssue=$lastIssues[$issn][$previousYear];
    $firstIssueCurrentYear = reset($uniqueIssues);
    $lastIssueCurrentYear = end($uniqueIssues);
    // Determine the complete range of issues
    if ($prevLastIssue+1==$firstIssueCurrentYear && $firstIssueCurrentYear>1) {
        $rangeStart=$firstIssueCurrentYear;
        
    } else {
        $rangeStart=1;
    }
    $completeRange = range($rangeStart, max($uniqueIssues));
    $missing = array_diff($completeRange, $uniqueIssues);

    // Prepare issue display list
    $tempIssues = [];
    foreach ($completeRange as $issue) {
        if (!in_array($issue, $uniqueIssues)) {
            $manualMissingKey = $issn . '|' . $year;
            if (isset($manualMissing[$manualMissingKey]) && array_key_exists((string)$issue, $manualMissing[$manualMissingKey])) {
                $tempIssues[] = ['issuenum' => $issue, 'status' => 'manuallymissing'];
            } else {
                $tempIssues[] = ['issuenum' => $issue, 'status' => 'detectedmissing'];
            }
        }
    }

    
    // Include non-missing issues
    foreach ($issues as $issue) {
        $tempIssues[] = ['issuenum' => $issue, 'status' => 'ok'];
    }

    // Sort all issues
    usort($tempIssues, 'sortIssuesWithText');
    
    // Generate HTML output for issue list
    $issueList = [];
    $showYear=false;
    foreach ($tempIssues as $tempIssue) {
                
        $issueDisplay = htmlspecialchars($tempIssue['issuenum']);

        switch ($tempIssue['status']) {
            case 'ok':
                $issueList[] = $issueDisplay;
                $showYear=true;
                break;
            case 'detectedmissing':
                $issueList[] = "<span style='color:red;'>$issueDisplay</span>";
                    $showYear=true;
                break;
            case 'manuallymissing':
                $issueList[] = "<span style='color:Indigo;font-weight:bold;'>$issueDisplay</span>";
                    $showYear=true;
                break;
        }
    }
    if ($showYear) {
        echo "<tr><td>" . htmlspecialchars($title) . "</td><td>" . htmlspecialchars($issn) . "</td><td><a href=\"https://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".$year."\" target=\"_new\">$year</td><td>" . implode(', ', $issueList) . "</td></tr>";
    }
}

echo "</table>";

echo "</body></html>";

oci_free_statement($stid);
oci_free_statement($missingStid);
oci_close($conn);

?>
