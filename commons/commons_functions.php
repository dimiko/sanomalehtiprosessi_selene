<?php

function getDocPoolFolder($docId, $isServer = 0) {

    $docid = $docId;    
    $folder = substr(substr($docid, -4), 0, 2);
    $subFolder = substr($docid, -2);
    
    $dw_pool_folder = ($isServer) ? "\\\\mnt\\akte\\dw-pool\\$folder\\$subFolder\\" : "\\\\akte\\dw-pool\\$folder\\$subFolder\\";

    return $dw_pool_folder;
}

function getRejectString($xmlPath) {
  
  $xml=simplexml_load_file($xmlPath) or die("Error: Cannot create object");
  
  foreach($xml->ATTRIBUTES[0]->FIELD as $books) {
    if($books['NAME'] == 'REJECTRESULT') {
      return $books;
    }
  
  }
  return "";
}

?>