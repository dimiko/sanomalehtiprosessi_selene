<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_niteet.php");
?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <!-- <meta http-equiv="refresh" content="240" > -->
    <title>Sanomalehtiprosessin nidekohtainen tilanne</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css?v1">
    <link rel="stylesheet" type="text/css" href="css/style.css?v1">
    <link rel="stylesheet" type="text/css" href="css/style2.css?v1">
    <script type="text/javascript" src="jquerys/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="jquerys/jquery.cookie.js"></script>
    <script type="text/javascript" src="jquerys/sorttable.js"></script> 
     <script type="text/javascript">
    $(function()
    {
        var piilota = $.cookie("piilotaValmiit");
        if (piilota=="kylla") {
            $('.valmis').hide();
        } else if (piilota=="ei") {
            $('.valmis').show();
        } else {
            $('.valmis').show();
            
        }
        
        $('#piilotaNappi').click(function(e) {
            $.cookie('piilotaValmiit', 'kylla');
            $('.valmis').hide();
            //$('.valmiseven').hide();
            //$('.valmisodd').hide();
            }
        );

        $('#naytaNappi').click(function(e) {
            $.cookie('piilotaValmiit', 'ei');
            $('.valmis').show();
            //$('.valmiseven').show();
            //$('.valmisodd').show();
        });
        $('#vainhelpNappi').click(function(e) {
            $.cookie('vainHelp', 'kylla');
            $('.valmis').hide();
            $('.odd').hide();
            $('.even').hide();
            //$('.valmiseven').hide();
            //$('.valmisodd').hide();
            $('.tyhjiaeven').hide();
            $('.tyhjiaodd').hide();
        });
            $('#kaikkiNappi').click(function(e) {
            $.cookie('vainHelp', 'ei');
            $.cookie('piilotaKorjatut', 'ei');
            $('.valmis').show();
            $('.odd').show();
            $('.even').show();
            //$('.valmiseven').show();
            //$('.valmisodd').show();
            $('.tyhjiaeven').show();
            $('.tyhjiaodd').show();
        });
});

</script>
<script type="text/javascript">
  function byId(id) {
    return document.getElementById ? document.getElementById(id) : document.all[id];
  }
  var prevLink = "";
  function changeActiveStates(ele) {
    if (prevLink) byId(prevLink).className = "";
    ele.className = 'active';
    prevLink = ele.id;
  }
</script>
<script type="text/javascript">
    function getPages(lomakeFrame,pdfFrame){
    parent.window.lomake.location=lomakeFrame;
    parent.window.pdf.location=pdfFrame;
}

</script>
<style type="text/css">
th, td {
  padding: 3px !important;
}


/* Sortable tables */
table.sortable thead {
    background-color: #333;
    color: #cccccc;
    font-weight: bold;
    cursor: default;
}
th {
  font-size: 100%;
  position: sticky;

}

</style>

</head>
<body>
<br>
<br>
<br>
<br>
<br>
<br>

<?php

$toiminto="listaa";

if ($toiminto=="listaa") {
  
  if (isset($_POST["ryhma"])) {
    $ryhma=$_POST["ryhma"];
  } elseif (isset($_GET["ryhma"])) {
    $ryhma=$_GET["ryhma"];
  } else $ryhma="";
  
  if (isset($_POST["sort"])) {
    $sort=$_POST["sort"];
  } elseif (isset($_GET["sort"])) {
    $sort=$_GET["sort"];
  } else {
    $sort="pvm";
  }
  
  $ryhma=urldecode($ryhma);


  echo "<div class=\"buttonx\">
  <input id=\"piilotaNappi\" type=\"button\" value=\"Piilota valmiit\" />
  <input id=\"naytaNappi\" type=\"button\" value=\"Nayta valmiit\" />
  <!-- <input id=\"vainhelpNappi\" type=\"button\" value=\"Nayta lisatarkastettavat\" /> -->
  <input id=\"kaikkiNappi\" type=\"button\" value=\"Nayta Kaikki\" />&nbsp;&nbsp;";
  
  naviHeader();

  echo "<form action=\"niteet.php\" method=\"get\">";
  echo"<select name=\"ryhma\">";
  echo"<option></option>";
  naviHeader();
  echo "</div>";
  option_selection($db, $query_option, $sort);
  //rescan_docs($db, $query_rescans, basename(__FILE__)); 
  include("acknowledgment_messages.php");
  $query = $query_sort;
  
  if ($ryhma!="") {
    $query.=" where title='$ryhma' ";
  } else {
    $query.=" where title is null ";
  }
  if ($sort=="skannattu") {
    $query.="order by scandate";
  } elseif ($sort=="pvm") {
    $query.="order by issuedate desc, title";
  } elseif ($sort=="nimeke") {
    $query.="order by title,issuedate";
  } elseif ($sort=="docid") {
    $query.="order by docid";
  } elseif ($sort=="digi-import") {
    $query.="order by digi_importdate desc";
  }

  niteet_listing($db, $query);

}
close_sql($db);
oci_close($conn);
oci_close($conn_diona);
?> 
</body>
</html>
