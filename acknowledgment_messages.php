<?php

  if(isset($_SESSION['displayErrorMessage'])) {
    $messageError =  $_SESSION['displayErrorMessage'];
    if($messageError != "") {
      echo "<div id='displayErrorMessage'>$messageError</div>";
      echo "<script> displayErrorMessage(); </script>";
      $_SESSION['displayErrorMessage'] = "";
    }
  }

  if(isset($_SESSION['displayOkMessage'])) {
    $messageOk =  $_SESSION['displayOkMessage'];
    if($messageOk != "") {
      
      echo "<div id='displayOkMessage'>$messageOk</div>";
      echo "<script> displayOkMessage(); </script>";
      $_SESSION['displayOkMessage'] = "";
    }
  }
  
?>