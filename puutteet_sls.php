<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_karttuvat.php");
include("global_variables.php");
?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <meta http-equiv="refresh" content="180" >
    <title>SLS puutteet</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
</head>
<body>

<?php

directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);
docIdSearch(basename(__FILE__), $db);
echo"<div class='dailyReportFrame'>";
  echo"<h3>Tänään</h3>";

  $export_sivuja = dailyReport($db, $queryExportted);
  $accepted_sivuja = dailyReport($db, $queryAccepted);
  $rejected_sivuja = dailyReport($db, $queryRejected);
  $filmattu_sivuja = dailyReport($db, $queryFilmed);
  $importoitu_sivuja = dailyReport($db, $queryImported);
  $deletoitu_sivuja = dailyReport($db, $queryDeleted);

  echo"<table class='dailyTable'>";
  echo"<tr'>";
    echo "<th>Exportoitu&nbsp;</th>";
    echo "<th>Tarkastettu&nbsp;</th>";
    echo "<th> Tulostettu&nbsp;</th>";
    echo "<th>Importoitu&nbsp;</th>";
    echo "<th>Poistettu</th>";
  echo"<tr>";
    echo "<td>$export_sivuja</td>";
    echo "<td>$accepted_sivuja</td>";
    echo "<td>$filmattu_sivuja</td>";
    echo "<td>$importoitu_sivuja</td>";
    echo "<td>$deletoitu_sivuja</td>";
  echo"</tr>";
  echo "</table>";

echo"</div>";




naviHeader();
$ora_db = oci_connect('diona', 'diona', 'pythia/dionapdb.docworks.lib.helsinki.fi','AL32UTF8');
echo "<div>";
#digiTitlePaperRescans ($caption,$issns,,$startyear,$eandyear,$langs,$types,$db) {
digiTitlePaperRescans ("Nimekkeet joissa paperilta skannattavia puutteita","","'swe','swefin','finswe'","",$ora_db);
echo "/<div>";
#echo "<div>";
#digiPaperRescans ($caption,$issns,$startyear,$eandyear,$langs,$types,$db)
#digiPaperRescans ("Ruotsinkielisten puutteiden paperiskannaukset","","","","'swe','swefin','finswe'","",$ora_db);
#echo "<div>";

echo "<div>";
#digiFaults($caption,$issns,$langs,$types,$skipFaults,$db)
#digiFaults("Ruotsinkielisten puutteet","'0785-398X','fk20714','0355-9149','fk11035','0782-7105','fk11010','fk10001','0789-8274'
#,'fk10000','0356-164X','1457-4292','fk20223','0358-6294','2242-1009','0357-1548','0780-8704'
#,'0789-8193','1458-0780','1238-0164','0356-0724','0782-5528','0357-1521','1235-8355'
#,'fk14622','fk14981','0780-5063','fk20861','fk11407','0356-1275','fk20365','0023-8015'
#,'fk11496','fk15549','0786-2644','fk11668','fk23821','fk16887','0359-1441','fk25016'
#,'1235-9289','fk11688','1456-0518','0356-3553','1458-8803','2342-7728','0356-5653'
#,'1235-8398','0785-3998','fk11800','1235-824X','fk11989','fk17113','0789-838X','0358-2434'
#,'0789-8363','0355-5461','0356-1844','fk16278','1235-8460','0782-6559','0787-9598','1797-9021','1458-0764'","1770","","'swe','swefin'","'SAN'","",$ora_db);

digiFaults("Ruotsinkielisten puutteet","","1940","","'swe','swefin'","'SAN','AIK'","'INVALID_METADATA','SUPPLEMENT','QA_ACCEPTED','ISSUE_NUMBERING_ANOMALY','NOT_PUBLISHED','DIGITISATION_ANOMALY','QA_REJECTED'",$ora_db);
echo "</div>";
echo "<div id=\"not_published\">";
digiFaults("Ruotsinkielisten ei ilmestyneet","","1940","","'swe','swefin'","'SAN','AIK'","'NOT_IN_COLLECTIONS','TO_BE_SCANNED_FROM_ORIGINAL','INVALID_METADATA','SUPPLEMENT','QA_ACCEPTED','ISSUE_NUMBERING_ANOMALY','MISSING','DIGITISATION_ANOMALY','QA_REJECTED','OTHER','MISSING_PAGES','SHOULD_BE_REMOVED'",$ora_db);

echo"</div>";


close_sql($db);
oci_close($ora_db);

?>
</body>
</html>

