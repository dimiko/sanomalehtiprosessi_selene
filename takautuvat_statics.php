<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_takautuva.php");
include("sql_querys/query_diona.php");
include("global_variables.php");
?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <meta http-equiv="refresh" content="180" >
    <title>Takautuvan sanomalehtiprosessin tila</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
    <script type="text/javascript" src="chart/Chart.bundle.js"></script> 
</head>
<body>

<?php

directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);
docIdSearch(basename(__FILE__), $db);
include("acknowledgment_messages.php");

       $eraStyle="";
        $eraFileCount=countFiles("/mnt/sl_import/era/*.zip");
        if ($eraFileCount>40) {
         $eraStyle="background-color:red; color: white;";
         $era="$eraFileCount Zip";

        } elseif ($eraFileCount==0) {
          $era="";
        } else {
        $era="$eraFileCount Zip";
        }

        echo"<div class='dailyReportFrame'>";
        echo"<h3>Tänään</h3>";
        $export_sivuja = dailyReport($db, $query_export);
        $accepted_sivuja = dailyReport($db, $query_accepted);
        $rejected_sivuja = dailyReport($db, $query_rejected);
        $importoitu_sivuja = dailyReport($db, $query_imported);
        $deletoitu_sivuja = dailyReport($db, $query_deleted);
        echo"<table class='dailyTable'>";
        echo"<tr style='text-align: center;'>";
        echo "<th>Exportoitu&nbsp;</th>";
        echo "<th>Tarkastettu&nbsp;</th>";
        echo "<th>Importoitumassa&nbsp;</th>";
        echo "<th>Importoitu&nbsp;</th>";
        echo "<th>Poistettu</th>";
        echo"<tr style='text-align: center;'>";
        echo "<td>$export_sivuja</td>";
        echo "<td>$accepted_sivuja</td>";
        echo "<td style='$eraStyle'>$era</td>";
        echo "<td>$importoitu_sivuja</td>";
        echo "<td>$deletoitu_sivuja</td>";
        echo"</tr>";
        echo "</table>";

echo"</div>";


naviHeader();

# Käppyrä
echo "<div class=\"chart-container\" style=\"clear:both;\">
  <canvas id=\"myChart\" width=\"1800\" height=\"500\">sdffds</canvas>\n
</div>";

echo "<div class='dailyStaticInnerFrame'>";
status_list_table_latest($db, $query_hyvaksytyt, "Hyväksytyt", "niteet");
echo "</div>";
echo"<div class='staticHeader'>";
  echo "<h2>Tilastot</h2>";
echo "</div>";


/*
echo "<div class='dailyStaticInnerFrame'>";
  echo"<h2 style='text-align:center;'>Sähköinen vastaanotto</h2>";
  $comellus_arvot2024=comellus_stat_table_takautuva('2024',$conn);
  $comellus_arvot2023=comellus_stat_table_takautuva('2023',$conn);
  $comellus_arvot2022=comellus_stat_table_takautuva('2022',$conn);
  $comellus_arvot2021=comellus_stat_table_takautuva('2021',$conn);
  $comellus_arvot2020=comellus_stat_table_takautuva('2020',$conn);
  $comellus_arvot2019=comellus_stat_table_takautuva('2019',$conn);
  $comellus_arvot2018=comellus_stat_table_takautuva('2018',$conn);   
echo"</div>";
*/

echo "<div class='dailyStaticInnerFrame'>";
  echo"<h2 style='text-align:center;'>skannaus</h2>";
  stat_table_takautuva($db, $query_daily_scans, "Skannaus", "PVM");
  stat_table_takautuva($db, $query_monthly_scans_2024, "Skannaus 2024", "KK");
  stat_table_takautuva($db, $query_monthly_scans_2023, "Skannaus 2023", "KK");
  stat_table_takautuva($db, $query_monthly_scans_2022, "Skannaus 2022", "KK");
  stat_table_takautuva($db, $query_monthly_scans_2021, "Skannaus 2021", "KK");
echo"</div>";

echo "<div class='dailyStaticInnerFrame'>";
  echo"<h2 style='text-align:center;'>docWorks</h2>";
  stat_table_takautuva($db, $query_dw_daily_exports, "docworks", "PVM");
  stat_table_takautuva($db, $query_dw_monthly_exports, "docworks", "KK");
echo"</div>";

echo "<div class='dailyStaticInnerFrame'>";
  echo"<h2 style='text-align:center;'>tarkastettu</h2>";
  stat_table_takautuva($db, $query_depositbrowser_daily, "tarkastettu", "PVM");
  stat_table_takautuva($db, $query_depositbrowser_monthly, "tarkastettu", "KK");
echo"</div>";

echo "<div class='dailyStaticInnerFrame'>";
  echo"<h2 style='text-align:center;'>digi-import</h2>";
  stat_table_takautuva($db, $query_digi_daily_imports, "importoitu", "PVM");
  stat_table_takautuva($db, $query_digi_monthly_imports, "importoitu", "KK");
echo"</div>";

echo "<div class='dailyStaticInnerFrame_2'>";

  echo"<h2 style='text-align:left;'>Lehdet skannattu ja saatu sähköisesti</h2>";

  echo "<div style='float: left;'>";
    digi_yearly_stats_takautuva ($db, $query_digissa);
  echo "</div>";
  echo "<div style='margin-left: 600px;'>";
    digi_electronic_yearly_stats_takautuva ($conn_diona, $query_digital_issues);
  echo "</div>";
  
echo"</div>";  


?>

<script>

var densityCanvas = document.getElementById("myChart");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var Data2021 = {
  label: '2021',
  data: [
<?php echo chart_data("2021","viikko","takautuva");  ?>
],
  backgroundColor: 'rgba( 148,0,211,0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};

var Data2022 = {
  label: '2022',
  data: [
<?php echo chart_data("2022","viikko","takautuva");  ?>
],
  backgroundColor: 'rgba( 0,0,211,0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};

var Data2023 = {
  label: '2023',
  data: [
<?php echo chart_data("2023","viikko","takautuva");  ?>
],
  backgroundColor: 'rgba(0,100,0,0.6)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};

var Data2024 = {
  label: '2024',
  data: [
<?php echo chart_data("2024","viikko","takautuva");  ?>
],
  backgroundColor: 'rgba(0,0,0,0.6)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};


var Data2021Sum = {
  label: '2021',
  data: [
<?php echo chart_data("2021","summa","takautuva");  ?>
],
  borderColor: 'rgba(148,0,211,0.6)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var Data2022Sum = {
  label: '2022',
  data: [
<?php echo chart_data("2022","summa","takautuva");  ?>
],
  borderColor: 'rgba(0,0,211,0.6)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var Data2023Sum = {
  label: '2023',
  data: [
<?php echo chart_data("2023","summa","takautuva");  ?>
],
  borderColor: 'rgba(0,100,0,0.3)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var Data2024Sum = {
  label: '2024',
  data: [
<?php echo chart_data("2024","summa","takautuva");  ?>
],
  borderColor: 'rgba(0,0,0,0.6)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var scanData = {
  labels: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53],
  datasets: [Data2021, Data2022, Data2023, Data2024, Data2021Sum, Data2022Sum, Data2023Sum, Data2024Sum]
};

var chartOptions = {
  legend: {
   display:true,
   position: 'bottom',
   labels: {
	reverse: true
	}
  },
  title: {
    display: true,
    text: 'Takautuvien sanomalehtien skannaustuotanto'
  },
  responsive: false,
  scales: {
    xAxes: [{
      barPercentage: 1,
      categoryPercentage: 0.6,
      scaleLabel: {
      labelString: "viikko",
	display: true
     },
    }],
    yAxes: [{
      id: "y-axis",
      ticks: {
	stepSize: 10000,
      },
      scaleLabel: {
      	labelString: "viikossa",
      	display: true
     	}}, {
      id: "y-axis-sum",
      position: 'right',
      ticks: {
        stepSize: 100000,
      },
      scaleLabel: {
      labelString: "vuodessa",
      display: true
     }
    }]
  }
};

var barChart = new Chart(densityCanvas, {
  type: 'line',
  data: scanData,
  options: chartOptions
});
</script>
<?php
close_sql($db);
oci_close($conn);
oci_close($conn_diona);

?>
</body>
</html>

