<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_niteet.php");
include("global_variables.php");
?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <title>Sanomalehtiprosessiin jumittuneet niteet</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css?v1">
    <link rel="stylesheet" type="text/css" href="css/style.css?v1">
    <link rel="stylesheet" type="text/css" href="css/style2.css?v1">
    <script type="text/javascript" src="javascript/javaScripts.js?v1"></script>
<style type="text/css">
th, td {
  padding: 3px !important;
}

</style>

</head>
<body>
<?php
directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);
docIdSearch(basename(__FILE__), $db);
naviHeader();
problem_documents ($db, $query_problem_docs ,"Yli 6 päivää tuotannossa olleet niteet","ongelmat",$DW_directorys, $DW_directorys_alert_space);
problem_documents ($db, $query_embargo_docs ,"Alle 28pv sitten ilmestyneet niteet odottamassa digiin tuontia.","embargo",$DW_directorys, $DW_directorys_alert_space);
include("acknowledgment_messages.php");
//problem_documents ($db, $query);

close_sql($db);
oci_close($conn);
oci_close($conn_diona);
?>

</body>
</html>
