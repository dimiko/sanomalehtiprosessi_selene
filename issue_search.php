<?php
include("sessio.php");
include("functions.php");

$page = $_GET["page"];
$issue = $_GET["nimike"];
$issueNumber = $_GET["issueNum"];
$issueDate = $_GET["issueDate"];

$_SESSION['nimi'] = $issue;
$_SESSION['issn'] = $issueNumber;

if($issueDate != "") {

    $docId = getDocIdByIssueIssueNumberIssueDate($issue, $issueNumber, $issueDate, $db);

    unset($_SESSION['nimi']);
    unset($_SESSION['issn']);
    $docIdHasRowInDocWorksDB = true;//checkIfDocIdExistsInDocWorksDB($db, $docId);
    if($docIdHasRowInDocWorksDB) {
        header("Location: docid_page.php?page=$page&docId=$docId");    
    } else {
        $_SESSION['displayErrorMessage'] = "Työtä ei löytynyt tietokannasta.";
        header("Location: $page");
    }
    
} else {
    header("Location: $page");
}


?>

