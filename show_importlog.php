<?php

    if (isset($_POST["file"])) {
        $file=$_POST["file"];
    } elseif (isset($_GET["file"])) {
        $file=$_GET["file"];
    }

$replacement = "/mnt/akte/pas_process";
$newfile = str_replace("\\", "/", $file); // Replace backslashes with slashes
$newfile = str_replace("//akte/pas_process", $replacement,$newfile);
$file=$newfile;


$info = pathinfo($file);

switch($info["extension"])
{
    case "html" :
	$contentType="Content-Type: text/html; charset=UTF-8";
        break;

    case "xml" : 
	$contentType="Content-Type: application/xml";
$doc = new DOMDocument;
$doc->preserveWhiteSpace = true;
$doc->formatOutput = true;
$doc->load($file);
    header('Content-Description: File Transfer');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header($contentType);

// Output the XML with retained whitespace
echo $doc->saveXML();
exit;
	break;

    default : 
	$contentType="Content-Type: text/plain";

}
if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header($contentType);

    header('Content-Length: ' . filesize($file));
    readfile($file);

} else {
    echo "$file not found!";
}


exit;
?>

