<?php

    if (isset($_POST["file"])) {
        $file=$_POST["file"];
    } elseif (isset($_GET["file"])) {
        $file=$_GET["file"];
    }

$replacement = "/mnt/akte/pas_process";
$newfile = str_replace("\\", "/", $file); // Replace backslashes with slashes
$newfile = str_replace("//akte/pas_process", $replacement,$newfile);
$file=$newfile;


if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Type: text/plain');

$xml = simplexml_load_file($file);

$prefix = 'premis';

// Define the namespace URI based on the prefix
$namespaceUri = $xml->getDocNamespaces()[$prefix];

// Register the namespace
$xml->registerXPathNamespace($prefix, $namespaceUri);

// Find nodes with the specified conditions
$nodes = $xml->xpath("//premis:event[premis:eventOutcomeInformation/premis:eventOutcome='failure']");

// Iterate through the nodes and echo their content
foreach ($nodes as $node) {
    echo "\n\n\n\r";
    echo $node->asXML();
}



} else {
    echo "$file not found!";
}


$dom = new DOMDocument('1.0');
$dom->preserveWhiteSpace = false;
$dom->formatOutput = true;
$dom->loadXML($xml->asXML());

// Loop through each premis:event node
foreach ($dom->getElementsByTagName('premis:event') as $event) {
echo $event;
    // Check if the eventOutcome is 'failure'
    $outcomeNode = $event->getElementsByTagName('premis:eventOutcome')->item(0);
    if ($outcomeNode && $outcomeNode->nodeValue === 'failure') {
        // Echo the indented XML of the matching node
        echo $dom->saveXML($event), PHP_EOL;
    }
}


exit;
?>

