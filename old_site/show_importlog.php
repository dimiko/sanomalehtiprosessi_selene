
<?php

    if (isset($_POST["file"])) {
        $file=$_POST["file"];
    } elseif (isset($_GET["file"])) {
        $file=$_GET["file"];
    }


if (file_exists($file)) {
    header('Content-Description: File Transfer');
    header('Content-Type: text/plain');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    echo "$file\n\n";
    readfile($file);
    exit;
}
?>

