<?php
//include("sessio.php");
include("sessio_php_version_5.php");

?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <meta http-equiv="refresh" content="180" >
    <title>Sanomalehtiprosessin tila</title>
    <link rel="stylesheet" type="text/css" href="reset.css">
    <link rel="stylesheet" type="text/css" href="style.css">
<!--    <script type="text/javascript" src="/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/jquery.cookie.js"></script>
    <script type="text/javascript" src="/sorttable.js"></script> -->
    <script type="text/javascript" src="chart/Chart.bundle.js"></script> 
 


</head>
<body>


<?php

###############################################################
echo "<div style=\"float:left; margin-right:20px;margin-top:10px;\">";
echo"<h3 style=\"text-align:center;\">Karttuvat | <a href=\"index_takautuva.php\">Takautuvat</a> |
 <a href=\"niteet.php\">Nidekohtaiset tiedot</a> |
 <a href=\"ongelmaniteet.php\">Ongelmaniteet</a> |
 <a href=\"sl-rdy.txt\">Kansiot ei vielä käsittelyssä</a> |
 <a href=\"/dw-in.txt\">IN kulutukset</a> |
 <a href=\"http://akeso.docworks.lib.helsinki.fi/DepositBrowser/\">DepositBrowser</a></h3>\n";
echo"</div>\n";
    #######################################################################################################################################################
echo "<div class=\"chart-container\" style=\"clear:both;\">
	<canvas id=\"myChart\" width=\"1800\" height=\"500\"></canvas>\n
</div>\n";
mssql_close($db);
?>

<?php
################################################################################################################
#
# Chart
#
#
###############################################################################################################
function chart_data ($year,$type) {
    $chart_data=array();
    $query="
	set datefirst 1
	SELECT datepart(wk,scandate) as viikko,COUNT(docid) as niteita,SUM(pages) as sivuja
  	FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='".$year."' and title not like '%z+%'
  	group by datepart(wk,scandate)
  	order by datepart(wk,scandate) asc
	";


    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }

    $sum_sivut=0;
    while($row = mssql_fetch_array($result))
        {
            $viikko=$row["viikko"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            $sum_sivut=$sum_sivut+$sivuja;
	    if ($type=="summa") {
		 array_push($chart_data,$sum_sivut);
	    } elseif ($type=="viikko") {
		 array_push($chart_data,$sivuja);
	    } else {
   
	   }
    }

    return implode(", ",$chart_data);

}

?>

<script>

var densityCanvas = document.getElementById("myChart");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var Data2018 = {
  label: '2018',
  data: [<?php echo chart_data("2018","viikko");  ?>],
  backgroundColor: 'rgba(0, 99, 132, 0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'
};

var Data2019 = {
  label: '2019',
  data: [
<?php echo chart_data("2019","viikko");  ?>
],
  backgroundColor: 'rgba(240, 120, 132, 0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};

var Data2020 = {
  label: '2020',
  data: [
<?php echo chart_data("2020","viikko");  ?>
],
  backgroundColor: 'rgba(20, 150, 20, 0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};

var Data2021 = {
  label: '2021',
  data: [
<?php echo chart_data("2021","viikko");  ?>
],
  backgroundColor: 'rgba( 148,0,211,0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};

var Data2022 = {
  label: '2022',
  data: [
<?php echo chart_data("2022","viikko");  ?>
],
  backgroundColor: 'rgba( 0,0,211,0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};


var Data2018Sum = {
  label: '2018',
  data: [<?php echo chart_data("2018","summa");  ?>],
  borderColor: 'rgba(0, 99, 132, 0.6)',
  borderWidth: 2,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var Data2019Sum = {
  label: '2019',
  data: [
<?php echo chart_data("2019","summa");  ?>
],
  borderColor: 'rgba(240, 120, 132, 0.6)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var Data2020Sum = {
  label: '2020',
  data: [
<?php echo chart_data("2020","summa");  ?>
],
  borderColor: 'rgba(100, 200, 100, 0.6)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var Data2021Sum = {
  label: '2021',
  data: [
<?php echo chart_data("2021","summa");  ?>
],
  borderColor: 'rgba(148,0,211,0.6)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};
var Data2022Sum = {
  label: '2022',
  data: [
<?php echo chart_data("2022","summa");  ?>
],
  borderColor: 'rgba(0,0,211,0.6)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var scanData = {
  labels: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53],
  datasets: [Data2018, Data2019, Data2020, Data2021, Data2022, Data2018Sum,Data2019Sum, Data2020Sum, Data2021Sum, Data2022Sum]
};

var chartOptions = {
  legend: {
   display:true,
   position: 'bottom',
   labels: {
	reverse: true
	}
  },
  title: {
    display: true,
    text: 'Karttuvien sanomalehtien skannaustuotanto'
  },
  responsive: false,
  scales: {
    xAxes: [{
      barPercentage: 1,
      categoryPercentage: 0.6,
      scaleLabel: {
      labelString: "viikko",
	display: true
     },
    }],
    yAxes: [{
      id: "y-axis",
      ticks: {
	stepSize: 10000,
      },
      scaleLabel: {
      	labelString: "viikossa",
      	display: true
     	}}, {
      id: "y-axis-sum",
      position: 'right',
      ticks: {
        stepSize: 100000,
      },
      scaleLabel: {
      labelString: "vuodessa",
      display: true
     }
    }]
  }
};

var barChart = new Chart(densityCanvas, {
  type: 'line',
  data: scanData,
  options: chartOptions
});
</script>

</body>
</html>

