<?php
include("sessio.php");
 

?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <meta http-equiv="refresh" content="180" >
    <title>Sanomalehtiprosessin tila</title>
    <link rel="stylesheet" type="text/css" href="reset.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/jquery.cookie.js"></script>
    <script type="text/javascript" src="/sorttable.js"></script> 
 


</head>
<body>


<?php

echo "<div style=\"float:none; clear:both;margin-right:20px;padding-top:50px;padding-bottom:50px;\">";
    echo"<h1 style=\"text-align:center;\">Sanomalehtiprosessi</h1>\n";
echo"</div>\n";

  
    #echo"<div style=\"float:none; clear:both;padding-top:100px;text-align:center;\"></div>\n";
## skannaus ##################################################################################################
$query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,scandate)=convert(date,GETDATE())";
    $result= sqlsrv_query($dbhandle, $query);
    $row=sqlsrv_fetch_array($result);
    $skannattu_sivuja=$row["sivuja"];
    echo "<div style=\"float:left; margin-right:30px;\">";
    echo"<h3 style=\"text-align:center;\">$skannattu_sivuja<br>skannattu</h3>\n";
    $query_scan_daily = "
      SELECT top 7 convert(varchar(10),scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,scandate)>convert(date,GETDATE()-21) and  datepart(dw,scandate) in (2,3,4,5,6)
  group by convert(varchar(10),scandate)
  order by convert(varchar(10),scandate) desc;";
    $result_scan_daily = sqlsrv_query($dbhandle, $query_scan_daily);
    if( $result_scan_daily === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table width=\"250px\">\n";
    echo"<tr><th>PVM</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $paivia=0;
    while($row = sqlsrv_fetch_array($result_scan_daily))
        {
            $pvm=$row["pvm"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
            echo"<td style=\"text-align:center;\">$pvm</td>";
            echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
            $sum_niteet=$sum_niteet+$niteita;
            $sum_sivut=$sum_sivut+$sivuja;
            $paivia++;
    }
    $avg_niteet=round($sum_niteet/$paivia);
    $avg_sivut=round($sum_sivut/$paivia);
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"<tr style=\"font-weight:normal;\"><td style=\"text-align:right;\">Keskiarvo:</td><td style=\"text-align:right;\">$avg_niteet</td><td style=\"text-align:right;\">$avg_sivut</td></tr>";
    echo"</table>";
    
        $query_scan_monthly = "
          SELECT year(scandate), month(scandate) as kk,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null
  group by year(scandate),month(scandate)
  order by month(scandate) desc;";
    $result_scan_monthly = sqlsrv_query($dbhandle, $query_scan_monthly);
    if( $result_scan_monthly === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table  width=\"250px\" style=\"margin-top:40px;\">\n";
    echo"<tr><th>KK</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = sqlsrv_fetch_array($result_scan_monthly))
        {
            $kk=$row["kk"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td style=\"text-align:center;\">$kk</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"</table>";
    echo"</div>\n";
## docworks export ##################################################################################################
    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_exportdate)=convert(date,GETDATE())";
    $result= sqlsrv_query($dbhandle, $query);
    $row=sqlsrv_fetch_array($result);
    $export_sivuja=$row["sivuja"];
    echo "<div style=\"float:left; margin-right:30px;\">";
        echo"<h3 style=\"text-align:center;\">$export_sivuja<br>DW exportoitu</h3>\n";
    $query_dw_daily_exports = "
    SELECT convert(varchar(10),dw_exportdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_exportdate)>convert(date,GETDATE()-7)
  group by convert(varchar(10),dw_exportdate)
  order by convert(varchar(10),dw_exportdate) desc;";
    $result_dw_daily_exports = sqlsrv_query($dbhandle, $query_dw_daily_exports);
    if( $result_dw_daily_exports === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table width=\"250px\">\n";
    echo"<tr><th>PVM</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = sqlsrv_fetch_array($result_dw_daily_exports))
        {
            $pvm=$row["pvm"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td style=\"text-align:center;\">$pvm</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    $avg_niteet=round($sum_niteet/7);
    $avg_sivut=round($sum_sivut/7);
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"<tr style=\"font-weight:normal;\"><td style=\"text-align:right;\">Keskiarvo:</td><td style=\"text-align:right;\">$avg_niteet</td><td style=\"text-align:right;\">$avg_sivut</td></tr>";
    
    echo"</table>";
    
        $query_dw_monthly_exports = "
      SELECT month(dw_exportdate) as kk,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is not null
  group by month(dw_exportdate)
  order by month(dw_exportdate) desc;";
    $result_dw_monthly_exports = sqlsrv_query($dbhandle, $query_dw_monthly_exports);
    if( $result_dw_monthly_exports === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table width=\"250px\" style=\"margin-top:40px;\">\n";
    echo"<tr><th>KK</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = sqlsrv_fetch_array($result_dw_monthly_exports))
        {
            $kk=$row["kk"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td style=\"text-align:center;\">$kk</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"</table>";
    echo"</div>\n";
## Tarkastetut sivut ##################################################################################################################################################################
$query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_statusdate)=convert(date,GETDATE()) and (depositbrowser_status='ACCEPTED' or depositbrowser_status='REJECTED')";
    $result= sqlsrv_query($dbhandle, $query);
    $row=sqlsrv_fetch_array($result);
    $accepted_sivuja=$row["sivuja"];
    echo "<div style=\"float:left; margin-right:30px;\">";
    echo"<h3 style=\"text-align:center;\">$accepted_sivuja<br>tarkastettu</h3>\n";
    $query_depositbrowser_daily = "
      SELECT top 7 convert(varchar(10),depositbrowser_statusdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_statusdate)>convert(date,GETDATE()-14) and depositbrowser_status is not null and datepart(dw,depositbrowser_statusdate) in (2,3,4,5,6)
  group by convert(varchar(10),depositbrowser_statusdate)
  order by convert(varchar(10),depositbrowser_statusdate) desc;";
    $result_depositbrowser_daily = sqlsrv_query($dbhandle, $query_depositbrowser_daily);
    if( $result_depositbrowser_daily === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table width=\"250px\">\n";
    echo"<tr><th>PVM</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $paivia=0;
    while($row = sqlsrv_fetch_array($result_depositbrowser_daily))
        {
            $pvm=$row["pvm"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
            echo"<td style=\"text-align:center;\">$pvm</td>";
            echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
            $sum_niteet=$sum_niteet+$niteita;
            $sum_sivut=$sum_sivut+$sivuja;
            $paivia++;
    }
    $avg_niteet=round($sum_niteet/$paivia);
    $avg_sivut=round($sum_sivut/$paivia);
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"<tr style=\"font-weight:normal;\"><td style=\"text-align:right;\">Keskiarvo:</td><td style=\"text-align:right;\">$avg_niteet</td><td style=\"text-align:right;\">$avg_sivut</td></tr>";
    echo"</table>";
    
        $query_depositbrowser_monthly = "
          SELECT month(depositbrowser_statusdate) as kk,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where depositbrowser_statusdate is not null and depositbrowser_status is not null
  group by month(depositbrowser_statusdate)
  order by month(depositbrowser_statusdate) desc;";
    $result_depositbrowser_monthly = sqlsrv_query($dbhandle, $query_depositbrowser_monthly);
    if( $result_depositbrowser_monthly === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table  width=\"250px\" style=\"margin-top:40px;\">\n";
    echo"<tr><th>KK</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = sqlsrv_fetch_array($result_depositbrowser_monthly))
        {
            $kk=$row["kk"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td style=\"text-align:center;\">$kk</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"</table>";
    echo"</div>\n";
########################################################################################################################################################
    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,digi_importdate)=convert(date,GETDATE())";
    $result= sqlsrv_query($dbhandle, $query);
    $row=sqlsrv_fetch_array($result);
    $importoitu_sivuja=$row["sivuja"];
    echo "<div style=\"float:left; margin-right:30px;\">";
        echo"<h3 style=\"text-align:center;\">$importoitu_sivuja<br>digi-import</h3>\n";
    $query_digi_daily_imports = "
    SELECT convert(varchar(10),digi_importdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,digi_importdate)>convert(date,GETDATE()-7)
  group by convert(varchar(10),digi_importdate)
  order by convert(varchar(10),digi_importdate) desc;";
    $result_digi_daily_imports = sqlsrv_query($dbhandle, $query_digi_daily_imports);
    if( $result_digi_daily_imports === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table width=\"250px\">\n";
    echo"<tr><th>PVM</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $paivia=0;
    while($row = sqlsrv_fetch_array($result_digi_daily_imports))
        {
            $pvm=$row["pvm"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td style=\"text-align:center;\">$pvm</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
        $paivia++;
    }
    $avg_niteet=round($sum_niteet/$paivia);
    $avg_sivut=round($sum_sivut/$paivia);
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"<tr style=\"font-weight:normal;\"><td style=\"text-align:right;\">Keskiarvo:</td><td style=\"text-align:right;\">$avg_niteet</td><td style=\"text-align:right;\">$avg_sivut</td></tr>";
    
    echo"</table>";
    
        $query_digi_monthly_imports = "
      SELECT month(digi_importdate) as kk,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where digi_importdate is not null and year(digi_importdate)=year(getdate())
  group by month(digi_importdate)
  order by month(digi_importdate) desc;";
    $result_digi_monthly_imports = sqlsrv_query($dbhandle, $query_digi_monthly_imports);
    if( $result_digi_monthly_imports === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table width=\"250px\" style=\"margin-top:40px;\">\n";
    echo"<tr><th>KK</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = sqlsrv_fetch_array($result_digi_monthly_imports))
        {
            $kk=$row["kk"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td style=\"text-align:center;\">$kk</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"</table>";
    echo"</div>\n";
    
## MF-tulostus ###############################################################################################################################################
$query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_mfdate)=convert(date,GETDATE())";
       $result= sqlsrv_query($dbhandle, $query);
    $row=sqlsrv_fetch_array($result);
    $filmattu_sivuja=$row["sivuja"];
    echo "<div style=\"float:left; margin-right:30px;\">";
    echo"<h3 style=\"text-align:center;\">$filmattu_sivuja<br>tulostettu</h3>\n";
    $query_mf_daily = "
      SELECT top 7 convert(varchar(10),depositbrowser_mfdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_mfdate)>convert(date,GETDATE()-14) and depositbrowser_status is not null and datepart(dw,depositbrowser_mfdate) in (2,3,4,5,6)
  group by convert(varchar(10),depositbrowser_mfdate)
  order by convert(varchar(10),depositbrowser_mfdate) desc;";
    $result_mf_daily = sqlsrv_query($dbhandle, $query_mf_daily);
    if( $result_mf_daily === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table width=\"250px\">\n";
    echo"<tr><th>PVM</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $paivia=0;
    while($row = sqlsrv_fetch_array($result_mf_daily))
        {
            $pvm=$row["pvm"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
            echo"<td style=\"text-align:center;\">$pvm</td>";
            echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
            $sum_niteet=$sum_niteet+$niteita;
            $sum_sivut=$sum_sivut+$sivuja;
            $paivia++;
    }
    $avg_niteet=round($sum_niteet/$paivia);
    $avg_sivut=round($sum_sivut/$paivia);
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"<tr style=\"font-weight:normal;\"><td style=\"text-align:right;\">Keskiarvo:</td><td style=\"text-align:right;\">$avg_niteet</td><td style=\"text-align:right;\">$avg_sivut</td></tr>";
    echo"</table>";
    
        $query_mf_monthly = "
          SELECT month(depositbrowser_mfdate) as kk,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where depositbrowser_mfdate is not null and depositbrowser_status is not null
  group by month(depositbrowser_mfdate)
  order by month(depositbrowser_mfdate) desc;";
    $result_mf_monthly = sqlsrv_query($dbhandle, $query_mf_monthly);
    if( $result_mf_monthly === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table  width=\"250px\" style=\"margin-top:40px;\">\n";
    echo"<tr><th>KK</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = sqlsrv_fetch_array($result_mf_monthly))
        {
            $kk=$row["kk"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td style=\"text-align:center;\">$kk</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"</table>";
    echo"</div>\n";

## MF-tulostus ###############################################################################################################################################
    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_deletedate)=convert(date,GETDATE())";
    $result= sqlsrv_query($dbhandle, $query);
    $row=sqlsrv_fetch_array($result);
    $deletoitu_sivuja=$row["sivuja"];
    echo "<div style=\"float:left; margin-right:30px;\">";
    echo"<h3 style=\"text-align:center;\">$deletoitu_sivuja<br>poistettu poolista</h3>\n";
    $query_delete_daily = "
      SELECT top 7 convert(varchar(10),dw_deletedate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_deletedate)>convert(date,GETDATE()-14) and depositbrowser_status is not null and datepart(dw,dw_deletedate) in (2,3,4,5,6)
  group by convert(varchar(10),dw_deletedate)
  order by convert(varchar(10),dw_deletedate) desc;";
    $result_delete_daily = sqlsrv_query($dbhandle, $query_delete_daily);
    if( $result_delete_daily === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table width=\"250px\">\n";
    echo"<tr><th>PVM</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $paivia=0;
    while($row = sqlsrv_fetch_array($result_delete_daily))
        {
            $pvm=$row["pvm"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
            echo"<td style=\"text-align:center;\">$pvm</td>";
            echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
            $sum_niteet=$sum_niteet+$niteita;
            $sum_sivut=$sum_sivut+$sivuja;
            $paivia++;
    }
    $avg_niteet=round($sum_niteet/$paivia);
    $avg_sivut=round($sum_sivut/$paivia);
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"<tr style=\"font-weight:normal;\"><td style=\"text-align:right;\">Keskiarvo:</td><td style=\"text-align:right;\">$avg_niteet</td><td style=\"text-align:right;\">$avg_sivut</td></tr>";
    echo"</table>";
    
        $query_delete_monthly = "
          SELECT month(dw_deletedate) as kk,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where dw_deletedate is not null and depositbrowser_status is not null
  group by month(dw_deletedate)
  order by month(dw_deletedate) desc;";
    $result_delete_monthly = sqlsrv_query($dbhandle, $query_delete_monthly);
    if( $result_delete_monthly === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    echo "<table  width=\"250px\" style=\"margin-top:40px;\">\n";
    echo"<tr><th>KK</th><th>Niteit�</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = sqlsrv_fetch_array($result_delete_monthly))
        {
            $kk=$row["kk"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td style=\"text-align:center;\">$kk</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteens�:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"</table>";
    echo"</div>\n";
########################################################################################################################################################
    
sqlsrv_close($dbhandle);
?>
</body>
</html>