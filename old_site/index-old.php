<?php
include("sessio.php");
 

?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <meta http-equiv="refresh" content="180" >
    <title>Sanomalehtiprosessin tila</title>
    <link rel="stylesheet" type="text/css" href="reset.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/jquery.cookie.js"></script>
    <script type="text/javascript" src="/sorttable.js"></script> 
 


</head>
<body>


<?php
//$conn_diona = oci_connect('diona', 'diona', 'vega2/diona','AL32UTF8');
//$select_skannaamatta="select nimi,max(pvm)+1 as alkaen, numeroita-count(*) as puuttuu
//from saapumisvalvonta v
//left outer join sarjajulkaisu_nide sn on (v.issn=sn.issn and to_char(pvm,'YYYY')='2017')
//where trim(nimi) not in ('DEMOKRAATT','ETEL�-SUOMEN SANOMAT','ILTA-SANOMAT TV-LIITE 6kpl','IT�-H�ME','KALEVA','L�NSI-SAVO','MAASEUDUN TULEVAISUUS','YL�-SATAKUNTA')
//and (to_char(pvm,'YYYY')='2017' or pvm is null)
//group by nimi,numeroita
//having numeroita-count(*)>1
//order by max(pvm);";
//$stid_skannaamatta = oci_parse($conn_diona, $select_skannaamatta);
//$res_skannaamatta=oci_execute($stid_skannaamatta);
//
//        if (!$res_rejected) {
//                echo "error:".oci_error()."--";
//		    $e = oci_error();
//		    print_r($e);
//		    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
//                exit(9);
//
//        }
//	
//# echo "query ok\n";


function status_list_table ($query, $table_title,$link) {

    $result_dw = mssql_query($query);
    if( $result_dw === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    echo "<table><caption> $table_title </caption>\n";
    echo"<tr><th>Nimeke</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_dw))
        {
            $nimeke=$row["title"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
	    switch ($link) {
		case "mikrofilmi":
	    		echo "<td><a onClick=\"MyWindow=window.open('mikrofilmi.php?ryhma=".urlencode($nimeke)."','MyWindow','width=600,height=1100'); return false;\" style=\"color:black; text-decoration:none;\" href=\"mikrofilmi.php?ryhma=".urlencode($nimeke)."\">".$nimeke."</a></td>";
			break;
		case "niteet":
			echo "<td><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($nimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\" href=\"niteet-popup.php?ryhma=".urlencode($nimeke)."\">".$nimeke."</a></td>";
			break;
		case "digi":
		       echo"<td><a href=\"http://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=CALENDAR&year=".$vuosi."\" target=\"_blank\" style=\"color:black; text-decoration:none;\">".$nimeke."</a></td>";
		       break;
		}


        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result_dw);    
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>\n";


}

function stat_table ($query,$table_title,$col_title) {

    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }

    echo "<table width=\"250px\">\n";

    echo"<tr><th> $col_title </th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $paivia=0;
    while($row = mssql_fetch_array($result))
        {
            $pvm=$row["pvm"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td style=\"text-align:center;\">$pvm</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
        $paivia++;
    }
    $avg_niteet=round($sum_niteet/$paivia);
    $avg_sivut=round($sum_sivut/$paivia);
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"<tr style=\"font-weight:normal;\"><td style=\"text-align:right;\">Keskiarvo:</td><td style=\"text-align:right;\">$avg_niteet</td><td style=\"text-align:right;\">$avg_sivut</td></tr>";
    
    echo"</table>";

}

echo"<div style=\"float:left; margin-right:20px;\">\n";
echo"<h3 style=\"text-align:center;\">Sivumäärät tänään</h3>";
    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_exportdate)=convert(date,GETDATE()) and dw_deletedate is null";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $export_sivuja=$row["sivuja"];
    mssql_free_result($result);    
    
    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_statusdate)=convert(date,GETDATE()) and (depositbrowser_status='ACCEPTED' or depositbrowser_status='REJECTED') and dw_deletedate is null";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $accepted_sivuja=$row["sivuja"];
    mssql_free_result($result);

    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_statusdate)=convert(date,GETDATE()) and depositbrowser_status='REJECTED' and dw_deletedate is null";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $rejected_sivuja=$row["sivuja"];
    mssql_free_result($result);

    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_mfdate)=convert(date,GETDATE())";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $filmattu_sivuja=$row["sivuja"];
    mssql_free_result($result);

    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,digi_importdate)=convert(date,GETDATE()) and dw_deletedate is null";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $importoitu_sivuja=$row["sivuja"];
    mssql_free_result($result);

    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_deletedate)=convert(date,GETDATE())";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $deletoitu_sivuja=$row["sivuja"];
    mssql_free_result($result);
    
    echo"<table align=\"center\">\n";
    echo"<tr><th>Exportoitu&nbsp;</th><th>Tarkastettu&nbsp;</th><th> Tulostettu&nbsp;</th><th>Digiin importoitu&nbsp;</th><th>poistettu DW</th>";
    echo"<tr style=\"text-align:center;\"><td>$export_sivuja</td><td>$accepted_sivuja</td><td>$filmattu_sivuja</td><td>$importoitu_sivuja</td><td>$deletoitu_sivuja</td>";
    echo"</tr></table>\n";
echo"</div>\n";
echo "<div style=\"float:left; margin-right:20px;\">";
    echo"<h1 style=\"text-align:center;\">Sanomalehtiprosessi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=\"niteet.php\">Nidekohtaiset tiedot</a></h1>\n";
echo"</div>\n";
echo"<div style=\"float:none; clear:both;\">";
##############################################
    $query_rescans="SELECT P_ARTID as docid,p_filter2 as title, p_comment as comment FROM [dw_pool].[dbo].[Main] where p_filter3 in ('sl_karttuva','sl_takautuva') and P_JOBNAME='Re-Scan'";
    
    $result_rescans =mssql_query($query_rescans);
    if( $result_rescans === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    $rows=mssql_num_rows($result_rescans);
    if ($rows>0) {
        echo "<h2 class=\"help\">Uudelleen skannattavat niteet</h2><table>\n";
        echo"<tr><th>dwid</th><th>Työ</th><th>Huomautus</th></tr>";
         while($row =mssql_fetch_array($result_rescans)) {
        $title=$row["title"];
        $docworksid=$row["docid"];
        $comment=utf8_decode($row["comment"]);
        $comment = str_replace("\\u000d\\u000a", "", $comment);
        echo"<tr>";
        echo"<td width=\"100px\">$docworksid</td>";
        echo"<td>$title &nbsp;</td>\n";
        echo"<td>$comment</td></tr>\n";
       }
       echo"</table></div>\n";
    }
    mssql_free_result($result_rescans);    

################################################    
    ## and depositbrowser_receivedate is null and dw_deletedate is null
    $query_dw = "
    select nimi,alkaen,v.issn, replace(v.supag_tunniste,'_','+') as tunniste,min(issuedate),isnull(convert(varchar(24),max(sn.issuedate)),'2018') as viimeisin, numeroita-count(*) as puuttuu
from dw_nlf_custom.dbo.saapumisvalvonta v
left outer join dw_nlf_custom.dbo.sl_status sn on (replace(v.supag_tunniste,'_','+')=sn.title and year(issuedate)='2018' and dw_importdate is not null)
where comellus is null
and (year(issuedate)='2018' or issuedate is null)
and vuosikerta='2018'
and (valmis!='1' or valmis is null)
and piilota is null
group by nimi,alkaen,v.issn,replace(v.supag_tunniste,'_','+'),numeroita
having  (MAX(issuedate)<'2018-12-31' or MAX(issuedate) is null)
and numeroita-count(*)>0
-- order by max(issuedate) desc,alkaen asc,nimi;
   order by numeroita-count(*) desc,alkaen asc,nimi;";
    $result_dw = mssql_query($query_dw);
    if( $result_dw === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    echo "<div style=\"float:left; margin-right:20px;\"><table><caption>Skannaamatta 2018</caption>\n";
    echo"<tr><th></th><th>Nimeke</th><th>Niteitä</th><th>Viimeisin</th></tr>";
    $sum_niteet=0;
    $laskuri=1;
    while($row = mssql_fetch_array($result_dw))
        {
            $nimeke=$row["nimi"];
            $alkaen=$row["alkaen"];
            $viimeisin=$row["viimeisin"];
            $niteita=$row["puuttuu"];
            $issn=$row["issn"];
            $tunniste=$row["tunniste"];
            $vuosi='2017';
            if ($viimeisin=="2017" and $alkaen!="") {
               $viimeisin=$alkaen;
            }
            if ($tunniste=="") $tunniste=$nimeke;

            echo"<tr>";
        echo"<td style=\"text-align:right;\">&nbsp; $laskuri &nbsp;</td><td><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($tunniste)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\" href=\"niteet-popup.php?ryhma=".urlencode($tunniste)."\">".$tunniste."</a></td>";
        echo"<td style=\"text-align:left;\">&nbsp;$niteita</td><td style=\"text-align:right;\">$viimeisin</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $laskuri++;
    }
    mssql_free_result($result_dw);    
    echo"<tr style=\"font-weight:bold;\"><td></td><td>Yhteensä:</td><td style=\"text-align:left;\">$sum_niteet</td><td></td></tr>";
    echo"</table>\n\n";


################################################    
    ## and depositbrowser_receivedate is null and dw_deletedate is null
    $query_dw = "
    select nimi,v.issn, replace(v.supag_tunniste,'_','+') as tunniste,numeroita as puuttuu
from dw_nlf_custom.dbo.saapumisvalvonta v
where comellus is not null
and vuosikerta='2018'
and (valmis!='1' or valmis is null)
and piilota is null
group by nimi,alkaen,v.issn,replace(v.supag_tunniste,'_','+'),numeroita
order by nimi;";
    $result_dw =mssql_query($query_dw);
    if( $result_dw === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    echo "<table><caption>Sähköinen vastaanotto</caption>\n";
    echo"<tr><th></th><th>Nimeke</th><th>Niteitä</th></tr>";
    $sum_niteet=0;
    $laskuri=1;
    while($row = mssql_fetch_array($result_dw))
        {
            $nimeke=$row["nimi"];
            $niteita=$row["puuttuu"];
            $issn=$row["issn"];
            $tunniste=$row["tunniste"];
            if ($tunniste=="") $tunniste=$nimeke;

            echo"<tr>";
        echo"<td style=\"text-align:right;\">$laskuri &nbsp;</td><td>$tunniste</td>";
        echo"<td style=\"text-align:left;\">&nbsp;$niteita</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $laskuri++;
    }
    mssql_free_result($result_dw);    

    echo"<tr style=\"font-weight:bold;\"><td></td><td>Yhteensä:</td><td style=\"text-align:left;\">$sum_niteet</td></tr>";
    echo"</table>\n\n";



    
//################################################
//    ## and depositbrowser_receivedate is null and dw_deletedate is null
//    $query_valmiit = "
//    select nimi,min(issuedate),convert(varchar(24),max(sn.issuedate)) as alkaen, numeroita-count(*) as puuttuu
//from dw_nlf_custom.dbo.saapumisvalvonta v
//left outer join dw_nlf_custom.dbo.sl_status sn on (replace(v.supag_tunniste,'_','+')=sn.title and year(issuedate)='2017')
//where rtrim(nimi) not in ('DEMOKRAATTI','ETEL�-SUOMEN SANOMAT','ILTA-SANOMAT TV-LIITE 6kpl','IT�-H�ME','KALEVA','L�NSI-SAVO','MAASEUDUN TULEVAISUUS','YL�-SATAKUNTA')
//and (year(issuedate)='2017' or issuedate is null)
//and (valmis='1')
//group by nimi,numeroita
//--having MAX(issuedate)>'2017-12-30' and numeroita-count(*)>0
//order by nimi";


echo "</div>\n\n";
    
################################################  
    ## and depositbrowser_receivedate is null and dw_deletedate is null

    $query_dw = "
    SELECT title,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_importdate is not null and dw_exportdate is null and dw_replaced_by is null
     group by title
     order by title";
    
    echo "<div style=\"float:left; margin-right:20px;\">";	
    status_list_table($query_dw,"DW kesken","niteet");

    ###############################################    
    $query_dw = "
    SELECT title,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_importdate is not null
     and dw_exportdate is not null
     and depositbrowser_receivedate is null
     and title in (select distinct title from [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is not null)
     group by title
     order by title";
    status_list_table($query_dw,"DW exportoituja","niteet");

    
    
    $query_noudettavat = "
    SELECT title,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is null and dw_deletedate is null
     and title not in (select distinct title from [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is null)
     group by title
     order by title";
    status_list_table($query_noudettavat,"Noudettavat","niteet");


    #############################################
    $query_tarkastettavat = "
    SELECT title,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and (depositbrowser_status!='ACCEPTED' or depositbrowser_status is null) and dw_deletedate is null
     group by title
     order by title";
    status_list_table($query_tarkastettavat,"Tarkastettavat","niteet");

    #############################################
    $query_hylatyt = "
    SELECT title,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and (depositbrowser_status='REJECTED') and dw_deletedate is null
     group by title
     order by title";
    status_list_table($query_hylatyt,"Hylätyt","");


    #############################################
    $query_hyvaksytyt = "
    SELECT title,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and (depositbrowser_status='ACCEPTED') and depositbrowser_mfdate is null
     group by title
     having sum(pages)<1000
     order by title";
    status_list_table($query_hyvaksytyt,"Hyväksytyt","niteet");

    echo"</div>\n";
    
     # kuvattavat ##########################
    echo "<div style=\"float:left; margin-right:20px;\">\n";
 
    $query_kuvattavat = "
    SELECT title,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and depositbrowser_mfdate is null
     and title not in (select distinct title from [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is null or depositbrowser_receivedate is null or depositbrowser_status is null or depositbrowser_status='REJECTED')
     group by title
     having sum(pages)>999
     order by sum(pages) desc";
    status_list_table($query_kuvattavat,"Tulostettavat","mikrofilmi");


 /* 
    $query_kuvatut = "
    SELECT title,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and depositbrowser_mfdate is not null
     group by title
     order by title";
    status_list_table($query_kuvatut,"Kuvatut","");

     */
   
	 echo"</div>\n";

###################################################################################################

    echo "<div style=\"float:left; margin-right:20px;\">\n";
    $query_importoitavat = "
    SELECT title,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is null and dw_deletedate is null and datediff(day,depositbrowser_statusdate,GETDATE())>=1
     group by title
     order by title";
    status_list_table($query_importoitavat,"Importoitavat","niteet");


  
    $query_digissa = "
    SELECT title,issn,COUNT(*) as niteita, sum(pages) as sivuja,max(year(issuedate)) as vuosi
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is not null and year(issuedate)='2018'
     group by title,issn
     order by title";
    status_list_table($query_digissa,"Digissä 2018","digi");


    $query_digissa = "
    SELECT year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is not null
     group by year(issuedate)
     order by year(issuedate) desc";
    $result_digissa = mssql_query($query_digissa);
    if( $result_digissa === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_last_message(), true));
    }
    echo "<table><caption>Vuosittain</caption>\n";
    echo"<tr><th>Vuosi</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_digissa))
        {
            $niteita=$row["niteita"];
            $vuosi=$row["vuosi"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td>$vuosi</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result_digissa);    
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table></div>\n";


    

################################################################################################    
    echo"<div style=\"float:none; clear:both;padding-top:100px;text-align:center;\"><h2>Tilastot</h2></div>\n";
######################################################################################################
    echo "<div style=\"float:left; margin-right:30px;\">";
    echo"<h3 style=\"text-align:center;\">skannaus</h3>\n";
    $query_daily_scans = "
    SELECT convert(varchar(10),scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,scandate)>convert(date,GETDATE()-7)
  group by convert(varchar(10),scandate)
  order by convert(varchar(10),scandate) desc;";
   stat_table($query_daily_scans,"Skannaus","PVM");


    
        $query_monthly_scans = "
      SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)=year(getdate())
  group by month(scandate)
  order by month(scandate) desc;";
   stat_table($query_monthly_scans,"Skannaus","KK");

    echo "<br>\n";
    echo"<h3 style=\"text-align:center;\">skannaus 2018</h3>\n";
        $query_monthly_scans = "
      SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2018'
  group by month(scandate)
  order by month(scandate) desc;";
   stat_table($query_monthly_scans,"Skannaus 2018","KK");


    echo"</div>\n";
####################################################################################################################################################################
   echo "<div style=\"float:left; margin-right:30px;\">";
        echo"<h3 style=\"text-align:center;\">docWorks</h3>\n";
    $query_dw_daily_exports = "
    SELECT convert(varchar(10),dw_exportdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_exportdate)>convert(date,GETDATE()-7)
  group by convert(varchar(10),dw_exportdate)
  order by convert(varchar(10),dw_exportdate) desc;";
   stat_table($query_dw_daily_exports,"docworks","PVM");

    
        $query_dw_monthly_exports = "
      SELECT month(dw_exportdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is not null and year(dw_exportdate)=year(getdate())
  group by month(dw_exportdate)
  order by month(dw_exportdate) desc;";
   stat_table($query_dw_monthly_exports,"docworks","KK");

    echo"</div>\n";
###################################################################################################################################################################
    echo "<div style=\"float:left; margin-right:30px;\">";
    echo"<h3 style=\"text-align:center;\">tarkastettu</h3>\n";
    $query_depositbrowser_daily = "
      SELECT top 7 convert(varchar(10),depositbrowser_statusdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_statusdate)>convert(date,GETDATE()-14) and depositbrowser_status is not null and datepart(dw,depositbrowser_statusdate) in (2,3,4,5,6)
  group by convert(varchar(10),depositbrowser_statusdate)
  order by convert(varchar(10),depositbrowser_statusdate) desc;";
   stat_table($query_depositbrowser_daily,"tarkastettu","PVM");


    
        $query_depositbrowser_monthly = "
          SELECT month(depositbrowser_statusdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where depositbrowser_statusdate is not null and depositbrowser_status is not null and year(depositbrowser_statusdate)=year(getdate())
  group by month(depositbrowser_statusdate)
  order by month(depositbrowser_statusdate) desc;";
   stat_table($query_depositbrowser_monthly,"tarkastettu","KK");

    echo"</div>\n";
###################################################################################################################################################################
    echo "<div style=\"float:left; margin-right:30px;\">";
    echo"<h3 style=\"text-align:center;\">tulostettu</h3>\n";
    $query_mf_daily = "
      SELECT top 7 convert(varchar(10),depositbrowser_mfdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_mfdate)>convert(date,GETDATE()-14) and depositbrowser_status is not null
  group by convert(varchar(10),depositbrowser_mfdate)
  order by convert(varchar(10),depositbrowser_mfdate) desc;";
   stat_table($query_mf_daily,"tarkastettu","PVM");

    
        $query_mf_monthly = "
          SELECT month(depositbrowser_mfdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where depositbrowser_mfdate is not null and depositbrowser_status is not null and year(depositbrowser_mfdate)=year(getdate())
  group by month(depositbrowser_mfdate)
  order by month(depositbrowser_mfdate) desc;";
   stat_table($query_mf_monthly,"kuvattu","KK");


    echo"</div>\n";
########################################################################################################################################################
    echo "<div style=\"float:left; margin-right:30px;\">";
        echo"<h3 style=\"text-align:center;\">digi-import</h3>\n";
    $query_digi_daily_imports = "
    SELECT convert(varchar(10),digi_importdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,digi_importdate)>convert(date,GETDATE()-7)
  group by convert(varchar(10),digi_importdate)
  order by convert(varchar(10),digi_importdate) desc;";
   stat_table($query_digi_daily_imports,"importoitu","PVM");

    
        $query_digi_monthly_imports = "
      SELECT month(digi_importdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where digi_importdate is not null and year(digi_importdate)=year(getdate())
  group by month(digi_importdate)
  order by month(digi_importdate) desc;";
   stat_table($query_digi_monthly_imports,"importoitu","KK");


    echo"</div>\n";

########################################################################################################################################################
    echo "<div style=\"float:left; margin-right:30px;\">";
            echo"<h3 style=\"text-align:center;\">2018 lehdet</h3>\n";
        $query_digissa = "
    SELECT year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is not null and year(issuedate)='2018'
     group by year(issuedate)
     order by year(issuedate) desc";
    $result_digissa = mssql_query($query_digissa);
    if( $result_digissa === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    echo "<table>\n";
    echo"<tr><th>Prosessi</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_digissa))
        {
            $niteita=$row["niteita"];
            $vuosi=$row["vuosi"];
            $sivuja=$row["sivuja"];
    }
        echo"<tr>";
        echo"<td>SUPAG</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
        $sivuja=83708;
        $niteita=2351;
        echo"<tr>";
        echo"<td>Comellus</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
        
        
        
           echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    $sivuennuste=900000;
    $valmistumisprosentti=round($sum_sivut/$sivuennuste*100,1);
    echo"<tr style=\"font-weight:normal;\"><td>Ennakkoarvio:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$sivuennuste</td></tr>";
    echo"<tr style=\"font-weight:bold;\"><td>Valmistunut:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$valmistumisprosentti %</td></tr>";
    mssql_free_result($result_digissa);    
    

    echo"</table>\n<br>";
    
    
        echo"<h3 style=\"text-align:center;\">2017 lehdet</h3>\n";
        $query_digissa = "
    SELECT year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is not null and year(issuedate)='2017'
     group by year(issuedate)
     order by year(issuedate) desc";
    $result_digissa = mssql_query($query_digissa);
    if( $result_digissa === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    echo "<table>\n";
    echo"<tr><th>Prosessi</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_digissa))
        {
            $niteita=$row["niteita"];
            $vuosi=$row["vuosi"];
            $sivuja=$row["sivuja"];
    }
        echo"<tr>";
        echo"<td>SUPAG</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
        $sivuja=129750;
        $niteita=2817;
        echo"<tr>";
        echo"<td>MF-skannaus</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
        $sivuja=67914;
        $niteita=1926;
        echo"<tr>";
        echo"<td>Comellus</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
        
        
        
           echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    $sivuennuste=950000;
    $valmistumisprosentti=round($sum_sivut/$sivuennuste*100,1);
    echo"<tr style=\"font-weight:normal;\"><td>Ennakkoarvio:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$sivuennuste</td></tr>";
    echo"<tr style=\"font-weight:bold;\"><td>Valmistunut:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$valmistumisprosentti %</td></tr>";
    
    mssql_free_result($result_digissa);    

    echo"</table></div>\n";
    #######################################################################################################################################################
    echo "<div style=\"float:left; margin-right:30px;\">";
        echo"<h3 style=\"text-align:center;\">2017 niteet</h3>\n";
    echo "<table>\n";
    echo"<tr><th></th><th>Niteitä</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
        $niteita=24091;
        echo"<tr>";
        echo"<td>Digissä</td>";
        echo"<td style=\"text-align:right;\">$niteita</td></tr>\n";
        echo"<tr>";
        echo"<td>Saapunut taloon</td>";
        $niteita_tullut=31259;
        echo"<td style=\"text-align:right;\">$niteita_tullut</td></tr>\n";
    $valmistumisprosentti=round($niteita/$niteita_tullut*100,1);
    echo"<tr style=\"font-weight:bold;\"><td>Valmistunut:</td><td style=\"text-align:right;\">&nbsp;$valmistumisprosentti %</td></tr>";
    mssql_free_result($result_digissa);    
    

    echo"</table></div>\n";
    
mssql_close($db);
?>
<script type="text/javascript" src="https://jira.kansalliskirjasto.fi/s/c5b08edcdbae63cb8519ad222a24c65d-T/-8btokn/73013/b6b48b2829824b869586ac216d119363/2.0.23/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=a72b35a7"></script>


</body>
</html>
