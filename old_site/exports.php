<?php
include("sessio.php");
 

?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <title>Exports seuranta</title>
    <link rel="stylesheet" type="text/css" href="reset.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/jquery.cookie.js"></script>  
    <script type="text/javascript">
    $(function()
    {
        var piilota = $.cookie("piilotaKorjatut");
        if (piilota=="kylla") {
            $('.korjattu').hide();
        } else if (piilota=="ei") {
            $('.korjattu').show();
        } else {
            $('.korjattu').show();
        }
        
        $('#piilotaNappi').click(function(e) {
            $.cookie('piilotaKorjatut', 'kylla');
            $('.korjattu').hide();
            }
        );

        $('#naytaNappi').click(function(e) {
            $.cookie('piilotaKorjatut', 'ei');
            $('.korjattu').show();
        });
        $('#vainhelpNappi').click(function(e) {
            $.cookie('vainHelp', 'kylla');
            $('.korjattu').hide();
            $('.odd').hide();
            $('.even').hide();
        });
            $('#kaikkiNappi').click(function(e) {
            $.cookie('vainHelp', 'ei');
            $.cookie('piilotaKorjatut', 'ei');
            $('.korjattu').show();
            $('.odd').show();
            $('.even').show();
        });
});

</script>
</head>
<body>

<?php
 /*   if (isset($_POST["ryhma"])) {
        $ryhma=$_POST["ryhma"];
    } elseif (isset($_GET["ryhma"])) {
        $ryhma=$_GET["ryhma"];
    }
    
     if (isset($_POST["sort"])) {
        $sort=$_POST["sort"];
    } elseif (isset($_GET["sort"])) {
        $sort=$_GET["sort"];
    } else {
        $sort="kotelo";
    }
    
    $ryhma=urldecode($ryhma);
    $query = "
    SELECT distinct(e.colltitle)
    FROM DW_WORKFLOW.dbo.MDCorrections t
    inner join DW_WORKFLOW.dbo.exports e on e.logunitid=t.logunitid and e.type='pdf' and e.replaced is null
    left outer join DW_WORKFLOW.dbo.MDCorrections s on t.logunitid=s.logunitid and (s.mdfield='245b' or s.mdfield is null) 
    left outer join DW_WORKFLOW.dbo.MDCorrections p on t.logunitid=p.logunitid and (p.mdfield='260b' or p.mdfield is null) 
    where t.mdfield='245a' 
    order by e.colltitle asc";
    $result = sqlsrv_query($dbhandle, $query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    
    echo "<div class=\"buttonx\">
    <input id=\"piilotaNappi\" type=\"button\" value=\"Piilota korjatut\" />
    <input id=\"naytaNappi\" type=\"button\" value=\"Nayta korjatut\" />
    <input id=\"vainhelpNappi\" type=\"button\" value=\"Nayta Help\" />
    <input id=\"kaikkiNappi\" type=\"button\" value=\"Nayta Kaikki\" />&nbsp;&nbsp;";
    echo "<a href=\"/metakorjaus/index.php\">Etusivu</a>";
    echo "<form action=\"exports.php\" method=\"post\">";
    echo"<select name=\"ryhma\">";
    echo"<option value=\"\">Ei viel� exportattu</option>";
    while($row = sqlsrv_fetch_array($result))
        {
            $niteenryhma=$row["colltitle"];
            $encodedniteenryhma=urlencode($niteenryhma);
            echo "<option value=\"$encodedniteenryhma\" ".($niteenryhma==$ryhma ? 'selected="selected"' : '').">$niteenryhma</option>";
    }
            echo "</select>";
            echo"<input type=\"submit\" value=\"P�ivit�\">";
            // echo"<div>J�rjestys:<input type=\"radio\" name=\"sort\" value=\"kotelo\" ".($sort=="kotelo" ? 'checked="checked"' : '').">kotelo - <input type=\"radio\" name=\"sort\" value=\"kustantaja\" ".($sort=="kustantaja" ? 'checked="checked"' : '').">kustantaja - <input type=\"radio\" name=\"sort\" value=\"nimeke\" ".($sort=="nimeke" ? 'checked="checked"' : '').">nimeke</div>";
           
            echo "</form>\n";
echo "</div><br><br><br>\n";
*/
echo "<h3 align=\"center\">Exporttaus</h3>";

$query2="SELECT m.colltitle as colltitle,
    c.corrected as title,s.docworksid,s.logunitid,m.nbn as nbn,
    convert(varchar,m.exported,120) as mets_exported,
    convert(varchar,a.exported,120) as marc_exported,
    convert(varchar,m.transfered,120) as mets_transfered,
    convert(varchar,a.transfered,120) as marc_transfered,
    convert(varchar,m.ingested,120) as mets_ingested,
    convert(varchar,a.ingested,120) as marc_ingested,
    convert(varchar,m.available,120) as mets_available,
    convert(varchar,a.available,120) as marc_available,
    convert(varchar,m.rejected,120) as mets_rejected,
    convert(varchar,a.rejected,120) as marc_rejected,
    convert(varchar,m.replaced,120) as mets_replaced,
    convert(varchar,a.replaced,120) as marc_replaced,
    m.url as mets_url,
    a.url as marc_url,
    convert(varchar,m.uningested,120) as mets_uningested,
    convert(varchar,a.uningested,120) as marc_uningested,
    m.comments as mets_comment,
    a.comments as marc_comment
    FROM DW_WORKFLOW.dbo.MDapprovals s
    left outer join DW_WORKFLOW.dbo.exports m on s.logunitid=m.logunitid and m.type='mets' and m.replaced is null and m.destination!='debug' and ((m.exported between '27-nov-2012' and '01-jan-2014') or m.exported is null)
    left outer join DW_WORKFLOW.dbo.exports a on s.logunitid=a.logunitid and a.type='marcxml-issue' and a.replaced is null and a.destination!='debug' and ((a.exported between '27-nov-2012' and '01-jan-2014') or a.exported is null)
    left outer join DW_WORKFLOW.dbo.mdcorrections c on s.logunitid=c.logunitid and c.mdfield='245a'
    where m.colltitle is not null ";
    // if ($ryhma!="kaikki") $query2.="where m.colltitle='$ryhma' "; elseif  ($ryhma=="") $query2.="where m.colltitle is null ";
    $query2.="order by colltitle,s.docworksid, s.logunitid ";

$query3="SELECT m.colltitle as colltitle,
    c.corrected as title,s.docworksid,s.logunitid,m.nbn as nbn,
    convert(varchar,m.exported,120) as mets_exported,
    convert(varchar,a.exported,120) as marc_exported,
    convert(varchar,m.transfered,120) as mets_transfered,
    convert(varchar,a.transfered,120) as marc_transfered,
    convert(varchar,m.ingested,120) as mets_ingested,
    convert(varchar,a.ingested,120) as marc_ingested,
    convert(varchar,m.available,120) as mets_available,
    convert(varchar,a.available,120) as marc_available,
    convert(varchar,m.rejected,120) as mets_rejected,
    convert(varchar,a.rejected,120) as marc_rejected,
    convert(varchar,m.replaced,120) as mets_replaced,
    convert(varchar,a.replaced,120) as marc_replaced,
    m.url as mets_url,
    a.url as marc_url,
    convert(varchar,m.uningested,120) as mets_uningested,
    convert(varchar,a.uningested,120) as marc_uningested,
    m.comments as mets_comment,
    a.comments as marc_comment
    FROM DW_WORKFLOW.dbo.MDapprovals s
    left outer join DW_WORKFLOW.dbo.exports m on s.logunitid=m.logunitid and m.type='mets' and m.replaced is null and m.destination!='debug' and ((m.exported between '27-nov-2012' and '01-jan-2014') or m.exported is null)
    left outer join DW_WORKFLOW.dbo.exports a on s.logunitid=a.logunitid and a.type='marcxml-issue' and a.replaced is null and a.destination!='debug' and ((a.exported between '27-nov-2012' and '01-jan-2014') or a.exported is null)
    left outer join DW_WORKFLOW.dbo.mdcorrections c on s.logunitid=c.logunitid and c.mdfield='245a'
    where m.colltitle is not null ";
    // if ($ryhma!="kaikki") $query2.="where m.colltitle='$ryhma' "; elseif  ($ryhma=="") $query2.="where m.colltitle is null ";
    $query3.="order by m.exported desc ";
//execute the SQL query and return records
$result = sqlsrv_query($dbhandle, $query3);
if( $result === false )
{
 echo "Error in statement preparation/execution.\n";
 die( print_r( sqlsrv_errors(), true));
}


echo "<table>\n";
$edellinenkokoelma="";
$i=0;
while($row = sqlsrv_fetch_array($result))
    {
      $colltitle=$row["colltitle"];
      $title=substr($row["title"],0,40)."....";
      $docworksid=$row["docworksid"];
      $logunitid=$row["logunitid"];
      $nbn=$row["nbn"];
      $mets_exported=$row["mets_exported"];
      $marc_exported=$row["marc_exported"];
      $mets_transfered=$row["mets_transfered"];
      $marc_transfered=$row["marc_transfered"];
      $mets_ingested=$row["mets_ingested"];
      $marc_ingested=$row["marc_ingested"];
      $mets_available=$row["mets_available"];
      $marc_available=$row["marc_available"];
      $mets_rejected=$row["mets_rejected"];
      $marc_rejected=$row["marc_rejected"];
      $mets_replaced=$row["mets_replaced"];
      $marc_replaced=$row["marc_replaced"];
      $mets_url=$row["mets_url"];
      $marc_url=$row["marc_url"];
      $mets_uningested=$row["mets_uningested"];
      $marc_uningested=$row["marc_uningested"];
      $mets_comment=$row["mets_comment"];
      $marc_comment=$row["marc_comment"];
      $mets_url=str_replace("http://doria.fi","http://www.doria.fi",$mets_url);
      if ($edellinenkokoelma!=$colltitle) {
            echo "<tr><th colspan=\"10\" align=\"left\">$colltitle</th></tr>\n";
            echo"<tr><th>no</th><th>dwid</th><th>logid</th><th>Nimeke</th><th>NBN</th><th>Tyyppi</th><th>exportattu</th><th>siirretty</th><th>importoitu</th><th>k�ytet�viss�</th><th>hyl�tty</th><th>Kommentti</th></tr>\n";
     }
    if ($i % 2 == 0) $class="even"; else $class="odd";
    if (!$mets_transfered) $mets_transfered_class="help"; else $mets_transfered_class="korjattuodd";
    if (!$mets_exported) $mets_exported_class="help"; else $mets_exported_class="korjattuodd";
    if (!$mets_ingested) $mets_ingested_class="help"; else $mets_ingested_class="korjattuodd";
    if ($mets_rejected) $mets_rejected_class="vika"; else $mets_rejected_class="";
    if (!$marc_transfered) $marc_transfered_class="help"; else $marc_transfered_class="korjattuodd";
    if (!$marc_exported) $marc_exported_class="help"; else $marc_exported_class="korjattuodd";
    if (!$marc_ingested) $marc_ingested_class="help"; else $marc_ingested_class="korjattuodd";
    if ($marc_rejected) $marc_rejected_class="vika"; else $marc_rejected_class="";
    echo"<tr class=\"$class\"><td>$i</td><td>&nbsp;$docworksid&nbsp;</td><td>&nbsp;$logunitid&nbsp;</td><td>$title</td><td>$nbn &nbsp;&nbsp;</td><td>METS</td><td class=\"$mets_exported_class\">&nbsp;$mets_exported&nbsp;</td><td class=\"$mets_transfered_class\">&nbsp;$mets_transfered&nbsp;</td><td class=\"$mets_ingested_class\">&nbsp;$mets_ingested&nbsp;</td>";
    echo"<td class=\"$mets_ingested_class\"><a href=\"\" href=\"\" onclick=\"window.open('$mets_url','', 'width=1000, height=800, location=no, menubar=yes, status=no,toolbar=no, scrollbars=yes, resizable=yes')\">$mets_available $mets_url</a></td><td class=\"$mets_rejected_class\">$mets_rejected&nbsp;</td><td>&nbsp;$mets_comment</td></tr>\n";
    echo"<tr class=\"$class\"><td></td><td></td><td></td><td></td><td></td><td>MARC</td><td class=\"$marc_exported_class\">&nbsp;$marc_exported&nbsp;</td><td class=\"$marc_transfered_class\">&nbsp;$marc_transfered&nbsp;</td><td class=\"$marc_ingested_class\">&nbsp;$marc_ingested&nbsp;</td>";
    echo"<td class=\"$marc_ingested_class\"><a href=\"$marc_url\" target=\"_new\">$marc_available $marc_url</a></td><td class=\"$marc_rejected_class\">$marc_rejected&nbsp;</td><td>&nbsp;$marc_comment</td></tr>\n";
    $edellinenkokoelma=$colltitle;
    $i+=1;
    }
echo "</table>\n";
echo "<h1>Niteit� yhteens�: $i</h1>";
sqlsrv_close($dbhandle);
?> 
</body>
</html>