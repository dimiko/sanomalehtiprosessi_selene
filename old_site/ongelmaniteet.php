<?php
include("sessio.php");
 

?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <!-- <meta http-equiv="refresh" content="240" > -->
    <title>Sanomalehtiprosessiin jumittuneet niteet</title>
    <link rel="stylesheet" type="text/css" href="reset.css">
    <link rel="stylesheet" type="text/css" href="style.css">

<style type="text/css">
th, td {
  padding: 3px !important;
}

</style>


</head>
<body>
<?php


    $query = "
    SELECT  datediff(day,scandate,GETDATE()) as kesto,
	title, issn,docid, m.p_artid as pool,m.p_jobname as jobname,m.p_status as status,issuenum,issn,depositid, pages, scannedby, depositbrowser_errorpages, depositbrowser_comments,depositbrowser_status,digi_status,dw_replaced_by,status_processed,
    convert(varchar,issuedate,120) as issuedate,
    convert(varchar,dw_importdate,120) as dw_importdate,
    convert(varchar,dw_exportdate,120) as dw_exportdate,
    convert(varchar,m.p_date,120) as dw_date,
    convert(varchar,depositbrowser_statusdate,120) as  depositbrowser_statusdate,
    convert(varchar,depositbrowser_receivedate,120) as depositbrowser_receivedate,
    convert(varchar,depositbrowser_processdate,120) as depositbrowser_processdate,
    convert(varchar,depositbrowser_mfdate,120) as depositbrowser_mfdate,
    convert(varchar,dw_deletedate,120) as dw_deletedate,
    convert(varchar,digi_importdate,120) as digi_importdate,
    convert(varchar,scandate,120) as scandate, s.id
    from dw_nlf_custom.dbo.sl_status s
    left outer join dw_pool.dbo.main m on (m.p_artid=s.docid)
    where digi_importdate is null
    and datediff(day,scandate,GETDATE())>5
    order by 1 desc,2,issuedate ";
    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
###############################################################
echo "<div style=\"margin-right:auto;margin-left:auto;margin-top:20px; margin-bottom:40px;\">";
echo"<h3 style=\"text-align:center;\">
<a href=\"index.php\">Karttuvat</a> |
 <a href=\"index_takautuva.php\">Takautuvat</a> |
 <a href=\"niteet.php\">Nidekohtaiset tiedot</a> |
 Ongelmaniteet |
 <a href=\"sl-rdy.txt\">Kansiot ei vielä käsittelyssä</a> |
 <a href=\"/dw-in.txt\">IN kulutukset</a> |
 <a href=\"http://akeso.docworks.lib.helsinki.fi/DepositBrowser/\">DepositBrowser</a></h3>\n";
echo"</div>\n";
echo"<div style=\"float:none; clear:both;\">";
##############################################


    
  echo "<div style=\"margin-left:auto;margin-right:auto;\"><table align=\"center\"><caption>yli 5 päivää tuotannossa olleet niteet</caption>\n";
  //echo "<tr><th colspan=\"10\" align=\"left\">$title</th></tr>\n";
   echo"<tr><th>tuotannossa (pv)</th><th>dwid</th><th>Nimeke</th><th>pvm</th><th>no</th><th>sivuja</th><th>skannattu</th><th>DW tuonti</th><th>DW aika</th><th>DW vaihe</th><th>DW status</th><th>DW export</th><th>COM Import</th><th>COM status</th><th>Import</th><th>DIGI</th><th>DW delete</th><th>MF date</th></tr>\n";
$edellinenkokoelma="";
$i=0;
$sivuja=0;
while($row = mssql_fetch_array($result))
    {
      $kesto=$row["kesto"];
      $title=$row["title"];
      $issn=$row["issn"];
      $docworksid=$row["docid"];
      $pool=$row["pool"];
      $pvm=$row["issuedate"];
      $issn=$row["issn"];
      $issuenum=$row["issuenum"];
      $pages=$row["pages"];
      $scandate=$row["scandate"];
      $dw_importdate=$row["dw_importdate"];
      $dw_exportdate=$row["dw_exportdate"];
      $dw_jobname=$row["jobname"];
      $dw_status=$row["status"];
      $dw_date=$row["dw_date"];
      $depositbrowser_receivedate=$row["depositbrowser_receivedate"];
      $depositbrowser_processdate=$row["depositbrowser_processdate"];
      $depositbrowser_statusdate=$row["depositbrowser_statusdate"];
      $depositbrowser_status=$row["depositbrowser_status"];
      $depositbrowser_mfdate=$row["depositbrowser_mfdate"];
      $digi_importdate=$row["digi_importdate"];
      $dw_deletedate=$row["dw_deletedate"];
      $digi_status=trim($row["digi_status"]);
      $depositbrowser_comment=$row["depositbrowser_comments"];
      $depositbrowser_errorpages=$row["depositbrowser_errorpages"];
      $digi_url="http://digi.kansalliskirjasto.fi/sanomalehti/binding/".$digi_status."?page=1";
      
     // $dw_deletedate="";
     // $digi_importdate="";
     //  $digi_status="";
     //  $depositbrowser_status="REJECTED";
      if ($scandate=="") $scandate="2017-01-01";
      // if ($digi_importdate=="") $date_end=date("Y-m-d"); else $date_end=$digi_importdate;
      if ($digi_importdate=="") $date_end=date("Y-m-d"); else $date_end=$digi_importdate;
      $startdate=date_create($scandate);
      $enddate=date_create($date_end);
      $productiontime=date_diff($startdate,$enddate)->format("%a");
      
      $pubdate=date_create($pvm);
      $delay=date_diff($pubdate,$enddate)->format("%a");
      
      
    if ($i % 2 == 0) {
            $class="even";
            $rdyclass="valmiseven";
        } else {
            $class="odd";
            $rdyclass="valmisodd";
        }

    //if ($dw_deletedate!="") $class="valmis $rdyclass";
    if ($depositbrowser_mfdate!="" && $digi_importdate!="") $class="valmis mf $rdyclass";
    if ($dw_importdate) $dw_importdate_class=$rdyclass; else $dw_importdate_class="";
    if ($dw_exportdate) $dw_exportdate_class=$rdyclass; else $dw_exportdate_class="";
    if ($depositbrowser_receivedate) $depositbrowser_receivedate_class=$rdyclass; else $depositbrowser_receivedate_class="";
    if ($depositbrowser_status=="REJECTED") $depositbrowser_status_class="vika"; elseif ($depositbrowser_status=="ACCEPTED") $depositbrowser_status_class=$rdyclass ; else $depositbrowser_status_class="";
    if ($depositbrowser_mfdate) $depositbrowser_mfdate_class=$rdyclass; else $depositbrowser_mfdate_class="";
    if ($digi_importdate) $digi_importdate_class=$rdyclass; else $digi_importdate_class="";


    $failed_path="/mnt/sl_import/failed/";
    $failed_logfile=$failed_path.$issn."_".$pvm."_".$issuenum.".zip.log";
    if ($digi_status=="") {
        if (file_exists($failed_logfile)) {
		$failedclass="vika"; 
		$logcell="&nbsp;<a href=\"show_importlog.php?file=$failed_logfile\" target=\"_new\">X</a>";
	} else {
		$failedclass=$class; 
		$logcell="";
	}
    }

    if ($dw_deletedate) $dw_deletedate_class=$rdyclass; else $dw_deletedate_class="";
    
    if ($productiontime<4) $productiontime_class="production_ok";
    if ($productiontime>3) $productiontime_class="production_slow";
    if ($productiontime>7) $productiontime_class="production_veryslow";
   
    if ($pool=="") {
	$pool_class="vika";
    } else {
      $pool_class=$class;
    }    
    
    echo"<tr class=\"$class\">
    <td class=\"$productiontime_class $digi_importdate_class\" align=\"center\">$kesto</td>
    <td class=\"$pool_class\">&nbsp;$docworksid&nbsp;</td>
    <td>$title</td>
    <td>&nbsp;$pvm&nbsp;</td>
    <td>&nbsp;$issuenum&nbsp;</td>
    <td>&nbsp;$pages</td>
    <td class=\"$class\">&nbsp;$scandate&nbsp;</td>
    <td class=\"$dw_importdate_class\">&nbsp;$dw_importdate&nbsp;</td>
    <td class=\"$class\">&nbsp;$dw_date&nbsp;</td>
    <td class=\"$class\">&nbsp;$dw_jobname&nbsp;</td>
    <td class=\"$class\">&nbsp;$dw_status&nbsp;</td>
    <td class=\"$dw_exportdate_class\">&nbsp;$dw_exportdate&nbsp;</td>
    <td class=\"$depositbrowser_receivedate_class\">&nbsp;$depositbrowser_receivedate&nbsp;</td>
    <td class=\"$depositbrowser_status_class tooltip\">$depositbrowser_statusdate";
    if ($depositbrowser_status=="REJECTED") echo "<span class=\"tooltiptext\">$depositbrowser_comment<br>sivut: $depositbrowser_errorpages</span>";
    echo"</td>
    <!-- <td class=\"$depositbrowser_status_class\">&nbsp;$depositbrowser_status&nbsp;</td> -->
    <td class=\"$failedclass\">$logcell</td>    
    <td class=\"$digi_importdate_class\"><a href=\"$digi_url\" target=\"_new\"\">$digi_importdate</a></td>
    
    
    <td class=\"$dw_deletedate_class\">$dw_deletedate&nbsp;</td>
    <td class=\"$depositbrowser_mfdate_class\">&nbsp;$depositbrowser_mfdate&nbsp;</td></tr>\n";
   $edellinenkokoelma=$title;
    $i+=1;
    $sivuja=$sivuja+$pages;
    }
echo "</table>\n";
echo "<h3>Niteitä:$i, sivuja: $sivuja</h3></div>";
    //close the connection
sqlsrv_close($dbhandle);
?> 
</body>
</html>
