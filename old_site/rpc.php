<?php
function ms_escape_string($data) {
        if ( !isset($data) or empty($data) ) return '';
        if ( is_numeric($data) ) return $data;

        $non_displayables = array(
            '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/',             // url encoded 16-31
            '/[\x00-\x08]/',            // 00-08
            '/\x0b/',                   // 11
            '/\x0c/',                   // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ( $non_displayables as $regex )
            $data = preg_replace( $regex, '', $data );
        $data = str_replace("'", "''", $data );
        return $data;
}

	
$server = "SIRIUS\SQLEXPRESS";
$myUser = "md_audit";
$myPass = "md_audit";
$myDB = "DW_WORKFLOW";
$params = array();
$options = array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

// $server = "DW-JUKKA\SQLEXPRESS";
// $myUser = "sa";
// $myPass = "qwerty";
// $myDB = "DW_WORKFLOW";

$connectinfo=array("UID"=>$myUser, "PWD"=>$myPass, "Database"=>"DW_WORKFLOW");
//connection to the database
$db = sqlsrv_connect($server,$connectinfo);
  // or die("Couldn't connect to SQL Server on $server");
	
	if(!$db) {
		// Show error if we cannot connect.
		echo 'ERROR: Could not connect to the database.';
	} else {
		// Is there a posted query string?
		if(isset($_POST['queryString']) && isset($_POST['mdfield'])) {
			$queryString=($_POST['queryString']);
			$mdfield=($_POST['mdfield']);
			$queryString=mb_convert_encoding( $queryString, 'Windows-1252','UTF-8');
			// echo "$queryString<br>";
			// Is the string length greater than 0?
			
			if(strlen($queryString) >0) {
				// Run the query: We use LIKE '$queryString%'
				// The percentage sign is a wild-card, in my example of countries it works like this...
				// $queryString = 'Uni';
				// Returned data = 'United States, United Kindom';
				
				// YOU NEED TO ALTER THE QUERY TO MATCH YOUR DATABASE.
				// eg: SELECT yourColumnName FROM yourTable WHERE yourColumnName LIKE '$queryString%' LIMIT 10
				
				$query="SELECT top 10 corrected from (select distinct(corrected) FROM DW_WORKFLOW.dbo.MDCorrections t WHERE mdfield='$mdfield' and corrected LIKE '$queryString%') a";
				$result = sqlsrv_query($db, $query);
				if($result) {
					echo '<li onClick="fill(\''.$queryString.'\');">'.$queryString.'</li>';
					// While there are results loop through them - fetching an Object (i like PHP5 btw!).
					while ($row = sqlsrv_fetch_array($result)) {
						// Format the results, im using <li> for the list, you can change it.
						// The onClick function fills the textbox with the result.
						
						// YOU MUST CHANGE: $result->value to $result->your_colum
						$julkaisija=$row["corrected"];
						$julkaisija=mb_convert_encoding( $julkaisija, 'UTF-8');
	         			echo '<li onClick="fill(\''.$julkaisija.'\');">'.$julkaisija.'</li>';
	         		}
				} else {
					echo 'ERROR: There was a problem with the query.';
				}
			} else {
				// Dont do anything.
			} // There is a queryString.
		} else {
			echo 'There should be no direct access to this script!';
		}
	}
?>