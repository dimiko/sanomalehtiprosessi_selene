<?php
include("sessio.php");
 
?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <!-- <meta http-equiv="refresh" content="240" > -->
    <title>Tulostettavat niteet</title>
    <link rel="stylesheet" type="text/css" href="reset.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/jquery.cookie.js"></script>
    <script type="text/javascript" src="/sorttable.js"></script> 
<style type="text/css">
th, td {
  padding: 3px !important;
}
  html {
    overflow: -moz-scrollbars-vertical;
}


/* Sortable tables */
table.sortable thead {
    background-color: #333;
    color: #cccccc;
    font-weight: bold;
    cursor: default;
}
th {
  font-size: 100%;
}

</style>


</head>
<body>
<br>



<?php

$toiminto="listaa";

if ($toiminto=="listaa") {
    echo "<div class=\"buttonx\">";
    if (isset($_POST["ryhma"])) {
        $ryhma=$_POST["ryhma"];
    } elseif (isset($_GET["ryhma"])) {
        $ryhma=$_GET["ryhma"];
    } else $ryhma="";
    
    
    
    $query = "
    SELECT title, count(docid) as niteita,sum(pages) as sivuja,convert(varchar(10),min(issuedate),104) as pvm
    FROM dw_nlf_custom.dbo.sl_status 
	where depositbrowser_mfdate is null 
	and dw_exportdate is not null 
	and depositbrowser_status='ACCEPTED'
    and title not in (
	select distinct title 
	from [dw_nlf_custom].[dbo].[sl_status] 
	where dw_exportdate is null 
	or depositbrowser_receivedate is null 
	or depositbrowser_status is null 
	or depositbrowser_status='REJECTED')
    group by title order by title";
    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    
    echo "<form action=\"mikrofilmi.php\" method=\"get\">";
    echo"<select name=\"ryhma\">";
    echo"<option></option>";
    while($row = mssql_fetch_array($result))
        {
            $niteenryhma=$row["title"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            $pvm_min=$row["pvm"];
            $encodedniteenryhma=$niteenryhma;
            echo "<option value=\"$encodedniteenryhma\" ".($niteenryhma==$ryhma ? 'selected="selected"' : '').">$niteenryhma $pvm_min ($niteita n. / $sivuja s.)</option>";
    }
            echo "</select>";
            echo"<input type=\"submit\" value=\"Päivitä\">";
            echo "</form>\n";
    
    
    
    
    $mdfield="";

    
    
    

    $query = "
    SELECT  title, docid, issuenum,issuedate,issn,pages, digi_status, depositbrowser_status,
    convert(varchar,issuedate,104) as pvm, month(issuedate) as kk, year(issuedate) as vuosi, datepart(wk,issuedate) as viikko
    from dw_nlf_custom.dbo.sl_status
    where depositbrowser_mfdate is null
    and depositbrowser_status!='PURGED' 
    and title not in (
	select distinct title 
	from [dw_nlf_custom].[dbo].[sl_status] 
	where dw_exportdate is null 
	or depositbrowser_receivedate is null 
	or depositbrowser_status is null 
	or depositbrowser_status='REJECTED') 
    and title in (
	select title from
     		(select title,sum(pages) as b
    		 from [dw_nlf_custom].[dbo].[sl_status]
     		 where depositbrowser_mfdate is null
     		 group by title
     		 having sum(pages)>599) 
	a)
    ";
    $query.=" and title='$ryhma' ";
    $query.="order by issuedate";
    //echo "<div>$query</div>";
    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
 echo"</div>";   
  echo "<div style=\"float:none; margin-left:50px;margin-top:30px;\"><table><caption>$ryhma tulostettavat niteet</caption>\n";
  //echo "<tr><th colspan=\"10\" align=\"left\">$title</th></tr>\n";
   echo"<tr><th>kk</th><th>viikko</th><th>sivuja</th><th>PVM</th><th>nro</th><th>DIGI</th></tr>\n";
$edellinenkk="";
$edellinenviikko="";
$i=0;
$sivuja=0;
while($row = mssql_fetch_array($result))
    {
      $title=$row["title"];
      $pvm=$row["pvm"];
      $issuenum=$row["issuenum"];
      $pages=$row["pages"];
      $kk=$row["kk"];
      $vuosi=$row["vuosi"];
      $viikko=$row["viikko"];
      $sivuja=$sivuja+$pages;
      $depositbrowser_status=$row["depositbrowser_status"];
      $digi_status=trim($row["digi_status"]);
      $digi_url="http://digi.kansalliskirjasto.fi/sanomalehti/binding/".$digi_status."?page=1";
      
    if ($kk % 2 == 0) {
            $class="even";
            $rdyclass="valmiseven";
        } else {
            $class="odd";
            $rdyclass="valmisodd";
        }    
    if ($depositbrowser_status=="REJECTED") $depositbrowser_status_class="vika"; elseif ($depositbrowser_status=="ACCEPTED") $depositbrowser_status_class=$rdyclass ; else $depositbrowser_status_class="";

    echo"<tr class=\"$class\">";
    echo"<td>";
    if ($kk!=$edellinenkk) {
        echo "$kk";
    }
    echo"</td>";
    echo"<td>";
    if ($viikko!=$edellinenviikko) {
        echo "$viikko";
    }
    echo"</td>";
    echo"</td>
    <td style=\"font-weight:bold;\">&nbsp;$sivuja</td>
    <td>&nbsp;$pvm&nbsp;</td>
    <td>&nbsp;$issuenum&nbsp;</td>
    <td>";
    if ($digi_status!="") echo "<a href=\"$digi_url\" target=\"_new\"\">digi</a>";
    echo"</td>";
    $edellinenkk=$kk;
    $edellinenviikko=$viikko;
    $i+=1;
    }
echo "</table>\n<br><br>";
}
mssql_close($db);
?> 
</body>
</html>
