<?php
include("sessio.php");

?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <!-- <meta http-equiv="refresh" content="240" > -->
    <title>Hyväksytyt niteet</title>
    <link rel="stylesheet" type="text/css" href="reset.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/jquery.cookie.js"></script>
    <script type="text/javascript" src="/sorttable.js"></script> 
    

<style type="text/css">
th, td {
  padding: 3px !important;
}
  html {
    overflow: -moz-scrollbars-vertical;
}

/* Sortable tables */
table.sortable thead {
    background-color: #333;
    color: #cccccc;
    font-weight: bold;
    cursor: default;
}
th {
  font-size: 100%;
}

</style>


</head>
<body>


<?php

$toiminto="listaa";

if ($toiminto=="listaa") {
    if (isset($_POST["ryhma"])) {
        $ryhma=$_POST["ryhma"];
    } elseif (isset($_GET["ryhma"])) {
        $ryhma=$_GET["ryhma"];
    } else $ryhma="";
    
    
    
    
    #$ryhma=urldecode($ryhma);
  
        

    $query = "
    SELECT  title, docid, issuenum,issn,depositid, pages, scannedby, depositbrowser_errorpages, depositbrowser_comments,depositbrowser_status,digi_status,dw_replaced_by,status_processed,
    convert(varchar,issuedate,120) as issuedate,
    convert(varchar,dw_importdate,120) as dw_importdate,
    convert(varchar,dw_exportdate,120) as dw_exportdate,
    convert(varchar,depositbrowser_statusdate,120) as  depositbrowser_statusdate,
    convert(varchar,depositbrowser_receivedate,120) as depositbrowser_receivedate,
    convert(varchar,depositbrowser_processdate,120) as depositbrowser_processdate,
    convert(varchar,depositbrowser_mfdate,120) as depositbrowser_mfdate,
    convert(varchar,dw_deletedate,120) as dw_deletedate,
    convert(varchar,digi_importdate,120) as digi_importdate,
    convert(varchar,scandate,120) as scandate
    from dw_nlf_custom.dbo.sl_status
    where title='$ryhma'
    order by issuedate desc";
    
    #echo "$query";
    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    
  echo "<div style=\"float:none; padding-top:0px;\"><table><caption>$ryhma</caption>\n";
  //echo "<tr><th colspan=\"10\" align=\"left\">$title</th></tr>\n";
   echo"<tr><th>dwid</th><th>pvm</th><th>no</th><th>sivuja</th><th>skannattu</th><th>DW tuonti</th><th>DW export</th><th>COM Import</th><th>COM status</th><th>DIGI</th><th>Tuotanto digiin</th><th>Viive digiin</th><th>DW delete</th><th>MF date</th></tr>\n";
$edellinenkokoelma="";
$i=0;
$sivuja=0;
while($row = mssql_fetch_array($result))
    {
      $title=$row["title"];
      $docworksid=$row["docid"];
      $pvm=$row["issuedate"];
      $issn=$row["issn"];
      $issuenum=$row["issuenum"];
      $pages=$row["pages"];
      $scandate=$row["scandate"];
      $dw_importdate=$row["dw_importdate"];
      $dw_exportdate=$row["dw_exportdate"];
      $depositbrowser_receivedate=$row["depositbrowser_receivedate"];
      $depositbrowser_processdate=$row["depositbrowser_processdate"];
      $depositbrowser_statusdate=$row["depositbrowser_statusdate"];
      $depositbrowser_status=$row["depositbrowser_status"];
      $depositbrowser_mfdate=$row["depositbrowser_mfdate"];
      $digi_importdate=$row["digi_importdate"];
      $dw_deletedate=$row["dw_deletedate"];
      $digi_status=trim($row["digi_status"]);
      $depositbrowser_comment=$row["depositbrowser_comments"];
      $depositbrowser_errorpages=$row["depositbrowser_errorpages"];
      $digi_url="http://digi.kansalliskirjasto.fi/sanomalehti/binding/".$digi_status."?page=1";
      
     // $dw_deletedate="";
     // $digi_importdate="";
     //  $digi_status="";
     //  $depositbrowser_status="REJECTED";
      if ($scandate=="") $scandate="2017-01-01";
      if ($digi_importdate=="") $date_end=date("Y-m-d"); else $date_end=$digi_importdate;
      $startdate=date_create($scandate);
      $enddate=date_create($date_end);
      $productiontime=date_diff($startdate,$enddate)->format("%a");
      
      $pubdate=date_create($pvm);
      $delay=date_diff($pubdate,$enddate)->format("%a");
      
      
    //  if ($edellinenkokoelma!=$title) {
    //        echo "<tr><th colspan=\"10\" align=\"left\">$title</th></tr>\n";
    //        echo"<tr><th>dwid</th><th>Nimeke</th><th>pvm</th><th>no</th><th>sivuja</th><th>skannattu</th><th>DW tuonti</th><th>DW export</th><th>COM Import</th><th>COM status</th><th></th><th>MF</th><th>DIGI</th><th>DW delete</th><th>Tuotannossa (pv)</th></tr>\n";
    // }
    if ($i % 2 == 0) {
            $class="even";
            $rdyclass="valmiseven";
        } else {
            $class="odd";
            $rdyclass="valmisodd";
        }
    //if ($dw_deletedate!="") $class="valmis $rdyclass";
    if ($depositbrowser_mfdate!="" && $digi_importdate!="") $class="valmis mf $rdyclass";
    if ($dw_importdate) $dw_importdate_class=$rdyclass; else $dw_importdate_class="";
    if ($dw_exportdate) $dw_exportdate_class=$rdyclass; else $dw_exportdate_class="";
    if ($depositbrowser_receivedate) $depositbrowser_receivedate_class=$rdyclass; else $depositbrowser_receivedate_class="";
    if ($depositbrowser_status=="REJECTED") $depositbrowser_status_class="vika"; elseif ($depositbrowser_status=="ACCEPTED") $depositbrowser_status_class=$rdyclass ; else $depositbrowser_status_class="";
    if ($depositbrowser_mfdate) $depositbrowser_mfdate_class=$rdyclass; else $depositbrowser_mfdate_class="";
    if ($digi_importdate) $digi_importdate_class=$rdyclass; else $digi_importdate_class="";
    if ($dw_deletedate) $dw_deletedate_class=$rdyclass; else $dw_deletedate_class="";
    
    if ($productiontime<4) $productiontime_class="production_ok";
    if ($productiontime>3) $productiontime_class="production_slow";
    if ($productiontime>7) $productiontime_class="production_veryslow";
    
    
    echo"<tr class=\"$class\">
    <td>&nbsp;$docworksid&nbsp;</td>
    <td>&nbsp;$pvm&nbsp;</td>
    <td>&nbsp;$issuenum&nbsp;</td>
    <td>&nbsp;$pages</td>
    <td class=\"$rdyclass\">&nbsp;$scandate&nbsp;</td>
    <td class=\"$dw_importdate_class\">&nbsp;$dw_importdate&nbsp;</td>
    <td class=\"$dw_exportdate_class\">&nbsp;$dw_exportdate&nbsp;</td>
    <td class=\"$depositbrowser_receivedate_class\">&nbsp;$depositbrowser_receivedate&nbsp;</td>
    <td class=\"$depositbrowser_status_class tooltip\">$depositbrowser_statusdate";
    if ($depositbrowser_status=="REJECTED") echo "<span class=\"tooltiptext\">$depositbrowser_comment<br>sivut: $depositbrowser_errorpages</span>";
    echo"</td>
    <!-- <td class=\"$depositbrowser_status_class\">&nbsp;$depositbrowser_status&nbsp;</td> -->
    <td class=\"$digi_importdate_class\"><a href=\"$digi_url\" target=\"_new\"\">$digi_importdate</a></td>
    
    
    <td align=\"center\" class=\"$productiontime_class $digi_importdate_class\">&nbsp;$productiontime</td>
    <td class=\"$digi_importdate_class\">$delay</td>
    <td class=\"$dw_deletedate_class\">$dw_deletedate&nbsp;</td>
    <td class=\"$depositbrowser_mfdate_class\">&nbsp;$depositbrowser_mfdate&nbsp;</td></tr>\n";
   $edellinenkokoelma=$title;
    $i+=1;
    $sivuja=$sivuja+$pages;
    }
echo "</table>\n";
echo "<h3>Niteitä:$i, sivuja: $sivuja</h3></div>";
    //close the connection
}
mssql_close($db);
?> 
</body>
</html>
