<?php
include("sessio.php");
$conn = oci_connect('comellus', 'comellus', 'vega2/ora11g','AL32UTF8');
 

?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <meta http-equiv="refresh" content="180" >
    <title>Sanomalehtiprosessin tila</title>
    <link rel="stylesheet" type="text/css" href="reset.css">
    <link rel="stylesheet" type="text/css" href="style.css">
<!--    <script type="text/javascript" src="/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/jquery.cookie.js"></script>
    <script type="text/javascript" src="/sorttable.js"></script> -->
    <script type="text/javascript" src="chart/Chart.bundle.js"></script> 
 


</head>
<body>


<?php

function status_list_table_latest ($query, $table_title,$link) {

    $result_dw = mssql_query($query);
    if( $result_dw === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    echo "<table><caption> $table_title </caption>\n";
    echo"<tr><th>Nimeke</th><th>Niteitä</th><th>Sivuja</th><th style=\"color:#dddddd\">Uusin</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_dw))
        {
            $nimeke=$row["nimeke"];
	    $fullnimeke=$row["fullnimeke"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            $uusin=$row["uusin"];
            echo"<tr>";
	    switch ($link) {
		case "mikrofilmi":
	    		echo "<td><a onClick=\"MyWindow=window.open('mikrofilmi.php?ryhma=".urlencode($fullnimeke)."','MyWindow','width=600,height=1100'); return false;\" style=\"color:black; text-decoration:none;\" href=\"mikrofilmi.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
			break;
		case "niteet":
			echo "<td><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($fullnimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\" href=\"niteet-popup.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
			break;
		case "digi":
		       echo"<td><a href=\"http://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=CALENDAR&year=".$vuosi."\" target=\"_blank\" style=\"color:black; text-decoration:none;\">".$nimeke."</a></td>";
		       break;
		}


        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td><td style=\"color:#dddddd\">&nbsp;&nbsp;$uusin</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result_dw);    
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td><td></td></tr>";
    echo"</table>\n";


}

function dw_status_list_table ($query, $table_title,$link) {

    $result_dw = mssql_query($query);
    if( $result_dw === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    $rows=mssql_num_rows($result_dw);
    if ($rows==0) {
	return;
 	}
    echo "<table  style=\"margin-top:10px;\"><caption style=\"text-align:center;\"> $table_title </caption>\n";
    echo"<tr><th>Nimeke</th><th>Vaihe</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $prev_nimeke="";
    while($row = mssql_fetch_array($result_dw))
        {
            $nimeke=$row["nimeke"];
 	    $fullnimeke=$row["fullnimeke"];
	    $vaihe=$row["vaihe"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];

	   if ($nimeke!=$prev_nimeke && $prev_nimeke!="") {
		echo "<tr><td>&nbsp;</td></tr>\n";
	    }

            echo"<tr>";
			echo "<td><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($fullnimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\" href=\"niteet-popup.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
        echo"<td>&nbsp;$vaihe</td><td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";

	$prev_nimeke=$nimeke;
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result_dw);    
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td></td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>\n";


}
function status_list_table ($query, $table_title,$link) {

    $result_dw = mssql_query($query);
    if( $result_dw === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    $rows=mssql_num_rows($result_dw);
    if ($rows==0) {
	return;
 	}
    echo "<table  style=\"margin-top:10px;\"><caption style=\"text-align:center;\"> $table_title </caption>\n";
    echo"<tr><th>Nimeke</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_dw))
        {
            $nimeke=$row["nimeke"];
            $fullnimeke=$row["fullnimeke"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
	    switch ($link) {
		case "mikrofilmi":
	    		echo "<td><a onClick=\"MyWindow=window.open('mikrofilmi.php?ryhma=".urlencode($fullnimeke)."','MyWindow','width=600,height=1100'); return false;\" style=\"color:black; text-decoration:none;\" href=\"mikrofilmi.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
			break;
		case "niteet":
			echo "<td><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($fullnimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\" href=\"niteet-popup.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
			break;
		case "digi":
		       echo"<td><a href=\"http://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=CALENDAR&year=".$vuosi."\" target=\"_blank\" style=\"color:black; text-decoration:none;\">".$nimeke."</a></td>";
		       break;
		}


        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result_dw);    
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>\n";


}


function status_error_table ($query, $table_title) {

    $result_dw = mssql_query($query);
    if( $result_dw === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    if (mssql_num_rows($result_dw)==0) {
	return;
    }
    echo "<table  style=\"background-color:yellow; margin-top:10px;\"><caption  style=\"background-color:yellow;\"> $table_title </caption>\n";
    echo"<tr><th>Nimeke</th><th>PVM</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_dw))
        {
            $nimeke=$row["nimeke"];
            $pvm=$row["issuedate"];
            $status=$row["p_status"];
            echo"<tr style=\"background-color:yellow;\"><td>$nimeke &nbsp;</td><td>$pvm</td></tr>";


    }
    mssql_free_result($result_dw);    
    echo"</table>\n";


}

function stat_table ($query,$table_title,$col_title) {

    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }

    echo "<table width=\"250px\">\n";

    echo"<tr><th> $col_title </th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $paivia=0;
    while($row = mssql_fetch_array($result))
        {
            $pvm=$row["pvm"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td style=\"text-align:center;\">$pvm</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
        $paivia++;
    }
    $avg_niteet=round($sum_niteet/$paivia);
    $avg_sivut=round($sum_sivut/$paivia);
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"<tr style=\"font-weight:normal;\"><td style=\"text-align:right;\">Keskiarvo:</td><td style=\"text-align:right;\">$avg_niteet</td><td style=\"text-align:right;\">$avg_sivut</td></tr>";

    echo"</table>";

}


function comellus_stat_table ($year,$conn) {
$select_comellus="
select depositor, count(distinct d.id) as NITEITA, count(*) AS SIVUJA, to_char(max(d.depositdate),'DD.MM.YYYY') as UUSIN
from deposits d, files f
where d.id=f.depositid
and depositor not like '%+%'
and to_char(d.depositdate,'YYYY')='".$year."'
group by depositor
order by depositor
";

$stid_comellus = oci_parse($conn, $select_comellus);
$res_comellus=oci_execute($stid_comellus);

        if (!$res_comellus) {
                echo "error:".oci_error()."--";
                    $e = oci_error();
                    print_r($e);
                    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
                exit(9);

        }

# echo "query ok\n";

    echo "<br><br><table><caption>".$year."</caption>\n";
    echo"<tr><th></th><th>Nimeke</th><th>Niteitä</th><th>Sivuja</th><th style=\"color:#888888\">Uusin</th</tr>";
    $sum_niteet_comellus=0;
    $sum_sivut_comellus=0;
    $laskuri=1;
while (($row = oci_fetch_array($stid_comellus, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
          $depositor=$row['DEPOSITOR'];
	  $niteita=$row['NITEITA'];
          $sivuja=$row['SIVUJA'];
          $uusin=$row['UUSIN'];
	echo"<tr><td style=\"text-align:right;\">$laskuri&nbsp; </td><td> $depositor</td><td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td><td style=\"color:#cccccc\">&nbsp; $uusin</td><tr>\n";
	$sum_sivut_comellus=$sum_sivut_comellus+$sivuja;
 	$sum_niteet_comellus=$sum_niteet_comellus+$niteita;
	$laskuri++;
}
	echo"<tr style=\"font-weight:bold;\"><td></td><td>Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet_comellus</td><td style=\"text-align:right;\">&nbsp;$sum_sivut_comellus</td></tr>";
	echo"</table>\n";
	$arvot=array($sum_niteet_comellus,$sum_sivut_comellus);
	return $arvot;

}


function digi_yearly_table ($vuosi,$sivuennuste,$comellus_arvot) {

	$sum_niteet_comellus=$comellus_arvot[0];
	$sum_sivut_comellus=$comellus_arvot[1];
    
    echo"<h3 style=\"text-align:center;\">".$vuosi."</h3>\n";
        $query_digissa = "
    SELECT year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is not null and year(issuedate)='".$vuosi."'
     group by year(issuedate)
     order by year(issuedate) desc";
    $result_digissa = mssql_query($query_digissa);
    if( $result_digissa === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    echo "<table>\n";
    echo"<tr><th>Prosessi</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $niteita=0;
    $sivuja=0;
    while($row = mssql_fetch_array($result_digissa))
        {
            $niteita=$row["niteita"];
            $vuosi=$row["vuosi"];
            $sivuja=$row["sivuja"];
    }
        echo"<tr>";
        echo"<td>SUPAG</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
        $sivuja=$sum_sivut_comellus;
        $niteita=$sum_niteet_comellus;
        echo"<tr>";
        echo"<td>Comellus</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
           echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    $valmistumisprosentti=round($sum_sivut/$sivuennuste*100,1);
    echo"<tr style=\"font-weight:normal;\"><td>Ennakkoarvio:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$sivuennuste</td></tr>";
    echo"<tr style=\"font-weight:bold;\"><td>Valmistunut:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$valmistumisprosentti %</td></tr>";
    mssql_free_result($result_digissa);    
    

    echo"</table>\n<br>";


}


function chart_data ($year,$type) {
    $chart_data=array();
    $query="
	set datefirst 1
	SELECT datepart(wk,scandate) as viikko,COUNT(docid) as niteita,SUM(pages) as sivuja
  	FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='".$year."' and title not like '%z+%'
  	group by datepart(wk,scandate)
  	order by datepart(wk,scandate) asc
	";


    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }

    $sum_sivut=0;
    while($row = mssql_fetch_array($result))
        {
            $viikko=$row["viikko"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            $sum_sivut=$sum_sivut+$sivuja;
	    if ($type=="summa") {
		 array_push($chart_data,$sum_sivut);
	    } elseif ($type=="viikko") {
		 array_push($chart_data,$sivuja);
	    } else {
   
	   }
    }

    return implode(", ",$chart_data);

}

echo"<div style=\"float:left; margin-right:20px;margin-top:10px; background-color: #eeffee;\">\n";
echo"<h3 style=\"text-align:center;\">Tänään</h3>";
    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_exportdate)=convert(date,GETDATE()) and dw_deletedate is null and title not like '%z+%'";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $export_sivuja=$row["sivuja"];
    mssql_free_result($result);    
    
    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_statusdate)=convert(date,GETDATE()) and (depositbrowser_status='ACCEPTED' or depositbrowser_status='REJECTED') and dw_deletedate is null and title not like '%z+%'";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $accepted_sivuja=$row["sivuja"];
    mssql_free_result($result);

    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_statusdate)=convert(date,GETDATE()) and depositbrowser_status='REJECTED' and dw_deletedate is null and title not like '%z+%'";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $rejected_sivuja=$row["sivuja"];
    mssql_free_result($result);

    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_mfdate)=convert(date,GETDATE()) and title not like '%z+%'";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $filmattu_sivuja=$row["sivuja"];
    mssql_free_result($result);

    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,digi_importdate)=convert(date,GETDATE()) and dw_deletedate is null and title not like '%z+%'";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $importoitu_sivuja=$row["sivuja"];
    mssql_free_result($result);

    $query="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_deletedate)=convert(date,GETDATE()) and title not like '%z+%'";
    $result=mssql_query($query);
    $row=mssql_fetch_array($result);
    $deletoitu_sivuja=$row["sivuja"];
    mssql_free_result($result);
    
    echo"<table align=\"center\" style=\"margin:10px;\">\n";
    echo"<tr style=\"font-weight:normal;\"><th  style=\"font-weight:normal;\">Exportoitu&nbsp;</th><th  style=\"font-weight:normal;\">Tarkastettu&nbsp;</th><th  style=\"font-weight:normal;\"> Tulostettu&nbsp;</th><th  style=\"font-weight:normal;\">Importoitu&nbsp;</th><th  style=\"font-weight:normal;\">Poistettu</th>";
    echo"<tr style=\"text-align:center;\"><td>$export_sivuja</td><td>$accepted_sivuja</td><td>$filmattu_sivuja</td><td>$importoitu_sivuja</td><td>$deletoitu_sivuja</td>";
    echo"</tr></table>\n";
echo"</div>\n";
###############################################################
echo "<div style=\"float:left; margin-right:20px;margin-top:10px;\">";
echo"<h3 style=\"text-align:center;\">Karttuvat | <a href=\"index_takautuva.php\">Takautuvat</a> |
 <a href=\"niteet.php\">Nidekohtaiset tiedot</a> |
 <a href=\"ongelmaniteet.php\">Ongelmaniteet</a> |
 <a href=\"sl-rdy.txt\">Kansiot ei vielä käsittelyssä</a> |
 <a href=\"/dw-in.txt\">IN kulutukset</a> |
 <a href=\"http://akeso.docworks.lib.helsinki.fi/DepositBrowser/\">DepositBrowser</a></h3>\n";
echo"</div>\n";
echo"<div style=\"float:none; clear:both;\">";
##############################################
    $query_rescans="SELECT P_ARTID as docid,p_filter2 as title, p_comment as comment FROM [dw_pool].[dbo].[Main] where p_filter3 in ('sl_karttuva','sl_takautuva') and P_JOBNAME='Re-Scan' and p_filter2 not like '%z+%'";
    
    $result_rescans =mssql_query($query_rescans);
    if( $result_rescans === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    $rows=mssql_num_rows($result_rescans);
    if ($rows>0) {
        echo "<h2 class=\"help\">Uudelleen skannattavat niteet</h2><table>\n";
        echo"<tr><th>dwid</th><th>Työ</th><th>Huomautus</th></tr>";
         while($row =mssql_fetch_array($result_rescans)) {
        $title=$row["title"];
        $docworksid=$row["docid"];
        $comment=utf8_decode($row["comment"]);
        $comment = str_replace("\\u000d\\u000a", "", $comment);
        echo"<tr>";
        echo"<td width=\"100px\">$docworksid</td>";
        echo"<td>$title &nbsp;</td>\n";
        echo"<td>$comment</td></tr>\n";
       }
       echo"</table></div>\n";
    }
    mssql_free_result($result_rescans);    

################################################    
    echo "<div style=\"float:left; margin-left:20px; margin-right:20px;\">\n";
################################################    

    ### Käsittelyssä olevat niteet ############################################    
   # $query_dw = "
   # SELECT title,COUNT(*) as niteita, sum(pages) as sivuja
   #  FROM [dw_nlf_custom].[dbo].[sl_status] 
   #  where dw_importdate is not null and dw_exportdate is null and dw_replaced_by is null and title not like '%z+%'
   #  group by title
   #  order by title";
   #  status_list_table($query_dw,"Käsittelyssä","niteet");

    $query_dw =" SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,case 
		when  M.P_JOBNAME='PrepareImport' then 'Tuonti'
		when  M.P_JOBNAME='Import' then 'Tuonti'
		when  M.P_JOBNAME='Scan' then 'Tuonti'
		when  M.P_JOBNAME='DetectPageFrames' then 'Rajaus'
		when  M.P_JOBNAME='VerifyPageFrames' then 'Rajaus'
		when  M.P_JOBNAME='ModifyPages' then 'Rajaus'  
		when  M.P_JOBNAME='VerifyPages' then 'Rajaus'
		when  M.P_JOBNAME='DetectLayoutElements' then 'Palstoitus'
		when  M.P_JOBNAME='VerifyLayoutElements' then 'Palstoitus'
		when  M.P_JOBNAME='DetectPageNumbers' then 'OCR'
		when  M.P_JOBNAME='VerifyPageNumbers' then 'OCR'
		when  M.P_JOBNAME='BuildPagesHierarchy' then 'Hierarkia'  
		when  M.P_JOBNAME='VerifyPagesHierarchy' then 'Hierarkia'
		when  M.P_JOBNAME='BuildHierarchy' then 'Hierarkia' 
		when  M.P_JOBNAME='VerifyHierarchy' then 'Hierarkia'
		when  M.P_JOBNAME='Export' then 'Export'
		when  M.P_JOBNAME='ExportXML' then 'Export'  
		else M.P_JOBNAME end as vaihe,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] s, dw_pool.dbo.main m
     where s.dw_importdate is not null and s.dw_exportdate is null and s.dw_replaced_by is null and s.title not like '%z+%' and s.docid=m.P_ARTID
     group by  SUBSTRING(title,0, CHARINDEX('+',title)),title, case
                when  M.P_JOBNAME='PrepareImport' then 'Tuonti'
                when  M.P_JOBNAME='Import' then 'Tuonti'
                when  M.P_JOBNAME='Scan' then 'Tuonti'
                when  M.P_JOBNAME='DetectPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='ModifyPages' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPages' then 'Rajaus'
                when  M.P_JOBNAME='DetectLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='VerifyLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='DetectPageNumbers' then 'OCR'
                when  M.P_JOBNAME='VerifyPageNumbers' then 'OCR'
                when  M.P_JOBNAME='BuildPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='BuildHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='Export' then 'Export'
                when  M.P_JOBNAME='ExportXML' then 'Export'
                else M.P_JOBNAME end 
     order by 	1,2 ";
	dw_status_list_table($query_dw,"Käsittelyssä","niteet");




    ### Virheeseen jääneet niteet ############################################    

    $query_dw="	    
    SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke, format(issuedate,'yyyy-MM-dd') as issuedate,p_status
     FROM [dw_nlf_custom].[dbo].[sl_status] s, dw_pool.dbo.main m
     where dw_importdate is not null and dw_exportdate is null and dw_replaced_by is null and s.docid=m.P_ARTID and p_status not in ('W','R') and title not like '%z+%'
     order by title,issuedate ";    
    status_error_table($query_dw,"Virhetilassa");

    ### Reject ehtoon jääneet niteet ############################################

    $query_dw="
    SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,format (issuedate,'YYYY-MM-DD') as issuedate,p_status
     FROM [dw_nlf_custom].[dbo].[sl_status] s, dw_pool.dbo.main m
     where dw_importdate is not null and dw_exportdate is null and dw_replaced_by is null and s.docid=m.P_ARTID and p_status='R' and p_jobname!='Re-Scan'  and s.title not like '%z+%'
     order by title,issuedate ";
    status_error_table($query_dw,"DW hylkäys");



    ### Valmistuneet niteet ############################################    
    $query_dw = "
    SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_importdate is not null
     and dw_exportdate is not null
     and depositbrowser_receivedate is null
     and title in (select distinct title from [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is not null)  and title not like '%z+%'
     group by SUBSTRING(title,0, CHARINDEX('+',title)),title
     order by title";
    status_list_table($query_dw,"Valmistuneet","niteet");

    
    ### Tarkistukseen noudettavissa olevat  nimekkeet ############################################        
    $query_noudettavat = "
    SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is null and dw_deletedate is null
     and title not in (select distinct title from [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is null)  and title not like '%z+%'
     group by SUBSTRING(title,0, CHARINDEX('+',title)),title
     order by title";
    status_list_table($query_noudettavat,"Noudettavat","niteet");


    #############################################
    $query_tarkastettavat = "
    SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and (depositbrowser_status not in ('ACCEPTED','PURGED') or depositbrowser_status is null) and dw_deletedate is null and title not like '%z+%'
     group by SUBSTRING(title,0, CHARINDEX('+',title)),title
     order by title";
    status_list_table($query_tarkastettavat,"Tarkastettavat","niteet");

    #############################################
    $query_hylatyt = "
    SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where depositbrowser_status='REJECTED' and dw_deletedate is null and title not like '%z+%'
     group by SUBSTRING(title,0, CHARINDEX('+',title)),title
     order by title";
    status_list_table($query_hylatyt,"Hylätyt","niteet");
    echo "</div>\n";
    echo "<div style=\"float:left; margin-right:20px;\">\n";

    #############################################
    $query_hyvaksytyt = "
    SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja, format(max(issuedate),'yyyy-MM-dd') as uusin
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and (depositbrowser_status='ACCEPTED') and depositbrowser_mfdate is null and title not like '%z+%'
     group by SUBSTRING(title,0, CHARINDEX('+',title)),title
     having sum(pages)<1000
     order by title";
    status_list_table_latest($query_hyvaksytyt,"Hyväksytyt","niteet");

    echo"</div>\n";
    
     # kuvattavat ##########################
    echo "<div style=\"float:left; margin-right:20px;\">\n";
 
    $query_kuvattavat = "
    SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as  nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja,format(max(issuedate),'yyyy-MM-dd')  as uusin
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and depositbrowser_mfdate is null  and title not like '%z+%'
     and title not in (select distinct title from [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is null or depositbrowser_receivedate is null or depositbrowser_status is null or depositbrowser_status='REJECTED')
     group by SUBSTRING(title,0, CHARINDEX('+',title)),title
     having sum(pages)>999
     order by sum(pages) desc";
    status_list_table_latest($query_kuvattavat,"Tulostettavat","mikrofilmi");


   
	 echo"</div>\n";

###################################################################################################


################################################
echo "<div style=\"float:left; margin-right:20px;\">\n";
################################################
    ## and depositbrowser_receivedate is null and dw_deletedate is null

    $query_importoitavat = "
    SELECT title as nimeke, title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is null and dw_deletedate is null and datediff(day,depositbrowser_statusdate,GETDATE())>=1 and title not like '%z+%'
     group by title
     order by title";
    status_list_table($query_importoitavat,"Importoitavat","niteet");


###########################
# Sähköinen vastaanotto 
########################
echo"<h2 style=\"text-align:center;\">Sähköinen vastaanotto</h2>\n";
$comellus_arvot2022=comellus_stat_table('2022',$conn);
$comellus_arvot2021=comellus_stat_table('2021',$conn);
$comellus_arvot2020=comellus_stat_table('2020',$conn);
$comellus_arvot2019=comellus_stat_table('2019',$conn);
$comellus_arvot2018=comellus_stat_table('2018',$conn);

    echo"</div>\n\n";
##############################
# Käppyrä
#############################
echo "<div class=\"chart-container\" style=\"clear:both;\">
	<canvas id=\"myChart\" width=\"1800\" height=\"500\"></canvas>\n
</div>\n";


    

################################################################################################    
    echo"<div style=\"float:none; clear:both;padding-top:100px;text-align:center;\"><h2>Tilastot</h2></div>\n";
######################################################################################################
    echo "<div style=\"float:left; margin-right:30px;\">";
    echo"<h3 style=\"text-align:center;\">skannaus</h3>\n";
    $query_daily_scans = "
    SELECT convert(varchar(10),scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,scandate)>convert(date,GETDATE()-31)  
 and year(scandate)='2022'
and title not like '%z+%'
  group by convert(varchar(10),scandate)
  order by convert(varchar(10),scandate) desc;";
   stat_table($query_daily_scans,"Skannaus","PVM");


    
        $query_monthly_scans = "
      SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)=year(getdate()) 
and title not like '%z+%'
  group by month(scandate)
  order by month(scandate) desc;";
   stat_table($query_monthly_scans,"Skannaus","KK");

    echo "<br>\n";
    echo"<h3 style=\"text-align:center;\">2021</h3>\n";
        $query_monthly_scans = "
      SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2021'  and title not like '%z+%'
  group by month(scandate)
  order by month(scandate) desc;";
   stat_table($query_monthly_scans,"Skannaus 2021","KK");

    echo "<br>\n";
    echo"<h3 style=\"text-align:center;\">2020</h3>\n";
        $query_monthly_scans = "
      SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2020'  and title not like '%z+%'
  group by month(scandate)
  order by month(scandate) desc;";
   stat_table($query_monthly_scans,"Skannaus 2020","KK");

    echo "<br>\n";
    echo"<h3 style=\"text-align:center;\">2019</h3>\n";
        $query_monthly_scans = "
      SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2019'  and title not like '%z+%'
  group by month(scandate)
  order by month(scandate) desc;";
   stat_table($query_monthly_scans,"Skannaus 2019","KK");
    echo "<br>\n";
    echo"<h3 style=\"text-align:center;\">2018</h3>\n";
        $query_monthly_scans = "
      SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2018'  and title not like '%z+%'
  group by month(scandate)
  order by month(scandate) desc;";
   stat_table($query_monthly_scans,"Skannaus 2018","KK");


    echo"</div>\n";
####################################################################################################################################################################
   echo "<div style=\"float:left; margin-right:30px;\">";
        echo"<h3 style=\"text-align:center;\">docWorks</h3>\n";
    $query_dw_daily_exports = "
    SELECT convert(varchar(10),dw_exportdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_exportdate)>convert(date,GETDATE()-31)  
 and year(scandate)='2022'
and title not like '%z+%'
  group by convert(varchar(10),dw_exportdate)
  order by convert(varchar(10),dw_exportdate) desc;";
   stat_table($query_dw_daily_exports,"docworks","PVM");

    
        $query_dw_monthly_exports = "
      SELECT month(dw_exportdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is not null and year(dw_exportdate)=year(getdate())  and title not like '%z+%'
  group by month(dw_exportdate)
  order by month(dw_exportdate) desc;";
   stat_table($query_dw_monthly_exports,"docworks","KK");

    echo"</div>\n";
###################################################################################################################################################################
    echo "<div style=\"float:left; margin-right:30px;\">";
    echo"<h3 style=\"text-align:center;\">tarkastettu</h3>\n";
    $query_depositbrowser_daily = "
      SELECT top 7 convert(varchar(10),depositbrowser_statusdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_statusdate)>convert(date,GETDATE()-31) 
 and year(scandate)='2022'
and depositbrowser_status is not null and datepart(dw,depositbrowser_statusdate) in (2,3,4,5,6)  and title not like '%z+%'
  group by convert(varchar(10),depositbrowser_statusdate)
  order by convert(varchar(10),depositbrowser_statusdate) desc;";
   stat_table($query_depositbrowser_daily,"tarkastettu","PVM");


    
        $query_depositbrowser_monthly = "
          SELECT month(depositbrowser_statusdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where depositbrowser_statusdate is not null and depositbrowser_status is not null and year(depositbrowser_statusdate)=year(getdate())  and title not like '%z+%'
  group by month(depositbrowser_statusdate)
  order by month(depositbrowser_statusdate) desc;";
   stat_table($query_depositbrowser_monthly,"tarkastettu","KK");

    echo"</div>\n";
###################################################################################################################################################################
    echo "<div style=\"float:left; margin-right:30px;\">";
    echo"<h3 style=\"text-align:center;\">tulostettu</h3>\n";
    $query_mf_daily = "
      SELECT top 7 convert(varchar(10),depositbrowser_mfdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_mfdate)>convert(date,GETDATE()-31)
 and year(scandate)='2022'
 and depositbrowser_status is not null  and title not like '%z+%'
  group by convert(varchar(10),depositbrowser_mfdate)
  order by convert(varchar(10),depositbrowser_mfdate) desc;";
   stat_table($query_mf_daily,"tarkastettu","PVM");

    
        $query_mf_monthly = "
          SELECT month(depositbrowser_mfdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where depositbrowser_mfdate is not null and depositbrowser_status is not null and year(depositbrowser_mfdate)=year(getdate())  and title not like '%z+%'
  group by month(depositbrowser_mfdate)
  order by month(depositbrowser_mfdate) desc;";
   stat_table($query_mf_monthly,"kuvattu","KK");


    echo"</div>\n";
########################################################################################################################################################
    echo "<div style=\"float:left; margin-right:30px;\">";
        echo"<h3 style=\"text-align:center;\">digi-import</h3>\n";
    $query_digi_daily_imports = "
    SELECT convert(varchar(10),digi_importdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,digi_importdate)>convert(date,GETDATE()-31)
 and year(scandate)='2022'
 and title not like '%z+%'
  group by convert(varchar(10),digi_importdate)
  order by convert(varchar(10),digi_importdate) desc;";
   stat_table($query_digi_daily_imports,"importoitu","PVM");

    
        $query_digi_monthly_imports = "
      SELECT month(digi_importdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where digi_importdate is not null and year(digi_importdate)=year(getdate()) and title not like '%z+%'
  group by month(digi_importdate)
  order by month(digi_importdate) desc;";
   stat_table($query_digi_monthly_imports,"importoitu","KK");


    $query_digissa = "
    SELECT year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] 
     where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is not null and title not like '%z+%'
     group by year(issuedate)
     order by year(issuedate) desc";
    $result_digissa = mssql_query($query_digissa);
    if( $result_digissa === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_last_message(), true));
    }
    echo "<table><caption>Digissä vuosittain</caption>\n";
    echo"<tr><th>Vuosi</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_digissa))
        {
            $niteita=$row["niteita"];
            $vuosi=$row["vuosi"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td>$vuosi</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result_digissa);    
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>\n";


    echo"</div>\n";

##Ilmestymisvuosittain lehdet digissä######################################################################################################################################################
    echo "<div style=\"float:left; margin-right:30px;\">";

    echo"<h3 style=\"text-align:center;\">Ilmestymisvuosittain</h3><br>\n";
    digi_yearly_table ('2022','850000',$comellus_arvot2022);
    digi_yearly_table ('2021','850000',$comellus_arvot2021);
    digi_yearly_table ('2020','850000',$comellus_arvot2020);
    digi_yearly_table ('2019','970000',$comellus_arvot2019);
    digi_yearly_table ('2018','900000',$comellus_arvot2019);
    digi_yearly_table ('2017','950000',$comellus_arvot2019);

    echo"</table></div>\n";
    #######################################################################################################################################################

mssql_close($db);
?>

<?php
################################################################################################################
#
# Chart
#
#
###############################################################################################################
?>



<!--
set datefirst 1
SELECT datepart(wk,scandate) as viikko,COUNT(docid) as niteita,SUM(pages) as sivuja
  FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2018'
  group by datepart(wk,scandate)
  order by datepart(wk,scandate) asc;

-->

<script>

var densityCanvas = document.getElementById("myChart");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 18;

var Data2018 = {
  label: '2018',
  data: [<?php echo chart_data("2018","viikko");  ?>],
  backgroundColor: 'rgba(0, 99, 132, 0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'
};

var Data2019 = {
  label: '2019',
  data: [
<?php echo chart_data("2019","viikko");  ?>
],
  backgroundColor: 'rgba(240, 120, 132, 0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};

var Data2020 = {
  label: '2020',
  data: [
<?php echo chart_data("2020","viikko");  ?>
],
  backgroundColor: 'rgba(20, 150, 20, 0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};

var Data2021 = {
  label: '2021',
  data: [
<?php echo chart_data("2021","viikko");  ?>
],
  backgroundColor: 'rgba( 148,0,211,0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};

var Data2022 = {
  label: '2022',
  data: [
<?php echo chart_data("2022","viikko");  ?>
],
  backgroundColor: 'rgba( 0,0,211,0.3)',
  borderWidth: 0,
  yAxisID: "y-axis",
  type:'bar'

};


var Data2018Sum = {
  label: '2018',
  data: [<?php echo chart_data("2018","summa");  ?>],
  borderColor: 'rgba(0, 99, 132, 0.6)',
  borderWidth: 2,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var Data2019Sum = {
  label: '2019',
  data: [
<?php echo chart_data("2019","summa");  ?>
],
  borderColor: 'rgba(240, 120, 132, 0.6)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var Data2020Sum = {
  label: '2020',
  data: [
<?php echo chart_data("2020","summa");  ?>
],
  borderColor: 'rgba(100, 200, 100, 0.6)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var Data2021Sum = {
  label: '2021',
  data: [
<?php echo chart_data("2021","summa");  ?>
],
  borderColor: 'rgba(148,0,211,0.6)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};
var Data2022Sum = {
  label: '2022',
  data: [
<?php echo chart_data("2022","summa");  ?>
],
  borderColor: 'rgba(0,0,211,0.6)',
  borderWidth: 3,
  backgroundColor: 'rgba(255, 255, 255, 0)',
  yAxisID: "y-axis-sum",
  type:'line'

};

var scanData = {
  labels: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53],
  datasets: [Data2018, Data2019, Data2020, Data2021, Data2022, Data2018Sum,Data2019Sum, Data2020Sum, Data2021Sum, Data2022Sum]
};

var chartOptions = {
  legend: {
   display:true,
   position: 'bottom',
   labels: {
	reverse: true
	}
  },
  title: {
    display: true,
    text: 'Karttuvien sanomalehtien skannaustuotanto'
  },
  responsive: false,
  scales: {
    xAxes: [{
      barPercentage: 1,
      categoryPercentage: 0.6,
      scaleLabel: {
      labelString: "viikko",
	display: true
     },
    }],
    yAxes: [{
      id: "y-axis",
      ticks: {
	stepSize: 10000,
      },
      scaleLabel: {
      	labelString: "viikossa",
      	display: true
     	}}, {
      id: "y-axis-sum",
      position: 'right',
      ticks: {
        stepSize: 100000,
      },
      scaleLabel: {
      labelString: "vuodessa",
      display: true
     }
    }]
  }
};

var barChart = new Chart(densityCanvas, {
  type: 'line',
  data: scanData,
  options: chartOptions
});
</script>

</body>
</html>

