<?php
include("sessio.php");
 

?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <title>Inventointi</title>
    <link rel="stylesheet" type="text/css" href="reset.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/jquery.cookie.js"></script>
    <script type="text/javascript" src="/sorttable.js"></script> 
     <script type="text/javascript">
    $(function()
    {
        var piilota = $.cookie("piilotaKorjatut");
        if (piilota=="kylla") {
            $('.korjattu').hide();
        } else if (piilota=="ei") {
            $('.korjattu').show();
        } else {
            $('.korjattu').show();
        }
        
        $('#piilotaNappi').click(function(e) {
            $.cookie('piilotaKorjatut', 'kylla');
            $('.korjattu').hide();
            $('.korjattueven').hide();
            $('.korjattuodd').hide();
            }
        );

        $('#naytaNappi').click(function(e) {
            $.cookie('piilotaKorjatut', 'ei');
            $('.korjattu').show();
            $('.korjattueven').show();
            $('.korjattuodd').show();
        });
        $('#vainhelpNappi').click(function(e) {
            $.cookie('vainHelp', 'kylla');
            $('.korjattu').hide();
            $('.odd').hide();
            $('.even').hide();
            $('.korjattueven').hide();
            $('.korjattuodd').hide();
            $('.tyhjiaeven').hide();
            $('.tyhjiaodd').hide();
        });
            $('#kaikkiNappi').click(function(e) {
            $.cookie('vainHelp', 'ei');
            $.cookie('piilotaKorjatut', 'ei');
            $('.korjattu').show();
            $('.odd').show();
            $('.even').show();
            $('.korjattueven').show();
            $('.korjattuodd').show();
            $('.tyhjiaeven').show();
            $('.tyhjiaodd').show();
        });
});

</script>
<script type="text/javascript">
  function byId(id) {
    return document.getElementById ? document.getElementById(id) : document.all[id];
  }
  var prevLink = "";
  function changeActiveStates(ele) {
    if (prevLink) byId(prevLink).className = "";
    ele.className = 'active';
    prevLink = ele.id;
  }
</script>
<script type="text/javascript">
    function getPages(lomakeFrame,pdfFrame){
    parent.window.lomake.location=lomakeFrame;
    parent.window.pdf.location=pdfFrame;
}

</script>
<style type="text/css">
th, td {
  padding: 3px !important;
}

/* Sortable tables */
table.sortable thead {
    background-color: #333;
    color: #cccccc;
    font-weight: bold;
    cursor: default;
}
th {
  font-size: 100%;
}

</style>


</head>
<body>
<br>
<br>
<br>
<br>

<?php

if (isset($_POST["toiminto"])) $toiminto=$_POST["toiminto"]; else $toiminto="listaa";

if ($toiminto=="listaa") {

    if (isset($_POST["ryhma"])) {
        $ryhma=$_POST["ryhma"];
    } elseif (isset($_GET["ryhma"])) {
        $ryhma=$_GET["ryhma"];
    }
    
     if (isset($_POST["sort"])) {
        $sort=$_POST["sort"];
    } elseif (isset($_GET["sort"])) {
        $sort=$_GET["sort"];
    } else {
        $sort="aikaleima";
    }
    
    $ryhma=urldecode($ryhma);
    $query = "
    SELECT distinct(title)
    FROM dw_nlf_custom.dbo.sl_status";
    $result = sqlsrv_query($dbhandle, $query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    
    echo "<div class=\"buttonx\">
    <input id=\"piilotaNappi\" type=\"button\" value=\"Piilota korjatut\" />
    <input id=\"naytaNappi\" type=\"button\" value=\"Nayta korjatut\" />
    <input id=\"vainhelpNappi\" type=\"button\" value=\"Nayta lisatarkastettavat\" />
    <input id=\"kaikkiNappi\" type=\"button\" value=\"Nayta Kaikki\" />&nbsp;&nbsp;";
    // echo "<a href=\"massakorjaa.php?mdfield=260b\" target=\"_top\">Massakorjaa julkaisijat</a>";
    echo "<form action=\"nidetiedot_nidelista.php\" method=\"post\">";
    echo"<select name=\"ryhma\">";
    echo"<option></option>";
    while($row = sqlsrv_fetch_array($result))
        {
            $niteenryhma=$row["title"];
            $encodedniteenryhma=urlencode($niteenryhma);
            echo "<option value=\"$encodedniteenryhma\" ".($niteenryhma==$ryhma ? 'selected="selected"' : '').">$niteenryhma</option>";
    }
            echo "</select>";
            echo"<input type=\"submit\" value=\"P�ivit�\">";
            echo"<div>J�rjestys:<input type=\"radio\" name=\"sort\" value=\"skannattu\" ".($sort=="skannattu" ? 'checked="checked"' : '').">skannattu - <input type=\"radio\" name=\"sort\" value=\"pvm\" ".($sort=="pvm" ? 'checked="checked"' : '').">pvm - <input type=\"radio\" name=\"sort\" value=\"nimeke\" ".($sort=="nimeke" ? 'checked="checked"' : '').">nimeke - <input type=\"radio\" name=\"sort\" value=\"docid\" ".($sort=="docid" ? 'checked="checked"' : '').">docid</div>";
           
            echo "</form>\n";
    
    
    
    
    $mdfield="";

    
    
    
    echo"</div>";

    
    $query = "
    SELECT  title, docid, issuenum,issn,depositid, pages, scannedby, depositbrowser_errorpages, depositbrowser_comments,depositbrowser_status,digi_status,dw_replaced_by,status_processed,
    convert(varchar,issuedate,104) as issuedate,
    convert(varchar,dw_importdate,120) as dw_importdate,
    convert(varchar,dw_exportdate,120) as dw_exportdate,
    convert(varchar,depositbrowser_statusdate,120) as  depositbrowser_statusdate,
    convert(varchar,depositbrowser_receivedate,120) as depositbrowser_receivedate,
    convert(varchar,depositbrowser_processdate,120) as depositbrowser_processdate,
    convert(varchar,depositbrowser_mfdate,120) as depositbrowser_mfdate,
    convert(varchar,dw_deletedate,120) as dw_deletedate,
    convert(varchar,digi_importdate,120) as digi_importdate
    from dw_nlf_custom.dbo.sl_status where title='$ryhma'";
    if ($sort=="skannattu") {
        $query.="order by dw_importdate";
    } elseif ($sort=="pvm") {
        $query.="order by issuedate, title";
    } elseif ($sort=="nimeke") {
         $query.="order by title,issuedate";
    } elseif ($sort=="docid") {
         $query.="order by docid";
    }
    $result = sqlsrv_query($dbhandle, $query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( sqlsrv_errors(), true));
    }
    
  echo "<table>\n";
$edellinenkokoelma="";
$i=0;
$sivuja=0;
while($row = sqlsrv_fetch_array($result))
    {
      $title=$row["title"];
      $docworksid=$row["docid"];
      $pvm=$row["issuedate"];
      $issn=$row["issn"];
      $pages=$row["pages"];
      $dw_importdate=$row["dw_importdate"];
      $dw_exportdate=$row["dw_exportdate"];
      $depositbrowser_receivedate=$row["depositbrowser_receivedate"];
      $depositbrowser_processdate=$row["depositbrowser_processdate"];
      $depositbrowser_statusdate=$row["depositbrowser_statusdate"];
      $depositbrowser_status=$row["depositbrowser_status"];
      $depositbrowser_mfdate=$row["depositbrowser_mfdate"];
      $digi_importdate=$row["digi_importdate"];
      $marc_available=$row["digi_importdate"];
      $mets_rejected=$row["digi_importdate"];
      $marc_rejected=$row["digi_importdate"];
      $mets_replaced=$row["digi_importdate"];
      $marc_replaced=$row["digi_importdate"];
      $digi_status=$row["digi_status"];
      $marc_url=$row["depositid"];
      $mets_uningested=$row["digi_importdate"];
      $marc_uningested=$row["digi_importdate"];
      $mets_comment=$row["depositbrowser_comments"];
      $marc_comment=$row["depositbrowser_errorpages"];
      $digi_url="http://digi.kansalliskirjasto.fi/sanomalehti/binding/".$digi_status."?page=1";
      if ($edellinenkokoelma!=$title) {
            echo "<tr><th colspan=\"10\" align=\"left\">$title</th></tr>\n";
            echo"<tr><th>no</th><th>dwid</th><th>Nimeke</th><th>PVM</th><th>sivuja</th><th>DW tuonti</th><th>DW export</th><th>COM Import</th><th>COM status</th><th></th><th>MF</th><th>DIGI</th></tr>\n";
     }
    if ($i % 2 == 0) $class="even"; else $class="odd";
    if (!$dw_importdate) $dw_importdate_class="help"; else $dw_importdate_class="korjattuodd";
    if (!$dw_exportdate) $dw_exportdate_class="help"; else $dw_exportdate_class="korjattuodd";
    if (!$depositbrowser_receivedate) $depositbrowser_receivedate_class="help"; else $depositbrowser_receivedate_class="korjattuodd";
    if ($depositbrowser_status=="REJECTED") $depositbrowser_status_class="vika"; else $depositbrowser_status_class="korjattuodd";
    if (!$depositbrowser_mfdate) $depositbrowser_mfdate_class="help"; else $depositbrowser_mfdate_class="korjattuodd";
    if (!$digi_importdate) $digi_importdate_class="help"; else $digi_importdate_class="korjattuodd";
    if (!$marc_ingested) $marc_ingested_class="help"; else $marc_ingested_class="korjattuodd";
    if ($marc_rejected) $marc_rejected_class="vika"; else $marc_rejected_class="";
    echo"<tr class=\"$class\">
    <td>$i</td>
    <td>&nbsp;$docworksid&nbsp;</td>

    <td>$title</td>
    <td>&nbsp;$pvm&nbsp;</td>
    <td>&nbsp;$pages</td>
    <td class=\"$dw_importdate_class\">&nbsp;$dw_importdate&nbsp;</td>
    <td class=\"$dw_exportdate_class\">&nbsp;$dw_exportdate&nbsp;</td>
    <td class=\"$depositbrowser_receivedate_class\">&nbsp;$depositbrowser_receivedate&nbsp;</td>
    <td class=\"$depositbrowser_status_class\">&nbsp;$depositbrowser_statusdate&nbsp;</td>
    <td class=\"$depositbrowser_status_class\">&nbsp;$depositbrowser_status&nbsp;</td>
    <td class=\"$depositbrowser_mfdate_class\">&nbsp;$depositbrowser_mfdate&nbsp;</td>";
    
    echo"<td class=\"$digi_importdate_class\"><a href=\"$digi_url\" target=\"_new\"\">$digi_importdate $digi_status</a></td>
    <td class=\"$mets_rejected_class\">$mets_rejected&nbsp;</td><td>&nbsp;$mets_comment</td></tr>\n";
#    echo"<tr class=\"$class\"><td></td><td></td><td></td><td></td><td></td><td>MARC</td><td class=\"$marc_exported_class\">&nbsp;$marc_exported&nbsp;</td><td class=\"$marc_transfered_class\">&nbsp;$marc_transfered&nbsp;</td><td class=\"$marc_ingested_class\">&nbsp;$marc_ingested&nbsp;</td>";
#    echo"<td class=\"$marc_ingested_class\"><a href=\"$marc_url\" target=\"_new\">$marc_available $marc_url</a></td><td class=\"$marc_rejected_class\">$marc_rejected&nbsp;</td><td>&nbsp;$marc_comment</td></tr>\n";
    $edellinenkokoelma=$title;
    $i+=1;
    $sivuja=$sivuja+$pages;
    }
echo "</table>\n";
echo "<h1>Niteit�:$i, sivuja: $sivuja</h1>";
    //close the connection
}
sqlsrv_close($dbhandle);
?> 
</body>
</html>