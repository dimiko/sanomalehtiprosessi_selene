<?php
include("sessio.php");
 

?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <!-- <meta http-equiv="refresh" content="240" > -->
    <title>Sanomalehtiprosessin nidekohtainen tilanne</title>
    <link rel="stylesheet" type="text/css" href="reset.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="jquery.cookie.js"></script>
    <script type="text/javascript" src="sorttable.js"></script> 
     <script type="text/javascript">
    $(function()
    {
        var piilota = $.cookie("piilotaValmiit");
        if (piilota=="kylla") {
            $('.valmis').hide();
        } else if (piilota=="ei") {
            $('.valmis').show();
        } else {
            $('.valmis').show();
            
        }
        
        $('#piilotaNappi').click(function(e) {
            $.cookie('piilotaValmiit', 'kylla');
            $('.valmis').hide();
            //$('.valmiseven').hide();
            //$('.valmisodd').hide();
            }
        );

        $('#naytaNappi').click(function(e) {
            $.cookie('piilotaValmiit', 'ei');
            $('.valmis').show();
            //$('.valmiseven').show();
            //$('.valmisodd').show();
        });
        $('#vainhelpNappi').click(function(e) {
            $.cookie('vainHelp', 'kylla');
            $('.valmis').hide();
            $('.odd').hide();
            $('.even').hide();
            //$('.valmiseven').hide();
            //$('.valmisodd').hide();
            $('.tyhjiaeven').hide();
            $('.tyhjiaodd').hide();
        });
            $('#kaikkiNappi').click(function(e) {
            $.cookie('vainHelp', 'ei');
            $.cookie('piilotaKorjatut', 'ei');
            $('.valmis').show();
            $('.odd').show();
            $('.even').show();
            //$('.valmiseven').show();
            //$('.valmisodd').show();
            $('.tyhjiaeven').show();
            $('.tyhjiaodd').show();
        });
});

</script>
<script type="text/javascript">
  function byId(id) {
    return document.getElementById ? document.getElementById(id) : document.all[id];
  }
  var prevLink = "";
  function changeActiveStates(ele) {
    if (prevLink) byId(prevLink).className = "";
    ele.className = 'active';
    prevLink = ele.id;
  }
</script>
<script type="text/javascript">
    function getPages(lomakeFrame,pdfFrame){
    parent.window.lomake.location=lomakeFrame;
    parent.window.pdf.location=pdfFrame;
}

</script>
<style type="text/css">
th, td {
  padding: 3px !important;
}


/* Sortable tables */
table.sortable thead {
    background-color: #333;
    color: #cccccc;
    font-weight: bold;
    cursor: default;
}
th {
  font-size: 100%;
  position: sticky;

}

</style>


</head>
<body>
<br>
<br>
<br>
<br>
<br>
<br>

<?php

$toiminto="listaa";

if ($toiminto=="listaa") {

    if (isset($_POST["ryhma"])) {
        $ryhma=$_POST["ryhma"];
    } elseif (isset($_GET["ryhma"])) {
        $ryhma=$_GET["ryhma"];
    } else $ryhma="";
    
     if (isset($_POST["sort"])) {
        $sort=$_POST["sort"];
    } elseif (isset($_GET["sort"])) {
        $sort=$_GET["sort"];
    } else {
        $sort="pvm";
    }
    
    $ryhma=urldecode($ryhma);
    $query = "
    SELECT title, count(docid) as niteita,sum(pages) as sivuja
    FROM dw_nlf_custom.dbo.sl_status
    group by title order by title";
    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    echo "<div class=\"buttonx\">
    <input id=\"piilotaNappi\" type=\"button\" value=\"Piilota valmiit\" />
    <input id=\"naytaNappi\" type=\"button\" value=\"Nayta valmiit\" />
    <!-- <input id=\"vainhelpNappi\" type=\"button\" value=\"Nayta lisatarkastettavat\" /> -->
    <input id=\"kaikkiNappi\" type=\"button\" value=\"Nayta Kaikki\" />&nbsp;&nbsp;";

    
echo"<h3 style=\"text-align:center;\">
<a href=\"index.php\">Karttuvat |
 <a href=\"index_takautuva.php\">Takautuvat</a> |
 <a href=\"niteet.php\">Nidekohtaiset tiedot</a> |
 Ongelmaniteet |
 <a href=\"sl-rdy.txt\">Kansiot ei vielä käsittelyssä</a> |
 <a href=\"/dw-in.txt\">IN kulutukset</a> |
 <a href=\"http://akeso.docworks.lib.helsinki.fi/DepositBrowser/\">DepositBrowser</a></h3>\n";
    echo "<form action=\"niteet.php\" method=\"get\">";

    echo"<select name=\"ryhma\">";
    echo"<option></option>";
    while($row = mssql_fetch_array($result))
        {
            $niteenryhma=$row["title"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            $encodedniteenryhma=urlencode($niteenryhma);
            echo "<option value=\"$encodedniteenryhma\" ".($niteenryhma==$ryhma ? 'selected="selected"' : '').">$niteenryhma ($niteita n. / $sivuja s.)</option>";
    }
            echo "</select>";
            echo"<input type=\"submit\" value=\"Päivitä\">";
            echo" Järjestys:<input type=\"radio\" name=\"sort\" value=\"skannattu\" ".($sort=="skannattu" ? 'checked="checked"' : '').">skannattu - <input type=\"radio\" name=\"sort\" value=\"pvm\" ".($sort=="pvm" ? 'checked="checked"' : '').">pvm - <input type=\"radio\" name=\"sort\" value=\"nimeke\" ".($sort=="nimeke" ? 'checked="checked"' : '').">nimeke - <input type=\"radio\" name=\"sort\" value=\"docid\" ".($sort=="docid" ? 'checked="checked"' : '').">docid - <input type=\"radio\" name=\"sort\" value=\"digi-import\" ".($sort=="digi-import" ? 'checked="checked"' : '').">digi-import";
            echo "</form>\n";
    

    
    
    
    $mdfield="";


    
    
    
    echo"</div>";
    $query_rescans="SELECT P_ARTID as docid,p_filter2 as title FROM [dw_pool].[dbo].[Main] where p_filter3 in ('sl_karttuva','sl_takautuva') and P_JOBNAME='Re-Scan'";
    
    $result_rescans = mssql_query($query_rescans);
    if( $result_rescans === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    $rows=mssql_num_rows($result_rescans);
    if ($rows>0) {
        echo "<table><caption class=\"help\">Uudelleen skannattavat niteet</caption>\n";
        echo"<tr><th>dwid</th><th>Työ</th></tr>";
         while($row = mssql_fetch_array($result_rescans)) {
       $title=$row["title"];
        $docworksid=$row["docid"];
        echo"<tr>";
        echo"<td>$docworksid</td>";
        echo"<td>$title</td></tr>\n";
       }
       echo"</table>\n";
    }
        

    $query = "
    SELECT  title, docid, issuenum,issn,depositid, pages, scannedby, depositbrowser_errorpages, depositbrowser_comments,depositbrowser_status,digi_status,dw_replaced_by,status_processed,
    convert(varchar,issuedate,120) as issuedate,
    convert(varchar,dw_importdate,120) as dw_importdate,
    convert(varchar,dw_exportdate,120) as dw_exportdate,
    convert(varchar,depositbrowser_statusdate,120) as  depositbrowser_statusdate,
    convert(varchar,depositbrowser_receivedate,120) as depositbrowser_receivedate,
    convert(varchar,depositbrowser_processdate,120) as depositbrowser_processdate,
    convert(varchar,depositbrowser_mfdate,120) as depositbrowser_mfdate,
    convert(varchar,dw_deletedate,120) as dw_deletedate,
    convert(varchar,digi_importdate,120) as digi_importdate,
    convert(varchar,scandate,120) as scandate
    from dw_nlf_custom.dbo.sl_status ";
    if ($ryhma!="") {
        $query.=" where title='$ryhma' ";
    } else {
       $query.=" where title is null ";
     }
    if ($sort=="skannattu") {
        $query.="order by scandate";
    } elseif ($sort=="pvm") {
        $query.="order by issuedate desc, title";
    } elseif ($sort=="nimeke") {
         $query.="order by title,issuedate";
    } elseif ($sort=="docid") {
         $query.="order by docid";
    } elseif ($sort=="digi-import") {
         $query.="order by digi_importdate desc";
    }
    #echo "$query";
    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    
  echo "<div style=\"float:none; padding-top:0px;\"><table>";
  //echo "<tr><th colspan=\"10\" align=\"left\">$title</th></tr>\n";
   echo"<tr style=\"position: relative; background-color: white; margin-top: -27px;\"><th>dwid</th><th>Nimeke</th><th>pvm</th><th>no</th><th>sivuja</th><th>skannattu</th><th>DW tuonti</th><th>DW export</th><th>COM Import</th><th>COM status</th><th>DIGI</th><th>Tuotanto digiin</th><th>Viive digiin</th><th>DW delete</th><th>MF date</th></tr>\n";
$edellinenkokoelma="";
$i=0;
$sivuja=0;
while($row = mssql_fetch_array($result))
    {
      $title=$row["title"];
      $docworksid=$row["docid"];
      $pvm=$row["issuedate"];
      $issn=$row["issn"];
      $issuenum=$row["issuenum"];
      $pages=$row["pages"];
      $scandate=$row["scandate"];
      $dw_importdate=$row["dw_importdate"];
      $dw_exportdate=$row["dw_exportdate"];
      $depositbrowser_receivedate=$row["depositbrowser_receivedate"];
      $depositbrowser_processdate=$row["depositbrowser_processdate"];
      $depositbrowser_statusdate=$row["depositbrowser_statusdate"];
      $depositbrowser_status=$row["depositbrowser_status"];
      $depositbrowser_mfdate=$row["depositbrowser_mfdate"];
      $digi_importdate=$row["digi_importdate"];
      $dw_deletedate=$row["dw_deletedate"];
      $digi_status=trim($row["digi_status"]);
      $depositbrowser_comment=$row["depositbrowser_comments"];
      $depositbrowser_errorpages=$row["depositbrowser_errorpages"];
      $digi_url="http://digi.kansalliskirjasto.fi/sanomalehti/binding/".$digi_status."?page=1";
      
     // $dw_deletedate="";
     // $digi_importdate="";
     //  $digi_status="";
     //  $depositbrowser_status="REJECTED";
      if ($scandate=="") $scandate="2017-01-01";
      // if ($digi_importdate=="") $date_end=date("Y-m-d"); else $date_end=$digi_importdate;
      if ($digi_importdate=="") $date_end=date("Y-m-d"); else $date_end=$digi_importdate;
      $startdate=date_create($scandate);
      $enddate=date_create($date_end);
      $productiontime=date_diff($startdate,$enddate)->format("%a");
      
      $pubdate=date_create($pvm);
      $delay=date_diff($pubdate,$enddate)->format("%a");
      
      
    //  if ($edellinenkokoelma!=$title) {
    //        echo "<tr><th colspan=\"10\" align=\"left\">$title</th></tr>\n";
    //        echo"<tr><th>dwid</th><th>Nimeke</th><th>pvm</th><th>no</th><th>sivuja</th><th>skannattu</th><th>DW tuonti</th><th>DW export</th><th>COM Import</th><th>COM status</th><th></th><th>MF</th><th>DIGI</th><th>DW delete</th><th>Tuotannossa (pv)</th></tr>\n";
    // }
    if ($i % 2 == 0) {
            $class="even";
            $rdyclass="valmiseven";
        } else {
            $class="odd";
            $rdyclass="valmisodd";
        }
    //if ($dw_deletedate!="") $class="valmis $rdyclass";
    if ($depositbrowser_mfdate!="" && $digi_importdate!="") $class="valmis mf $rdyclass";
    if ($dw_importdate) $dw_importdate_class=$rdyclass; else $dw_importdate_class="";
    if ($dw_exportdate) $dw_exportdate_class=$rdyclass; else $dw_exportdate_class="";
    if ($depositbrowser_receivedate) $depositbrowser_receivedate_class=$rdyclass; else $depositbrowser_receivedate_class="";
    if ($depositbrowser_status=="REJECTED") $depositbrowser_status_class="vika"; elseif ($depositbrowser_status=="ACCEPTED") $depositbrowser_status_class=$rdyclass ; else $depositbrowser_status_class="";
    if ($depositbrowser_mfdate) $depositbrowser_mfdate_class=$rdyclass; else $depositbrowser_mfdate_class="";
    if ($digi_importdate) $digi_importdate_class=$rdyclass; else $digi_importdate_class="";
    if ($dw_deletedate) $dw_deletedate_class=$rdyclass; else $dw_deletedate_class="";
    
    if ($productiontime<4) $productiontime_class="production_ok";
    if ($productiontime>3) $productiontime_class="production_slow";
    if ($productiontime>7) $productiontime_class="production_veryslow";
    
    
    echo"<tr class=\"$class\">
    <td>&nbsp;$docworksid&nbsp;</td>
    <td>$title</td>
    <td>&nbsp;$pvm&nbsp;</td>
    <td>&nbsp;$issuenum&nbsp;</td>
    <td>&nbsp;$pages</td>
    <td class=\"$rdyclass\">&nbsp;$scandate&nbsp;</td>
    <td class=\"$dw_importdate_class\">&nbsp;$dw_importdate&nbsp;</td>
    <td class=\"$dw_exportdate_class\">&nbsp;$dw_exportdate&nbsp;</td>
    <td class=\"$depositbrowser_receivedate_class\">&nbsp;$depositbrowser_receivedate&nbsp;</td>
    <td class=\"$depositbrowser_status_class tooltip\">$depositbrowser_statusdate";
    if ($depositbrowser_status=="REJECTED") echo "<span class=\"tooltiptext\">$depositbrowser_comment<br>sivut: $depositbrowser_errorpages</span>";
    echo"</td>
    <!-- <td class=\"$depositbrowser_status_class\">&nbsp;$depositbrowser_status&nbsp;</td> -->
    <td class=\"$digi_importdate_class\"><a href=\"$digi_url\" target=\"_new\"\">$digi_importdate</a></td>
    
    
    <td align=\"center\" class=\"$productiontime_class $digi_importdate_class\">&nbsp;$productiontime</td>
    <td class=\"$digi_importdate_class\">$delay</td>
    <td class=\"$dw_deletedate_class\">$dw_deletedate&nbsp;</td>
    <td class=\"$depositbrowser_mfdate_class\">&nbsp;$depositbrowser_mfdate&nbsp;</td></tr>\n";
   $edellinenkokoelma=$title;
    $i+=1;
    $sivuja=$sivuja+$pages;
    }
echo "</table>\n";
echo "<h3>Niteitä:$i, sivuja: $sivuja</h3></div>";
    //close the connection
}
sqlsrv_close($dbhandle);
?> 
</body>
</html>
