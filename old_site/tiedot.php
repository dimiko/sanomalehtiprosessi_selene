<?php
include("sessio.php");
 

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1"/>
    <title>MD korjaus Alfa</title>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <script type="text/javascript" src="/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="/jquery.cookie.js"></script>  

</head>
<div style="width:40%;float:left;">
    <?php
     if (isset($_GET["logunitid"]) && isset($_GET["path"])) { 
        $logunitid=$_GET["logunitid"];
        $path=urldecode($_GET["path"]);
    echo "<table border=\"1\">";
    
// nimeke
        $original="";
        $corrected="";
        $query = "
        SELECT original, corrected
        FROM DW_WORKFLOW.dbo.MDCorrections c
        WHERE c.mdfield='245a' and c.logunitid='$logunitid'";
        $result = sqlsrv_query($dbhandle, $query);
        if( $result === false )
        {
             echo "Error in statement preparation/execution.\n";
             die( print_r( sqlsrv_errors(), true));
        }
       $tuloksia=sqlsrv_has_rows($result);
        //display the results
        $i=0;
        while($row = sqlsrv_fetch_array($result))
            {
                $original=$row["original"];
                $corrected=$row["corrected"];
                echo "<tr colspan=\"2\">";
                echo "<td>";
                echo "<textarea rows=\"2\" cols=\"60\">$corrected</textarea>\n";
                echo "</td></tr>\n";
                $i+=1;
        }

      
// alanimeke
        $original="";
        $corrected="";
        $query2 = "SELECT c.original, c.corrected  FROM DW_WORKFLOW.dbo.MDCorrections c WHERE c.MDfield='245b' and c.logunitid='$logunitid'";
        $result2 = sqlsrv_query($dbhandle, $query2,$params, $options);
        if( $result === false )
        {
             echo "Error in statement preparation/execution.\n";
             die( print_r( sqlsrv_errors(), true));
        }
        $tuloksia=sqlsrv_has_rows($result2);
        //display the results
        $i=0;
        echo "<tr colspan=\"2\">";
        echo "<td><textarea rows=\"3\" cols=\"60\">";
        while($row = sqlsrv_fetch_array($result2))
            {
             
              $original=$row["original"];
              $corrected=$row["corrected"];

            echo "$corrected";
           
            $i+=1;
            }
        echo "</textarea>\n</td></tr>\n";

        
// Julkaisija:
        $original="";
        $corrected="";
        $query = "
        SELECT original, corrected
        FROM DW_WORKFLOW.dbo.MDCorrections c
        WHERE c.mdfield='260b' and c.logunitid='$logunitid'";
        $result = sqlsrv_query($dbhandle, $query);
        if( $result === false )
        {
             echo "Error in statement preparation/execution.\n";
             die( print_r( sqlsrv_errors(), true));
        }
        
        //display the results
        $i=0;
        
        while($row = sqlsrv_fetch_array($result))
            {
              $original=$row["original"];
              $corrected=$row["corrected"];
              
              
            $i+=1;
        }
        echo "<tr colspan=\"2\">";
        echo "<td>$original</td></tr>";
        echo "<tr colspan=\"2\"><td><textarea rows=\"1\" cols=\"60\">$corrected</textarea></td></tr>\n";
        echo "</table>";
        echo "<table border=\"1\">";
        
        $original="";
        $corrected="";
        $query = "
        SELECT original, corrected
        FROM DW_WORKFLOW.dbo.MDCorrections c
        WHERE c.mdfield='260a' and c.logunitid='$logunitid'";
        $result = sqlsrv_query($dbhandle, $query);
        if( $result === false )
        {
             echo "Error in statement preparation/execution.\n";
             die( print_r( sqlsrv_errors(), true));
        }
        
        //display the results
        $i=0;
        while($row = sqlsrv_fetch_array($result))
            {
              $original=$row["original"];
              $corrected=$row["corrected"];
              echo "<tr>";
              echo "<td>Julkaisupaikka</td>";
              echo "<td><textarea rows=\"1\" cols=\"40\">$corrected</textarea></td></tr>\n";
            $i+=1;
        }
// julkaisuaika
        $original="";
        $corrected="";
        $query = "
        SELECT original, corrected
        FROM DW_WORKFLOW.dbo.MDCorrections c
        WHERE c.mdfield='260c' and c.logunitid='$logunitid'";
        $result = sqlsrv_query($dbhandle, $query);
        if( $result === false )
        {
             echo "Error in statement preparation/execution.\n";
             die( print_r( sqlsrv_errors(), true));
        }
        
        //display the results
        $i=0;
        while($row = sqlsrv_fetch_array($result))
            {
              $original=$row["original"];
              $corrected=$row["corrected"];
                          echo "<tr>";
              echo "<td>Julkaisuaika</td>";
              echo "<td><textarea rows=\"1\" cols=\"10\">$corrected</textarea></td></tr>\n";
            $i+=1;
        }

// genre
        $original="";
        $corrected="";
        $query = "
        SELECT original, corrected
        FROM DW_WORKFLOW.dbo.MDCorrections c
        WHERE c.mdfield='655a' and c.logunitid='$logunitid'";
        $result = sqlsrv_query($dbhandle, $query);
        if( $result === false )
        {
             echo "Error in statement preparation/execution.\n";
             die( print_r( sqlsrv_errors(), true));
        }
        
        //display the results
        $i=0;
        while($row = sqlsrv_fetch_array($result))
            {
              $original=$row["original"];
              $corrected=$row["corrected"];
                          echo "<tr>";
              echo "<td>genre</td>";
              echo "<td><textarea rows=\"1\" cols=\"40\">$corrected</textarea></td></tr>\n";
            $i+=1;
        }
        
        
// asiasanat
        $original="";
        $corrected="";
        $query = "
        SELECT original, corrected, authority
        FROM DW_WORKFLOW.dbo.MDCorrections c
        WHERE c.mdfield='650a' and c.logunitid='$logunitid'";
        $result = sqlsrv_query($dbhandle, $query);
        if( $result === false )
        {
             echo "Error in statement preparation/execution.\n";
             die( print_r( sqlsrv_errors(), true));
        }
        
        //display the results
        $i=0;
        while($row = sqlsrv_fetch_array($result))
            {
              $original=$row["original"];
              $corrected=$row["corrected"];
              $authority=$row["authority"];
              echo "<tr>";
              echo "<td>Asiasana ($authority)</td>";
              echo "<td><textarea rows=\"1\" cols=\"40\">$corrected</textarea></td></tr>\n";
            $i+=1;
        }
        
// maantieteelliset asiasanat
        $original="";
        $corrected="";
        $authority="";
        $query = "
        SELECT original, corrected, authority
        FROM DW_WORKFLOW.dbo.MDCorrections c
        WHERE c.mdfield='651a' and c.logunitid='$logunitid'";
        $result = sqlsrv_query($dbhandle, $query);
        if( $result === false )
        {
             echo "Error in statement preparation/execution.\n";
             die( print_r( sqlsrv_errors(), true));
        }
        
        //display the results
        $i=0;
        while($row = sqlsrv_fetch_array($result))
            {
              $corrected=$row["corrected"];
              $authority=$row["authority"];
              echo "<tr>";
              echo "<td>Maantieteellinen asiasana ($authority)</td>";
              echo "<td><textarea rows=\"1\" cols=\"40\">$corrected</textarea></td></tr>\n";
            $i+=1;
        }        
        
      echo "</table>";  
    }
//close the connection
sqlsrv_close($dbhandle);
?>
</div>
    <?php

   if (isset($_GET["path"])) {
    $path=$_GET["path"];   
    
    echo "<div style=\"float:right;\">";
    echo "<iframe src=\"".$path."#pagemode=none\" width=\"800\" height=\"1000\" style=\"position:absolute;right:0;top:0;\">\n";
    echo "</div>";

    }
?>

</html>