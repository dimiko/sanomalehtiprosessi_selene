<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_karttuvat.php");
include("global_variables.php");
?>


<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <title>Arc PAS yhteenveto</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
<style>
    table {
        border-collapse: collapse;
    }
    td {
        padding: 8px;
        border: 1px solid #dddddd;
        text-align: right; /* Aligning content to the right */
    }

    th {
        padding: 8px;
	padding-right:20px;
	padding-left:20px;
        border: 1px solid #dddddd;
        text-align: center; /* Aligning content to the right */
    }
    tr:nth-child(even) {
        background-color: #f2f2f2; /* Light gray background for even rows */
    }

    table tr:first-of-type {
        font-weight: bold; /* Make the first row bold */
    }

   thead th {
        position: sticky;
        top: 0;
        background-color: white; /* Ensure the background color is white to make it visible */
    }

   caption {
 	margin-top:20px;
	margin-bottom:20px;
   }

</style>

</head>
<body>

<?php

directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);

$query = "SELECT TO_CHAR(last_refresh_date, 'HH24:MI DD.MM.YYYY') AS last_refresh_time
          FROM user_mviews
          WHERE mview_name = 'DIGI_PAS_SUMMARY'";
$statement = oci_parse($conn_diona, $query);
oci_execute($statement);
while ($row = oci_fetch_assoc($statement)) {
    $paivitetty=$row['LAST_REFRESH_TIME'];
}

naviHeader();


echo"<div>";
echo"<p style='text-align: center; margin-top: 10px;'>$paivitetty</p>";
$select="select * from digi_pas_summary order by 1 desc";

$stid2 = oci_parse($conn_diona, $select);
$res1=oci_execute($stid2);
        if (!$res1) {
                echo "error:".oci_error()."--";
                exit(9);
        }
echo "<table style=\"margin: 0 auto;\"><caption>Arc PAS yhteenveto. Kaikki aineistolajit.</caption>";
echo "<thead>";
echo "<tr>
<th>ARC</th>
<th>Zippejä ARCilla</th>
<th>Valmis %</th>
<th>Aloittamatta</th>
<th>Ohitettu</th>
<th>Zip kateissa</th>
<th>Muunnettavana</th>
<th>Hyväksytty</th>
<th>Hylätty</th>
<th>Säilytyksessä</th>
</tr></thead>";
echo "<tbody>";
$i=0;

while (($row = oci_fetch_array($stid2, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
        $arc=$row['ARC'];
        $aloittamatta=number_format($row['ALOITTAMATTA'],0,""," ");
        $ohitettu=number_format($row['OHITETTU'],0,""," ");
        $zip_kateissa=number_format($row['ZIP_KATEISSA'],0,""," ");
        $muunnettavana=number_format($row['MUUNNETTAVANA'],0,""," ");
        $hyvaksytty=number_format($row['HYVAKSYTTY'],0,""," ");
        $hylatty=number_format($row['HYLATTY'],0,""," ");
        $sailytyksessa=number_format($row['SAILYTYKSESSA'],0,""," ");
        $yhteensa=number_format($row['YHTEENSA'],0,""," ");
        $valmis=$row['VALMIS'];
echo "<tr>
<td>$arc</td>
<td>$yhteensa</td>
<td>$valmis</td>
<td>$aloittamatta</td>
<td>$ohitettu</td>
<td>$zip_kateissa</td>
<td>$muunnettavana</td>
<td>$hyvaksytty</td>
<td>$hylatty</td>
<td>$sailytyksessa</td>
</tr>";

}
echo "</tbody>";
echo "<tfoot>";
echo "<tr>
<th>ARC</th>
<th>Zippejä ARCilla</th>
<th>Valmis %</th>
<th>Aloittamatta</th>
<th>Ohitettu</th>
<th>Zip kateissa</th>
<th>Muunnettavana</th>
<th>Hyväksytty</th>
<th>Hylätty</th>
<th>Säilytyksessä</th>
</tr></tfoot>";


echo "</table>";

        oci_free_statement($stid2);

echo"</div>";

close_sql($db);
oci_close($conn);
oci_close($conn_diona);

?>
</body>
</html>

