<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_niteet.php");
include("global_variables.php");
//include("javascript/testi.php");

// Tarkistetaan, että POST-metodi on käytössä
if ($_SERVER["REQUEST_METHOD"] == "GET") {
    // Otetaan vastaan lähetetyt arvot ja tallennetaan ne muuttujiin
    $title = $_GET["title"];
    $year = $_GET["year"];

} else {
    // Jos lomaketta ei ole lähetetty POST-metodilla, tulostetaan virheviesti
    echo "Virhe: Lomaketta ei ole lähetetty oikealla metodilla.";
}

?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <title>Sanomalehtiprosessiin jumittuneet niteet</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css?v1">
    <link rel="stylesheet" type="text/css" href="css/style.css?v1">
    <link rel="stylesheet" type="text/css" href="css/style2.css?v1">
    <link rel="stylesheet" type="text/css" href="css/confirmation.css?v1">
    <script type="text/javascript" src="javascript/javaScripts.js?v1"></script>
<style type="text/css">
th, td {
  padding: 3px !important;
}

</style>

</head>
<body>
<?php
directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);
docIdSearch(basename(__FILE__), $db);
naviHeader();

$formattedTitle = generateFormattedString($title);
$issn = getIssnByTitle($title);
echo "<button class='addVolumeProblems-button' onclick=\"openPopup($year, 1, 1, '$issn', '$formattedTitle')\">Lisää vuosikerralle puute</button>";

$query_volume_by_year = getVolumeByYearQuery($title, $year);

//problem_documents ($db, $query_volume_by_year ,"Niteen $title vuosikerta $year","ongelmat",$DW_directorys, $DW_directorys_alert_space);
niteet_listing($db, $query_volume_by_year);
include("acknowledgment_messages.php");
//problem_documents ($db, $query);

close_sql($db);
oci_close($conn);
oci_close($conn_diona);
?>

</body>
</html>