<?php
$queryAccepted = "select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status]
        where CONVERT(date,depositbrowser_statusdate)=convert(date,GETDATE())
        and (depositbrowser_status='ACCEPTED' or depositbrowser_status='REJECTED') and dw_deletedate is null and title not like '%z+%'";
                    
$queryExportted = "select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status]
        where CONVERT(date,dw_exportdate)=convert(date,GETDATE()) and dw_deletedate is null and title not like '%z+%'";
                    
$queryRejected = "select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status]
        where CONVERT(date,depositbrowser_statusdate)=convert(date,GETDATE())
        and depositbrowser_status='REJECTED' and dw_deletedate is null and title not like '%z+%'";
                    
$queryFilmed="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status]
        where CONVERT(date,depositbrowser_mfdate)=convert(date,GETDATE()) and title not like '%z+%'";
                    
$queryImported = "select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status]
        where CONVERT(date,digi_importdate)=convert(date,GETDATE()) and dw_deletedate is null and title not like '%z+%'";
                    
$queryDeleted="select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status]
        where CONVERT(date,dw_deletedate)=convert(date,GETDATE()) and title not like '%z+%'";
                    
$query_rescans = "SELECT P_ARTID as docid,p_filter2 as title, p_comment as comment FROM [dw_pool].[dbo].[Main]
        where p_filter3 in ('sl_karttuva') and P_JOBNAME='Re-Scan'";
        
$query_dw_process =" SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,case 
when  M.P_JOBNAME='PrepareImport' then 'Tuonti'
when  M.P_JOBNAME='Import' then 'Tuonti'
when  M.P_JOBNAME='Scan' then 'Tuonti'
when  M.P_JOBNAME='DetectPageFrames' then 'Rajaus'
when  M.P_JOBNAME='VerifyPageFrames' then 'Rajaus'
when  M.P_JOBNAME='ModifyPages' then 'Rajaus'  
when  M.P_JOBNAME='VerifyPages' then 'Rajaus'
when  M.P_JOBNAME='DetectLayoutElements' then 'Palstoitus'
when  M.P_JOBNAME='VerifyLayoutElements' then 'Palstoitus'
when  M.P_JOBNAME='DetectPageNumbers' then 'OCR'
when  M.P_JOBNAME='VerifyPageNumbers' then 'OCR'
when  M.P_JOBNAME='BuildPagesHierarchy' then 'Hierarkia'  
when  M.P_JOBNAME='VerifyPagesHierarchy' then 'Hierarkia'
when  M.P_JOBNAME='BuildHierarchy' then 'Hierarkia' 
when  M.P_JOBNAME='VerifyHierarchy' then 'Hierarkia'
when  M.P_JOBNAME='Export' then 'Export'
when  M.P_JOBNAME='ExportXML' then 'Export'  
else M.P_JOBNAME end as vaihe,COUNT(*) as niteita, sum(pages) as sivuja
FROM [dw_nlf_custom].[dbo].[sl_status] s, dw_pool.dbo.main m
where s.dw_importdate is not null and s.dw_exportdate is null and s.dw_replaced_by is null and s.title not like '%z+%' and s.docid=m.P_ARTID
group by  SUBSTRING(title,0, CHARINDEX('+',title)),title, case
        when  M.P_JOBNAME='PrepareImport' then 'Tuonti'
        when  M.P_JOBNAME='Import' then 'Tuonti'
        when  M.P_JOBNAME='Scan' then 'Tuonti'
        when  M.P_JOBNAME='DetectPageFrames' then 'Rajaus'
        when  M.P_JOBNAME='VerifyPageFrames' then 'Rajaus'
        when  M.P_JOBNAME='ModifyPages' then 'Rajaus'
        when  M.P_JOBNAME='VerifyPages' then 'Rajaus'
        when  M.P_JOBNAME='DetectLayoutElements' then 'Palstoitus'
        when  M.P_JOBNAME='VerifyLayoutElements' then 'Palstoitus'
        when  M.P_JOBNAME='DetectPageNumbers' then 'OCR'
        when  M.P_JOBNAME='VerifyPageNumbers' then 'OCR'
        when  M.P_JOBNAME='BuildPagesHierarchy' then 'Hierarkia'
        when  M.P_JOBNAME='VerifyPagesHierarchy' then 'Hierarkia'
        when  M.P_JOBNAME='BuildHierarchy' then 'Hierarkia'
        when  M.P_JOBNAME='VerifyHierarchy' then 'Hierarkia'
        when  M.P_JOBNAME='Export' then 'Export'
        when  M.P_JOBNAME='ExportXML' then 'Export'
        else M.P_JOBNAME end 
order by 	1,2 ";  

$query_dw_error="SELECT SUBSTRING(P_FILTER2,CHARINDEX('\',P_FILTER2) + 1, LEN(P_FILTER2)) as nimeke, P_FILTER2 as fullnimeke, format(P_DATE,'yyyy-MM-dd') as issuedate,
IIF(p_status = 'E', 'Virhe', 'Hylätty') as p_status, P_ARTID as docId, P_COMMENT as comment, P_JOBNAME as workflow
  FROM [dW_Pool].[dbo].[Main]
  where P_STATUS in ('E', 'R')
  AND P_JOBNAME != 'Re-Scan'
  AND P_FILTER3 = 'sl_karttuva'
  order by p_status desc, issuedate";
/*
$query_dw_error="SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke, format(issuedate,'yyyy-MM-dd') as issuedate,p_status
    FROM [dw_nlf_custom].[dbo].[sl_status] s, dw_pool.dbo.main m
    where dw_importdate is not null and dw_exportdate is null and dw_replaced_by is null and s.docid=m.P_ARTID and p_status not in ('W','R') and title not like '%z+%'
    order by title,issuedate";
*/
    
$query_dw_rejected="SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,format (issuedate,'YYYY-MM-DD') as issuedate, p_status, P_ARTID as docId
    FROM [dw_nlf_custom].[dbo].[sl_status] s, dw_pool.dbo.main m
    where dw_importdate is not null and dw_exportdate is null and dw_replaced_by is null and s.docid=m.P_ARTID and p_status='R' and p_jobname!='Re-Scan'  and s.title not like '%z+%'
    order by title,issuedate";
    
$query_dw_ready = "SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_importdate is not null
    and dw_exportdate is not null
    and depositbrowser_receivedate is null
    and title in (select distinct title from [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is not null)  and title not like '%z+%'
    group by SUBSTRING(title,0, CHARINDEX('+',title)),title
    order by title";
    
$query_noudettavat = "SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_exportdate is not null and depositbrowser_receivedate is null and dw_deletedate is null
    and title not in (select distinct title from [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is null)  and title not like '%z+%'
    group by SUBSTRING(title,0, CHARINDEX('+',title)),title
    order by title";
    
$query_tarkastettavat = "SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_exportdate is not null and depositbrowser_receivedate is not null and (depositbrowser_status not in ('ACCEPTED','PURGED') or depositbrowser_status is null) and dw_deletedate is null and title not like '%z+%'
    group by SUBSTRING(title,0, CHARINDEX('+',title)),title
    order by title";
    
$query_hylatyt = "SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where depositbrowser_status='REJECTED' and dw_deletedate is null and title not like '%z+%'
    group by SUBSTRING(title,0, CHARINDEX('+',title)),title
    order by title"; 
 
$query_hyvaksytyt = "SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja, format(max(issuedate),'yyyy-MM-dd') as uusin
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_exportdate is not null and depositbrowser_receivedate is not null and (depositbrowser_status='ACCEPTED') and depositbrowser_mfdate is null and title not like '%z+%'
    group by SUBSTRING(title,0, CHARINDEX('+',title)),title
    having sum(pages)<1000
    order by title";
    
$query_kuvattavat = "SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as  nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja,format(max(issuedate),'yyyy-MM-dd')  as uusin
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and depositbrowser_mfdate is null  and title not like '%z+%'
    and title not in (select distinct title from [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is null or depositbrowser_receivedate is null or depositbrowser_status is null or depositbrowser_status='REJECTED')
    group by SUBSTRING(title,0, CHARINDEX('+',title)),title
    having sum(pages)>999
    order by sum(pages) desc";
    
$query_importoitavat = "SELECT title as nimeke, title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is null and dw_deletedate is null and datediff(day,depositbrowser_statusdate,GETDATE())>=1 and title not like '%z+%'
    group by title
    order by title";
    
$query_daily_scans = "SELECT convert(varchar(10),scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,scandate)>convert(date,GETDATE()-31)  
    and year(scandate)='2024'
    and title not like '%z+%'
    group by convert(varchar(10),scandate)
    order by convert(varchar(10),scandate) desc";
    
$query_monthly_scans = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)=year(getdate()) 
    and title not like '%z+%'
    group by month(scandate)
    order by month(scandate) desc";
    
$query_monthly_scans_2024 = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2024'  and title not like '%z+%'
    group by month(scandate)
    order by month(scandate) desc";

$query_monthly_scans_2023 = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2023'  and title not like '%z+%'
    group by month(scandate)
    order by month(scandate) desc";
    
$query_monthly_scans_2022 = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2022'  and title not like '%z+%'
    group by month(scandate)
    order by month(scandate) desc";
    
$query_monthly_scans_2021 = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2021'  and title not like '%z+%'
    group by month(scandate)
    order by month(scandate) desc";
    
$query_monthly_scans_2020 = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2020'  and title not like '%z+%'
    group by month(scandate)
    order by month(scandate) desc";
    
$query_monthly_scans_2019 = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2019'  and title not like '%z+%'
    group by month(scandate)
    order by month(scandate) desc";
    
$query_monthly_scans_2018 = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2018'  and title not like '%z+%'
    group by month(scandate)
    order by month(scandate) desc";
    
$query_dw_daily_exports = "SELECT convert(varchar(10),dw_exportdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_exportdate)>convert(date,GETDATE()-31)  
    and year(scandate)='2023'
    and title not like '%z+%'
    group by convert(varchar(10),dw_exportdate)
    order by convert(varchar(10),dw_exportdate) desc";
    
$query_dw_monthly_exports = "SELECT month(dw_exportdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is not null and year(dw_exportdate)=year(getdate())  and title not like '%z+%'
    group by month(dw_exportdate)
    order by month(dw_exportdate) desc";
    
$query_depositbrowser_daily = "SELECT top 7 convert(varchar(10),depositbrowser_statusdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_statusdate)>convert(date,GETDATE()-31) 
    and year(scandate)='2023'
    and depositbrowser_status is not null and datepart(dw,depositbrowser_statusdate) in (2,3,4,5,6)  and title not like '%z+%'
    group by convert(varchar(10),depositbrowser_statusdate)
    order by convert(varchar(10),depositbrowser_statusdate) desc;";
    
$query_depositbrowser_monthly = "SELECT month(depositbrowser_statusdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where depositbrowser_statusdate is not null and depositbrowser_status is not null and year(depositbrowser_statusdate)=year(getdate())  and title not like '%z+%'
    group by month(depositbrowser_statusdate)
    order by month(depositbrowser_statusdate) desc;";
    
$query_mf_daily = "SELECT top 7 convert(varchar(10),depositbrowser_mfdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_mfdate)>convert(date,GETDATE()-31)
    and year(scandate)='2023'
    and depositbrowser_status is not null  and title not like '%z+%'
    group by convert(varchar(10),depositbrowser_mfdate)
    order by convert(varchar(10),depositbrowser_mfdate) desc";
    
$query_mf_monthly = "SELECT month(depositbrowser_mfdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where depositbrowser_mfdate is not null and depositbrowser_status is not null and year(depositbrowser_mfdate)=year(getdate())  and title not like '%z+%'
    group by month(depositbrowser_mfdate)
    order by month(depositbrowser_mfdate) desc";
    
$query_digi_daily_imports = "SELECT convert(varchar(10),digi_importdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,digi_importdate)>convert(date,GETDATE()-31)
    and year(scandate)='2023'
    and title not like '%z+%'
    group by convert(varchar(10),digi_importdate)
    order by convert(varchar(10),digi_importdate) desc";
    
$query_digi_monthly_imports = "SELECT month(digi_importdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where digi_importdate is not null and year(digi_importdate)=year(getdate()) and title not like '%z+%'
    group by month(digi_importdate)
    order by month(digi_importdate) desc";
    
$query_digissa = "SELECT year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is not null and title not like '%z+%'
    group by year(issuedate)
    order by year(issuedate) desc";
      

    
?>
