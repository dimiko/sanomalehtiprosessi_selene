<?php
$query_export = "select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status]
    where CONVERT(date,dw_exportdate)=convert(date,GETDATE()) and dw_deletedate is null and title like '%z+%'";
    
$query_accepted = "select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status]
    where CONVERT(date,depositbrowser_statusdate)=convert(date,GETDATE()) and (depositbrowser_status='ACCEPTED'
    or depositbrowser_status='REJECTED') and dw_deletedate is null and title like '%z+%'";

$query_rejected = "select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status]
    where CONVERT(date,depositbrowser_statusdate)=convert(date,GETDATE()) and depositbrowser_status='REJECTED'
    and dw_deletedate is null and title like '%z+%'";
    
$query_imported = "select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status]
    where CONVERT(date,digi_importdate)=convert(date,GETDATE()) and dw_deletedate is null and title like '%z+%'";
    
$query_deleted = "select sum(pages) as sivuja from [dw_nlf_custom].[dbo].[sl_status]
    where CONVERT(date,dw_deletedate)=convert(date,GETDATE()) and title like '%z+%'";
    
$query_rescans = "SELECT 
    P_ARTID AS docid,
    SUBSTRING(
        p_filter2, 
        CHARINDEX('\', p_filter2) + 1, 
        CHARINDEX('\', p_filter2, CHARINDEX('\', p_filter2) + 1) - CHARINDEX('\', p_filter2) - 1
    ) AS title,
    p_comment AS comment
FROM 
    [dw_pool].[dbo].[Main]
WHERE 
    p_filter3 IN ('sl_karttuva', 'sl_takautuva')
    AND P_JOBNAME = 'Re-Scan'";
    
$query_dw = "SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,case
                when  M.P_JOBNAME='PrepareImport' then 'Tuonti'
                when  M.P_JOBNAME='Import' then 'Tuonti'
                when  M.P_JOBNAME='Scan' then 'Tuonti'
                when  M.P_JOBNAME='DetectPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='ModifyPages' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPages' then 'Rajaus'
                when  M.P_JOBNAME='DetectLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='VerifyLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='DetectPageNumbers' then 'OCR'
                when  M.P_JOBNAME='VerifyPageNumbers' then 'OCR'
                when  M.P_JOBNAME='BuildPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='BuildHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='Export' then 'Export'
                when  M.P_JOBNAME='ExportXML' then 'Export'
                else M.P_JOBNAME end as vaihe,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] s, dw_pool.dbo.main m
     where s.dw_importdate is not null and s.dw_exportdate is null and s.dw_replaced_by is null and s.title like '%z+%' and s.docid=m.P_ARTID and m.P_JOBNAME!='Exported'
     group by  SUBSTRING(title,0, CHARINDEX('+',title)),title, year(issuedate),case
                when  M.P_JOBNAME='PrepareImport' then 'Tuonti'
                when  M.P_JOBNAME='Import' then 'Tuonti'
                when  M.P_JOBNAME='Scan' then 'Tuonti'
                when  M.P_JOBNAME='DetectPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='ModifyPages' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPages' then 'Rajaus'
                when  M.P_JOBNAME='DetectLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='VerifyLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='DetectPageNumbers' then 'OCR'
                when  M.P_JOBNAME='VerifyPageNumbers' then 'OCR'
                when  M.P_JOBNAME='BuildPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='BuildHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='Export' then 'Export'
                when  M.P_JOBNAME='ExportXML' then 'Export'
                else M.P_JOBNAME end
     order by   1,2,3,4";
     
$query_importing_process_old = "SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,case
                when  M.P_JOBNAME='PrepareImport' then 'Tuonti'
                when  M.P_JOBNAME='Import' then 'Tuonti'
                when  M.P_JOBNAME='Scan' then 'Tuonti'
                else M.P_JOBNAME end as vaihe,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] s, dw_pool.dbo.main m
     where s.dw_importdate is not null and s.dw_exportdate is null and s.dw_replaced_by is null and s.title like '%z+%' and s.docid=m.P_ARTID
     and m.P_JOBNAME in ('PrepareImport', 'Import', 'Scan', 'Re-Scan')
     group by  SUBSTRING(title,0, CHARINDEX('+',title)),title, year(issuedate),case
                when  M.P_JOBNAME='PrepareImport' then 'Tuonti'
                when  M.P_JOBNAME='Import' then 'Tuonti'
                when  M.P_JOBNAME='Scan' then 'Tuonti'
                else M.P_JOBNAME end
     order by   1,2,3,4";
$query_importing_process = "SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,case
                when  M.P_JOBNAME='PrepareImport' then 'Tuonti'
                when  M.P_JOBNAME='Import' then 'Tuonti'
                when  M.P_JOBNAME='Scan' then 'Tuonti'
                when  M.P_JOBNAME='DetectPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='ModifyPages' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPages' then 'Rajaus'
                when  M.P_JOBNAME='DetectLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='VerifyLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='DetectPageNumbers' then 'OCR'
                when  M.P_JOBNAME='VerifyPageNumbers' then 'OCR'
                when  M.P_JOBNAME='BuildPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='BuildHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='Export' then 'Export'
                when  M.P_JOBNAME='ExportXML' then 'Export'
                else M.P_JOBNAME end as vaihe,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] s, dw_pool.dbo.main m
     where s.[status] = 'processing' and s.dw_importdate is not null and s.dw_exportdate is null and s.dw_replaced_by is null and s.docid=m.P_ARTID
     and m.P_JOBNAME in ('DetectPageFrames', 'VerifyPageFrames', 'ModifyPages', 'VerifyPages', 'DetectLayoutElements', 'VerifyLayoutElements',
     'DetectPageNumbers', 'VerifyPageNumbers', 'BuildPagesHierarchy', 'VerifyPagesHierarchy', 'BuildHierarchy', 'VerifyHierarchy', 'Export', 'ExportXML')
     group by  SUBSTRING(title,0, CHARINDEX('+',title)),title, year(issuedate),case
                when  M.P_JOBNAME='PrepareImport' then 'Tuonti'
                when  M.P_JOBNAME='Import' then 'Tuonti'
                when  M.P_JOBNAME='Scan' then 'Tuonti'
                when  M.P_JOBNAME='DetectPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='ModifyPages' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPages' then 'Rajaus'
                when  M.P_JOBNAME='DetectLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='VerifyLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='DetectPageNumbers' then 'OCR'
                when  M.P_JOBNAME='VerifyPageNumbers' then 'OCR'
                when  M.P_JOBNAME='BuildPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='BuildHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='Export' then 'Export'
                when  M.P_JOBNAME='ExportXML' then 'Export'
                else M.P_JOBNAME end
     order by   1,2,3,4";
     
$query_exporting_process = "SELECT SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,case
                when  M.P_JOBNAME='DetectPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='ModifyPages' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPages' then 'Rajaus'
                when  M.P_JOBNAME='DetectLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='VerifyLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='DetectPageNumbers' then 'OCR'
                when  M.P_JOBNAME='VerifyPageNumbers' then 'OCR'
                when  M.P_JOBNAME='BuildPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='BuildHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='Export' then 'Export'
                when  M.P_JOBNAME='ExportXML' then 'Export'
                else M.P_JOBNAME end as vaihe,COUNT(*) as niteita, sum(pages) as sivuja
     FROM [dw_nlf_custom].[dbo].[sl_status] s, dw_pool.dbo.main m
     where s.dw_importdate is not null and s.dw_exportdate is null and s.dw_replaced_by is null and s.title like '%z+%' and s.docid=m.P_ARTID
     and m.P_JOBNAME in ('DetectPageFrames', 'VerifyPageFrames', 'ModifyPages', 'VerifyPages', 'DetectLayoutElements', 'VerifyLayoutElements',
     'DetectPageNumbers', 'VerifyPageNumbers', 'BuildPagesHierarchy', 'VerifyPagesHierarchy', 'BuildHierarchy', 'VerifyHierarchy', 'Export', 'ExportXML')
     group by  SUBSTRING(title,0, CHARINDEX('+',title)),title, year(issuedate),case
                when  M.P_JOBNAME='DetectPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPageFrames' then 'Rajaus'
                when  M.P_JOBNAME='ModifyPages' then 'Rajaus'
                when  M.P_JOBNAME='VerifyPages' then 'Rajaus'
                when  M.P_JOBNAME='DetectLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='VerifyLayoutElements' then 'Palstoitus'
                when  M.P_JOBNAME='DetectPageNumbers' then 'OCR'
                when  M.P_JOBNAME='VerifyPageNumbers' then 'OCR'
                when  M.P_JOBNAME='BuildPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyPagesHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='BuildHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='VerifyHierarchy' then 'Hierarkia'
                when  M.P_JOBNAME='Export' then 'Export'
                when  M.P_JOBNAME='ExportXML' then 'Export'
                else M.P_JOBNAME end
     order by   1,2,3,4";

$query_dw_error = "SELECT 
    SUBSTRING(
        P_FILTER2, 
        CHARINDEX('\', P_FILTER2) + 1, 
        CHARINDEX('\', P_FILTER2, CHARINDEX('\', P_FILTER2) + 1) - CHARINDEX('\', P_FILTER2) - 1
    ) AS nimeke,
    P_FILTER2 AS fullnimeke,
    FORMAT(P_DATE, 'yyyy-MM-dd') AS issuedate,
    CASE 
        WHEN p_status = 'E' THEN 'Virhe'
        WHEN p_status = 'R' THEN 'Hylätty'
        WHEN p_status = 'V' THEN 'Satunnaistarkistus'
    END AS p_status,
    P_ARTID AS docId,
    P_JOBNAME AS workflow
FROM 
    [dW_Pool].[dbo].[Main]
WHERE 
    P_STATUS IN ('E', 'R', 'V')
    AND P_JOBNAME = 'Re-Scan'
    AND P_FILTER3 IN ('sl_karttuva', 'sl_takautuva')
ORDER BY 
    p_status DESC, issuedate;";
    
$query_dw_reject = "SELECT SUBSTRING(title,0, CHARINDEX('z',title)) as nimeke,format(issuedate,'yyyy-MM-dd') as issuedate,p_status,docid
    FROM [dw_nlf_custom].[dbo].[sl_status] s, dw_pool.dbo.main m
    where dw_importdate is not null and dw_exportdate is null and dw_replaced_by is null and s.docid=m.P_ARTID and p_status='R' and p_jobname!='Re-Scan'  and s.title like '%z+%'
    order by 1,2 ";

$query_valmis = "SELECT issn,SUBSTRING(title,0, CHARINDEX('z',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_importdate is not null
    and dw_exportdate is not null
    and depositbrowser_receivedate is null
    and title in (select distinct title from [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is not null)
    and title like '%z+%'
    /* FIXME ss 3.4.2024  status
    and [status] in ('processing', 'hold', 'completed')*/
    and [status] in ('processing')
    group by issn,SUBSTRING(title,0, CHARINDEX('z',title)),title, year(issuedate)
    order by title,issn,year(issuedate) desc";

$query_noudettavat = "SELECT issn,SUBSTRING(title,0, CHARINDEX('z',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_exportdate is not null and depositbrowser_receivedate is null and dw_deletedate is null
    -- and concat(title,year(issuedate)) not in (select distinct concat(title,year(issuedate)) from [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is null)
    and title like '%z+%'
    and [status] not in ('QA', 'hold', 'digi_importing', 'digi', 'accepted', 'rescan')
    group by issn,SUBSTRING(title,0, CHARINDEX('z',title)),title, year(issuedate)
    order by title,issn,year(issuedate) desc";
    
$query_inspection_state = "SELECT issn,SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where [status] = 'QA'
    and dw_deletedate is null
    group by issn,SUBSTRING(title,0, CHARINDEX('+',title)),title, year(issuedate)
    order by title,issn,year(issuedate) desc";
    
$query_inspection_state_HOX = "SELECT issn,SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where [status] = 'QA'
    and dw_exportdate is not null
    and dw_deletedate is null
    group by issn,SUBSTRING(title,0, CHARINDEX('+',title)),title, year(issuedate)
    order by title,issn,year(issuedate) desc";

    
$query_tarkastettavat = "SELECT issn,SUBSTRING(title,0, CHARINDEX('z',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_exportdate is not null and depositbrowser_receivedate is not null and (depositbrowser_status not in ('ACCEPTED','PURGED') or depositbrowser_status is null) and dw_deletedate is null
    and title like '%z+%'
    group by issn,SUBSTRING(title,0, CHARINDEX('z',title)),title, year(issuedate)
    order by title,issn,year(issuedate) desc";
    
$query_hylatyt = "SELECT issn,SUBSTRING(title,0, CHARINDEX('z',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where depositbrowser_status='REJECTED' and dw_deletedate is null
    and title like '%z+%'
    group by issn,SUBSTRING(title,0, CHARINDEX('z',title)),title, year(issuedate)
    order by title,issn,year(issuedate) desc";
    
$query_importoitavat = "SELECT issn,SUBSTRING(title,0, CHARINDEX('+',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja, 'siirrossa digiin' as vaihe
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_exportdate is not null
    and status in ('accepted', 'digi_importing') 
    group by issn,SUBSTRING(title,0, CHARINDEX('+',title)),title, year(issuedate)
    order by title,issn,year(issuedate) desc";
    
$query_hyvaksytyt = "SELECT SUBSTRING(title,0, CHARINDEX('z',title)) as nimeke,title as fullnimeke,COUNT(*) as niteita, sum(pages) as sivuja, format(min(issuedate),'yyyy-MM-dd') as vanhin,format(max(issuedate),'yyyy-MM-dd') as uusin
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_exportdate is not null and depositbrowser_receivedate is not null and (depositbrowser_status='ACCEPTED')
    and title like '%z+%'
    group by SUBSTRING(title,0, CHARINDEX('z',title)),title
    order by title";
    
$query_digissa = "SELECT issn,SUBSTRING(title,0, CHARINDEX('z',title)) as nimeke,title as fullnimeke,year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] 
    where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is not null
    and title like '%z+%'
    group by issn,SUBSTRING(title,0, CHARINDEX('z',title)),title, year(issuedate)
    order by title desc,issn,year(issuedate) desc";
    
$query_daily_scans = "SELECT convert(varchar(10),scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,scandate)>convert(date,GETDATE()-31)
    and title like '%z+%'
    and year(scandate)='2024'
    group by convert(varchar(10),scandate)
    order by convert(varchar(10),scandate) desc;";
    
$query_monthly_scans_2024 = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2024'
    and title like '%z+%'
    group by month(scandate)
    order by month(scandate) desc;";

$query_monthly_scans_2023 = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2023'
    and title like '%z+%'
    group by month(scandate)
    order by month(scandate) desc;";

$query_monthly_scans_2022 = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2022'
    and title like '%z+%'
    group by month(scandate)
    order by month(scandate) desc;";
    
$query_monthly_scans_2021 = "SELECT month(scandate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='2021'
    and title like '%z+%'
    group by month(scandate)
    order by month(scandate) desc;";
    
$query_dw_daily_exports = "SELECT convert(varchar(10),dw_exportdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,dw_exportdate)>convert(date,GETDATE()-31)
    and year(scandate)='2023'
    and title like '%z+%'
    group by convert(varchar(10),dw_exportdate)
    order by convert(varchar(10),dw_exportdate) desc;";
    
$query_dw_monthly_exports = "SELECT month(dw_exportdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where dw_exportdate is not null and year(dw_exportdate)=year(getdate())
    and title like '%z+%'
    group by month(dw_exportdate)
    order by month(dw_exportdate) desc;";
    
$query_depositbrowser_daily = "SELECT top 7 convert(varchar(10),depositbrowser_statusdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,depositbrowser_statusdate)>convert(date,GETDATE()-31) and depositbrowser_status is not null and datepart(dw,depositbrowser_statusdate) in (2,3,4,5,6)
    and year(scandate)='2023' 
    and title like '%z+%'
    group by convert(varchar(10),depositbrowser_statusdate)
    order by convert(varchar(10),depositbrowser_statusdate) desc;";
    
$query_depositbrowser_monthly = "SELECT month(depositbrowser_statusdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where depositbrowser_statusdate is not null and depositbrowser_status is not null and year(depositbrowser_statusdate)=year(getdate())
    and title like '%z+%'
    group by month(depositbrowser_statusdate)
    order by month(depositbrowser_statusdate) desc;";
    
$query_digi_daily_imports = "SELECT convert(varchar(10),digi_importdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where CONVERT(date,digi_importdate)>convert(date,GETDATE()-31)
    and year(scandate)='2023' 
    and title like '%z+%'
    group by convert(varchar(10),digi_importdate)
    order by convert(varchar(10),digi_importdate) desc;";
    
$query_digi_monthly_imports = "SELECT month(digi_importdate) as pvm,COUNT(docid) as niteita,SUM(pages) as sivuja
    FROM [dw_nlf_custom].[dbo].[sl_status] where digi_importdate is not null and year(digi_importdate)=year(getdate())
    and title like '%z+%'
    group by month(digi_importdate)
    order by month(digi_importdate) desc;";

?>
