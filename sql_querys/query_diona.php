<?php

$query_digital_issues = "select paanimeke as nimeke,to_char(julkaisu_pvm,'YYYY') as vuosi,count(distinct n.nide_id) as niteita, count(sivu_id) as sivuja, kl.issn as issn
from nide n, kausi_lehti kl, sivu s
where n.tunnus=kl.issn and n.alkupera='d' and n.saantitapa='s' and n.nide_id=s.nide_id and
n.tunnus in (
'0785-398X'
,'fk20714'
,'0355-9149'
,'fk11035'
,'0782-7105'
,'fk11010'
,'fk10001'
,'0789-8274'
,'0356-164X'
,'fk20223'
,'0358-6294'
,'0357-1548'
,'0780-8704'
,'0789-8193'
,'1458-0780'
,'0356-0724'
,'0355-2055'
,'0782-5528'
,'1235-8355'
,'fk14622'
,'fk14981'
,'0780-5063'
,'fk20861'
,'fk11407'
,'fk20365'
,'0023-8015'
,'fk15549'
,'0786-2644'
,'fk11668'
,'fk23821'
,'fk16887'
,'0359-1441'
,'fk25016'
,'fk11688'
,'0356-3553'
,'1458-8803'
,'0356-5653'
,'1235-8398'
,'0785-3998'
,'fk11800'
,'fk11989'
,'0355-9440'
,'fk17113'
,'0789-838X'
,'0358-2434'
,'0789-8363'
,'0355-5461'
,'0356-1844'
,'fk16278'
,'1235-8460'
,'0782-6559')
group by paanimeke,to_char(julkaisu_pvm,'YYYY'), kl.issn
order by 1 asc, 2 desc";

$query_all_processing_zip = "select count(*) as muunnettavia from nide where aineistoyleismaare in ('AIK','SAN')";


function insertVolumeShortage($conn, $nideId, $tiedostoId, $status, $uri, $issnIsNotNumber = 0, $nideHasProblem = 0, $nbn = null, $softwareNameVersion = null) {
   $zipNotFoundQuery = "insert into pas_status (nide_id, tiedosto_id, pas_status, nbn, software_name_and_version, issn_is_not_number, nide_has_problem, uri)
                    values ($nideId, $tiedostoId, '$status', '$nbn', '$softwareNameVersion', $issnIsNotNumber, $nideHasProblem, '$uri')";
   $zpiNotFoundStatementId = ociParseQuery($conn, $zipNotFoundQuery);
   @executeOciQuery($zpiNotFoundStatementId, $zipNotFoundQuery);
   oci_free_statement($zpiNotFoundStatementId);
}

?>