<?php

$query_option = "SELECT title, count(docid) as niteita,sum(pages) as sivuja
    FROM dw_nlf_custom.dbo.sl_status group by title order by title";
    
$query_rescans = "SELECT P_ARTID as docid,p_filter2 as title FROM [dw_pool].[dbo].[Main]
    where p_filter3 in ('sl_karttuva','sl_takautuva') and P_JOBNAME='Re-Scan'";

$query_sort = "SELECT title, docid, issuenum, issn, depositid, pages, QA_error_pages as depositbrowser_errorpages, QA_error_comment as depositbrowser_comments,
[status] as depositbrowser_status, digi_status, dw_replaced_by, status_processed,
convert(varchar, issuedate,120) as issuedate,
convert(varchar,dw_importdate,120) as dw_importdate,
convert(varchar,dw_exportdate,120) as dw_exportdate,
convert(varchar, accepted_datetime, 23) as depositbrowser_statusdate,
convert(varchar,accepted_datetime,23) as depositbrowser_receivedate,
convert(varchar,accepted_datetime,23) as depositbrowser_processdate,
convert(varchar,depositbrowser_mfdate,120) as depositbrowser_mfdate,
convert(varchar,dw_deletedate,120) as dw_deletedate,
convert(varchar,digi_importdate,120) as digi_importdate,
convert(varchar,scandate,120) as scandate, restart, remove
from dw_nlf_custom.dbo.sl_status ";

$query_problem_docs = "SELECT  datediff(day,scandate,GETDATE()) as kesto,
  title, issn, docid, m.p_artid as pool, m.p_jobname as jobname, m.p_status as status,
  issuenum, issn, depositid, pages, QA_error_pages as depositbrowser_errorpages, QA_error_comment as depositbrowser_comments,
  status as depositbrowser_status, digi_status, dw_replaced_by, status_processed,
  convert(varchar,issuedate,120) as issuedate,
  convert(varchar,dw_importdate,120) as dw_importdate,
  convert(varchar,dw_exportdate,120) as dw_exportdate,
  convert(varchar,m.p_date,120) as dw_date,
convert(varchar, accepted_datetime, 23) as accepted_datetime,
  convert(varchar,depositbrowser_mfdate,120) as depositbrowser_mfdate,
  convert(varchar,dw_deletedate,120) as dw_deletedate,
  convert(varchar,digi_importdate,120) as digi_importdate,
  convert(varchar,scandate,120) as scandate, s.id,
  s.restart as restart_status,
  s.remove as remove_status
  from dw_nlf_custom.dbo.sl_status s
  left outer join dw_pool.dbo.main m on (m.p_artid=s.docid)
  where digi_importdate is null
  and datediff(day,scandate,GETDATE())>4
  and docid not in (
select docid
from dw_nlf_custom.dbo.sl_status 
where 
 digi_importdate is null
 and datediff(day,issuedate,GETDATE())<28
 and depositbrowser_status='ACCEPTED'
) order by 1 desc,2,issuedate";

$query_embargo_docs = "SELECT 
28-datediff(day,issuedate,GETDATE()) as kesto
,title
,issn
,docid
,m.p_artid as pool
,m.p_jobname as jobname
,m.p_status as status
,issuenum
,issn
,depositid
,pages
,QA_error_pages as depositbrowser_errorpages
,QA_error_comment as depositbrowser_comments
,status as depositbrowser_status
,digi_status
,dw_replaced_by
,status_processed
,convert(varchar,issuedate,120) as issuedate
,convert(varchar,dw_importdate,120) as dw_importdate
,convert(varchar,dw_exportdate,120) as dw_exportdate
,convert(varchar,m.p_date,120) as dw_date
,convert(varchar, accepted_datetime, 23) as accepted_datetime
,convert(varchar,depositbrowser_mfdate,120) as depositbrowser_mfdate
,convert(varchar,dw_deletedate,120) as dw_deletedate
,convert(varchar,digi_importdate,120) as digi_importdate
,convert(varchar,scandate,120) as scandate
,s.id
,s.restart as restart_status
,s.remove as remove_status
from dw_nlf_custom.dbo.sl_status s
left outer join dw_pool.dbo.main m on (m.p_artid=s.docid)
where digi_importdate is null
and datediff(day,issuedate,GETDATE())<28
and depositbrowser_status='ACCEPTED'
order by issuedate asc,2";

function getVolumeByYearQuery($title, $year) {
    
    $query_volume_by_year = "SELECT  datediff(day,scandate,GETDATE()) as kesto,
    title, issn, docid, m.p_artid as pool, m.p_jobname as jobname, m.p_status as status,
    issuenum, issn, depositid, pages, QA_error_pages as depositbrowser_errorpages, QA_error_comment as depositbrowser_comments,
    status as depositbrowser_status, digi_status, dw_replaced_by, status_processed,
    convert(varchar,issuedate,120) as issuedate,
    convert(varchar,dw_importdate,120) as dw_importdate,
    convert(varchar,dw_exportdate,120) as dw_exportdate,
    convert(varchar,m.p_date,120) as dw_date,
    convert(varchar, accepted_datetime, 23) as accepted_datetime,
    convert(varchar,depositbrowser_mfdate,120) as depositbrowser_mfdate,
    convert(varchar,dw_deletedate,120) as dw_deletedate,
    convert(varchar,digi_importdate,120) as digi_importdate,
    convert(varchar,scandate,120) as scandate, s.id,
    s.restart as restart_status,
    s.remove as remove_status
    from dw_nlf_custom.dbo.sl_status s
    left outer join dw_pool.dbo.main m on (m.p_artid=s.docid)
    where title = '$title'
    and SUBSTRING(CONVERT(VARCHAR(10), issuedate, 120), 1, 4) = '$year'
    order by 1 desc,2,issuedate";
    
    return $query_volume_by_year;
}


?>
