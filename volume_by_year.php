<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_karttuvat.php");
include("global_variables.php");

    // Tarkistetaan, että POST-metodi on käytössä
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Otetaan vastaan lähetetyt arvot ja tallennetaan ne muuttujiin
        $title = $_POST["title"];
        $year = $_POST["year"];
        $page = $_POST["page"];
        
        // Tulostetaan "Puutteet lisätty" ruudulle ja lisätään Sulje ruutu -nappi
        $_SESSION['issnTitle'] = $title;
        
        if(isset($title) && isset($year)) {
            unset($_SESSION['issnTitle']);
            $encoded_param_value = urlencode($title);
            header("Location: volume_year_listing.php?title=$encoded_param_value&year=$year");
            exit();
        } else {
            header("Location: $page");
            exit();
        }
    
    } else {
        // Jos lomaketta ei ole lähetetty POST-metodilla, tulostetaan virheviesti
        echo "Virhe: Lomaketta ei ole lähetetty oikealla metodilla.";
    }

?>

