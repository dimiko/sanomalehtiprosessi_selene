

<?php
ini_set('memory_limit', '512M'); 
include("sessio.php");
include("functions.php");
include("sql_querys/querys_karttuvat.php");
include("global_variables.php");
$dbUsername = getenv('DB_USERNAME');
$dbPassword = getenv('DB_PASSWORD');
$connectionString = getenv('DB_CONNECTION_STRING');

if (!$dbUsername || !$dbPassword || !$connectionString) {
    #die("Error: Database credentials are not properly set in environment variables.\n");
}

#$conn = oci_connect($dbUsername, $dbPassword, $connectionString, 'AL32UTF8');
$conn=oci_connect('diona', 'diona', '//192.168.10.10:1521/dionapdb.docworks.lib.helsinki.fi', 'AL32UTF8');

if (!$conn) {
    $e = oci_error();
    file_put_contents('db_errors.log', date('[Y-m-d H:i:s] ') . $e['message'], FILE_APPEND);
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
    exit("Database connection error, execution stopped.\n");
}
?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>Vuosikertojen tarkistus</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
</head>
<style>
table tr:nth-child(even) {background-color: #f9f9f9;}
table tr:nth-child(odd) {background-color: #ffffff;}

</style>
<body>

<?php
directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space);
docIdSearch(basename(__FILE__), $db);

        $eraStyle="";
        $eraFileCount=countFiles("/mnt/sl_import/era/*.zip");
        if ($eraFileCount>40) {
         $eraStyle="background-color:red; color: white;";
         $era="$eraFileCount Zip";

        } elseif ($eraFileCount==0) {
          $era="";
        } else {
        $era="$eraFileCount Zip";
        }

        echo"<div class='dailyReportFrame'>";
        echo"<h3>Tänään</h3>";
        $export_sivuja = dailyReport($db, $queryExportted);
        $accepted_sivuja = dailyReport($db, $queryAccepted);
        $rejected_sivuja = dailyReport($db, $queryRejected);
        $importoitu_sivuja = dailyReport($db, $queryImported);
        $deletoitu_sivuja = dailyReport($db, $queryDeleted);

        echo"<table class='dailyTable'>";
        echo"<tr style='text-align: center;'>";
        echo "<th>Exportoitu&nbsp;</th>";
        echo "<th>Tarkastettu&nbsp;</th>";
        echo "<th>Importoitumassa&nbsp;</th>";
        echo "<th>Importoitu&nbsp;</th>";
        echo "<th>Poistettu</th>";
        echo"<tr style='text-align: center;'>";
        echo "<td>$export_sivuja</td>";
        echo "<td>$accepted_sivuja</td>";
        echo "<td style='$eraStyle'>$era</td>";
        echo "<td>$importoitu_sivuja</td>";
        echo "<td>$deletoitu_sivuja</td>";
        echo"</tr>";
        echo "</table>";

echo"</div>";

naviHeader();
?>

<?php
function signalHandler($signal) {
    switch ($signal) {
        case SIGINT:
            echo "Script execution stopped by user.\n";
            exit;
    }
}

if (function_exists('pcntl_signal')) {
    pcntl_signal(SIGINT, "signalHandler");
} else {
    #echo "PCNTL functions are not available on this system.\n";
}

// Function to extract numeric parts from an issue number and to handle textual parts
function extractNumeric($issueNum) {
    if (preg_match('/(\d+)/', $issueNum, $matches)) {
        return (int)$matches[0];
    }
    return 0;  // Return 0 if no numeric part is found
}

// Function to parse issue number ranges into individual numbers
function expandIssueRange($issueRange) {
    $expanded = [];
    foreach (explode(',', $issueRange) as $part) {
        if (strpos($part, '-') !== false) {
            list($start, $end) = explode('-', $part);
            $expanded = array_merge($expanded, range($start, $end));
        } else {
            $expanded[] = $part;
        }
    }
    return $expanded;
}

function sortIssuesWithText($a, $b) {
    $numA = extractNumeric($a['issuenum']);
    $numB = extractNumeric($b['issuenum']);
    if ($numA === $numB) {
        return strcmp($a['issuenum'], $b['issuenum']); // Sort textually if numbers are the same
    } else if ($numA < $numB) {
        return -1;  // Return -1 if $numA is less than $numB
    } else {
        return 1;   // Return 1 if $numA is greater than $numB
    }
}

function expandIssues($issues) {
    $expandedIssues = [];
    foreach ($issues as $issue) {
        if (strpos($issue, '-') !== false) {
            list($start, $end) = explode('-', $issue);
            $expandedIssues = array_merge($expandedIssues, range($start, $end));
        } else {
            $expandedIssues[] = $issue;
        }
    }
    return $expandedIssues;
}

function containsNumberStart($array) {
    foreach ($array as $item) {
        // Check if the item starts with a digit
        if (preg_match('/^\d/', $item)) {
            return true;
        }
    }
    return false;
}


function containsNumericIssues($tempIssues) {
    foreach ($tempIssues as $item) {
        // Check if the 'issuenum' starts with a digit or is entirely numeric
        if (preg_match('/^\d+/', $item['issuenum'])) {
            return true;
        }
    }
    return false;
}

function issueExistsInTempIssues($issue, $tempIssues) {
    foreach ($tempIssues as $tempIssue) {
        // Expand the range if the current issuenum is in a range format
        $currentIssueNumbers = explode('-', $tempIssue['issuenum']);
        if (count($currentIssueNumbers) > 1) {
            $range = range($currentIssueNumbers[0], $currentIssueNumbers[1]);
            if (in_array($issue, $range)) {
                return true;
            }
        } elseif ($tempIssue['issuenum'] == $issue) {
            // Direct comparison if not a range
            return true;
        }
    }
    return false;
}


if (isset($_POST["issn"])) {
    $selectedIssn=$_POST["issn"];
  } elseif (isset($_GET["issn"])) {
    $selectedIssn=$_GET["issn"];
  } else { $selectedIssn="";
}



echo "<div style=\"text-align: center; margin-top:20px;\"><h1>Sanomalehtien vuosikertojen tarkistus</h1>";

echo "<p style='margin-top:10px;'><span style='color:grey;'>Numero</span>&nbsp;&nbsp;|&nbsp;&nbsp;Tekstiä numerossa&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:Green; font-weight:bold;'>kaksi samaa nro:a </span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:Blue; font-weight:bold;'>Merkitty puute/puuteväli</span>&nbsp;&nbsp;|&nbsp;&nbsp;
<span style='color:red;'>Havaittu puute</span></p></div>";


$missingIssuesQuery = "SELECT issn, TO_CHAR(pvm,'YYYY') AS year, REPLACE(no,' ','') AS issuenum FROM nidepuutteet";
    $missingStid = oci_parse($conn, $missingIssuesQuery);
    oci_execute($missingStid);

    $manualMissing = [];
    while ($row = oci_fetch_assoc($missingStid)) {
        $key = $row['ISSN'] . '|' . $row['YEAR'];
        foreach (expandIssueRange($row['ISSUENUM']) as $issue) {
            $manualMissing[$key][$issue] = true;
        }
    }

    oci_free_statement($missingStid);


                    
                    
 $query_yearRange = "SELECT ISSN,START_YEAR,END_YEAR
from nimeke_vuosikerta_tarkastus
where type='SAN'
order by title, issn";                   

$stid_yearRange = oci_parse($conn, $query_yearRange);
oci_execute($stid_yearRange);

$issnYearRange = [];
while ($row = oci_fetch_assoc($stid_yearRange)) {
    $issn = $row['ISSN'];
    $issnYearRange[$issn] = ["start" => $row['START_YEAR'], "end" => $row['END_YEAR']];
}

// Main query to fetch publication data
$query = "SELECT kl.paanimeke AS title, sn.issn AS issn, TO_CHAR(julkaisu_pvm,'YYYY') AS year, nvl(no,'##') AS issuenum
FROM nide n
JOIN sarjajulkaisu_nide sn ON n.nide_id = sn.sarjanide_id
JOIN kausi_lehti kl ON kl.issn = sn.issn
JOIN niteet_joilla_sivuja njs on njs.nide_id=n.nide_id
where n.aineistoyleismaare='SAN' 
ORDER BY title, issn, year, julkaisu_pvm, issuenum";

$stid = oci_parse($conn, $query);
oci_execute($stid);

$results = [];
$allYears = [];
$issnTitles = [];

// Collect all data and prepare a structure to verify years per ISSN
while ($row = oci_fetch_assoc($stid)) {
    $key = $row['TITLE'] . '|' . $row['YEAR'] . '|' . $row['ISSN'];
    $results[$key][] = $row['ISSUENUM'];
    $allYears[$row['ISSN']][$row['YEAR']] = true;  // Track years we have data for each ISSN
    $issnTitles[$row['ISSN']] = $row['TITLE']; 
}
// Iterate through each ISSN year range
foreach ($issnYearRange as $issn => $years) {
    $start = $years["start"];
    $end = $years["end"];
    
    for ($year = $start; $year <= $end; $year++) {
        if (!isset($allYears[$issn][$year])) {  // Check if the year is missing in data
            #$title = ) ? $issnTitles[$issn] : "Unknown Title";
            if (isset($issnTitles[$issn])) {
                $title = $issnTitles[$issn];
                $key = "$title|$year|$issn";  // Using a placeholder for title or determine a method to find the title
                $results[$key][] = "Vuosikerta puuttuu";  // Notate the missing volume
                
            } else {
                continue;
            }
            
        }
    }
}
ksort($results);

$lastIssues = [];
foreach ($results as $key => $issues) {
    list($title, $year, $issn) = explode('|', $key);
    if (!isset($lastIssues[$issn])) {
        $lastIssues[$issn] = [];
    }
    $lastIssues[$issn][$year] = end($issues); // Assuming $issues is already sorted
}



echo "<table border='1' style='margin-left:auto; margin-right:auto;margin-top:20px'>";
echo "<tr><th>Nimeke</th><th>ISSN</th><th>Vuosi</th><th>Numerot</th></tr>";

foreach ($results as $key => $issues) {
    list($title, $year, $issn) = explode('|', $key);

    $expandedIssues = expandIssues($issues);
    $issueComponents = array_map('extractNumeric', $issues);
    $issueComponents2 = array_map('extractNumeric', $expandedIssues);
    $uniqueIssues = array_unique($issueComponents);
    
    // Check if only contains "Vuosikerta puuttuu"
    if (count($issues) == 1 && in_array("Vuosikerta puuttuu", $issues)) {

        #echo "<tr><td>". htmlspecialchars($title) ."</td><td>". htmlspecialchars($issn) ."</td><td>" . htmlspecialchars($year) . "</td><td style='color:orange;'>Vuosikerta puuttuu</td></tr>";
        continue; // Skip to the next iteration of the foreach loop
    }
    
    sort($uniqueIssues);

    $previousYear = $year - 1;
    $prevLastIssue = isset($lastIssues[$issn][$previousYear]) ? $lastIssues[$issn][$previousYear] : 0;
    $firstIssueCurrentYear = reset($uniqueIssues);
    
    $rangeStart = $prevLastIssue + 1 == $firstIssueCurrentYear ? $firstIssueCurrentYear : 1;
    $rangeEnd = max($uniqueIssues);
    $completeRange = range($rangeStart, $rangeEnd);
    
    $tempIssues = [];
    
     // Add manually missing issues
    if (isset($manualMissing[$issn . '|' . $year])) {
        foreach ($manualMissing[$issn . '|' . $year] as $issue => $flag) {
            if (!in_array($issue, $uniqueIssues)) {
            $tempIssues[] = ['issuenum' => $issue, 'status' => 'manuallymissing'];
            }
        }
    } 

    // Include existing issues first
    foreach ($issues as $issue) {
        $tempIssues[] = ['issuenum' => $issue, 'status' => 'ok'];
    }


    #$numericIssues = array_filter($issueComponents, 'is_numeric');
    if (containsNumericIssues($tempIssues)) {
            // Identify and add detected missing issues
           foreach ($completeRange as $issue) {
              // Ensure that manualMissing array for the specific ISSN and year is checked properly
              $isManuallyMissing = isset($manualMissing[$issn . '|' . $year]) && array_key_exists((string)$issue, $manualMissing[$issn . '|' . $year]);
              if (!issueExistsInTempIssues($issue, $tempIssues) && !$isManuallyMissing ) {
                  $tempIssues[] = ['issuenum' => $issue, 'status' => 'detectedmissing'];
              }
          }
    }

    // Sort all issues by numeric and textual content
    usort($tempIssues, 'sortIssuesWithText');

    // Generate HTML output for the issue list
    $issueList = [];
    $prevIssueDisplay = null;  // Initialize to ensure it is set before use.
    $showYear=false;
foreach ($tempIssues as $tempIssue) {
    $issueDisplay = htmlspecialchars($tempIssue['issuenum']);
    switch ($tempIssue['status']) {
        case 'ok':
            if (is_numeric($issueDisplay)) {
                if ($prevIssueDisplay != $issueDisplay) {
                    $issueList[] = "<span style='color:LightGrey;'>$issueDisplay</span>";
                } else {
                    $issueList[] = "<span style='color:Green; font-weight:bold;'>$issueDisplay</span>";
                    $showYear=true;
                }
            } else {
                if ($prevIssueDisplay != $issueDisplay) {
                    $issueList[] = "<span style='color:Grey;'>$issueDisplay</span>";
                } else {
                    $issueList[] = "<span style='color:Green; font-weight:bold;'>$issueDisplay</span>";
                    $showYear=true;
                }
            }
            break;
        case 'detectedmissing':
            $issueList[] = "<span style='color:red;'>$issueDisplay</span>";
            $showYear=true;
            break;
        case 'manuallymissing':
            if ($issueDisplay != "") {
                $issueList[] = "<span style='color:Blue; font-weight:bold;'>$issueDisplay</span>";
                $showYear=true;
            } else {
                $issueList[] = "<span style='color:Blue; font-weight:bold;'>??</span>";
                $showYear=true;
            }
            break;
        default:
            $issueList[] = "<span style='color:blue;'>??</span>"; // For unexpected or empty cases
            $showYear=true;
            break;
    }
    $prevIssueDisplay = $issueDisplay; // Update the previous display for the next iteration.
}
    if ($showYear) {
      echo "<tr><td>". htmlspecialchars($title) ."</td><td>". htmlspecialchars($issn) ."</td><td><a href='https://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".htmlspecialchars($year)."' target='_blank'>" . htmlspecialchars($year) . "</a></td>";
      echo "<td><a style='all: unset; cursor:pointer;' href='https://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".htmlspecialchars($year)."' target='_blank'>" . implode(', ', $issueList) . "</a></td></tr>";
    }
}

echo "</table>";

echo "</body></html>";

oci_free_statement($stid);
oci_close($conn);

?>
