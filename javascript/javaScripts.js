$('head').append('<link rel="stylesheet" type="text/css" href="css/style2.css">');
//$('head').append('<link rel="stylesheet" type="text/css" href="css/confirmation.css?v1">');

    function displayIssinumListing() {
        var x = document.getElementById("myDIV");
        if (x.style.display === "block") {
            x.style.display = "none";
        } else {
            x.style.display = "block";
        }
    }


function openPopup(vuosi, day, month, issn, nimeke) {
    const dropdownOptions = [
        { value: 'MISSING', text: 'Nide puuttuu' },
        { value: 'NOT_IN_COLLECTIONS', text: 'Nide ei löydy kokoelmista' },
        { value: 'OTHER', text: 'Muu huomio' },
        { value: 'NOT_PUBLISHED', text: 'Lehteä ei ole ilmestynyt tänä päivänä' },
        { value: 'TO_BE_SCANNED_FROM_ORIGINAL', text: 'Skannataan myöhemmin alkuperäisestä' },
        { value: 'DIGITISATION_ANOMALY', text: 'Digitointipoikkeama' },
        { value: 'ISSUE_NUMBERING_ANOMALY', text: 'Numerointipoikkeama' },
        { value: 'NOT_PUBLISHED_IN_PRINT', text: 'Lehti ei ole ilmestynyt painettuna' },
    ];

    const dropdownOptionsPvmAccuracy = [
        { value: '', text: 'Valitse' },
        { value: 'v', text: 'V' },
        { value: 'k', text: 'KK' },
        { value: 'p', text: 'P' },
    ];

    function createDropdownOptions(options, selectedValue) {
        return options.map(option => 
            `<option value="${option.value}" ${option.value === selectedValue ? 'selected' : ''}>${option.text}</option>`
        ).join('');
    }

    const dayOptions = [{ value: '1', text: 'pp' }, ...Array.from({ length: 31 }, (_, i) => ({
        value: i + 1,
        text: i + 1
    }))];

    const monthOptions = [{ value: '1', text: 'kk' }, ...Array.from({ length: 12 }, (_, i) => ({
        value: i + 1,
        text: i + 1
    }))];

    const popupContent = `
        <form id='popupForm' action='confirmation_UI/processVolumeLogs.php' method='post'>
            <div style='margin-top: 50px;'>
                <h2 style='text-align: center;'>Lisää nidepuute</h2>
                <table class='nidePuutteetPopup' align='center' style='width: 80%;'>
                    <tr>
                        <td colspan='2'>
                            <input type='text' style='width: 100%;' value='${nimeke}' disabled>
                        </td>
                    </tr>
                    <input type='text' name='nimeke' value='${nimeke}' hidden>
                    <input type='text' name='issn' value='${issn}' hidden>
                </table>
                
                <table class='nidePuutteetPopup' align='center' style='width: 80%;'>
                    <tr>
                        <th style='text-align: left;'>PVM tarkkuus:</th>
                        <th style='text-align: left;'>PVM:</th>
                        <th style='text-align: left;'>NUMERO:</th>
                    </tr>
                    
                    <tr>
                        <td><select name='pvmAccuracy' id='pvmAccuracy' onchange='toggleDateFields()'>${createDropdownOptions(dropdownOptionsPvmAccuracy, '')}</select></td>
                        <td>
                            <select name='day' id='daySelect' disabled>${createDropdownOptions(dayOptions, '')}</select>
                            <select name='month' id='monthSelect' disabled>${createDropdownOptions(monthOptions, '')}</select>
                            -<input type='text' maxlength='4' style='width: 40px;' placeholder='${vuosi}' disabled>
                            <input type='text' name='year' value='${vuosi}' hidden>
                            <input type='hidden' id='hiddenDay' name='day' value='1'>
                            <input type='hidden' id='hiddenMonth' name='month' value='1'>
                        </td>
                        <td><input type='text' maxlength='6' size='6' class='inputNumero' name='numero'></td>
                    </tr>
                </table>
                
                <table class='nidePuutteetPopup' align='center' style='width: 80%;'>
                    <tr>
                        <th style='text-align: left;'>Tyyppi:</th><th></th><th></th>
                    </tr>
                    <tr>
                        <td><select name='tyyppi'>${createDropdownOptions(dropdownOptions)}</select></td><td></td><td></td>
                    </tr>
                    <tr>
                        <th style='text-align: left;'>Selitys:</th>
                    </tr>
                    <tr>
                        <td colspan='2'><input type='text' style='width: 100%;' name='selitys'></td>
                    </tr>
                    <tr>
                        <td colspan='2'><input type='submit' id='submitBtn' value='Lähetä' style='display: none;'></td>
                    </tr>
                </table>
            </div>
        </form>
        <script>
            function toggleSubmitButton() {
                const select = document.getElementById('pvmAccuracy');
                const submitBtn = document.getElementById('submitBtn');
                if (select.value !== '') {
                    submitBtn.style.display = 'block';
                } else {
                    submitBtn.style.display = 'none';
                }
            }

            function toggleDateFields() {
                const accuracySelect = document.getElementById('pvmAccuracy');
                const daySelect = document.getElementById('daySelect');
                const monthSelect = document.getElementById('monthSelect');
                const hiddenDay = document.getElementById('hiddenDay');
                const hiddenMonth = document.getElementById('hiddenMonth');

                switch (accuracySelect.value) {
                    case 'v':
                        daySelect.disabled = true;
                        monthSelect.disabled = true;
                        hiddenDay.value = '1';
                        hiddenMonth.value = '1';
                        break;
                    case 'k':
                        daySelect.disabled = true;
                        monthSelect.disabled = false;
                        hiddenDay.value = '1';
                        break;
                    case 'p':
                        daySelect.disabled = false;
                        monthSelect.disabled = false;
                        hiddenDay.value = daySelect.value;
                        hiddenMonth.value = monthSelect.value;
                        break;
                    default:
                        daySelect.disabled = true;
                        monthSelect.disabled = true;
                        hiddenDay.value = '1';
                        hiddenMonth.value = '1';
                        break;
                }
            }

            document.getElementById('pvmAccuracy').addEventListener('change', function() {
                toggleSubmitButton();
                toggleDateFields();
            });

            document.getElementById('daySelect').addEventListener('change', function() {
                document.getElementById('hiddenDay').value = this.value;
            });

            document.getElementById('monthSelect').addEventListener('change', function() {
                document.getElementById('hiddenMonth').value = this.value;
            });

        </script>`;

    const popupWindow = window.open("", "_blank", "width=700,height=450");
    popupWindow.document.write(`
        <html>
            <head>
                <title>Puuttuvat tiedot</title>
                <style>body { margin: 0; }</style>
            </head>
            <body>${popupContent}</body>
        </html>
    `);
    popupWindow.document.close();
}




function openPopupOLD(vuosi, day, month, issn, nimeke) {
    var dropdownOptions = "<option value='MISSING'>Nide puuttuu</option>" +
                          "<option value='NOT_IN_COLLECTIONS'>Nide ei löydy kokoelmista</option>" +
                          "<option value='OTHER'>Muu huomio</option>" +
                          "<option value='NOT_PUBLISHED'>Lehteä ei ole ilmestynyt tänä päivänä</option>" +
                          "<option value='TO_BE_SCANNED_FROM_ORIGINAL'>Skannataan myöhemmin alkuperäisestä</option>" +
                          "<option value='DIGITISATION_ANOMALY'>Digitointipoikkeama</option>" +
                          "<option value='ISSUE_NUMBERING_ANOMALY'>Numerointipoikkeama</option>" +
                          "<option value='NOT_PUBLISHED_IN_PRINT'>Lehti ei ole ilmestynyt painettuna</option>";
                          
    var dropdownOptionsPvmAccuracy = "<option value='v'>V</option>" +
                          "<option value='k'>KK</option>" +
                          "<option value='p'>P</option>";
                          
    var defaultOptionDay = day;
    var dropdownOptionsDay = "";
    
    for (var i = 1; i <= 31; i++) {
        if (i === defaultOptionDay) {
            dropdownOptionsDay += "<option value='" + i + "' selected>" + i + "</option>";
        } else {
            dropdownOptionsDay += "<option value='" + i + "'>" + i + "</option>";
        }
    }
    
    var defaultOptionMonth = month;
    var dropdownOptionsMonth = "";

    for (var j = 1; j <= 12; j++) {
        if (j === defaultOptionMonth) {
            dropdownOptionsMonth += "<option value='" + j + "' selected>" + j + "</option>";
        } else {
            dropdownOptionsMonth += "<option value='" + j + "'>" + j + "</option>";
        }
    }
    
    var popupContent =  "<form id='popupForm' action='confirmation_UI/processVolumeLogs.php' method='post'>" +
                            "<div style='margin-top: 50px;'>" +
                                "<h2 style='text-align: center;'>Lisää nidepuute</h2>" +
                                "<table class='nidePuutteetPopup' align='center' style='width: 80%;'>" +
                                    "<tr>" +
                                        "<td colspan='2'>"+
                                            "<input type='text' style='width: 100%;' value='" + nimeke + "' disabled>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<input type='text' name='nimeke' value='" + nimeke + "' hidden>" +
                                    "<input type='text' name='issn' value='" + issn + "' hidden>" +
                                "</table>" +
                                
                                "<table class='nidePuutteetPopup' align='center' style='width: 80%;'>" +
                                    "<tr>" +
                                        "<th style='text-align: left;'>PVM tarkkuus:</th>" +
                                        "<th style='text-align: left;'>PVM:</th>" +
                                        "<th style='text-align: left;'>NUMERO:</th>" +
                                    "</tr>" +
                                    
                                    "<tr>" +
                                        "<td><select name='pvmAccuracy'>" + dropdownOptionsPvmAccuracy + "</select></td>" +
                                        "<td>" +
                                            "<select name='day'>" + dropdownOptionsDay + "</select>" +
                                            "<select name='month'>" + dropdownOptionsMonth + "</select>" +
                                            "-<input type='text' maxlength='4' style='width: 40px;' placeholder='" + vuosi + "' disabled>" +
                                            "<input type='text' name='year' value='" + vuosi + "' hidden>" +
                                        "</td>" +
                                        "<td><input type='text' maxlength='6' size='6' class='inputNumero' name='numero'></td>" +
                                    "</tr>" +
                                "</table>" +
                                
                                "<table class='nidePuutteetPopup' align='center' style='width: 80%;'>" +
                                    "<tr>" +
                                        "<th style='text-align: left;'>Tyyppi:</th><th></th><th></th>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td><select name='tyyppi'>" + dropdownOptions + "</select></td><td></td><td></td>" +
                                    "</tr>" +
                                    "<tr>" +
                                    "   <th style='text-align: left;'>Selitys:</th>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td colspan='2'><input type='text' style='width: 100%;' name='selitys'></td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td colspan='2'><input type='submit' value='Lähetä'></td>" +
                                    "</tr>" +
                                "</table>" +
                            "</div>" +
                        "</form>";
    
    var popupWindow = window.open("Puute lisäys", "_blank", "width=700,height=450");
    popupWindow.document.write("<html><head><title>Puuttuvat tiedot</title><style>body { margin: 0; }</style></head><body>" + popupContent + "</body></html>");
}

function suljeIkkuna() {
    // Tarkistetaan, onko ikkuna avattu
    alert("Ikkunaa ei ole avattu");
    if(window.opener) {
            // Suljetaan ikkuna
    window.close();
    } else {
        alert("Ikkunaa ei ole avattu");
    }
}

function haeKaupunki() {
    var input = document.getElementById('tekstikentta');
    var filter = input.value.toUpperCase();
    var ul = document.getElementById("kaupunki-lista");
    var li = ul.getElementsByTagName('li');
    var dropdown = document.getElementById("kaupunki-dropdown");

    dropdown.style.display = "block";
    dropdown.innerHTML = '';

    for (var i = 0; i < li.length; i++) {
        var javascript_variable = "success";
        var kaupunki = li[i].innerText;
        if (kaupunki.toUpperCase().startsWith(filter)) {
            var option = document.createElement("li");
            option.innerHTML = kaupunki;
            option.onclick = function() {
                input.value = this.innerHTML;
                dropdown.style.display = "none";
                // Näytä vuosi-kenttä kun kaupunki on annettu
                //document.getElementById("vuosi-input-container").style.display = "block";
                document.getElementById("kaupunki-form").submit();
            }
            dropdown.appendChild(option);
            
        }
    }

}

function haeVuosi() {
    var input = document.getElementById('vuosikentta');
    var filter = input.value.toUpperCase();
    var ul = document.getElementById("vuosi-lista");
    var li = ul.getElementsByTagName('li');
    var dropdown = document.getElementById("vuosi-dropdown");

    dropdown.style.display = "block";
    dropdown.innerHTML = '';

    for (var i = 0; i < li.length; i++) {
        var vuosi = li[i].innerText;
        if (vuosi.startsWith(filter)) {
            var option = document.createElement("li");
            option.innerHTML = vuosi;
            option.onclick = function() {
                input.value = this.innerHTML;
                dropdown.style.display = "none";
            }
            dropdown.appendChild(option);
            
        }
    }
}

// Piilotetaan dropdown, kun klikataan muualla kuin tekstikentässä tai dropdownissa
document.addEventListener("click", function(event) {
    var kaupunkiDropdown = document.getElementById("kaupunki-dropdown");
    var vuosiDropdown = document.getElementById("vuosi-dropdown");
    var kaupunkiInput = document.getElementById("tekstikentta");
    var vuosiInput = document.getElementById("vuosikentta");
    if (event.target !== kaupunkiDropdown && event.target !== kaupunkiInput && event.target !== vuosiDropdown && event.target !== vuosiInput) {
        kaupunkiDropdown.style.display = "none";
        vuosiDropdown.style.display = "none";
    }
});

window.onload = function() {
    var frames = document.querySelectorAll('.frame');
    var maxHeight = 0;
    frames.forEach(function(frame) {
        maxHeight = Math.max(maxHeight, frame.clientHeight);
    });
    frames.forEach(function(frame) {
        frame.style.height = maxHeight + 'px';
    });
};

document.addEventListener('DOMContentLoaded', function() {
    const toggleButton = document.querySelector('.errorFrame .toggleButton');
    const errorFrame = document.querySelector('.errorFrame');
    const erroAndRejectedTable = document.querySelector('.erroAndRejectedTable');
    const rescanTable = document.querySelector('.rescanTable');

    toggleButton.addEventListener("click", function() {
        errorFrame.classList.toggle("expanded");
        erroAndRejectedTable.classList.toggle("expanded");
        rescanTable.classList.toggle("expanded");

        // Ensure errorFrame height adjusts to its content
        if (errorFrame.classList.contains("expanded")) {
            errorFrame.style.height = errorFrame.scrollHeight + "px";
            toggleButton.textContent = 'PIILOTA';
        } else {
            errorFrame.style.height = "auto"; // Reset to auto or initial height
            toggleButton.textContent = 'NÄYTÄ VIRHEELLISET';
        }
    });
});