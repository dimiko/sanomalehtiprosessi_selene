<?php

?>

<script>
$('head').append('<link rel="stylesheet" type="text/css" href="css/style2.css">');
//$('head').append('<link rel="stylesheet" type="text/css" href="css/confirmation.css?v1">');

    function displayIssinumListing() {
        var x = document.getElementById("myDIV");
        if (x.style.display === "block") {
            x.style.display = "none";
        } else {
            x.style.display = "block";
        }
    }

function openPopup(vuosi, issn, nimeke) {
    var dropdownOptions = "<option value='Nide puuttuu'>Nide puuttuu</option>" +
                          "<option value='Nide ei löydy kokoelmista'>Nide ei löydy kokoelmista</option>" +
                          "<option value='Muu huomio'>Muu huomio</option>" +
                          "<option value='Lehteä ei ole ilmestynyt tänä päivänä'>Lehteä ei ole ilmestynyt tänä päivänä</option>" +
                          "<option value='Skannataan myöhemmin alkuperäisestä'>Skannataan myöhemmin alkuperäisestä</option>" +
                          "<option value='Digitointipoikkeama'>Digitointipoikkeama</option>" +
                          "<option value='Numerointipoikkeama'>Numerointipoikkeama</option>" +
                          "<option value='Lehti ei ole ilmestynyt painettuna'>Lehti ei ole ilmestynyt painettuna</option>";
                          
    var dropdownOptionsPvm = "<option value='V'>V</option>" +
                          "<option value='KK'>KK</option>" +
                          "<option value='P'>P</option>";
    
    var popupContent =  "<form id='popupForm' action='confirmation_UI/processVolumeLogs.php' method='post'>" +
                            "<div style='margin-top: 50px;'>" +
                                "<h2 style='text-align: center;'>Lisää nidepuute</h2>" +
                                "<table class='nidePuutteetPopup' align='center' style='width: 80%;'>" +
                                    "<tr>" +
                                        "<td colspan='2'>"+
                                            "<input type='text' style='width: 100%;' value='" + nimeke + "' disabled>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<input type='text' name='nimeke' value='" + nimeke + "' hidden>" +
                                "</table>" +
                                
                                "<table class='nidePuutteetPopup' align='center' style='width: 80%;'>" +
                                    "<tr>" +
                                        "<th style='text-align: left;'>PVM tarkkuus:</th>" +
                                        "<th style='text-align: left;'>PVM:</th>" +
                                        "<th style='text-align: left;'>NUMERO:</th>" +
                                    "</tr>" +
                                    
                                    "<tr>" +
                                        "<td><select name='pvmAccuracy'>" + dropdownOptionsPvm + "</select></td>" +
                                        "<td><input type='text' maxlength='2' style='width: 30px;' name='day' placeholder='pp' disabled>" +
                                            "-<input type='text' maxlength='2' style='width: 30px;' name='month' placeholder='kk' disabled>" +
                                            "-<input type='text' maxlength='4' style='width: 40px;' placeholder='" + vuosi + "' disabled>" +
                                            "<input type='text' name='year' value='" + vuosi + "' hidden>" +
                                        "</td>" +
                                        "<td><input type='text' maxlength='6' size='6' class='inputNumero' name='numero'></td>" +
                                    "</tr>" +
                                "</table>" +
                                
                                "<table class='nidePuutteetPopup' align='center' style='width: 80%;'>" +
                                    "<tr>" +
                                        "<th style='text-align: left;'>Tyyppi:</th><th></th><th></th>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td><select name='tyyppi'>" + dropdownOptions + "</select></td><td></td><td></td>" +
                                    "</tr>" +
                                    "<tr>" +
                                    "   <th style='text-align: left;'>Selitys:</th>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td colspan='2'><input type='text' style='width: 100%;' name='selitys'></td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td colspan='2'><input type='submit' value='Lähetä'></td>" +
                                    "</tr>" +
                                "</table>" +
                            "</div>" +
                        "</form>";
    
    var popupWindow = window.open("Puute lisäys", "_blank", "width=500,height=450");
    popupWindow.document.write("<html><head><title>Puuttuvat tiedot</title><style>body { margin: 0; }</style></head><body>" + popupContent + "</body></html>");
}

function haeKaupunki() {
    var input = document.getElementById('tekstikentta');
    var filter = input.value.toUpperCase();
    var ul = document.getElementById("kaupunki-lista");
    var li = ul.getElementsByTagName('li');
    var dropdown = document.getElementById("kaupunki-dropdown");

    dropdown.style.display = "block";
    dropdown.innerHTML = '';

    for (var i = 0; i < li.length; i++) {
        var javascript_variable = "success";
        var kaupunki = li[i].innerText;
        if (kaupunki.toUpperCase().startsWith(filter)) {
            var option = document.createElement("li");
            option.innerHTML = kaupunki;
            option.onclick = function() {
                input.value = this.innerHTML;
                dropdown.style.display = "none";
                // Näytä vuosi-kenttä kun kaupunki on annettu
                //document.getElementById("vuosi-input-container").style.display = "block";
                document.getElementById("kaupunki-form").submit();
            }
            dropdown.appendChild(option);
            
        }
    }

}

function haeVuosi() {
    var input = document.getElementById('vuosikentta');
    var filter = input.value.toUpperCase();
    var ul = document.getElementById("vuosi-lista");
    var li = ul.getElementsByTagName('li');
    var dropdown = document.getElementById("vuosi-dropdown");

    dropdown.style.display = "block";
    dropdown.innerHTML = '';

    for (var i = 0; i < li.length; i++) {
        var vuosi = li[i].innerText;
        if (vuosi.startsWith(filter)) {
            var option = document.createElement("li");
            option.innerHTML = vuosi;
            option.onclick = function() {
                input.value = this.innerHTML;
                dropdown.style.display = "none";
            }
            dropdown.appendChild(option);
            
        }
    }
}

// Piilotetaan dropdown, kun klikataan muualla kuin tekstikentässä tai dropdownissa
document.addEventListener("click", function(event) {
    var kaupunkiDropdown = document.getElementById("kaupunki-dropdown");
    var vuosiDropdown = document.getElementById("vuosi-dropdown");
    var kaupunkiInput = document.getElementById("tekstikentta");
    var vuosiInput = document.getElementById("vuosikentta");
    if (event.target !== kaupunkiDropdown && event.target !== kaupunkiInput && event.target !== vuosiDropdown && event.target !== vuosiInput) {
        kaupunkiDropdown.style.display = "none";
        vuosiDropdown.style.display = "none";
    }
});

</script>