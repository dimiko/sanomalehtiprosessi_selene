<?php

if (isset($_POST["filePath"])) {
    $filePath = $_POST["filePath"];
} elseif (isset($_GET["filePath"])) {
    $filePath = $_GET["filePath"];
} else {
     //echo "ei nideid:tä";
}

$tiffFile = $filePath;

$tiffStream = file_get_contents($tiffFile);

// Create Imagick object from TIFF stream
$tiffImage = new Imagick();
$tiffImage->readImageBlob($tiffStream);

// Convert TIFF to PNG
$tiffImage->setImageFormat('png');

// Get the PNG image as a binary string
$pngStream = $tiffImage->getImageBlob();

// Output the PNG stream
header('Content-Type: image/png');
echo $pngStream;

// Destroy the Imagick object to free up resources
$tiffImage->destroy();




/*
include("commons/commons_functions.php");

$docId = 427952;
echo $docId . "<br>";
$poolFolder = getDocPoolFolder($docId, 1);
echo $poolFolder . "<br>";

$poolFolderAkte = getDocPoolFolder($docId);
echo $poolFolderAkte . "<br>";

$xmlFile = "$poolFolder/$docId.xml";
echo $xmlFile . "<br>";
$xmlFile = str_replace("\\", "/", $xmlFile); // Replace backslashes with slashes
// Tiedoston polku
//$xmlFile = '/mnt/akte/dw-pool/79/52/427952.xml';

// Lataa XML-tiedosto SimpleXML-objektiin
$xml = simplexml_load_file($xmlFile);

if ($xml) {
    foreach ($xml->PAGES->PAGE as $page) {
        // Hae attribuutin PAGE ID arvo
        $pageId = (string) $page['ID'];
        // Hae PIMAGE-arvo ja käytä basename-funktiota
        $pimage = basename((string) str_replace("\\", "/", $page->PIMAGE));
        $pimage = str_ireplace('.tif', '.low', str_ireplace('.TIF', '.low', $pimage));
        // Tulosta PAGE ID ja PIMAGE-arvot samassa muuttujassa
        $lowImageName = $docId . '_' . $pageId . '_' . $pimage;
        //echo $poolFolderAkte . $lowImageName . "<br>";
    }
} else {
    echo "Failed to load XML";
}
*/
?>


