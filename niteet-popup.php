<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_niteet.php");
?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=ISO-8859-1">
    <!-- <meta http-equiv="refresh" content="240" > -->
    <title>Hyväksytyt niteet</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css?v1">
    <link rel="stylesheet" type="text/css" href="css/style.css?v1">
    <link rel="stylesheet" type="text/css" href="css/style2.css?v1">
    <script type="text/javascript" src="jquerys/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="jquerys/jquery.cookie.js"></script>
    <script type="text/javascript" src="jquerys/sorttable.js"></script> 
    

<style type="text/css">
th, td {
  padding: 3px !important;
}
  html {
    overflow: -moz-scrollbars-vertical;
}

/* Sortable tables */
table.sortable thead {
    background-color: #333;
    color: #cccccc;
    font-weight: bold;
    cursor: default;
}
th {
  font-size: 100%;
}

</style>


</head>
<body>

<?php

$toiminto="listaa";

if ($toiminto=="listaa") {
    if (isset($_POST["ryhma"])) {
        $ryhma=$_POST["ryhma"];
    } elseif (isset($_GET["ryhma"])) {
        $ryhma=$_GET["ryhma"];
    } else $ryhma="";
    
    
    
    
    #$ryhma=urldecode($ryhma);
  
        
    /*
    $query = "
    SELECT  title, docid, issuenum,issn,depositid, pages, depositbrowser_errorpages, depositbrowser_comments,depositbrowser_status,digi_status,dw_replaced_by,status_processed,
    convert(varchar,issuedate,120) as issuedate,
    convert(varchar,dw_importdate,120) as dw_importdate,
    convert(varchar,dw_exportdate,120) as dw_exportdate,
    convert(varchar,depositbrowser_statusdate,120) as  depositbrowser_statusdate,
    convert(varchar,depositbrowser_receivedate,120) as depositbrowser_receivedate,
    convert(varchar,depositbrowser_processdate,120) as depositbrowser_processdate,
    convert(varchar,depositbrowser_mfdate,120) as depositbrowser_mfdate,
    convert(varchar,dw_deletedate,120) as dw_deletedate,
    convert(varchar,digi_importdate,120) as digi_importdate,
    convert(varchar,scandate,120) as scandate
    from dw_nlf_custom.dbo.sl_status
    where title='$ryhma'
    order by issuedate desc";
    */
    $query = "SELECT title, docid, issuenum, issn, depositid, pages, QA_error_pages as depositbrowser_errorpages, QA_error_comment as depositbrowser_comments,
            [status] as depositbrowser_status, digi_status, dw_replaced_by, status_processed,
            convert(varchar, issuedate,120) as issuedate,
            convert(varchar,dw_importdate,120) as dw_importdate,
            convert(varchar,dw_exportdate,120) as dw_exportdate,
            convert(varchar, accepted_datetime, 23) as depositbrowser_statusdate,
            convert(varchar,accepted_datetime,23) as depositbrowser_receivedate,
            convert(varchar,accepted_datetime,23) as depositbrowser_processdate,
            convert(varchar,depositbrowser_mfdate,120) as depositbrowser_mfdate,
            convert(varchar,dw_deletedate,120) as dw_deletedate,
            convert(varchar,digi_importdate,120) as digi_importdate,
            convert(varchar,scandate,120) as scandate, restart, remove
            from dw_nlf_custom.dbo.sl_status
            where title='$ryhma'
            order by issuedate desc";
echo "<div class='niteetPopupHeader'><h1>$ryhma</h1></div>";
niteet_listing($db, $query);
}
mssql_close($db);
?> 
</body>
</html>
