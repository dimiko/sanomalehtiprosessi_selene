<?php

   if (isset($_POST["nide_id"])) {
        $nideid=$_POST["nideid"];
    } elseif (isset($_GET["nideid"])) {
        $nideid=$_GET["nideid"];
    } else {
     echo "ei nideid:tä";

}



$imageUrl = "$nideid";

// Get image data from the URL
$imageData = file_get_contents($imageUrl);

if ($imageData !== false) {
    // Create image resource from the image data
    $image = imagecreatefromstring($imageData);

    if ($image !== false) {
        // Get the image dimensions
        $width = imagesx($image);
        $height = imagesy($image);

        // Set the new height for cropping
        $newHeight = 300;

        // Create a new image with the same width and the specified height
        $croppedImage = imagecreatetruecolor($width, $newHeight);

        // Crop the image to 100 pixels from the top
        imagecopy($croppedImage, $image, 0, 0, 0, 0, $width, $newHeight);

        // Set the Content-Type header to indicate that we are sending an image
        header('Content-Type: image/jpeg');

        // Output the cropped image to the browser
        imagejpeg($croppedImage);

        // Free up memory by destroying the images
        imagedestroy($image);
        imagedestroy($croppedImage);
    } else {
        // Handle the case where creating an image resource from the data fails
        echo 'Failed to create image resource.';
    }
} else {
    // Handle the case where fetching the image data fails
    echo 'Failed to fetch image data.';
}
?>
