<?php
include("../sessio.php");
include("../functions.php");

$docId = $_POST["docId"];
$slStatusID = $_POST["slStatusID"];
$issn = $_POST["issn"];
$year = $_POST["year"];
$problemType = $_POST["problemType"];
$comment = $_POST["comment"];

insertVolumeProblemsNotes($docId, $problemType, $comment);
$headerLocation = "Location: ../tarkistus_UI.php?ryhma=$issn&year=$year";

close_sql($db);
oci_close($conn);
oci_close($conn_diona);

header($headerLocation);
?>