<?php
include("../sessio.php");
include("../functions.php");

function checkVolumeHasRow($fullDate, $pvmAccuracy, $issn, $conn_diona) {
    // SQL-kysely
    $query = "SELECT 1 FROM nidepuutteet WHERE issn = '$issn' AND pvm = TO_DATE('$fullDate', 'YYYY.MM.DD') AND pvm_tyyppi = '$pvmAccuracy'";

    // Valmistele SQL-lause
    $statement = oci_parse($conn_diona, $query);
    if (!$statement) {
        $e = oci_error($conn_diona);
        $errorMessage = "error in query : $query " . htmlentities($e['message'], ENT_QUOTES) . "\n";
        oci_free_statement($statementId);
        oci_close($conn_diona);
        echo $errorMessage . "\n";
    }

    // Suoritetaan SQL-lause
    $result = oci_execute($statement);
    if (!$result) {
        $e = oci_error($statement);
        $errorMessage = "error in query : $query " . htmlentities($e['message'], ENT_QUOTES) . "\n";
        oci_free_statement($statementId);
        oci_close($conn_diona);
        echo $errorMessage . "\n";
    }

    // Tarkistetaan, löytyikö rivi
    $rowExists = oci_fetch($statement) ? true : false;
    // Vapautetaan resurssit
    oci_free_statement($statement);

    return $rowExists;
}

function insertVolumeLog($issn, $fullDate, $numero, $selitys, $tyyppi, $pvmAccuracy, $conn_diona) {
    
    $digiUserId = 838016;
    
    $sqlOracle = "insert into nidepuutteet (issn, pvm, no, selitys, ilmoittaja, ilmoitettu, tyyppi, pvm_tyyppi)
                values ('$issn', TO_DATE('$fullDate', 'YYYY.MM.DD'), '$numero', '$selitys', $digiUserId, SYSDATE, '$tyyppi', '$pvmAccuracy')";

    $statementId = oci_parse($conn_diona, $sqlOracle);
    
    $hasErrors = false;
    if (!$statementId) {
        $e = oci_error($conn_diona);
        $errorMessage = "error in query : $sqlOracle " . htmlentities($e['message'], ENT_QUOTES) . "\n";
        oci_free_statement($statementId);
        $hasErrors = true;
    }

    $result = oci_execute($statementId);
    if (!$result) {  
        $e = oci_error($statementId);
        $errorMessage = "error in query : $sqlOracle " . htmlentities($e['message'], ENT_QUOTES) . "\n";
        oci_free_statement($statementId);
        echo $errorMessage . "\n";
        $hasErrors = true;
    }
    oci_free_statement($statementId);
    return $hasErrors;
}

function updateVolumeLog($issn, $fullDate, $numero, $selitys, $tyyppi, $pvmAccuracy, $conn_diona) {
    
    $digiUserId = 838016;
    
    $sqlOracle = "update nidepuutteet set selitys = selitys || ' ' || '$selitys', no = '$numero', tyyppi = '$tyyppi'
                    WHERE issn = '$issn' AND pvm = TO_DATE('$fullDate', 'YYYY.MM.DD') AND pvm_tyyppi = '$pvmAccuracy' and ilmoittaja = $digiUserId";

    $statementId = oci_parse($conn_diona, $sqlOracle);
    
    $hasErrors = false;
    if (!$statementId) {
        $e = oci_error($conn_diona);
        $errorMessage = "error in query : $sqlOracle " . htmlentities($e['message'], ENT_QUOTES) . "\n";
        oci_free_statement($statementId);
        $hasErrors = true;
    }

    $result = oci_execute($statementId);
    if (!$result) {  
        $e = oci_error($statementId);
        $errorMessage = "error in query : $sqlOracle " . htmlentities($e['message'], ENT_QUOTES) . "\n";
        oci_free_statement($statementId);
        echo $errorMessage . "\n";
        $hasErrors = true;
    }
    oci_free_statement($statementId);
    return $hasErrors;
}

get_current_user();
$username = get_current_user();
// Tarkistetaan, että POST-metodi on käytössä
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Otetaan vastaan lähetetyt arvot ja tallennetaan ne muuttujiin
    $pvmAccuracy = $_POST["pvmAccuracy"];
    $pvmAccuracy_must_be_range = array('v', 'k', 'p');
    if (!in_array($pvmAccuracy, $pvmAccuracy_must_be_range)) {
        echo "<div style='text-align: center;'>";
            echo "<h1 style='color: red;'>Annettu PVM tarkkuus: '$pvmAccuracy' ei ollu kelvollinen.<br>Ota yhteyttä Samuli<br><h1>";
        echo "</div>";
        exit();
    }
    $tyyppi = $_POST["tyyppi"];
    $tyyppi_must_be_range = array('MISSING', 'NOT_IN_COLLECTIONS', 'OTHER', 'NOT_PUBLISHED', 'TO_BE_SCANNED_FROM_ORIGINAL', 'DIGITISATION_ANOMALY', 'ISSUE_NUMBERING_ANOMALY', 'NOT_PUBLISHED_IN_PRINT');
    if (!in_array($tyyppi, $tyyppi_must_be_range)) {
        echo "<div style='text-align: center;'>";
            echo "<h1 style='color: red;'>Annettu tyyppi: '$tyyppi' ei ollu kelvollinen.<br>Ota yhteyttä Samuli<br><h1>";
        echo "</div>";
        exit();
    }

    $day = $_POST["day"];
    $month = $_POST["month"];
    $nimeke = $_POST["nimeke"];
    $issn = $_POST["issn"];
    $numero = $_POST["numero"];
    $year = $_POST["year"];
    $selitys = $_POST["selitys"];
    $fullDate = "$year-$month-$day";
    
    // Poista erikoismerkit käyttäen regular expressionia
    //$selitys = preg_replace('/[^\p{L}\p{N}\s]/u', '', $selitys);
    $selitys = preg_replace('/[^\p{L}\p{N}\s-]/u', '', $selitys);
    $numero = preg_replace('/[^\p{L}\p{N}\s]/u', '', $numero);
    
    $issnHasRow = checkVolumeHasRow($fullDate, $pvmAccuracy, $issn, $conn_diona);
    
    if($issnHasRow) {
        $hasErrors = updateVolumeLog($issn, $fullDate, $numero, $selitys, $tyyppi, $pvmAccuracy, $conn_diona);
        echo "<div style='text-align: center;'>";
        if ($hasErrors) {
            echo "<h1 style='color: red;'>Tietojen tallennuksessa tapahtui virhe.<br>Ota yhteyttä Samuli</h1><br>";
        } else {
            echo "<h1 style='color: green;'>Puute PÄIVITETTY onnistuneesti.</h1><br>ISSN = $issn<br>PVM = $fullDate<br>NUMERO = $numero<br>SELITYS = $selitys<br>TYYPPI = $tyyppi<br>TARKKUUS = '$pvmAccuracy";
        }        
        echo "<br><button onclick='suljeIkkuna()' style='font-size: 20px; padding: 10px 20px;'>Sulje ikkuna</button>";
        echo "</div>";
    } else {
        $hasErrors = insertVolumeLog($issn, $fullDate, $numero, $selitys, $tyyppi, $pvmAccuracy, $conn_diona);
        echo "<div style='text-align: center;'>";
        if ($hasErrors) {
            echo "<h1 style='color: red;'>Tietojen tallennuksessa tapahtui virhe.<br>Ota yhteyttä Samuli</h1><br>";
        } else {
            echo "<h1 style='color: green;'>Puute LISÄTTY onnistuneesti.</h1><br>ISSN = $issn<br>PVM = $fullDate<br>NUMERO = $numero<br>SELITYS = $selitys<br>TYYPPI = $tyyppi<br>TARKKUUS = $pvmAccuracy";
        }        
        echo "<br><button onclick='suljeIkkuna()' style='font-size: 20px; padding: 10px 20px;'>Sulje ikkuna</button>";
        echo "</div>";
    }

} else {
    // Jos lomaketta ei ole lähetetty POST-metodilla, tulostetaan virheviesti
    echo "Virhe: Lomaketta ei ole lähetetty oikealla metodilla.<br>Ota yhteyttä Samuli";
    echo "<br><button onclick='suljeIkkuna()' style='font-size: 20px; padding: 10px 20px;'>Sulje ikkuna</button>";
}
close_sql($db);
oci_close($conn);
oci_close($conn_diona);
?>

<script>
function suljeIkkuna() {
    // Tarkistetaan, onko ikkuna avattu
    if(window.opener) {
            // Suljetaan ikkuna
    window.close();
    } else {
        alert("Ikkunaa ei ole avattu");
    }
}
</script>
