<?php
include("../sessio.php");
include("../functions.php");

$docId = $_POST["docId"];
$accept = $_POST["accept"];
$rescan = $_POST["rescan"];
$remove = $_POST["remove"];
$hold = $_POST["hold"];
$comment = $_POST["comment"];
$commentAccepted = $_POST["commentAccepted"];
$issn = $_POST["issn"];
$year = $_POST["year"];
$dropValue = $_POST["problems"];
$errorPages = $_POST["errorPages"];

$fetchNextDoc = 1;

//get_current_user();
$username = get_current_user();
$username = $_SERVER['REMOTE_ADDR'];

$comment = preg_replace('/[^\p{L}\p{N}\s,.]/u', '', $comment);
$commentAccepted = preg_replace('/[^\p{L}\p{N}\s,.]/u', '', $commentAccepted);
$errorPages = preg_replace('/[^\p{L}\p{N}\s,.]/u', '', $errorPages);

if($hold) {
    updateDocumentHold($db, $docId, $username);
    $_SESSION['displayOkMessage'] = "Nide laitettu tilaan 'hold'.";
}
if($accept) {
    $isAccepted = checkIfDocumentIsAccepted($docId);
    if($isAccepted == "") {
        updateDocumentAccepted($db, $docId, $username, $dropValue, $commentAccepted);
        
        //$insertRetunValue = insertVolumeProblemsNotes($docId, $dropValue, $commentAccepted);
        
        $_SESSION['displayOkMessage'] = "Nide hyväksytty onnistuneesti.";

    } else {
        $_SESSION['displayErrorMessage'] = "Työ on joo hyväksytty.";
    }

    
}
if($remove) {
    echo "POISTA <br>";
    updateDocumentRemoveTEMP($db, $docId, $username);
    $_SESSION['displayOkMessage'] = "Nide laitettu odottamaan poistoa.";
}
if($rescan) {
    echo "UUDELLEEN SKANNAA <br>";
    // TODO uudelleen skannaus jutut
    if($comment == "") {
        $_SESSION['displayErrorMessage'] = "Rescan kommentti kenttä ei voi olla tyhjä.";
        $fetchNextDoc = 0;
        $headerLocation = "Location: ../tarkistus_UI.php?nextid=$docId&ryhma=$issn&year=$year";
    } else {
        updateDocumentRejected($docId, $username, $comment, $errorPages);
        $_SESSION['displayOkMessage'] = "Nide laitettu rescan tilaan.";
    }
}

if($fetchNextDoc) {
    echo "HAE SEURAAVA ID <br>";
    //$docId = GetNextDocidToProcess();
    $headerLocation = "Location: ../tarkistus_UI.php?ryhma=$issn&year=$year";
}


close_sql($db);
oci_close($conn);
oci_close($conn_diona);

header($headerLocation);
?>