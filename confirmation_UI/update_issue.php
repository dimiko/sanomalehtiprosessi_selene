<?php
include("../sessio.php");
include("../functions.php");

$docId = $_POST["docId"];
$number = $_POST["number"];
$day = $_POST["day"];
$month = $_POST["month"];
$issnDateYear = $_POST["issnDateYear"];
$issn = $_POST["issn"];
$year = $_POST["year"];

/*
echo $docId . "<br>";
echo $number . "<br>";
echo $day . "<br>";
echo $month . "<br>";
echo $issnDateYear . "<br>";
echo $issn . "<br>";
echo $year . "<br>";
*/
// 2024-03-27
$volumeDate = "$issnDateYear-$month-$day";

$username = $_SERVER['REMOTE_ADDR'];

if (validateDate($volumeDate, 'Y-m-d')) {
    $sql = "update dw_nlf_custom.dbo.sl_status set issuedate = '$volumeDate', issuenum = '$number', status = 'change', status_changed_by = '$username' where docid = $docId";
    $result = mssql_query($sql) or die(mssql_errors());
    if( !$result ) {
        $_SESSION['displayErrorMessage'] = "Niteen päivityksessä tapahtui virhe.";
    } else {
        $_SESSION['displayOkMessage'] = "Nide päivitetty onnistuneesti ja laitettu prosessoitavaksi.";
    }
    mssql_free_result($result);
} else {
    $_SESSION['displayErrorMessage'] = "Annettu päivämäärä '$volumeDate' ei ollut validi.<br>Päivämäärä tulee antaa muodosssa (vuosi-kuukausi-päivä)<br>esim. 2023-03-23";
}

close_sql($db);
oci_close($conn);
oci_close($conn_diona);
//$headerLocation = "Location: ../tarkistus_UI.php?nextid=$docId&ryhma=$issn&year=$year";
$headerLocation = "Location: ../tarkistus_UI.php?ryhma=$issn&year=$year";
header($headerLocation);
?>