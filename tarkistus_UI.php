<?php
include("sessio.php");
include("functions.php");
include("sql_querys/querys_karttuvat.php");
include("global_variables.php");

if (isset($_GET["nextid"])) {
     $issn = $_GET["ryhma"];
     $currentYear = $_GET["year"];
     $docidList = GetNextDocidToProcess($issn, $currentYear);
     $docidCount = count($docidList);
     $docid = $_GET["nextid"];
} else {
     $issn = $_GET["ryhma"];
     $currentYear = $_GET["year"];
     $docidList = GetNextDocidToProcess($issn, $currentYear);
     $docidCount = count($docidList);
     $docid = $docidList[0];
     //$docidCount = rand(0, 1);
}
//$docidCount = 0;
if( $docidCount == 0 ) {
    $_SESSION['displayOkMessage'] = "Ei löytynyt yhtää tarkastettavaa työtä ISSN = $issn";
    header("Location: index.php");
    exit();
}

?>

<html>
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>tarkastus UI</title>
    <link rel="stylesheet" type="text/css" href="css/reset.css?v1">
    <link rel="stylesheet" type="text/css" href="css/style.css?v1">
    <link rel="stylesheet" type="text/css" href="css/style2.css?v1">
    <link rel="stylesheet" type="text/css" href="css/confirmation.css?v1">
    <script type="text/javascript" src="javascript/javaScripts.js?v1"></script>
</head>

<body>

<?php

naviHeader();

include("acknowledgment_messages.php");

$message = $_SESSION['message'];
echo "<div id='okMessage'>$message</div>";

function showPages ($docId, $docidCount, $currentYear) {

     
    $query="select title as PAANIMEKE, issn as ISSN, issuenum as NO, format(issuedate,'yyyy-MM-dd') as PVM, pages, QA_error_comment from dw_nlf_custom.dbo.sl_status where docid = $docId";
    
    $result = mssql_query($query);
    
    while($row = mssql_fetch_array($result)) {
    
        $paanimeke = $row['PAANIMEKE'];
        $issn = $row['ISSN'];
        $no = $row['NO'];
        $pvm = $row['PVM'];
        $aineistoyleismaare = "SAN";
        $pages = $row['pages'];
        $acceptionComment = $row['QA_comment'];
    
    }

    $allTitles = "select distinct title from dw_nlf_custom.dbo.sl_status";
    $allIssues = getAllIssues($db, $allTitles, $paanimeke);
    
    switch ($aineistoyleismaare) {
    
        case "SAN" :
            $urlAYM="sanomalehti";
            $topImage=True;
            break;
    
        case "AIK" :
            $urlAYM="aikakausi";
            $topImage=False;
            break;
    
        case "pie" :
            $urlAYM="pienpainate";
            $topImage=False;
            break;
    
        default:
            $urlAYM="teos";
            $topImage=False;
            break;
    
    }
    
    $allIssuenum = GetAllIssuesByYearAndIssn($issn, $currentYear);
    
    $imagesPath = GetPoolFolderLowImages($docId);
    
    $imagesCount = count($imagesPath);
    
    if($imagesCount != $pages) {
        echo "$imagesCount != $pages <br>";
        exit("ERROR");
    }
    
    if ($topImage) {
        $topImageUrl = "http://selene.docworks.lib.helsinki.fi/sl-prosessi/confirmation_UI/display_images.php?filePath=$imagesPath[0]";
        echo "<div>";
            echo "<img style='width:100%' src='http://selene.docworks.lib.helsinki.fi/sl-prosessi/confirmation_UI/imagetop.php?nideid=$topImageUrl'\>";
        echo "</div>";              
    }

    echo "<div style='width:100%; background-color: #f8f8f7;'>";
              echo "<table class='GeneratedTable'>";
                  
                  echo "<form action='confirmation_UI/update_issue.php' method='POST'>";
                  
                  
                    echo "<input type='hidden' name='docId' value='$docId'/>";
                    echo "<input type='hidden' name='issn' value='$issn'/>";
                    echo "<input type='hidden' name='year' value='$currentYear'/>";
                    echo "<tr>";
                        echo "<th>Nide 1 / $docidCount</th>";
                        echo "<td >";//$paanimeke</td>";
                            echo"<select name='nimike' disabled>";
                                foreach ($allIssues as $values) {
                                   echo"<option >$values</option>";
                                }            
                             echo "</select>";
                        echo "</td>";
                        echo "<td style = 'background-color: #f8f8f7;'>";
                        echo "<span style='font-size:20px; margin-right: 20px;'>NRO <span>";
                            echo "<input type='text' name='number' size='6' value='$no' style='border-color: white;'>";
                                echo "</span></span></td><td style='background-color: #f8f8f7;'>";
                                echo "<span style='font-size:20px; margin-right: 20px;'>PVM <span>";
                                
                                $date_parts = explode("-", $pvm);
                                // Extract day, month, and year
                                $year = $date_parts[0];
                                $month = $date_parts[1];
                                $day = $date_parts[2];
                                
                                $allDaysOfMonth = ["01","02","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"];
                                array_unshift($allDaysOfMonth, $day);
                                
                                $allMonths = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
                                array_unshift($allMonths, $month);
    
                                //echo "<input type='text' name='issnDateDay' maxlength='4' size='2' value='$day' style='border-color: white; margin-right: 20px;'>";
                                echo"<select name='day'style='margin-right: 20px;'>";
                                    foreach ($allDaysOfMonth as $day) {
                                       echo"<option >$day</option>";
                                    }            
                                echo "</select>";
                                echo"<select name='month'style='margin-right: 20px;'>";
                                    foreach ($allMonths as $month) {
                                       echo"<option >$month</option>";
                                    }            
                                echo "</select>";
                                //echo "<input type='text' name='issnDateMonth' maxlength='4' size='2' value='$month' style='border-color: white; margin-right: 20px;'>";
                                echo "<input type='text' name='issnDateYear' maxlength='4' size='4' value='$year' style='border-color: white;margin-right: 20px;'>";
                            echo "</span>";
                        echo "</span>";
                        echo "</td>";

                        echo "<td style='background-color: #f8f8f7;'><input type='submit' value='Muuta'></td>";

                    echo "</tr>";

                  echo "</form>";
                  
              echo "</table>";
    echo "</div>";
    
    echo "<div class='image-grid'>";
    
        for ($x = 1; $x <= $pages; $x++) {
            $pageIndex = ($x -1);
            echo "<div class=\"image-item\"><img class='magniflier' style=\"width:100%;\"src=\"http://selene.docworks.lib.helsinki.fi/sl-prosessi/confirmation_UI/display_images.php?filePath=$imagesPath[$pageIndex]\">";
                echo "<div class=\"overlay\">";
                    echo "<h2>" . $x . "</h2>";
                echo "</div>";
            echo "</div>";
        }
    
    echo "</div>";
    
    
    echo "<div class='issueNumlistingButton'>";
        echo "<button class='buttonIssueListing' onclick='displayIssinumListing()'>Näytä vuosikerta</button>";
    echo "</div>";
    
    echo "<div id='myDIV' class='issueNumlisting'>";
        echo "<h2>Vuosi $currentYear</h2>";
        //echo "<button>Lisää vuosikerralle puute</button>";
        $formattedTitle = generateFormattedString($paanimeke);
        echo "<button class='addVolumeProblems-button' onclick=\"openPopup($currentYear, 1, 1, '$issn', '$formattedTitle')\">Lisää vuosikerralle puute</button>";
        echo "<table>";
            echo "<tr>";
                echo "<th>Työkalu</th>";
                echo "<th>status</th>";
                echo "<th>doc</th>";
                echo "<th>no</th>";
                echo "<th>pvm</th>";
                echo "<th>niteen ongelma</th>";
            echo "</tr>";
            foreach ($allIssuenum as $volume) {
                $issueNum = $volume["number"];
                $currentDocId = $volume["details"]["docid"];
                $issuedate = $volume["details"]["issuedate"];
                $shortageType = $volume["details"]["shortageType"];
                $shortageDescriptio = $volume["details"]["shortageDescriptio"];
                $issuStatus = $volume["details"]["status"];
                $statusColor = "";
                
                if($shortageType == "" && $shortageDescriptio == "") {
                    $buttonText = "Lisää puute";    
                } else {
                    $buttonText = "Muuta puute";
                }
                
                switch ($issuStatus) {
                    case "digi":
                        $statusColor = '#04D7F7';
                        break;
                    case "completed":
                        $statusColor = '#02FC34';
                        break;
                    case "rejected":
                        $statusColor = '#F785BA';
                        break;
                    case "missing":
                        $statusColor = '#F78325';
                        break;
                    case "accepted":
                        $statusColor = '#B9E2B1';
                        break;
                    case "hold":
                        $statusColor = '#EACB1E';
                        break;
                    case "rescan":
                        $statusColor = '#FC0004';
                        break;
                    case "QA":
                        $statusColor = '#DBF28A';
                        break;
                    default:
                        $statusColor = 'white';
                }
                
                $statusInfinnish = translateStatus($issuStatus);
                
                    echo "<tr style='background-color: $statusColor; border-bottom: solid'>";
                    if($issuStatus == "QA" || $issuStatus == "hold") {
                        echo "<td><button onclick=\"location.href='tarkistus_UI.php?ryhma=$issn&year=$currentYear&nextid=$currentDocId'\">Tarkistus työkalu</button></td>";
                    } else {
                        echo "<td><button disabled>Tarkistus työkalu</button>";
                    }
                        echo "<td>$statusInfinnish</td>";
                        echo "<td>$currentDocId</td>";
                        echo "<td>$issueNum</td>";
                        echo "<td>$issuedate</td>";
                        if($shortageDescriptio == "") {
                            echo "<td>          </td>";
                        } else {
                            echo "<td>$shortageDescriptio</td>";
                        }
                echo "</tr>";
    
            }
        echo "</table>";
    
    echo "</div>";
    
    $docId=$docId;
    
    echo "<div style='width:auto; background-color: white; border-top:solid;'>";
    
              echo "<table class='GeneratedTableAccepted'>";
                  
                  echo "<form action='confirmation_UI/process_docid.php' method='POST'>";
                      echo "<input type='hidden' name='issn' value='$issn'/>";
                      echo "<input type='hidden' name='year' value='$currentYear'/>";
                      echo "<input type='hidden' name='docId' value='$docId'/>";
                      echo "<td>";
                          echo "<tr>";
                              echo "<td style='background-color: white;'>
                              <button type='submit' name='remove' value='1' class='buttonRejected'>Poista</button> <!-- Lisää nappi -->
                              </td>";
                              echo "<td style='background-color: white;'>
                              <button type='submit' name='rescan' value='1' class='buttonRescan'>Rescan</button> <!-- Lisää nappi -->
                              </td>";
                              echo "<td style='background-color: white;'><button type='submit' name='accept' value='1' class='buttonAccepted'>Hyväksy</button> <!-- Lisää nappi --></td>";
                          echo "</tr>";
                          echo "<tr>";
                              echo "<td style='background-color: white;'>
                              <button type='submit' name='hold' value='1' class='buttonHold'>hold / ohita</button> <!-- Lisää nappi -->
                              </td>";
                              echo "<td><textarea  style='background-color: white; border:solid;' cols='60' rows='3' id='kommentti' name='errorPages' placeholder='Syötä virheellistet sivut'></textarea></td>";
                              echo "<td>";
                                  echo "<select name='problems'>
                                        <option value=''>Mahdollinen ongelma</option>
                                        <option value='SUPPLEMENT'>SUPPLEMENT</option>
                                        <option value='OTHER'>OTHER</option>
                                        <option value='MISSING_PAGES'>MISSING_PAGES</option>
                                        </select>";
                              echo "</td>";
                          echo "</tr>";
                          echo "<tr>";
                              echo "<td></td>";
                              echo "<td><textarea  style='background-color: white; border:solid;' cols='60' rows='3' id='kommentti' name='comment' placeholder='rescan kommentti'></textarea></td>";
                              echo "<td><textarea  style='background-color: white; border:solid;' cols='60' rows='3' id='kommentti' name='commentAccepted' placeholder='Kommentti'></textarea></td>";
                          echo "</tr>";
                      echo "</td>";
                      
                  echo "</form>";
                  
              echo "</table>";
    
    echo "</div>";

}

showPages($docid, $docidCount, $currentYear);

?>
</body>
</html>

<script>

 
 $(function() {

  var native_width = 0;
  var native_height = 0;
  var mouse = {x: 0, y: 0};
  var magnify;
  var cur_img;

  var ui = {
    magniflier: $('.magniflier')
  };

  // Add the magnifying glass
  if (ui.magniflier.length) {
    var div = document.createElement('div');
    div.setAttribute('class', 'glass');
    ui.glass = $(div);

    $('body').append(div);
  }

  
  // All the magnifying will happen on "mousemove"

  var mouseMove = function(e) {
    var $el = $(this);

    // Container offset relative to document
    var magnify_offset = cur_img.offset();

    // Mouse position relative to container
    // pageX/pageY - container's offsetLeft/offetTop
    mouse.x = e.pageX - magnify_offset.left;
    mouse.y = e.pageY - magnify_offset.top;
    
    // The Magnifying glass should only show up when the mouse is inside
    // It is important to note that attaching mouseout and then hiding
    // the glass wont work cuz mouse will never be out due to the glass
    // being inside the parent and having a higher z-index (positioned above)
    if (
      mouse.x < cur_img.width() &&
      mouse.y < cur_img.height() &&
      mouse.x > 0 &&
      mouse.y > 0
      ) {

      magnify(e);
    }
    else {
      ui.glass.fadeOut(100);
    }

    return;
  };

  var magnify = function(e) {

    // The background position of div.glass will be
    // changed according to the position
    // of the mouse over the img.magniflier
    //
    // So we will get the ratio of the pixel
    // under the mouse with respect
    // to the image and use that to position the
    // large image inside the magnifying glass

    var rx = Math.round(mouse.x/cur_img.width()*native_width - ui.glass.width()/2)*-1;
    var ry = Math.round(mouse.y/cur_img.height()*native_height - ui.glass.height()/2)*-1;
    var bg_pos = rx + "px " + ry + "px";
    
    // Calculate pos for magnifying glass
    //
    // Easy Logic: Deduct half of width/height
    // from mouse pos.

    // var glass_left = mouse.x - ui.glass.width() / 2;
    // var glass_top  = mouse.y - ui.glass.height() / 2;
    var glass_left = e.pageX - ui.glass.width() / 2;
    var glass_top  = e.pageY - ui.glass.height() / 2;
    //console.log(glass_left, glass_top, bg_pos)
    // Now, if you hover on the image, you should
    // see the magnifying glass in action
    ui.glass.css({
      left: glass_left,
      top: glass_top,
      backgroundPosition: bg_pos
    });

    return;
  };

  $('.magniflier').on('mousemove', function() {
    ui.glass.fadeIn(200);
    
    cur_img = $(this);

    var large_img_loaded = cur_img.data('large-img-loaded');
    var src = cur_img.data('large') || cur_img.attr('src');

    // Set large-img-loaded to true
    // cur_img.data('large-img-loaded', true)

    if (src) {
      ui.glass.css({
        'background-image': 'url(' + src + ')',
        'background-repeat': 'no-repeat'
      });
    }

    // When the user hovers on the image, the script will first calculate
    // the native dimensions if they don't exist. Only after the native dimensions
    // are available, the script will show the zoomed version.
    //if(!native_width && !native_height) {

      if (!cur_img.data('native_width')) {
        // This will create a new image object with the same image as that in .small
        // We cannot directly get the dimensions from .small because of the 
        // width specified to 200px in the html. To get the actual dimensions we have
        // created this image object.
        var image_object = new Image();

        image_object.onload = function() {
          // This code is wrapped in the .load function which is important.
          // width and height of the object would return 0 if accessed before 
          // the image gets loaded.
          native_width = image_object.width;
          native_height = image_object.height;

          cur_img.data('native_width', native_width);
          cur_img.data('native_height', native_height);

          //console.log(native_width, native_height);

          mouseMove.apply(this, arguments);

          ui.glass.on('mousemove', mouseMove);
        };


        image_object.src = src;
        
        return;
      } else {

        native_width = cur_img.data('native_width');
        native_height = cur_img.data('native_height');
      }
    //}
    //console.log(native_width, native_height);

    mouseMove.apply(this, arguments);

    ui.glass.on('mousemove', mouseMove);
  });

  ui.glass.on('mouseout', function() {
    ui.glass.off('mousemove', mouseMove);
  });

});
 
</script>

