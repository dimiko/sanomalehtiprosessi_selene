<?php
include("commons/commons_functions.php");


function countFiles ( $globPattern ){
    $files = glob($globPattern); 
    $filesCount = count($files);
    return $filesCount;
}


function supagTodayTable () {
	$eraStyle="";
	$eraFileCount=countFiles("/mnt/sl_import/era/*.zip");
	if ($eraFileCount>40) {
	 $eraStyle="background-color:red; color: white;";
  	 $era=$eraFileCount;

	} elseif ($eraFileCount==0) {
	  $era="";
	} else {
  	$era=$eraFileCount;
	}

	echo"<div class='dailyReportFrame'>";
  	echo"<h3>Tänään</h3>";
  	$export_sivuja = dailyReport($db, $queryExportted);
  	$accepted_sivuja = dailyReport($db, $queryAccepted);
  	$rejected_sivuja = dailyReport($db, $queryRejected);
  	$importoitu_sivuja = dailyReport($db, $queryImported);
  	$deletoitu_sivuja = dailyReport($db, $queryDeleted);

  	echo"<table class='dailyTable'>";
  	echo"<tr style='text-align: center;'>";
    	echo "<th>Exportoitu&nbsp;</th>";
    	echo "<th>Tarkastettu&nbsp;</th>";
    	echo "<th>Importoitumassa&nbsp;</th>";
    	echo "<th>Importoitu&nbsp;</th>";
    	echo "<th>Poistettu</th>";
  	echo"<tr>";
    	echo "<td>$export_sivuja</td>";
    	echo "<td>$accepted_sivuja</td>";
    	echo "<td style='$eraStyle'>$era</td>";
    	echo "<td>$importoitu_sivuja</td>";
    	echo "<td>$deletoitu_sivuja</td>";
  	echo"</tr>";
  	echo "</table>";

echo"</div>";


}


function pasProcessStatusTable () {

            $query_process_status = "select IIF(process_on_off = 1, 'ON', 'OFF') as 'ON/OFF',  processing_arc as 'arc',  IIF(process_is_running = 1, 'ON', 'OFF') as 'arcCopyRunning'
            from dw_nlf_custom.dbo.PAS_PROCESS_CONF";
            $result = mssql_query($query_process_status);
            while($row = mssql_fetch_assoc($result)) {
              $systemOnOff = $row['ON/OFF'];
              $systemArc = $row['arc'];
              $arcCopyRunning = $row['arcCopyRunning'];
            }


            $query_services = "SELECT count(*) as services FROM [dW_Log].[dbo].[ServiceStatus] where logtext like '%__akte_pas%';";
            $result = mssql_query($query_services);
            while($row = mssql_fetch_assoc($result)) {
              $numServices = $row['services'];
            }

            $query_runningServices = "SELECT count(*) as services FROM [dW_Log].[dbo].[ServiceStatus] where logtext not like '%Status:Stopped%';";
            $result = mssql_query($query_runningServices);
            while($row = mssql_fetch_assoc($result)) {
              $runningServices = $row['services'];
            }




if (!file_exists("/mnt/akte/pas_process/ready/sendReadyZip.lock")) { $sendStyle="background-color:LightGray; color:black;"; } else { $sendStyle="background-color:green; color:white;"; }
if (!file_exists("/mnt/akte/pas_process/backups/ready/fetchConfirmations.lock")) { $fetchStyle="background-color:LightGray; color:black;"; } else { $fetchStyle="background-color:green; color:white;"; }
if (!file_exists("/mnt/akte/pas_process/ready/moveReadyZip.lock")) { $moveStyle="background-color:LightGray; color:black;"; } else { $moveStyle="background-color:green; color:white;"; }
if ($systemOnOff=="OFF") { $systemStyle="background-color:red; color:white; "; } else { $systemStyle="background-color:green; color:white;"; }
if (!file_exists("/mnt/akte/pas_process/ready/arcCopy.lock"))  { $arcStyle="background-color:LightGray; color:black;"; } else { $arcStyle="background-color:green; color:white;"; }
if ($numServices<=30) { $srvStyle="background-color:red; color:white;"; }
elseif ($numServices>30 && $numServices<=60){ $srvStyle="background-color:orange; color:black;"; }
elseif ($numServices>60 && $numServices<=900){ $srvStyle="background-color:yellow; color:black;"; }
elseif ($numServices>90 && $numServices<=150){ $srvStyle="background-color:DarkOliveGreen; color:black;"; }
else { $srvStyle="background-color:DarkOliveGreen; color:black;"; }




echo"<div class='dailyReportFrame'>";
    echo"<table class='dailyTable'>";
    echo"<tr>";
    echo "<td style='$systemStyle'>Järjestelmä</td>";
    echo "<td style='$arcStyle'>ARC kopiointi</td>";
    echo "<td style='$srvStyle'> $numServices / $runningServices dwSrv</td>";
    echo "<td style='$sendStyle'>CSC siirto</td>";
    echo "<td style='$fetchStyle'>Kuittausten haku</td>";
    echo "<td style='$moveStyle'>NAS siirto</td>";
  echo"</tr>";
  echo "</table>";

echo"</div>";

}



function digiTitlePaperRescans ($caption,$issns,$langs,$types,$db) {

$select="select paanimeke
, kl.issn as ISSN
,min(to_char(np.pvm,'YYYY')) as VUOSI1
,max(to_char(np.pvm,'YYYY')) as VUOSI2
,count(*) as NITEITA
from nidepuutteet np, kausi_lehti kl
where kl.issn=np.issn and tyyppi in ('TO_BE_SCANNED_FROM_ORIGINAL')";

if ($types!="") {
    $select=$select." and ainyleismaare in (".$types.") ";

}
if ($langs!="") {
  $select=$select." and kieli in (".$langs.") ";
}

if ($issns!="") {
  $select=$select."and kl.issn in (".$issns.") ";
}






$select=$select." group by paanimeke, kl.issn order by 1,2,3";


$stid2 = oci_parse($db, $select);
$res1=oci_execute($stid2);
        if (!$res1) {
                echo "error:".oci_error()."--";
                exit(9);
        }
echo "<table style=\"margin: 0 auto;\"><caption>$caption</caption>";
echo "<tr><th>Nimeke</th><th>ISSN</th><th>Vuosi</th><th>Niteitä</th>
</tr>";

$i=0;

while (($row = oci_fetch_array($stid2, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
        $issn="";
        $vuosi="";
        $paanimeke="";
        $i=$i+1;
        $paanimeke=$row['PAANIMEKE'];
        $issn=$row['ISSN'];
        $vuosi1=$row['VUOSI1'];
        $vuosi2=$row['VUOSI2'];
        $niteita=$row['NITEITA'];
 echo "<tr>
<td><a href=\"http://selene.docworks.lib.helsinki.fi/sl-prosessi/puutteet_sls.php#$issn\">$paanimeke</a></td>
<td style=\"padding-left:2em;\">$issn</td>
<td style=\"padding-left:2em;\"> <a href=\"http://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".$vuosi1."\" target=\"_blank\" style=\"color:blue; text-decoration:none;\">$vuosi1 - $vuosi2</a></td>
<td style=\"padding-left:2em;\">$niteita</td>
</tr>";

}

        echo "</table>";

        oci_free_statement($stid2);


}



function digiPaperRescans ($caption,$issns,$startyear,$endyear,$langs,$types,$db) {

$select="select paanimeke
, kl.issn as ISSN
,to_char(pvm,'YYYY-MM-DD') as PVM
,no
,tyyppi
,to_char(ilmoitettu,'YYYY-MM-DD') as ILMOITETTU
, selitys
from nidepuutteet np, kausi_lehti kl
where kl.issn=np.issn and tyyppi in ('TO_BE_SCANNED_FROM_ORIGINAL')";

if ($types!="") {
    $select=$select." and ainyleismaare in (".$types.") ";

}
if ($langs!="") {
  $select=$select." and kieli in (".$langs.") ";
}

if ($issns!="") {
  $select=$select."and kl.issn in (".$issns.") ";
}

if ($startyear!="") {
 $select=$select."and to_char(pvm,'YYYY')>='".$startyear."' ";
}
if ($endyear!="") {
 $select=$select."and to_char(pvm,'YYYY')=<'".$endyear."' ";
}


$select=$select." order by 1,3,4";


$stid2 = oci_parse($db, $select);
$res1=oci_execute($stid2);
        if (!$res1) {
                echo "error:".oci_error()."--";
                exit(9);
        }
echo "<table style=\"margin: 0 auto;\"><caption>$caption</caption>";
echo "<tr><th>Nimeke</th><th>ISSN</th><th>PVM</th><th>Numero</th><th>Ilmoitettu</th><th>Huomautus</th>
<th>Skannattu</th>
<th>Exportattu</th>
<th>Hyväksytty</th>
<th>docid</th>
</tr>";

$i=0;

while (($row = oci_fetch_array($stid2, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
    $issn="";
    $pvm="";
    $no="";

    $i=$i+1;
    $paanimeke=$row['PAANIMEKE'];
    $issn=$row['ISSN'];
    $pvm=$row['PVM'];
    $no=$row['NO'];
    $vuosi=substr($pvm,0,4);
    $ilmoitettu=$row['ILMOITETTU'];
    $selitys=$row['SELITYS'];



    $query = "
    SELECT  docid,issuenum,depositbrowser_status,
    convert(varchar,issuedate,120) as issuedate,
    convert(varchar,dw_exportdate,120) as dw_exportdate,
    convert(varchar,scandate,120) as scandate,
    convert(varchar,depositbrowser_statusdate,120) as  depositbrowser_statusdate
    from dw_nlf_custom.dbo.sl_status
    where issn='$issn' and issuenum='$no' and issuedate='$pvm'";

    #echo "$query";
    $result = mssql_query($query);
    if( $result === false ) {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }

    $scandate="";
    $depositbrowser_status="";
    $depositbrowser_statusdate="";
    $dw_exportdate="";
    $docid="";

    while($row = mssql_fetch_array($result)) {

        $scandate=$row["scandate"];
        $depositbrowser_status=$row["depositbrowser_status"];
        $depositbrowser_statusdate=$row["depositbrowser_statusdate"];
        $dw_exportdate=$row["dw_exportdate"];
        $docid=$row["docid"];
        
    }


        if ($i % 2 == 0) {
            $class="even";
            $rdyclass="valmiseven";
        } else {
            $class="odd";
            $rdyclass="valmiseven";
        }
        if ($dw_exportdate) $dw_exportdate_class=$rdyclass; else $dw_exportdate_class="";
        if ($depositbrowser_status=="REJECTED") $depositbrowser_status_class="vika"; elseif ($depositbrowser_status=="ACCEPTED") $depositbrowser_status_class=$rdyclass ; else $depositbrowser_status_class="";
        if ($scandate) $scandate_class=$rdyclass; else $scandate_class="";

                 if ($paanimeke!=$prevnimeke) {
                        echo "<tr id=\"$issn\" style=\"background:white;\"><td></td><td style=\"padding-left:2em;\"></td><td style=\"padding-left:2em;\"></td><td style=\"padding-left:2em;\"></td><td></td><td></td></tr>";
                }
                echo "<tr class=\"$class\">
<td>$paanimeke</td>
<td style=\"padding-left:2em;\">$issn</td>
<td style=\"padding-left:2em;\"> <a href=\"http://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".$vuosi."\" target=\"_blank\" style=\"color:blue; text-decoration:none;\">$pvm</a></td>
<td style=\"padding-left:2em;\">$no</td>
<td>$ilmoitettu</td>
<td>$selitys</td>
<td class=\"$scandate_class\">$scandate</td>
<td class=\"$dw_exportdate_class\">$dw_exportdate</td>
<td class=\"$depositbrowser_status_class\">$depositbrowser_statusdate</td>
<td>$docid</td>

</tr>";



        $prevnimeke=$paanimeke;

}

        echo "</table>";
        oci_free_statement($stid2);

echo"</div>";


}


function digiFaults($caption,$issns,$startyear,$endyear,$langs,$types,$skipFaults,$db) {

$select="select ainyleismaare,paanimeke, kl.issn as ISSN,to_char(pvm,'YYYY-MM-DD') as PVM,no,tyyppi
,to_char(ilmoitettu,'YYYY-MM-DD') as ILMOITETTU,
selitys,null as ID,null as NIDE_ID
from nidepuutteet np, kausi_lehti kl
where kl.issn=np.issn";

if ($skipFaults!="") {
    $select=$select." and tyyppi not in (".$skipFaults.") ";

}
if ($types!="") {
    $select=$select." and ainyleismaare in (".$types.") ";

}
if ($langs!="") {
  $select=$select." and kieli in (".$langs.") ";
}

if ($issns!="") {
  $select=$select."and kl.issn in (".$issns.") ";
}

if ($startyear!="") {
 $select=$select."and to_char(pvm,'YYYY')>='".$startyear."' ";
}
if ($endyear!="") {
 $select=$select."and to_char(pvm,'YYYY')=<'".$endyear."' ";
}

$select=$select."
union
select ainyleismaare,paanimeke, kl.issn as ISSN,to_char(julkaisu_pvm,'YYYY-MM-DD') as PVM,no,tyyppi
,to_char(ilmoitettu,'YYYY-MM-DD') as ILMOITETTU,
selitys,no.id as ID, no.NIDE_ID
from nideongelmat no, kausi_lehti kl,nide n,sarjajulkaisu_nide sn
where kl.issn=sn.issn and no.nide_id=n.nide_id and n.nide_id=sn.sarjanide_id ";
if ($skipFaults!="") {
    $select=$select." and tyyppi not in (".$skipFaults.") ";

}
if ($types!="") {
    $select=$select." and ainyleismaare in (".$types.") ";

}
if ($langs!="") {
  $select=$select." and kieli in (".$langs.") ";
}

if ($issns!="") {
  $select=$select."and kl.issn in (".$issns.") ";
}

if ($startyear!="") {
 $select=$select."and to_char(julkaisu_pvm,'YYYY')>='".$startyear."' ";
}
if ($endyear!="") {
 $select=$select."and to_char(julkaisu_pvm,'YYYY')=<'".$endyear."' ";
}



$select=$select." order by 1 desc,2,4";
$stid2 = oci_parse($db, $select);
$res1=oci_execute($stid2);
        if (!$res1) {
                echo "error:".oci_error()."--";
                exit(9);
        }
echo "<table  style=\"margin: 0 auto;\"><caption>$caption</caption>";
echo "<tr><th>Nimeke</th><th>ISSN</th><th>PVM</th><th>Numero</th><th>Tyyppi</th><th></th><th>Ilmoitettu</th><th>Huomautus</th></tr>";
$i=0;

while (($row = oci_fetch_array($stid2, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
        $i=$i+1;
        $ainyleismaare=$row['AINYLEISMAARE'];
        $paanimeke=$row['PAANIMEKE'];
        $issn=$row['ISSN'];
        $pvm=$row['PVM'];
        $no=$row['NO'];
        $tyyppi=$row['TYYPPI'];
        $vuosi=substr($pvm,0,4);
        $ilmoitettu=$row['ILMOITETTU'];
        $selitys=$row['SELITYS'];
        $id=$row['ID'];
        $nideid=$row['NIDE_ID'];

        if ($i % 2 == 0) {
            $class="even";
        } else {
            $class="odd";
        }
                 if ($issn!=$previssn) {
                        echo "<tr id=\"$issn\"><td colspan=\"7\" style=\"background:white;\"></td></tr>";

                }

                echo "<tr class=\"$class\"><td>$paanimeke</td><td style=\"padding-left:2em;\">$issn</td>
<td style=\"padding-left:2em;\"> <a href=\"http://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=LIST&year=".$vuosi."\" target=\"_blank\" style=\"color:blue; text-decoration:none;\">$pvm</a></td>
<td style=\"padding-left:2em;\">$no</td><td>$tyyppi</td><td> <a href=\"https://digi.kansalliskirjasto.fi/sanomalehti/binding/".$nideid."\" target=\"_blank\" style=\"color:blue; text-decoration:none;\">$id</td><td>$ilmoitettu</td><td>$selitys</td></tr>";



        $previssn=$issn;
}
        echo "</table>";
        oci_free_statement($stid2);
}

function dailyReport($db, $query) {
    $result = mssql_query($query);
    $row = mssql_fetch_array($result);
    $sqlResult = $row["sivuja"];
    mssql_free_result( $result);
    return $sqlResult;
}

function naviHeaderNew () {
        $page=basename($_SERVER['PHP_SELF']);
        echo "<ul>";
        echo "<li "; if ($page=="index.php") {echo "class='active'";} echo "><a href='index.php'>Karttuvat/Takautuvat</a></li>";
        echo "<li "; if ($page=="index_old.php") {echo "class='active'";} echo "><a href=\"index_old.php\">Karttuvat</a></li>";
        echo "<li "; if ($page=="karttuvat_statics.php") {echo "class='active'";} echo "><a href='karttuvat_statics.php'>Karttuvat tilastot</a></li>";
        echo "<li "; if ($page=="index_takautuva.php") {echo "class='active'";} echo "><a href=\"index_takautuva.php\">Takautuvat</a></li>";
        echo "<li "; if ($page=="takautuvat_statics.php") {echo "class='active'";} echo "><a href='takautuvat_statics.php'>Takautuvat tilastot</a></li>";
        echo "<li "; if ($page=="niteet.php") {echo "class='active'";} echo "><a href='niteet.php'>Nidekohtaiset tiedot</a></li>";
        echo "<li "; if ($page=="ongelmaniteet.php") {echo "class='active'";} echo "><a href='ongelmaniteet.php'>Ongelmaniteet</a></li>";
        echo "<li "; if ($page=="ei_kasittelyssa.php") {echo "class='active'";} echo "><a href='ei_kasittelyssa.php'>Ei vielä käsittelyssä</a></li>";
        echo "<li><a href=\"http://akeso.docworks.lib.helsinki.fi/DepositBrowser/\" target='_blank'>DepositBrowser</a></li>";
        echo "<li "; if ($page=="levytila.php") {echo "class='active'";} echo "><a href='levytila.php'>Levytilat</a></li>";
        echo "<li><a href=\"https://delfoi.lib.helsinki.fi/_layouts/15/start.aspx#/Lists/Mikrofilmidigitointi/5%20SUPAG.aspx\" target='_blank'>MF-korjaukset</a></li>";
        echo "<li "; if ($page=="puutteet_sls.php") {echo "class='active'";} echo "><a href='puutteet_sls.php'>SLS puutteet</a></li>";
        echo "<li "; if ($page=="puutteet_hs.php") {echo "class='active'";} echo "><a href='puutteet_hs.php'>HS puutteet</a></li>";
        echo "<li "; if ($page=="pas_process.php") {echo "class='active'";} echo "><a href='pas_process.php'>PAS</a></li>";
        echo "<li "; if ($page=="pas_digi_status.php") {echo "class='active'";} echo "><a href='pas_digi_status.php'>Arcit</a></li>";
        echo "</ul>";

}

function naviHeader () {
        $page=basename($_SERVER['PHP_SELF']);
        echo "<ul>";
        echo "<li "; if ($page=="index_tarkastus.php") {echo "class='active'";} echo "\> <a href='index_tarkastus.php'>KUITTAUKSET</a></li>";
        echo "<li "; if ($page=="index.php") {echo "class='active'";} echo "\> <a href='index.php'>Karttuvat</a></li>";
        echo "<li "; if ($page=="karttuvat_statics.php") {echo "class='active'";} echo "\> <a href=\"karttuvat_statics.php\">Karttuvat tilastot</a></li>";
        echo "<li "; if ($page=="index_takautuva.php") {echo "class='active'";} echo "\> <a href=\"index_takautuva.php\">Takautuvat</a></li>";
        echo "<li "; if ($page=="takautuvat_statics.php") {echo "class='active'";} echo "\> <a href=\"takautuvat_statics.php\">Takautuvat tilastot</a></li>";
        echo "<li "; if ($page=="niteet.php") {echo "class='active'";} echo "\><a href=\"niteet.php\">Nidekohtaiset tiedot</a></li>";
        echo "<li "; if ($page=="ongelmaniteet.php") {echo "class='active'";} echo "\><a href=\"ongelmaniteet.php\">Ongelmaniteet</a></li>";
        echo "<li "; if ($page=="ei_kasittelyssa.php") {echo "class='active'";} echo "\> <a href='ei_kasittelyssa.php'>Ei vielÃ¤ kÃ¤sittelyssÃ¤</a></li>";
        echo "<li><a href=\"http://akeso.docworks.lib.helsinki.fi/DepositBrowser/\" target='_blank'>DepositBrowser</a></li>";
        echo "<li "; if ($page=="levytila.php") {echo "class='active'";} echo "\> <a href='levytila.php'>Levytilat</a></li>";
        echo "<li><a href=\"https://delfoi.lib.helsinki.fi/_layouts/15/start.aspx#/Lists/Mikrofilmidigitointi/5%20SUPAG.aspx\" target='_blank'>MF-korjaukset</a></li>";
        echo "<li "; if ($page=="puutteet_sls.php") {echo "class='active'";} echo "\><a href=\"puutteet_sls.php\">SLS puutteet</a></li>";
        echo "<li "; if ($page=="puutteet_hs.php") {echo "class='active'";} echo "\><a href=\"puutteet_hs.php\">HS puutteet</a></li>";
        echo "<li "; if ($page=="ams_tarkistus.php") {echo "class='active'";} echo "\><a href=\"ams_tarkistus.php\">AMS tarkistus</a></li>";
       echo "<li "; if ($page=="vuosikerta_tarkistus.php") {echo "class='active'";} echo "\><a href=\"vuosikerta_tarkistus.php\">Nimekkeiden tarkistus</a></li>";
        echo "<li "; if ($page=="pas_process.php") {echo "class='active'";} echo "\><a href=\"pas_process.php\">PAS</a></li>";
        echo "<li "; if ($page=="pas_digi_status.php") {echo "class='active'";} echo "\><a href=\"pas_digi_status.php\">Arcit</a></li>";
        echo "</ul>";
}

function rescan_docs($db, $query_rescans, $curren_page) {

   $result_rescans =mssql_query($query_rescans);
   if( $result_rescans === false ){
      echo "Error in statement preparation/execution.\n";
      die( print_r( mssql_get_last_message(), true));
   }
   $rows=mssql_num_rows($result_rescans);
   if ($rows>0) {
      echo "<div style=\"float:none; margin-right:20px;margin-bottom:30px;\">";
         echo "<h2>Uudelleen skannattavat niteet</h2>";
      echo"</div>\n";
      echo "<table width='100%'; style=margin-bottom:30px;>\n";
         echo"<tr><th>dwid</th><th>Työ</th><th>Huomautus</th></tr>";
         $i = 0;
         while($row =mssql_fetch_array($result_rescans)) {
            $i++;
            $title=$row["title"];
            $import_folder = "\\\\akte\\dw-in\\" . str_replace("\\", "/", $title);
            $docworksid=$row["docid"];
            $comment = $row["comment"];
            $comment = str_replace("\\u000d\\u000a", "", $comment);
            $input_id = "INPUT" . $docworksid;
            $hidden_div_id = "DIV" . $docworksid;
            $line_ood_or_even = ($i % 2 == 0) ? "rescaneven" : "rescanodd";
            echo"<tr class='$line_ood_or_even'>";
               echo "<form action='docid_page.php' method='GET'>";
                  echo "<input type='hidden' name='page' value='$curren_page'>";
                  echo "<input type='hidden' name='docId' value = '$docworksid'>";
                  echo "<th width='5%'><input type='submit' value = '$docworksid'></th>";
               echo "</form>";
                  //echo "<div id='$hidden_div_id' style='opacity: 0.0'; position: fixed; top: 0px;>";
                  //   echo "<input id='$input_id' style='opacity: 0.0'; type='text' value='$import_folder'/>";
                  //echo "</div>";
                  echo "<td width='30%'><a id='rescanLinks' onclick='copyTextToClipboard($docworksid)'>$title</a></td>";
                  echo"<td style='table-layout: auto; width: 45%;'>$comment</td>";
               echo "<form action='update_doc_status.php' method='post'>";
                  echo "<input type='hidden' name='docId' value='$docworksid'>";
                  echo "<input type='hidden' name='page' value='$curren_page'>";
                  if(checkDocIdStatus ($db, $docworksid) == 0) {
                     echo "<td width='5%' ><input type='submit' name = 'submit' onclick='return restartDoc($docworksid)' value = 'Restart'></td>";
                     echo "<td  width='5%'><input type='submit' name = 'submit' onclick='return deleteDoc($docworksid)' value = 'Remove'></td></tr>";
                  }else{
                     echo "<td width='5%' ><input type='submit' name = 'submit' value = 'Processing' disabled></td>";
                     echo "<td  width='5%'><input type='submit' name = 'submit' value = 'Processing' disabled></td>";
                  }
               echo "</form>";
            echo "</tr>";
         }
      echo"</table>";

   }
   mssql_free_result($result_rescans);
}

function status_list_table_latest ($db, $query, $table_title,$link) {

    $result_dw = mssql_query($query);
    echo "<table><caption> $table_title </caption>\n";
    echo"<tr><th>Nimeke</th><th>Niteitä</th><th>Sivuja</th><th style=\"color:#dddddd\">Uusin</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_dw)) {
        $nimeke=$row["nimeke"];
        $fullnimeke=$row["fullnimeke"];
        $niteita=$row["niteita"];
        $sivuja=$row["sivuja"];
        $uusin=$row["uusin"];
        echo"<tr>";
	    switch ($link) {
		case "mikrofilmi":
	    	echo "<td class='popup'><a onClick=\"MyWindow=window.open('mikrofilmi.php?ryhma=".urlencode($fullnimeke)."','MyWindow','width=600,height=1100'); return false;\" style=\"color:black; text-decoration:none;\" href=\"mikrofilmi.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
			break;
		case "niteet":
			echo "<td class='popup'><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($fullnimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\" href=\"niteet-popup.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
			break;
		case "digi":
            echo"<td class='popup'><a href=\"http://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=CALENDAR&year=".$vuosi."\" target=\"_blank\" style=\"color:black; text-decoration:none;\">".$nimeke."</a></td>";
            break;
		}
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td><td style=\"color:#dddddd\">&nbsp;&nbsp;$uusin</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result_dw);
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td><td></td></tr>";
    echo"</table>\n";
}

function dw_status_list_table ($db, $query, $table_title,$link) {

    $result_dw = mssql_query($query);
    if(mssql_num_rows($result_dw) == 0) {
        return;
 	}
    echo "<table  style=\"margin-top:10px;\"><caption style=\"text-align:center;\"> $table_title </caption>\n";
    echo"<tr><th>Nimeke</th><th>Vaihe</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $prev_nimeke="";
    while($row = mssql_fetch_array($result_dw)) {
        $nimeke=$row["nimeke"];
        $fullnimeke=$row["fullnimeke"];
        $vaihe=$row["vaihe"];
        $niteita=$row["niteita"];
        $sivuja=$row["sivuja"];

        if ($nimeke!=$prev_nimeke && $prev_nimeke!="") {
            echo "<tr><td>&nbsp;</td></tr>\n";
        }

        echo"<tr>";
        echo "<td class='popup'><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($fullnimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\" href=\"niteet-popup.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
        echo"<td>&nbsp;$vaihe</td><td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";

        $prev_nimeke=$nimeke;
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result_dw);
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td></td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>\n";


}
function status_list_table ($db, $query, $table_title,$link) {

    $result_dw = mssql_query($query);
    if(mssql_num_rows($result_dw)==0) {
        return;
    }

    echo "<table  style=\"margin-top:10px;\"><caption style=\"text-align:center;\"> $table_title </caption>\n";
    echo"<tr><th>Nimeke</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_dw)) {
        $nimeke=$row["nimeke"];
        $nimeke=$row["nimeke"];
        $fullnimeke=$row["fullnimeke"];
        $niteita=$row["niteita"];
        $sivuja=$row["sivuja"];
        echo"<tr>";
        switch ($link) {
            case "mikrofilmi":
            echo "<td><a onClick=\"MyWindow=window.open('mikrofilmi.php?ryhma=".urlencode($fullnimeke)."','MyWindow','width=600,height=1100'); return false;\" style=\"color:black; text-decoration:none;\" href=\"mikrofilmi.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
        break;
            case "niteet":
            echo "<td><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($fullnimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\" href=\"niteet-popup.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
            break;
        case "digi":
            echo"<td><a href=\"http://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=CALENDAR&year=".$vuosi."\" target=\"_blank\" style=\"color:black; text-decoration:none;\">".$nimeke."</a></td>";
            break;
        }
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result_dw);
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>\n";

}


function status_error_table ($db, $query, $table_title) {

   $result_dw = mssql_query($query);
   if (mssql_num_rows($result_dw)==0) {
      return;
   }
   echo "<div style=\"float:none; margin:20px;\">";
      echo "<h2 style='color: red;'>$table_title</h2>";
   echo"</div>";

   echo "<table class='erroAndRejectedTable'>";

      echo"<tr>";
         echo "<th>DocId</th>";
         echo "<th>Tila</th>";
         echo "<th>PVM</th>";
         echo "<th>Nimeke</th>";
      echo "</tr>";
      while($row = mssql_fetch_array($result_dw)) {
         $nimeke = $row["nimeke"];
         $pvm = $row["issuedate"];
         $status = $row["p_status"];
         $docId = $row["docId"];
         #$line_ood_or_even = ($status == 'Hylätty') ? "rejectedLine" : "errorLine";

	if ($status == "Hylätty") {
	    $line_odd_or_even = "rejectedLine";
	} elseif ($status=="Satunnaistarkistus") {
            $line_odd_or_even="randomReviewLine";
	} elseif ($status=="Virhe") {
    	    $line_odd_or_even = "errorLine";
	} else {
	   $line_odd_or_even = "x";
	}

         #$message = ($status == 'Hylätty') ? $row['workflow'] : $status;
         $message = $row['workflow'];
         echo"<tr class='$line_odd_or_even'>";
            echo "<form action='docid_page.php' method='GET'>";
               echo "<input type='hidden' name='page' value='index.php'>";
               echo "<input type='hidden' name='docId' value = '$docId'>";
               echo "<td><input type='submit' value = '$docId'></td>";
            echo "</form>";
            echo "<td>$message</td>";
            echo "<td>$pvm</td>";
            echo "<td>$nimeke</td>";
         echo "</tr>";
      }

   echo"</table>\n";
   mssql_free_result($result_dw);
}

function stat_table ($db, $query,$table_title,$col_title) {

    $result = mssql_query($query);
    echo "<table width=\"250px\">\n";
    echo"<tr><th> $col_title </th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $paivia=0;
    while($row = mssql_fetch_array($result)) {
        $pvm=$row["pvm"];
        $niteita=$row["niteita"];
        $sivuja=$row["sivuja"];
        echo"<tr>";
        echo"<td style=\"text-align:center;\">$pvm</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
        $paivia++;
    }
    $avg_niteet=round($sum_niteet/$paivia);
    $avg_sivut=round($sum_sivut/$paivia);
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"<tr style=\"font-weight:normal;\"><td style=\"text-align:right;\">Keskiarvo:</td><td style=\"text-align:right;\">$avg_niteet</td><td style=\"text-align:right;\">$avg_sivut</td></tr>";
    echo"</table>";
    mssql_free_result($result);
}

function digi_stat_by_years($db, $query_digissa) {
    $result_digissa = mssql_query($query_digissa);

    echo "<table><caption>Digissä vuosittain</caption>\n";
    echo"<tr><th>Vuosi</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_digissa)) {
        $niteita=$row["niteita"];
        $vuosi=$row["vuosi"];
        $sivuja=$row["sivuja"];
        echo"<tr>";
        echo"<td>$vuosi</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result_digissa);
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>\n";
}

function comellus_stat_table ($year,$conn) {
    $select_comellus="select depositor, count(distinct d.id) as NITEITA, count(*) AS SIVUJA, to_char(max(d.depositdate),'DD.MM.YYYY') as UUSIN
        from deposits d, files f
        where d.id=f.depositid
        and depositor not like '%+%'
        and to_char(d.depositdate,'YYYY')='".$year."'
        group by depositor
        order by depositor";

    $stid_comellus = oci_parse($conn, $select_comellus);
    $res_comellus=oci_execute($stid_comellus);

    if (!$res_comellus) {
        echo "error:".oci_error()."--";
        $e = oci_error();
        print_r($e);
        trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
        exit(9);
    }

    echo "<br><br><table><caption>".$year."</caption>\n";
    echo"<tr><th></th><th>Nimeke</th><th>Niteitä</th><th>Sivuja</th><th style=\"color:#888888\">Uusin</th</tr>";
    $sum_niteet_comellus=0;
    $sum_sivut_comellus=0;
    $laskuri=1;
    while (($row = oci_fetch_array($stid_comellus, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
        $depositor=$row['DEPOSITOR'];
        $niteita=$row['NITEITA'];
        $sivuja=$row['SIVUJA'];
        $uusin=$row['UUSIN'];
        echo"<tr><td style=\"text-align:right;\">$laskuri&nbsp; </td><td> $depositor</td><td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td><td style=\"color:#cccccc\">&nbsp; $uusin</td><tr>\n";
        $sum_sivut_comellus=$sum_sivut_comellus+$sivuja;
        $sum_niteet_comellus=$sum_niteet_comellus+$niteita;
        $laskuri++;
    }
	echo"<tr style=\"font-weight:bold;\"><td></td><td>Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet_comellus</td><td style=\"text-align:right;\">&nbsp;$sum_sivut_comellus</td></tr>";
	echo"</table>\n";
	$arvot=array($sum_niteet_comellus,$sum_sivut_comellus);
	return $arvot;

}


function digi_yearly_table ($db, $vuosi,$sivuennuste,$comellus_arvot) {


	$sum_niteet_comellus=$comellus_arvot[0];
	$sum_sivut_comellus=$comellus_arvot[1];

    echo"<h3 style=\"text-align:center;\">".$vuosi."</h3>\n";

    $query_digissa = "SELECT year(issuedate) as vuosi,COUNT(*) as niteita, sum(pages) as sivuja
        FROM [dw_nlf_custom].[dbo].[sl_status]
        where dw_exportdate is not null and depositbrowser_receivedate is not null and depositbrowser_status='ACCEPTED' and digi_importdate is not null and year(issuedate)='".$vuosi."'
        group by year(issuedate)
        order by year(issuedate) desc";

    $result_digissa = mssql_query($query_digissa);
    if( $result_digissa === false ) {
        echo "Error in statement preparation/execution.\n";
        die( print_r( mssql_get_last_message(), true));
    }
    echo "<table>\n";
    echo"<tr><th>Prosessi</th><th>Niteitä</th><th>Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $niteita=0;
    $sivuja=0;
    while($row = mssql_fetch_array($result_digissa)) {
        $niteita=$row["niteita"];
        $vuosi=$row["vuosi"];
        $sivuja=$row["sivuja"];
    }
    echo"<tr>";
    echo"<td>SUPAG</td>";
    echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
    $sum_niteet=$sum_niteet+$niteita;
    $sum_sivut=$sum_sivut+$sivuja;
    $sivuja=$sum_sivut_comellus;
    $niteita=$sum_niteet_comellus;
    echo"<tr>";
    echo"<td>Comellus</td>";
    echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
    $sum_niteet=$sum_niteet+$niteita;
    $sum_sivut=$sum_sivut+$sivuja;
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    $valmistumisprosentti=round($sum_sivut/$sivuennuste*100,1);
    echo"<tr style=\"font-weight:normal;\"><td>Ennakkoarvio:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$sivuennuste</td></tr>";
    echo"<tr style=\"font-weight:bold;\"><td>Valmistunut:</td><td style=\"text-align:right;\"></td><td style=\"text-align:right;\">&nbsp;$valmistumisprosentti %</td></tr>";
    mssql_free_result($result_digissa);
    echo"</table>\n<br>";
}


function chart_data ($year,$type,$query_type) {
    $chart_data=array();

    $query_karttuva = "set datefirst 1 SELECT datepart(wk,scandate) as viikko,COUNT(docid) as niteita,SUM(pages) as sivuja
        FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='".$year."' and title not like '%z+%'
        group by datepart(wk,scandate)
        order by datepart(wk,scandate) asc";
    $query_takautuva = "set datefirst 1
        SELECT datepart(wk,scandate) as viikko,COUNT(docid) as niteita,SUM(pages) as sivuja
        FROM [dw_nlf_custom].[dbo].[sl_status] where scandate is not null and year(scandate)='".$year."' and title like '%z+%'
        group by datepart(wk,scandate)
        order by datepart(wk,scandate) asc";

    $query = $query_type == "karttuva" ? $query_karttuva  : $query_takautuva;

    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }

    $sum_sivut=0;
    while($row = mssql_fetch_array($result))
        {
            $viikko=$row["viikko"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            $sum_sivut=$sum_sivut+$sivuja;
	    if ($type=="summa") {
		 array_push($chart_data,$sum_sivut);
	    } elseif ($type=="viikko") {
		 array_push($chart_data,$sivuja);
	    } else {

	   }
    }

    return implode(", ",$chart_data);

}

function status_table_yearly_steps ($db, $query, $caption) {
    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_last_message(), true));
    }
    echo "<table><caption>$caption</caption>\n";
    echo"<tr><th>Nimeke</th><th style=\"padding-left:10px;\">Vuosi</th><th style=\"padding-left:10px;\">Vaihe</th><th style=\"padding-left:10px;\">Niteitä</th><th style=\"padding-left:10px;\">Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result))
        {

	  //$issn=$row["issn"];
	  $nimeke=$row["nimeke"];
	  $fullnimeke=$row["fullnimeke"];
	  $niteita=$row["niteita"];
            $vuosi=$row["vuosi"];
            $sivuja=$row["sivuja"];
	  $vaihe=$row["vaihe"];
            echo"<tr>";
        //echo"<td class='popup'><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($fullnimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\">$nimeke</a></td>";
        echo "<td class='popup'><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($fullnimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\" href=\"niteet-popup.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
        echo"<td style=\"text-align:right;\">$vuosi&nbsp;&nbsp;</a></td>";
	echo"<td style=\"text-align:left;\">$vaihe</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result);
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td></td><td></td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>\n";
}

function status_table_yearly ($db, $query, $caption) {
    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_last_message(), true));
    }
    echo "<table><caption>$caption</caption>\n";
    echo"<tr><th>Nimeke</th><th style=\"padding-left:10px;\">Vuosi</th><th style=\"padding-left:10px;\">Niteitä</th><th style=\"padding-left:10px;\">Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result))
        {

	  $issn=$row["issn"];
	  $nimeke=$row["nimeke"];
	  $fullnimeke=$row["fullnimeke"];
	  $niteita=$row["niteita"];
            $vuosi=$row["vuosi"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        //echo"<td class='popup'><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($fullnimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\">$nimeke</a></td>";
        echo "<td class='popup'><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($fullnimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\" href=\"niteet-popup.php?ryhma=".urlencode($fullnimeke)."\">".$nimeke."</a></td>";
        echo"<td style=\"text-align:right;\">$vuosi</a></td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result);
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td></td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>\n";
}

function InspectionListing ($db, $query, $caption) {
    $result = mssql_query($query);
    if( $result === false ) {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_last_message(), true));
    }
    echo "<table><caption>$caption</caption>\n";
    echo"<tr><th>Nimeke</th><th style=\"padding-left:10px;\">Vuosi</th><th style=\"padding-left:10px;\">Niteitä</th><th style=\"padding-left:10px;\">Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result)) {

        $issn = $row["issn"];
        $nimeke = $row["nimeke"];
        $fullnimeke = $row["fullnimeke"];
        $niteita = $row["niteita"];
        $vuosi = $row["vuosi"];
        $sivuja = $row["sivuja"];
        echo"<tr>";
        //echo"<td class='popup'><a onClick=\"MyWindow6=window.open('niteet-popup.php?ryhma=".urlencode($fullnimeke)."','MyWindow6','width=1600,height=1000,resizable=yes,scrollbars=yes'); return false;\" style=\"color:black; text-decoration:none;\">$nimeke</a></td>";
        echo "<td class='popup'><a style='color:black; text-decoration:none;' href='tarkistus_UI.php?ryhma=$issn&year=$vuosi'>$nimeke</a></td>";
        echo"<td style=\"text-align:right;\">$vuosi</a></td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    
    }
    mssql_free_result($result);
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td></td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>\n";
}

function digi_yearly_stats_takautuva ($db, $query_digissa) {
    $result_digissa = mssql_query($query_digissa);
    if( $result_digissa === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_last_message(), true));
    }
    echo "<table><caption>Digissä</caption>\n";
    echo"<tr><th>Nimeke</th><th style=\"padding-left:10px;\">Vuosi</th><th style=\"padding-left:10px;\">Niteitä</th><th style=\"padding-left:10px;\">Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while($row = mssql_fetch_array($result_digissa)) {
        $issn=$row["issn"];
        $nimeke=$row["nimeke"];
        $niteita=$row["niteita"];
        $vuosi=$row["vuosi"];
        $sivuja=$row["sivuja"];
        echo"<tr>";
        echo"<td><a href=\"https://digi.kansalliskirjasto.fi/search?orderBy=DATE_DESC&title=".$issn."\" target=\"_blank\" style=\"color:black; text-decoration:none;\">$nimeke</td>";
        echo"<td style=\"text-align:right;\"><a href=\"http://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=CALENDAR&year=".$vuosi."\" target=\"_blank\" style=\"color:blue; text-decoration:none;\">$vuosi</a></td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
    }
    mssql_free_result($result_digissa);
    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td></td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>\n";
}

function digi_electronic_yearly_stats_takautuva ($conn_diona, $digital_issues) {

   $stid_diona = oci_parse($conn_diona, $digital_issues);
   $res_diona = oci_execute($stid_diona);

   if (!$res_diona) {
           echo "error:".oci_error()."--";
               $e = oci_error();
               print_r($e);
               trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
           exit(9);

   }
    echo "<table><caption>Sähköisesti Digissä</caption>";
    echo"<tr><th>Nimeke</th><th style=\"padding-left:10px;\">Vuosi</th><th style=\"padding-left:10px;\">Niteitä</th><th style=\"padding-left:10px;\">Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    while (($row = oci_fetch_array($stid_diona, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {

        $issn=$row["ISSN"];
        $nimeke=$row["NIMEKE"];
        $niteita=$row["NITEITA"];
        $vuosi=$row["VUOSI"];
        $sivuja=$row["SIVUJA"];
        echo"<tr>";
        echo"<td><a href=\"https://digi.kansalliskirjasto.fi/search?orderBy=DATE_DESC&title=".$issn."\" target=\"_blank\" style=\"color:black; text-decoration:none;\">$nimeke</td>";
        echo"<td style=\"text-align:right;\"><a href=\"http://digi.kansalliskirjasto.fi/sanomalehti/titles/".$issn."?display=CALENDAR&year=".$vuosi."\" target=\"_blank\" style=\"color:blue; text-decoration:none;\">$vuosi</a></td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;

    }

    echo"<tr style=\"font-weight:bold;\"><td>Yhteensä:</td><td></td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">&nbsp;$sum_sivut</td></tr>";
    echo"</table>";
}

function stat_table_takautuva ($db, $query,$table_title,$col_title) {
    $result = mssql_query($query);
    if( $result === false )
    {
         echo "Error in statement preparation/execution.\n";
         die( print_r( mssql_get_last_message(), true));
    }
    echo "<table width=\"250px\">\n";
    echo "<caption>$table_title</caption>";
    echo"<tr><th> $col_title </th><th style=\"padding-left:10px;\">Niteitä</th><th style=\"padding-left:10px;\">Sivuja</th></tr>";
    $sum_niteet=0;
    $sum_sivut=0;
    $paivia=0;
    while($row = mssql_fetch_array($result))
        {
            $pvm=$row["pvm"];
            $niteita=$row["niteita"];
            $sivuja=$row["sivuja"];
            echo"<tr>";
        echo"<td style=\"text-align:center;\">$pvm</td>";
        echo"<td style=\"text-align:right;\">$niteita</td><td style=\"text-align:right;\">$sivuja</td></tr>\n";
        $sum_niteet=$sum_niteet+$niteita;
        $sum_sivut=$sum_sivut+$sivuja;
        $paivia++;
    }
    $avg_niteet=round($sum_niteet/$paivia);
    $avg_sivut=round($sum_sivut/$paivia);
    echo"<tr style=\"font-weight:bold;\"><td style=\"text-align:right;\">Yhteensä:</td><td style=\"text-align:right;\">$sum_niteet</td><td style=\"text-align:right;\">$sum_sivut</td></tr>";
    echo"<tr style=\"font-weight:normal;\"><td style=\"text-align:right;\">Keskiarvo:</td><td style=\"text-align:right;\">$avg_niteet</td><td style=\"text-align:right;\">$avg_sivut</td></tr>";
    echo"</table>";
}

function option_selection ($db, $query, $sort) {

    $result = mssql_query($query);
    if( $result === false ) {
        echo "Error in statement preparation/execution.\n";
        die( print_r( mssql_get_last_message(), true));
    }
    while($row = mssql_fetch_array($result)) {
        $niteenryhma=$row["title"];
        $niteita=$row["niteita"];
        $sivuja=$row["sivuja"];
        $encodedniteenryhma=urlencode($niteenryhma);
        //echo "<option value=\"$encodedniteenryhma\" ".($niteenryhma==$ryhma ? 'selected="selected"' : '').">$niteenryhma ($niteita n. / $sivuja s.)</option>";
        echo "<option value=\"$encodedniteenryhma\" ".($niteenryhma=="" ? 'selected="selected"' : '').">$niteenryhma ($niteita n. / $sivuja s.)</option>";
    }
    echo "</select>";
    echo"<input type=\"submit\" value=\"Päivitä\">";
    echo" Järjestys:<input type=\"radio\" name=\"sort\" value=\"skannattu\" ".($sort=="skannattu" ? 'checked="checked"' : '').">skannattu - <input type=\"radio\" name=\"sort\" value=\"pvm\" ".($sort=="pvm" ? 'checked="checked"' : '').">pvm - <input type=\"radio\" name=\"sort\" value=\"nimeke\" ".($sort=="nimeke" ? 'checked="checked"' : '').">nimeke - <input type=\"radio\" name=\"sort\" value=\"docid\" ".($sort=="docid" ? 'checked="checked"' : '').">docid - <input type=\"radio\" name=\"sort\" value=\"digi-import\" ".($sort=="digi-import" ? 'checked="checked"' : '').">digi-import";
    echo "</form>\n";
    echo"</div>";
    mssql_free_result($result);

}

function modifieColor($rgbcolor, $percentage) {
    // Original color
    $statusColor = $rgbcolor;
    
    // Convert hexadecimal to RGB
    list($r, $g, $b) = sscanf($statusColor, "#%02x%02x%02x");
    
    $percent = ((100 - $percentage) / 100);

    // Make the color 5% darker (multiply by 0.95)
    $r = max(0, round($r * $percent));
    $g = max(0, round($g * $percent));
    $b = max(0, round($b * $percent));
    
    // Convert RGB back to hexadecimal
    $newColor = sprintf("#%02x%02x%02x", $r, $g, $b);
    
    return $newColor;

}

function niteet_listing ($db, $query) {

    $result = mssql_query($query);
    if( $result === false ) {
      echo "Error in statement preparation/execution.\n";
      die( print_r( mssql_get_last_message(), true));
    }
    
    echo "<div style='float:none; padding-top:50px;'>";
        echo "<table class='tableNiteet'>";
            echo"<tr style='position: relative; background-color: white; margin-top: -27px padding: 10px;'>";
                echo "<th>docid</th>";
                echo "<th>Nimeke</th>";
                echo "<th>pvm</th>";
                echo "<th>no</th>";
                echo "<th>sivuja</th>";
                echo "<th>skannattu</th>";
                echo "<th>DW tuonti</th>";
                echo "<th>DW export</th>";
                echo "<th>Hyväksytty</th>";
                echo "<th>Status</th>";
                echo "<th>DIGI</th>";
                echo "<th>Tuotanto digiin</th>";
                echo "<th>Viive digiin</th>";
                echo "<th>DW delete</th>";
            echo "</tr>\n";
            $edellinenkokoelma="";
            $i=0;
            $sivuja=0;
            while($row = mssql_fetch_array($result)) {

                  $title=$row["title"];
                  $formattedTitle = generateFormattedString($title);
                  $docworksid=$row["docid"];
                  $pvm=$row["issuedate"];
                  $issn=$row["issn"];
                  $issuenum=$row["issuenum"];
                  $pages=$row["pages"];
                  $scandate=$row["scandate"];
                  $dw_importdate=$row["dw_importdate"];
                  $dw_exportdate=$row["dw_exportdate"];
                  $accepted_datetime = $row["accepted_datetime"];
                  $depositbrowser_processdate=$row["depositbrowser_processdate"];
                  $depositbrowser_statusdate=$row["depositbrowser_statusdate"];
                  $depositbrowser_mfdate=$row["depositbrowser_mfdate"];
                  $digi_importdate=$row["digi_importdate"];
                  $dw_deletedate=$row["dw_deletedate"];
                  $digi_status=trim($row["digi_status"]);
                  $depositbrowser_comment=$row["depositbrowser_comments"];
                  $depositbrowser_errorpages=$row["depositbrowser_errorpages"];
                  $digi_url="http://digi.kansalliskirjasto.fi/sanomalehti/binding/".$digi_status."?page=1";
                  $restar_status = $row["restart"];
                  $remove_status = $row["remove"];
                  //$docStatus = $row["status"];
                  $depositbrowser_status=$row["depositbrowser_status"];
            
                  if ($scandate=="") $scandate="2017-01-01";
                  if ($digi_importdate=="") {
                      $date_end=date("Y-m-d");
                  } else {
                      $date_end=$digi_importdate;
                  }
                  $startdate=date_create($scandate);
                  $enddate=date_create($date_end);
                  $productiontime=date_diff($startdate,$enddate)->format("%a");
            
                  $pubdate=date_create($pvm);
                  $delay=date_diff($pubdate,$enddate)->format("%a");
            
                  $statusColorReady = modifieColor($statusColor, 20);
            
                  
                  if ($depositbrowser_mfdate!="" && $digi_importdate!="") {
                      $class="valmis mf $rdyclass";
                  }
                  if ($dw_importdate) {
                      $dw_importdate_class=$rdyclass;
                  } else {
                      $dw_importdate_class="";
                  }
                  if ($dw_exportdate) {
                      $dw_exportdate_class=$rdyclass;
                  } else {
                      $dw_exportdate_class="";
                  }
                  if ($accepted_datetime) {
                      $depositbrowser_receivedate_class=$rdyclass;
                  } else {
                      $depositbrowser_receivedate_class="";
                  }
                  
                          $statusColor = "";
            
                  switch ($depositbrowser_status) {
                      case "digi_importing":
                          $statusColor = '#57888E';
                          $depositbrowser_status_class="";
                          break;
                      case "digi":
                          $statusColor = '#04D7F7';
                          $depositbrowser_status_class="";
                          break;
                      case "completed":
                          $statusColor = '#02FC34';
                          $depositbrowser_status_class="";
                          break;
                      case "rejected":
                          $statusColor = '#F785BA';
                          $depositbrowser_status_class="rejectDiv";
                          break;
                      case "missing":
                          $statusColor = '#F78325';
                          $depositbrowser_status_class="";
                          break;
                      case "accepted":
                          $statusColor = '#B9E2B1';
                          $depositbrowser_status_class=$rdyclass;
                          break;
                      case "hold":
                          $statusColor = '#EACB1E';
                          $depositbrowser_status_class="";
                          break;
                      case "rescan":
                          $statusColor = '#FC0004';
                          $depositbrowser_status_class="rescanDiv";
                          break;
                      case "QA":
                          $statusColor = '#DBF28A';
                          $depositbrowser_status_class="";
                          break;
                      default:
                          $statusColor = '#FFFFFF';
                          $depositbrowser_status_class="";
                  }
                  
                  $statusInfinnish = translateStatus($depositbrowser_status);
                  
                  if ($i % 2 == 0) {
                      $class="even";
                      $rdyclass="valmiseven";
                      $statusColor = modifieColor($statusColor, 10);
                  } else {
                      $class="odd";
                      $rdyclass="valmisodd";
                  }
                  
                  if ($productiontime<4) $productiontime_class="production_ok";
                  if ($productiontime>3) $productiontime_class="production_slow";
                  if ($productiontime>7) $productiontime_class="production_veryslow";
            
                  echo"<tr style='background-color: $statusColor;'>";
                      echo "<form action='docid_page.php' method='GET'>";
                         echo "<input type='hidden' name='page' value='niteet.php'>";
                         echo "<input type='hidden' name='docId' value = '$docworksid'>";
                         echo "<td><input type='submit' value = '$docworksid'></td>";
                      echo "</form>";
                      echo "<td>$formattedTitle</td>";
                      echo "<td>$pvm</td>";
                      echo "<td>$issuenum</td>";
                      echo "<td>$pages</td>";
                      echo "<td>$scandate</td>";
                      echo "<td>$dw_importdate</td>";
                      echo "<td>$dw_exportdate</td>";
                      echo "<td>$accepted_datetime</td>";
                      
                      echo "<td  class='tooltip'>$statusInfinnish";
                          if ($depositbrowser_status == "rejected" || $depositbrowser_status == "rescan") {
                              echo "<span class='tooltiptext'>$depositbrowser_comment<br>sivut: $depositbrowser_errorpages</span>";
                          }
                      echo"</td>";
                      if($digi_importdate == "") {
                          echo "<form action='update_doc_status.php' method='post'>";
                              echo "<input type='hidden' name='docId' value='$docworksid'>";
                              echo "<input type='hidden' name='page' value='niteet.php'>";
                              
                              if($restar_status == 0 && $remove_status == 0) {
                                  echo "<td><input type='submit' onclick='return restartDoc($docworksid)' name = 'submit' value = 'Restart'><input type='submit' onclick='return deleteDoc($docworksid)' name = 'submit' value = 'Remove'></td>";
                              }else{
                                  echo "<td><input type='submit' onclick='return restartDoc($docworksid)' name = 'submit' value = 'Processing' disabled></td>";
                              }
                              
                          echo "</form>";
                      }else{
                          echo "<td><a href=\"$digi_url\" target=\"_new\"\">$digi_importdate</a></td>";
                      }
            
                      echo "<td>&nbsp;$productiontime</td>
                      <td>$delay</td>
                      <td>$dw_deletedate&nbsp;</td>
                  </tr>\n";
                  
                  $edellinenkokoelma=$title;
                  $i+=1;
                  $sivuja=$sivuja+$pages;
            }
        echo "</table>\n";
        echo "<h3>Niteitä:$i, sivuja: $sivuja</h3>";
    echo "</div>";

}

function checkDocIdStatus ($db, $docId) {

    $sql_query = "SELECT [restart], [remove] FROM sl_status where docid = $docId";
    $result = mssql_query($sql_query);

    while($row = mssql_fetch_array($result)) {
        $restart = $row["restart"];
        $remove = $row["remove"];
    }
    mssql_free_result($result);
    $processing = ($restart > 0 or $remove > 0) ? 1 : 0;
    return $processing;
}

function problem_documents ($db, $query,$caption,$type) {
    $result = mssql_query($query);
    if( $result === false ) {
      echo "Error in statement preparation/execution.\n";
      die( print_r( mssql_get_last_message(), true));
    }

    echo"<div style=\"float:none; clear:both;\">";

        echo "<div style=\"margin-left:auto;margin-right:auto;margin-top:20px;\">";
            echo "<table  class='tableNiteet'>";
                echo "<caption>$caption</caption>";
                if ($type=="embargo") {
                    $pv_otsikko="Odotusaika (pv)";
                } else {
                    $pv_otsikko="Tuotannossa (pv)";
                }
            
                echo"<tr>";
                    echo "<th>$pv_otsikko</th>";
                    echo "<th>dwid</th>";
                    echo "<th>Nimeke</th>";
                    echo "<th>pvm</th>";
                    echo "<th>no</th>";
                    echo "<th>sivuja</th>";
                    echo "<th>skannattu</th>";
                    echo "<th>DW tuonti</th>";
                    echo "<th>DW aika</th>";
                    echo "<th>DW vaihe</th>";
                    echo "<th>DW status</th>";
                    echo "<th>DW export</th>";
                    echo "<th>Hyväksytty</th>";
                    echo "<th>Status</th>";
                    echo "<th>Import</th>";
                    echo "<th>DIGI</th>";
                    echo "<th>DW delete</th>";
                    echo "<th>MF date</th>";
                echo "</tr>";
                $edellinenkokoelma="";
                $i=0;
                $sivuja=0;
                while($row = mssql_fetch_array($result)) {
            
                    $kesto = $row["kesto"];
                    $title = $row["title"];
                    $formattedTitle = generateFormattedString($title);
                    $issn = $row["issn"];
                    $docworksid = $row["docid"];
                    $pool = $row["pool"];
                    $pvm = $row["issuedate"];
                    $issn = $row["issn"];
                    $issuenum = $row["issuenum"];
                    $pages = $row["pages"];
                    $scandate = $row["scandate"];
                    $dw_importdate = $row["dw_importdate"];
                    $dw_exportdate = $row["dw_exportdate"];
                    $dw_jobname = $row["jobname"];
                    $dw_status = $row["status"];
                    $dw_date = $row["dw_date"];
                    $accepted_datetime = $row["accepted_datetime"];
                    $depositbrowser_status = $row["depositbrowser_status"];
                    $depositbrowser_mfdate = $row["depositbrowser_mfdate"];
                    $digi_importdate = $row["digi_importdate"];
                    $dw_deletedate = $row["dw_deletedate"];
                    $digi_status = trim($row["digi_status"]);
                    $depositbrowser_comment = $row["depositbrowser_comments"];
                    $depositbrowser_errorpages = $row["depositbrowser_errorpages"];
                    $digi_url = "http://digi.kansalliskirjasto.fi/sanomalehti/binding/".$digi_status."?page=1";
                    $restart_status = $row["restart_status"];
                    $remove_status = $row["remove_status"];
                
                    if ($scandate=="") {
                        $scandate="2017-01-01";
                    }
                    if ($digi_importdate=="") {
                        $date_end=date("Y-m-d");
                    } else {
                        $date_end=$digi_importdate;
                    }
                    $startdate=date_create($scandate);
                    $enddate=date_create($date_end);
                    $productiontime=date_diff($startdate,$enddate)->format("%a");
                    $pubdate=date_create($pvm);
                    $delay=date_diff($pubdate,$enddate)->format("%a");
                
                    if ($i % 2 == 0) {
                        $class="even";
                        $rdyclass="valmiseven";
                    } else {
                        $class="odd";
                        $rdyclass="valmisodd";
                    }
                
                    if ($depositbrowser_mfdate!="" && $digi_importdate!="") {
                        $class="valmis mf $rdyclass";
                    }
                    if ($dw_importdate) {
                        $dw_importdate_class=$rdyclass;
                    } else {
                        $dw_importdate_class="";
                    }
                    if ($dw_exportdate) {
                        $dw_exportdate_class=$rdyclass;
                    } else {
                        $dw_exportdate_class="";
                    }

                    if ($depositbrowser_status == "rejected" || $depositbrowser_status == "rescan") {
                        $depositbrowser_status_class="vika";
                    }
                    if ($depositbrowser_mfdate) {
                        $depositbrowser_mfdate_class=$rdyclass;
                    } else {
                        $depositbrowser_mfdate_class="";
                    }
                    if ($digi_importdate) {
                        $digi_importdate_class=$rdyclass;
                    } else {
                        $digi_importdate_class="";
                    }
                    
                  $statusColor = "";
            
                  switch ($depositbrowser_status) {
                      case "digi_importing":
                          $statusColor = '#57888E';
                          $depositbrowser_status_class="";
                          break;
                      case "digi":
                          $statusColor = '#04D7F7';
                          $depositbrowser_status_class="";
                          break;
                      case "completed":
                          $statusColor = '#02FC34';
                          $depositbrowser_status_class="";
                          break;
                      case "rejected":
                          $statusColor = '#F785BA';
                          $depositbrowser_status_class="rejectDiv";
                          break;
                      case "missing":
                          $statusColor = '#F78325';
                          $depositbrowser_status_class="";
                          break;
                      case "accepted":
                          $statusColor = '#B9E2B1';
                          $depositbrowser_status_class=$rdyclass;
                          break;
                      case "hold":
                          $statusColor = '#EACB1E';
                          $depositbrowser_status_class="";
                          break;
                      case "rescan":
                          $statusColor = '#FC0004';
                          $depositbrowser_status_class="rescanDiv";
                          break;
                      case "QA":
                          $statusColor = '#DBF28A';
                          $depositbrowser_status_class="";
                          break;
                      default:
                          $statusColor = '#FFFFFF';
                          $depositbrowser_status_class="";
                  }
                  
                  $statusInfinnish = translateStatus($depositbrowser_status);
                
                    $failed_path="/mnt/sl_import/failed/";
                    $failed_logfile=$failed_path.$issn."_".$pvm."_".$issuenum.".zip.log";
                    if ($digi_status=="") {
                        if (file_exists($failed_logfile)) {
                            $failedclass="vika";
                            $logcell="<a href='show_importlog.php?file=$failed_logfile' target='_new'>X</a>";
                        } else {
                            $failedclass="";
                            $logcell="";
                        }
                    }
                
                    if ($dw_deletedate) {
                        $dw_deletedate_class=$rdyclass;
                    } else {
                        $dw_deletedate_class="";
                    }
                    if ($productiontime<4) $productiontime_class="production_ok";
                    if ($productiontime>3) $productiontime_class="production_slow";
                    if ($productiontime>7) $productiontime_class="production_veryslow";
                    if ($type=="embargo") $productiontime_class="production_ok";
                    if ($pool=="") {
                        $pool_class="vika";
                    } else {
                        $pool_class="";
                    }
                
                    echo"<tr style='background-color: $statusColor;'>";
                         echo "<td class='$productiontime_class $digi_importdate_class' align='center'>$kesto</td>";
                         echo "<form action='docid_page.php' method='GET'>";
                         echo "<input type='hidden' name='page' value='ongelmaniteet.php'>";
                         echo "<input type='hidden' name='docId' value = '$docworksid'>";
                         echo "<td class=\"$pool_class\"><input type='submit' value = '$docworksid'></td>";
                         echo "</form>";
                         echo "<td>$formattedTitle</td>";
                         echo "<td>$pvm</td>";
                         echo "<td>$issuenum</td>";
                         echo "<td>$pages</td>";
                         echo "<td>$scandate</td>";
                         echo "<td>$dw_importdate</td>";
                         echo "<td>$dw_date</td>";
                         echo "<td>$dw_jobname</td>";
                         echo "<td>$dw_status</td>";
                         echo "<td>$dw_exportdate</td>";
                         echo "<td>$accepted_datetime</td>";
                         echo "<td class='$depositbrowser_status_class tooltip'>$statusInfinnish";
                         if ($depositbrowser_status == "rejected" || $depositbrowser_status == "rescan") {
                             echo "<span class='tooltiptext'>$depositbrowser_comment<br>sivut: $depositbrowser_errorpages</span>";
                         }
                         echo"</td>";
                         echo "<td>$logcell</td>";
                   
                         if($depositbrowser_status != "ACCEPTED") {
                             echo "<form action='update_doc_status.php' method='post'>";
                             echo "<input type='hidden' name='docId' value='$docworksid'>";
                             echo "<input type='hidden' name='page' value='ongelmaniteet.php'>";
                             if($restart_status == 0 && $remove_status == 0) {
                                 echo "<td><input type='submit' onclick='return restartDoc($docworksid)' name = 'submit' value = 'Restart'><input type='submit' onclick='return deleteDoc($docworksid)' name = 'submit' value = 'Remove'></td>";
                             }else{
                                 echo "<td><input type='submit' onclick='return restartDoc($docworksid)' name = 'submit' value = 'Processing' disabled></td>";
                             }
                             echo "</form>";
                         }else{
                             echo "<td><a href='$digi_url' target='_new'>$digi_importdate</a></td>";
                         }
               
                         echo "<td>$dw_deletedate</td>";
                         echo "<td>$depositbrowser_mfdate</td>";
                    echo "</tr>";
                    $edellinenkokoelma=$title;
                    $i+=1;
                    $sivuja=$sivuja+$pages;
                }
            echo "</table>";
            echo "<h3 style=\"text-align: center;\">Niteitä:$i, sivuja: $sivuja</h3>";
        echo "</div>";
    echo "</div>";
    mssql_free_result($result);
}

function updateDocumentStatus($db, $docId, $column) {
    $sql = "UPDATE sl_status SET [$column] = 1 WHERE docid = $docId";
    $result = mssql_query($sql) or die(mssql_errors());
    mssql_free_result($result);
}

function updateDocumentAccepted($db, $docId, $user, $problemType, $problemStatement) {
    $sql = "UPDATE dw_nlf_custom.dbo.sl_status SET status = 'accepted', status_changed_by = '$user', accepted_datetime = GETDATE(),
        problem_statement = CONCAT(problem_statement, ' ', '$problemStatement'), problem_type = CONCAT(problem_type, ' ', '$problemType') WHERE docid = $docId";
    $result = mssql_query($sql) or die(mssql_errors());
    mssql_free_result($result);
}

function updateDocumentRemove($db, $docId, $user) {
    $sql = "UPDATE dw_nlf_custom.dbo.sl_status SET status = 'remove', status_changed_by = '$user', remove = 1 WHERE docid = $docId";
    $result = mssql_query($sql) or die(mssql_errors());
    mssql_free_result($result);
}

function updateDocumentHold($db, $docId, $user) {
    $sql = "UPDATE dw_nlf_custom.dbo.sl_status SET status = 'hold', status_changed_by = '$user' WHERE docid = $docId";
    $result = mssql_query($sql) or die(mssql_errors());
    mssql_free_result($result);
}

function updateDocumentRemoveTEMP($db, $docId, $user) {
    $sql = "UPDATE dw_nlf_custom.dbo.sl_status SET status = 'remove', status_changed_by = '$user' WHERE docid = $docId";
    $result = mssql_query($sql) or die(mssql_errors());
    mssql_free_result($result);
}

function getDocidReferenceId($docId) {
    $query = "select ID from dw_nlf_custom.dbo.sl_status where docid = $docId";
    $result = mssql_query($query);
    $docid_ID = array();

    while($row = mssql_fetch_array($result)) {
        $docid_ID = $row["ID"];
    }
    mssql_free_result($result);
    
    return $docid_ID;
}

function updateDocumentRejected($docId, $user, $comment, $errorPages) {
    $sql = "UPDATE dw_nlf_custom.dbo.sl_status SET status = 'rejected', status_changed_by = '$user', QA_error_comment = '$comment', QA_error_pages = '$errorPages' WHERE docid = $docId";
    $result = mssql_query($sql) or die(mssql_errors());
    mssql_free_result($result);
}

function insertVolumeProblemsNotes($docId, $type, $description) {
 
    $docid_ID = getDocidReferenceId($docId);

    $hasRow = checkIfDocHAsVolumeProblemsNotes($docid_ID);
    
    if(!$hasRow) {
        $sql = "insert into dw_nlf_custom.dbo.volume_problems_log (docid, type, [description], ID) values ($docId, '$type', '$description', $docid_ID)";
        $result = mssql_query($sql) or die(mssql_errors());
        mssql_free_result($result);

    } else {
        $_SESSION['displayErrorMessage'] =  "Niteelle oli jo lisätty puute merkintä HOX!!";
    }
    
}

function checkIfDocHAsVolumeProblemsNotes($docid_ID) {

    // Your SQL query
    $query = "select * from dw_nlf_custom.dbo.volume_problems_log where ID = $docid_ID";
    
    // Execute the query
    $result = mssql_query($query);
    
    $hasRow = false;
    // Check if there are rows in the result set
    if (mssql_num_rows($result) > 0) {
        $hasRow = true;
    }
    
    return $hasRow;
    
}

function updateDocumentAcceptedComment($db, $docId, $comment) {
    $sql = "update dw_nlf_custom.dbo.sl_status set QA_comment = CONCAT('$comment' + CHAR(13) + CHAR(10), QA_comment)  where docid = $docId";
    echo $sql;
    $result = mssql_query($sql) or die(mssql_errors());
    mssql_free_result($result);
}

function HumanSize($Bytes) {
    $Type=array("", "K", "M", "G", "T", "P", "E", "Z", "Y");
    $Index=0;
    while($Bytes>=1024)
    {
      $Bytes/=1024;
      $Index++;
    }
    $Bytes=round($Bytes,1);
    return("".$Bytes." ".$Type[$Index]."t");
}

function getDirectoryFreeSpacePercentage($directory) {
    $total_space = disk_total_space($directory);
    $free_space = disk_free_space("$directory");
    $free_space_percentage = intval($free_space / $total_space * 100);
    return intval($free_space_percentage);
}

function directorysFreeSpaceIndicator($DW_directorys, $DW_directorys_alert_space) {
    $total_percent = 100;

    echo "<div class='directoryBase'>";
    foreach($DW_directorys as $x => $x_value) {
      $directory_free_space = getDirectoryFreeSpacePercentage($x_value);
      $directory_free_byte = HumanSize(disk_free_space("$x_value"));
      $used_percent = ($total_percent - $directory_free_space);
      $percent = intval($used_percent/$total_percent * 100)."%";
      //$indicator_color = "#81EF91";
      $indicator_color = "lightgray";
      if($directory_free_space < $DW_directorys_alert_space) {
        $indicator_color = "#F71B34";
      }
      echo "<div class='directory'>";
        echo "$x : $directory_free_byte <div title='Vapaata tila $directory_free_byte' id='progress_$x' style='width:200px;float:right;border:1px solid #ccc;'>";
          echo '<script language="javascript">
          document.getElementById("progress_'.$x.'").innerHTML="<div style=\"width:'.$percent.';background-color:'.$indicator_color.';\">'. $percent.' </div>";
          </script>';
        echo "</div>";
      echo "</div>";
    }
    echo "</div>";
}

function writeLog($string, $logFile = 'log.txt') {

    $date = new DateTime();
    $date = $date->format('Y-m-d H:i:s');
    $string = $date . " : " . $string . "\r\n";
    $log_file = "log/$logFile";
    if ($fh = @fopen($log_file, "a+")) {
        fputs($fh, $string, strlen($string));
        fclose($fh);
        return true;
    } else {
        return false;
    }

}

function deleteIssueFromDepositDatabase($db, $conn, $docId) {
    $query = "SELECT title, issuenum, convert(varchar,issuedate,104) as issuedate from dw_nlf_custom.dbo.sl_status where docid = $docId";
    $result = mssql_query($query);
    $docArray = array();

    while($row = mssql_fetch_array($result)) {
        $docArray = array("title" => $row["title"], "issuenum" => $row["issuenum"], "issuedate" => $row["issuedate"]);
    }
    mssql_free_result($result);

    if( sizeof($docArray) == 3 ) {

        $title = $docArray["title"];
        $issueNum = $docArray["issuenum"];
        $date = $docArray["issuedate"];

        $select = "select ID from deposits where depositor = '$title' and issuenumber = $issueNum and depositdate = TO_DATE('$date', 'DD-MM-YYYY')";
        $oci_query = oci_parse($conn, $select);
        oci_execute($oci_query);
        $id = "";
        while (($row = oci_fetch_array($oci_query, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
            $id = $row["ID"];
        }
        oci_free_statement($oci_query);

        if( $id != "" ) {

            $deleteQuery = "delete from files where depositid = $id";
            $oci_query = oci_parse($conn, $deleteQuery);
            if (false === oci_execute($oci_query)) {
                echo "<br>ERROR files";
            };
            oci_free_statement($oci_query);

            $deleteQuery = "delete from receipt where depositid = $id";
            $oci_query = oci_parse($conn, $deleteQuery);
            if (false === oci_execute($oci_query)) {
                echo "<br>ERROR receipt";
            };
            oci_free_statement($oci_query);

            $deleteQuery = "delete from deposits where ID = $id";
            $oci_query = oci_parse($conn, $deleteQuery);
            if (false === oci_execute($oci_query)) {
                return false;
            };
            oci_free_statement($oci_query);

        }
    }

    return true;
}
    
function getAllIssues($db, $query, $selectedTitle = "-") {
   
   $result = mssql_query($query);
   $titleArray = array($selectedTitle);

   while($row = mssql_fetch_array($result)) {
      array_push($titleArray, $row["title"]);
   }

   mssql_free_result($result);
   return $titleArray;
}

function getTitleAllIssuesNumbers($db, $title, $sessionIssn) {
   $query = "select distinct issuenum from dw_nlf_custom.dbo.sl_status where title = '$title'";
   $result = mssql_query($query);
   $issueNumbers = array();

   while($row = mssql_fetch_array($result)) {
      array_push($issueNumbers, $row["issuenum"]);
   }
   sort($issueNumbers);
   array_unshift($issueNumbers, $sessionIssn);
   mssql_free_result($result);
   return $issueNumbers;
}

function getTitleAllIssuesDates($db, $title, $sessionIssn) {

   $query = "select distinct FORMAT (issuedate, 'yyyy-MM-dd') as issuedate from dw_nlf_custom.dbo.sl_status where title = '$title' and issuenum = $sessionIssn";

   $result = mssql_query($query);
   $issueDates = array();

   while($row = mssql_fetch_array($result)) {
      array_push($issueDates, $row["issuedate"]);
   }

   sort($issueDates);
   mssql_free_result($result);

   return $issueDates;
}

function docIdSearchForDocPage($currentPage) {
   echo "<div class='seartch'>Hae docid:llä";

      echo "<form action='docid_page.php' method='GET'>";
         echo "<input type='hidden' name='page' value='$currentPage'>";
         echo "DocId: <input type='text' name='docId'>";
         echo "<input type='submit'>";
      echo "</form>";

   echo "</div>";  
}

function doc_info_page($db, $curren_page, $docid) {

   $curren_page = ($curren_page == 'docid_page.php') ? 'index.php' : $curren_page;
   
   if ( !is_numeric($docid) ) {
      $_SESSION['displayErrorMessage'] = "Työtä DocId : $docid, ei ole numeerinen arvo. Tarkista DocId?.";
      header("Location: $curren_page");
      return;
   }
   include("global_variables.php");
   
   $poolFolder = getDocPoolFolder($docid);
   
   $query_form_pool_main = "SELECT P_FILTER3 as Projekti, SUBSTRING(P_FILTER2, CHARINDEX('\', P_FILTER2) + 1, LEN(P_FILTER2)) as Nimi,
         case 
         when  P_STATUS = 'R' then 'Hylätty'
         when  P_STATUS = 'H' then 'Hold'
         when  P_STATUS = 'W' then 'Work'
         when  P_STATUS = 'E' then 'Virhe'
         else  P_STATUS end as Status,
         P_JOBNAME as Vaihe, P_PAGECOUNT as Sivuja,
         P_COMMENT as Huomautus from dW_Pool.dbo.Main where P_ARTID = $docid";
   $result = mssql_query($query_form_pool_main);
   if(mssql_num_rows($result) == 0) {
      //$_SESSION['displayErrorMessage'] = "Työtä DocId : $docid, ei löytynyt tietokannasta.";
      //header("Location: $curren_page");
      //return;
      echo "<div id = 'docIdPageFrame'>";
      echo "<div id = 'docInfo'>";
      echo "<h3 style='padding-bottom: 10'>Docid $docid ei löytynyt docworks tietokannasta.</h3>";
      echo "</div>";
      echo "</div>";
   }else{
      echo "<div id = 'docIdPageFrame'>";
         echo "<div id = 'docInfo'>";
   
         $input_id = "INPUT" . $docid . "_doc";
         $hidden_div_id = "DIV" . $docid . "_doc";
         $newDoc = $docid . "_doc";
         echo "<div id='$hidden_div_id' style='opacity: 0.0; position: fixed; top: 175px; right: 50%;'>";
         echo "<input id='$input_id' style='opacity: 0.0; type='text' value='$docid'/>";
         echo "</div>";
         echo "<h3 style='padding-bottom: 10'>Niteen tiedot docworks tietokannasta. DocId
               <a id='rescanLinks' onclick='copyTextToClipboard(\"" . $newDoc ."\")'>$docid</a></h3>";
            
            echo "<form action='update_doc_status.php' method='post'>";
               echo "<input type='hidden' name='docId' value='$docid'>";
               echo "<input type='hidden' name='page' value='$curren_page'>";
               if(checkDocIdStatus ($db, $docid) == 0) {
                  echo "<input type='submit' name = 'submit' onclick='return restartDoc($docid)' value = 'Restart'> ";
                  echo "<input type='submit' name = 'submit' onclick='return deleteDoc($docid)' value = 'Remove'> ";
                  echo "<input type='submit' name = 'submit' onclick='return deleteDocFromDebosit($docid)' value = 'Remove from Debosit'>";
               }else{
                  echo "<input type='submit' name = 'submit' value = 'Processing' disabled>";
               }
            echo "</form>";
            
            $input_id = "INPUT" . $docid . "_pool";
            $hidden_div_id = "DIV" . $docid . "_pool";
            $newDoc = $docid . "_pool";
            echo "<div id='$hidden_div_id' style='opacity: 0.0; position: fixed; top: 175px; right: 50%;'>";
            echo "<input id='$input_id' style='opacity: 0.0; type='text' value='$poolFolder'/>";
            echo "</div>"; 
            echo "<a id='rescanLinks' onclick='copyTextToClipboard(\"" . $newDoc ."\")'><img src='images/pool_folder.png' alt='IN-kansio' width='80' height='50'></a>";
            
            while($row = mssql_fetch_assoc($result)) {
            $input_id = "INPUT" . $docid . "_in";
            $hidden_div_id = "DIV" . $docid . "_in";
            $newDoc = $docid . "_in";
            $projekti = $row["Projekti"];
            $status = $row["Status"];
            $import_folder = "\\\\akte\\dw-in\\$projekti\\" . str_replace("\\", "/", $row["Nimi"]);
            echo "<div id='$hidden_div_id' style='opacity: 0.0; position: fixed; top: 175px; right: 50%;'>";
               echo "<input id='$input_id' style='opacity: 0.0; type='text' value='$import_folder'/>";
            echo "</div>";
            echo "<a id='rescanLinks' onclick='copyTextToClipboard(\"" . $newDoc ."\")'><img src='images/in_folder.png' alt='IN-kansio' width='80' height='50'></a>";
            echo "<table id = 'tableDoc'>";
               echo"<tr>";
                  foreach($row as $key=>$value){
                     echo "<th>$key</th>";
                  }
               echo "</tr>";
               echo"<tr>";
                  foreach($row as $key=>$value){
                     echo "<td>$value</td>";
                  }
               echo "</tr>";
            echo"</table>";
            }
            if($status == "Hylätty" || $status == "Virhe") {
               $xmlPath = getDocPoolFolder($docid, $isServer = 1) . $docid . ".xml";
               $rejectString = getRejectString($xmlPath);
               
               echo "<table id = 'tableDoc'>";
                  echo"<tr>";
                     echo "<th>Työ on $status tilassa:</th>";
                  echo "</tr>";
                  echo"<tr>";
                     echo "<td>$rejectString</td>";
                  echo "</tr>";
               echo"</table>";
            }      
         echo "</div>";
      echo "</div>";
   }
   echo "<div id = 'docIdPageFrame'>";
      echo "<div id = 'docInfo'>";
         echo "<h3 style='padding-bottom: 10'>Niteen tiedot dw_nlf_custom tietokannasta. DocId $docid</h3>";
         
         echo "<table id = 'tableDoc'>";
            $query_form_pool_main = "SELECT title as OTSIKKO, CAST(issuedate AS VARCHAR) ISSUE_PVM,
                  issn as ISSN, issuenum as nide, CAST(scandate AS VARCHAR) as skannattu,
                  CAST(dw_exportdate AS VARCHAR) as DW_export,
                  CONCAT('http://digi.kansalliskirjasto.fi/sanomalehti/binding/', digi_status,'?page=1') as Digi
                  from dw_nlf_custom.dbo.sl_status where docid = $docid";
            $result = mssql_query($query_form_pool_main);
            while($row = mssql_fetch_assoc($result)) {
               echo"<tr>";
                  foreach($row as $key=>$value){
                     echo "<th>$key</th>";
                  }
               echo "</tr>";
               echo"<tr>";
                  foreach($row as $key=>$value){
                     if($key == 'Digi') {
                        echo "<td><a href='$value' target='_new'>digi</a></td>";
                     } else {
                        echo "<td>$value</td>";                        
                     }
                  }
               echo "</tr>";
            }
         echo"</table>";
      echo "</div>";
   echo "</div>";
   
   mssql_free_result($result);
}

function docIdSearch($currentPage, $db) {
   
    include("sql_querys/querys_doc.php");
   
    echo "<div class='seartch'>Hae docid:llä";
        echo "<form action='docid_page.php' method='GET'>";
        echo "<input type='hidden' name='page' value='$currentPage'>";
        echo "<input type='text' name='docId'>";
        echo "<input type='submit'>";
        echo "</form>";

        if(isset($_SESSION['issnTitle'])) {
            
            $titleIssn = $_SESSION['issnTitle'];
            
            echo "<br>Hae vuosikerta: ";
            echo '<form id="kaupunki-form" method="post" action="volume_by_year.php">';
                echo "<input type='hidden' name='page' value='$currentPage'>";
                echo '<input type="text" id="tekstikentta" name="title" value="' . $titleIssn . '" onkeyup="haeKaupunki()" autocomplete="off">';
                echo '<ul id="kaupunki-dropdown"></ul><br><br>';
                // Syötä vuosi -kenttä näkyy vasta kun kaupunki on annettu
                echo '<div id="vuosi-input-container" style="display: block;">';
                   echo 'Vuosi:<br>';
                   echo '<input type="text" id="vuosikentta" name="year" onkeyup="haeVuosi()" autocomplete="off" autofocus>';
                   echo '<ul id="vuosi-dropdown"></ul>';
                   echo '<input type="submit" value="Lähetä">';
                echo '</div>';
            echo '</form>';
            
            echo "<ul id='vuosi-lista' style='display: none;'>";
                // Vuosilista
                $vuodet = getAllYearsFromSlStatusNEW($titleIssn);
                // Tulostetaan vuodet listana
                foreach ($vuodet as $vuosi) {
                    echo "<li>$vuosi</li>";
                }
            echo "</ul>";

        } else {
            echo "<br>Hae vuosikerta: ";
            echo '<form id="kaupunki-form" method="post" action="volume_by_year.php">';
                echo "<input type='hidden' name='page' value='$currentPage'>";
                echo '<input type="text" id="tekstikentta" name="title" onkeyup="haeKaupunki()" autocomplete="off">';
                echo '<ul id="kaupunki-dropdown"></ul><br><br>';
            echo '</form>';
        }
        
        
        echo "<ul id='kaupunki-lista' style='display: none;'>";
            // Kymmenen testikaupunkia
            $kaupungit = getAllTitleFromSlStatus();
            // Tulostetaan kaupungit listana
            foreach ($kaupungit as $kaupunki) {
                echo "<li>$kaupunki</li>";
            }
        echo "</ul>";
        


    echo "</div>";
    unset($_SESSION['issnTitle']);

   if (isset($_SESSION['nimi'])) {
   
      $sessioTitle = $_SESSION['nimi'];
      $sessionIssn = $_SESSION['issn'];
      
      $titles = getAllIssues($db, $allTitles, $sessioTitle);
      $issueNumbers = getTitleAllIssuesNumbers($db, $sessioTitle, $sessionIssn);
      
      echo "<div class='seartch'>Hae nimekkeellä";
      echo "<form action='issue_search.php' method='GET'>";
         echo "<input type='hidden' name='page' value='$currentPage'>";
         echo"<select name='nimike' onchange='this.form.submit()'>";
            foreach ($titles as $values) {
               echo"<option >$values</option>";
            }            
         echo "</select>";
         echo "<noscript><input type='submit' value='Submit'/></noscript>";
      echo "</form>";
      
      echo "<form action='issue_search.php' method='GET'>";
         echo "<input type='hidden' name='page' value='$currentPage'>";
         echo "<input type='hidden' name='nimike' value='$sessioTitle'>";
         echo"Numero: <select name='issueNum' onchange='this.form.submit()'>";
            foreach ($issueNumbers as $values) {
               echo"<option >$values</option>";
            }
         echo "</select>";
         echo "</br>";
         if($sessionIssn != "") {
            $issueDates = getTitleAllIssuesDates($db, $sessioTitle, $sessionIssn);
            
            echo"Päivä: <select name='issueDate'>";
               foreach ($issueDates as $values) {
                  echo"<option >$values</option>";
               }
            echo "</select>";
            echo "</br>";
            echo "<input type='submit' name = 'submit' value = 'Submit'>";
         }
         echo "<noscript><input type='submit' value='Submit'/></noscript>";
      echo "</form>";
      echo "</div>";
   
   } else {
   
   $titles = getAllIssues($db, $allTitles);
   echo "<div class='seartch'>Hae nimikkeellä";
   echo "<form action='issue_search.php' method='GET'>";
      echo "<input type='hidden' name='page' value='$currentPage'>";
      echo "<input type='hidden' name='issueNum' value=''>";
      echo "<input type='hidden' name='issueDate' value=''>";
      echo"<select name='nimike' onchange='this.form.submit()'>";
         foreach ($titles as $values) {
            echo"<option >$values</option>";
         }
      echo "</select>";
      echo "<noscript><input type='submit' value='Submit'/></noscript>";
   echo "</form>";
   echo "</div>";
   
   }
   unset($_SESSION['nimi']);
   unset($_SESSION['issn']); 
}

function getDocIdByIssueIssueNumberIssueDate($issue, $issueNumber, $issueDate, $db) {

    $query = "SELECT docid from dw_nlf_custom.dbo.sl_status where title = '$issue' and issuenum = $issueNumber and issuedate = '$issueDate'";
    $result = mssql_query($query);
    
    while($row = mssql_fetch_array($result)) {
        $docId = $row["docid"];
    }
    mssql_free_result($result);
    
    return $docId;

}

function checkIfDocIdExistsInDocWorksDB($db, $docId) {
   $query = "SELECT * from dW_Pool.dbo.Main where P_ARTID = $docId";
   $result = mssql_query($query);
   $hasRow = true;
   if(mssql_num_rows($result) == 0) {
      $hasRow = false;
   }
   mssql_free_result($result);
   return $hasRow;
}

function processErrors ( $errorFolder ) {
   
   $errorFilesCounter = 0;
   $filesInFolder = scandir($errorFolder);
   foreach ($filesInFolder as $file) {
      if (startsWith($file, '__akte_pas_process_transfer') && endsWith($file, '.err')) {
         $errorFilesCounter++;
      }  
   }
   
   return $errorFilesCounter;
   
}

function processingJobs ( $errorFolder ) {
   
   $errorFilesCounter = 0;
   $filesInFolder = scandir($errorFolder);
   foreach ($filesInFolder as $file) {
      if (startsWith($file, '__akte_pas_process_transfer') && endsWith($file, '.zip.tcl')) {
         $errorFilesCounter++;
      }  
   }
   
   return $errorFilesCounter;
   
}

function processErrorFiles ( $errorFolder, $tranferFolder, $transferFolderAkte, $current_page ) {
   
   $filesInFolder = scandir($errorFolder);
   $i = 0;

   $errorMessages = [];
   $errorMsgZipList = array();
   foreach ($filesInFolder as $file) {

      if (startsWith($file, '__akte_pas_process_transfer') && endsWith($file, '.err')) {
         
         $zipName = getZipNameFromErrorFile($file);
         $pathToZip = "$transferFolderAkte\\$zipName.zip\\$zipName";
         $errMessageFile = $errorFolder . "/" . $file;
         $errFile = str_replace(".err",".txt", $file);
         $errFile = $errorFolder . "/" . $errFile;
         $handle = fopen($errFile, "r");
         if ($handle) {
            $lineCounter = 0;
            $line_ood_or_even = ($i % 2 == 0) ? "errorfilenodd" : "errorfileeven";
            echo "<table width='50%';>";
               echo "<form action='pas_process/pas_process_error_update.php' method='post'>";
                  echo "<input type='hidden' name='zipPath' value='$tranferFolder/$zipName.zip'>";
                  echo "<input type='hidden' name='zipName' value='$zipName.zip'>";
                  echo "<input type='hidden' name='errFile' value='$errFile'>";
                  echo "<input type='hidden' name='errMessageFile' value='$errMessageFile'>";
                  echo "<input type='hidden' name='page' value='$current_page'>";
                  echo "<td class='$line_ood_or_even'>$pathToZip
                        <input type='submit' name = 'submit' onclick='return restartPasZip(\" $zipName.zip \")' value = 'Restart'>
                        <input type='submit' name = 'submit' onclick='return bypassPasZip(\" $zipName.zip \")' value = 'Ohita' disabled>
                        </td>";
               echo "</form>";
             while (($line = fgets($handle)) !== false) {
                if($lineCounter > 1) {
                   echo "<tr class='$line_ood_or_even'>";
                      echo "<th>$line</th>";
                   echo "</tr>";                 
                }
                 if($lineCounter == 2) {
                     $substrLine = substr($line, 0, 45);
                     array_push($errorMessages, $substrLine);
                     
                     if (array_key_exists($substrLine, $errorMsgZipList)) {
                         array_push($errorMsgZipList[$substrLine], "$zipName.zip");
                     } else {
                         $errorMsgZipList[$substrLine] = ["$zipName.zip"];
                     }
                 }
    
                $lineCounter++;
             }
             echo "<tr class='$line_ood_or_even'>.</tr>";
            echo "</table>";
            fclose($handle);

         }
      $i++;
      if($i == 100) {
          break;
      }
      }
     
   }
   
   
   $uniqueErrorMessage = array_unique($errorMessages);
   
   $messagesCount = array_count_values($errorMessages);

   echo "<table style='position:absolute; top:300px; left:55%;'>";
       echo "<tr>";
           echo "<th style='text-align: left'>Virheviesti subst(45)</th><th style='text-align: left'>num</th>";
       echo "</tr>";
       $i = 0;
       foreach($uniqueErrorMessage as $message) {
           $getNum = $messagesCount["$message"];
           $line_ood_or_even = ($i % 2 == 0) ? "errorfilenodd" : "errorfileeven";
           $zipListToString = implode(",", $errorMsgZipList[$message]);
           $errMsgStripped = preg_replace('/[^A-Za-z0-9\- ]/', '', $message);
           echo "<form action='pas_process/process_errors.php' method='post'>";
               echo "<input type='hidden' name='zipList' value='$zipListToString'>";
               echo "<input type='hidden' name='page' value='$current_page'>";
               echo "<input type='hidden' name='errorMsg' value='$errMsgStripped'>";
               
               echo "<tr class='$line_ood_or_even'>";
                   echo "<td>$message</td>";
                   echo "<td>$getNum</td>";
                   echo "<td><input type='submit' name = 'submit' onclick='return restartErrorJobs(\" $getNum \")' value = 'Restart' disabled></td>";
                   echo "<td><input type='submit' name = 'submit' onclick='return bypasstErrorJobs(\" $getNum \")' value = 'Ohita'></td>";
               echo "</tr>";
           echo "</form>";
           $i++;

}
   echo "</table>";

}

function failedJobs ( $faildeFolder ) {
   
   $failedFilesCounter = 0;
   $filesInFolder = scandir($faildeFolder);
   foreach ($filesInFolder as $file) {
      if (endsWith($file, '.log')) {
         $failedFilesCounter++;
      }  
   }
   
   return $failedFilesCounter;
   
}

function processFailedLogFiles ( $failedFolder ) {
   
   $filesInFolder = scandir($failedFolder);
   $i = 0;

   foreach ($filesInFolder as $file) {

      if (endsWith($file, '.log')) {

         $file = str_replace(".err",".txt", $file);
         $file = $failedFolder . "/" . $file;
         
         $handle = fopen($file, "r");
         if ($handle) {
            $lineCounter = 0;
            $line_ood_or_even = ($i % 2 == 0) ? "errorfilenodd" : "errorfileeven";
               echo "<table width='100%';>";
            echo "<td class='$line_ood_or_even'>$file</td>";
            while (($line = fgets($handle)) !== false) {
               if( strpos($line, 'ERROR') || strpos($line, 'WARNING')) {
                  echo "<tr class='$line_ood_or_even'>";
                     echo "<th>$line</th>";
                  echo "</tr>";                 
               }
               $lineCounter++;
            }
            echo "<tr class='$line_ood_or_even'>.</tr>";
            echo "</table>";
            fclose($handle);
         
         }
         
      $i++;
      }

   }
   
}

function filesInProcess ( $errorFolder ) {

   $filesInFolder = preg_grep('~\.(tcl)$~', scandir($errorFolder));
   $i = 0;

   foreach ($filesInFolder as $file) {

      $line_ood_or_even = ($i % 2 == 0) ? "errorfilenodd" : "errorfileeven";
      echo "<table width='100%';>";
         echo "<td class='$line_ood_or_even'>$file</td>";
         echo "<tr class='$line_ood_or_even'>.</tr>";
      echo "</table>";
      $i++;

   }
   
}

function startsWith( $haystack, $needle ) {
     $length = strlen( $needle );
     return substr( $haystack, 0, $length ) === $needle;
}

function endsWith( $haystack, $needle ) {
    $length = strlen( $needle );
    if( !$length ) {
        return true;
    }
    return substr( $haystack, -$length ) === $needle;
}

function transferFolder ( $Folder ) {
   
   $filesCounter = 0;
   $filesInFolder = scandir($Folder);
   foreach ($filesInFolder as $file) {
      if (endsWith($file, '.zip')) {
         $filesCounter++;
      }  
   }
   
   return $filesCounter;
   
}

function getZipNameFromErrorFile ( $fileName) {
   $file = $fileName;
   $strSeporator = "transfer_";
   $pos = strpos($file, $strSeporator) + strlen($strSeporator);
   $posLength = strlen($strSeporator);
   $zipname = substr($file, $pos);
   $extensionPos = strpos($zipname, ".zip");
   $zipname = substr($zipname, 0, $extensionPos);
   return $zipname;
}

function getAllProcessingZip ( $connection, $query ) {

   $stid_diona = oci_parse($connection, $query);
   $res_diona = oci_execute($stid_diona);

   if (!$res_diona) {
           echo "error:".oci_error()."--";
               $e = oci_error();
               print_r($e);
               trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
           exit(9);

   }

    while (($row = oci_fetch_array($stid_diona, OCI_ASSOC+OCI_RETURN_NULLS)) != false) {
        $allZip = $row["MUUNNETTAVIA"];
    }
    
    return $allZip;
}

function updatePasZipToBypass($zipName, $comment) {  
    $sql = "update dw_nlf_custom.dbo.lehtipas set [status] = 'ohitettu', comment = '$comment' where zip_name = '$zipName';";
    $result = mssql_query($sql) or die(mssql_errors());
    mssql_free_result($result);
    updatePasZipInprocessingToNull($zipName);
}

function updatePasZipInprocessingToNull($zipName) {
    $sql = "update dw_nlf_custom.dbo.lehtipas set [in_processing] = NULL where zip_name = '$zipName';";
    $result = mssql_query($sql) or die(mssql_errors());
    mssql_free_result($result);
}

function updatePasZipStatus( $zipName, $status ) {
    $sql = "update dw_nlf_custom.dbo.lehtipas set [status] = '$status' where zip_name = '$zipName';";
    $result = mssql_query($sql) or die(mssql_errors());
    mssql_free_result($result);
}

function updatePassedRestartedZip($zipName) {
    $sql = "update dw_nlf_custom.dbo.lehtipas set comment = 'Ohitettu paketti on restartattu', [status] = 'kopioitu muunnettavaksi', in_processing = NULL where zip_name = '$zipName';";
    echo $sql;
    $result = mssql_query($sql) or die(mssql_errors());
    mssql_free_result($result);    
}

function usedSpaceInPreservation($db) {
   $query = "select (sum(PAS_zip_size)/1024/1024/1024/1024) as summa from dw_nlf_custom.dbo.lehtipas where [status] = 'sailytyksessa PASsissa'";
   $result = mssql_query($query);
   
   while($row = mssql_fetch_array($result)) {
   $summa = $row["summa"];
   }
   mssql_free_result($result);
   return $summa;
}

function passedJobs () {
 
    $distinct_comments = "SELECT comment, count(*) as num FROM dw_nlf_custom.dbo.lehtipas where [status] = 'Ohitettu' GROUP BY comment";
    $result = mssql_query($distinct_comments);
    $i = 0;
    echo "<table width='50%';>";
        echo "<tr>";
            echo "<th style='text-align: left'>comment</th><th style='text-align: left'>num</th>";
        echo "</tr>";
        while($row = mssql_fetch_array($result)) {
            $comment = $row["comment"];
            $num = $row["num"];
            $line_ood_or_even = ($i % 2 == 0) ? "errorfilenodd" : "errorfileeven";
            echo "<tr>";
                echo "<td class='$line_ood_or_even'>$comment</td>";
                echo "<td class='$line_ood_or_even'>$num</td>";
                echo "<form action='pas_process/pas_process_restart_passed.php' method='post'>";
                    echo "<input type='hidden' name='comment' value = '$comment'>";
                    echo "<td class='$line_ood_or_even'><input type='submit' name = 'submit' onclick='return restartPassedJobs(\" $comment \")' value = 'restart'></td>";
                echo "</form>";
            echo "</tr>";
            $i++;
            
        }
    echo "</table>";
    
    mssql_free_result($result);
   
}

function RejectedJobs () {
 
    $query = "SELECT ROW_NUMBER() OVER (ORDER BY CSC_rejected_folder) as num, pas_zip_name, convert(varchar,CSC_rejected_folder,120) as CSC_rejected_folder FROM dw_nlf_custom.dbo.lehtipas where [status] = 'hylatty PASsissa'";
    $result = mssql_query($query);
    $i = 0;
    echo "<table>";
        echo "<tr>";
            echo "<th style='text-align: left'>ROW</th><th style='text-align: left'>pas_zip_name</th><th style='text-align: left'>CSC rejected kansio</th>";
        echo "</tr>";
        while($row = mssql_fetch_array($result)) {
            $rowNum = $row["num"];
            $zip = $row["pas_zip_name"];
            $rejectedFolder = $row["CSC_rejected_folder"];
            //$num = $row["num"];
            $line_ood_or_even = ($i % 2 == 0) ? "errorfilenodd" : "errorfileeven";
            echo "<tr>";
                echo "<td class='$line_ood_or_even'>$rowNum</td>";
                echo "<td class='$line_ood_or_even'><a href='pas_process_rejected_details.php?zip=$zip'>$zip</a></td>";
                echo "<td class='$line_ood_or_even'>$rejectedFolder</td>";
            echo "</tr>";
            $i++;
            
        }
    echo "</table>";
    
    mssql_free_result($result);
   
}

function RejectedDetailsJobs ($pasZipName, $PAS_csc_rejected_akte, $PAS_csc_rejected_MNT, $current_page) {
    
    $zipLocation = "$PAS_csc_rejected_akte\\$pasZipName";
    // Määritä polku, josta tiedostoja halutaan listata
    $polku = "$PAS_csc_rejected_MNT/$pasZipName";
    // Listaa tiedostot ja hakemistot
    $tiedostot = scandir($polku);
    // Tulosta tiedostojen ja hakemistojen lista
    $reportHtmlFile = "NOT_FOUND";
    $reportXmlFile = "NOT_FOUND";
    foreach ($tiedostot as $tiedosto) {
         if (endsWith($tiedosto, ".xml")) {
             $reportXmlFile = $tiedosto;
         }
         if (endsWith($tiedosto, ".html")) {
             $reportHtmlFile = $tiedosto;
         }
    }
    
    $query = "SELECT pas_zip_name, uri, zip_name, convert(varchar,CSC_upload_time,120) as CSC_upload_time,
             convert(varchar,CSC_rejected_folder,120) as CSC_rejected_folder,
             convert(varchar,CSC_accepted_folder,120) as CSC_accepted_folder
             FROM dw_nlf_custom.dbo.lehtipas where [status] = 'hylatty PASsissa' and pas_zip_name = '$pasZipName'";
    $result = mssql_query($query);
    echo "<table id = 'tableDoc'>";
        echo "<tr>";
            echo "<th>zip_name</th>";
            echo "<th>pas_zip_name</th>";
            echo "<th>CSC_upload_time</th>";
            echo "<th>CSC_rejected_folder</th>";
            echo "<th>CSC_accepted_folder</th>";
        echo "</tr>";
        while($row = mssql_fetch_array($result)) {
            $zip = $row["zip_name"];
            $pasZip = $row["pas_zip_name"];
            $upload = $row["CSC_upload_time"];
            $rejectedFolder = $row["CSC_rejected_folder"];
            $acceptedFolder = $row["CSC_accepted_folder"];
            $uri = $row["uri"];
            $line_ood_or_even = ($i % 2 == 0) ? "errorfilenodd" : "errorfileeven";

            echo "<tr>";
                echo "<td><input type='text' size='50' readonly='readonly' value='$zip' id='zipName'>";
                echo "<button onclick='copyRejectedFilePathToClipboard(\"zipName\")'>kopio</button></td>";
                echo "<td><input type='text' size='50' readonly='readonly' value='$pasZip' id='pasZipName'>";
                echo "<button onclick='copyRejectedFilePathToClipboard(\"pasZipName\")'>kopio</button></td>";
                echo "<td>$upload</td>";
                echo "<td>$rejectedFolder</td>";
                echo "<td>$acceptedFolder</td>";
            echo "</tr>";            
        }
    echo "</table>";
    echo "<table id = 'tableDoc'>";
            echo "<tr>";
            echo "<th>zip folder</th>";
        echo "</tr>";
        echo "<tr>";
            //echo "<td>$zipLocation</td>";
            echo "<td><input type='text' size='200' readonly='readonly' value='$zipLocation' id='folderInput'>";
            echo "<br><button onclick='copyRejectedFilePathToClipboard(\"folderInput\")'>kopio</button></td>";            
        echo "<tr>";
        echo "<tr>";
            echo "<th>HTML report</th>";
        echo "</tr>";
        echo "<tr>";
            //echo "<td>$zipLocation\\$reportHtmlFile</td>";
            echo "<td><input type='text' size='200' readonly='readonly' value='$zipLocation\\$reportHtmlFile' id='htmlInput'>";
            echo "<br><button onclick='copyRejectedFilePathToClipboard(\"htmlInput\")'>kopio</button></td>";
        echo "<tr>";
        echo "<tr>";
            echo "<th>XML report</th>";
        echo "</tr>";
        echo "<tr>";
            $showXmlReport = "http://selene.docworks.lib.helsinki.fi/sl-prosessi/show_xml_report_failures.php?file=$zipLocation\\$reportXmlFile";
            echo "<tr>";
                echo "<td><a href='$showXmlReport' target='_blank'>$reportXmlFile</a></td>";
            echo "</tr>";
            echo "<td><input type='text' size='200' readonly='readonly' value='$zipLocation\\$reportXmlFile' id='xmlInput'>";
            echo "<br><button onclick='copyRejectedFilePathToClipboard(\"xmlInput\")'>kopio</button></td>";
        echo "<tr>";
        echo "<tr>";
               echo "<form action='pas_process/pas_restart_zip.php' method='post'>";
                   echo "<input type='hidden' name='page' value='$current_page'>";
                   echo "<input type='hidden' name='zip_name' value='$zip'>";
                   echo "<input type='hidden' name='pas_zip_name' value='$pasZip'>";
                   echo "<input type='hidden' name='uri' value='$uri'>";
                   echo "<tr>";
                       if ($rejectedFolder < "2024-01-22"){
                           echo "<td>Restar toiminto ei ole mahdollista 'Samuli'</td>"; 
                       } else {
                           echo "<td><input type='submit' name = 'submit' onclick='return restartPasZip(\" $zip \")' value = 'Restart'></td>"; 
                       }
                       
                   echo "</tr>";
                   //echo "<tr>";
                   //    echo "<td><input type='submit' name = 'submit' onclick='return restartPasZip(\" $zip \")' value = 'Ohita'></td>";
                   //echo "</tr>";
               echo "</form>";
        echo "<tr>";
	echo "<tr><th>Failures</th><tr>";
	echo "<tr><td><pre>";
	listXmlFailures ("$zipLocation\\$reportXmlFile");
	echo "</pre></td>";
	echo "</tr>";
    echo "</table>";

    mssql_free_result($result);
   
}


function formatXml($xml) {
    $dom = new DOMDocument('');
    $dom->version = "";
    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;
    $dom->loadXML($xml);
    return $dom->saveXML();
}

function listXmlFailures ($file) {

$replacement = "/mnt/akte/pas_process";
$newfile = str_replace("\\", "/", $file); // Replace backslashes with slashes
$newfile = str_replace("//akte/pas_process", $replacement,$newfile);
$file=$newfile;

$xml = simplexml_load_file($file);

$prefix = 'premis';

$namespaceUri = $xml->getDocNamespaces()[$prefix];

$xml->registerXPathNamespace($prefix, $namespaceUri);


$nodes = $xml->xpath("//premis:event[premis:eventOutcomeInformation/premis:eventOutcome='failure']");

foreach ($nodes as $node) {
    /* echo htmlspecialchars(preg_replace("/<\\?xml.*\\?>/",'',formatXml($node->saveXML(),1))); */
    echo preg_replace("/<\\?xml.*\\?>/",'',formatXml($node->saveXML(),1));
    echo "\n--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n";

  }
}



function CreateTaskTclFile ($zipName) {
    $workDir="/mnt/akte/dw-config/work/extratasks/";
    $taskFileName="__akte_pas_process_transfer_".$zipName.".tcl";
    $taskFile = fopen($workDir.$taskFileName, "w") or die("Unable to open file!");
    $txt = "source [expandpath ***PROJECT_CFG***\\\\common_export_scripts\\\\PAS_migratio_v3\\\\tasked_process_script.tcl]\n";
    fwrite($taskFile, $txt);
    $txt = "ScriptExecuteTemp \\\\\\\\akte\\\\pas_process\\\\transfer ".$zipName." \n";
    fwrite($taskFile, $txt);
    fclose($taskFile);
}

function GetOneColumnFromSlstatusByDocid ($docid, $column) {
    $query = "select $column from dw_nlf_custom.dbo.sl_status where docid = $docid";
    $result = mssql_query($query);

    while($row = mssql_fetch_array($result)) {
        $value= $row[0];
    }
    mssql_free_result($result);
    return $value; 
}

function GetExtrataskFileForZip ($zipName, $extrataskFolder) {

    $matchFiles = [];
    $allfiles = scandir($extrataskFolder);
    foreach ($allfiles as $file) {
        if (strstr($file, $zipName)) {
            array_push($matchFiles, $file);
        }
    }
    
    return $matchFiles;
}

function GetPoolFolderLowImages ($docId) {

     $docId = $docId;
     $poolFolder = getDocPoolFolder($docId, 1);
     $poolFolder = str_replace("\\", "/", $poolFolder);
     $poolFolderAkte = getDocPoolFolder($docId);
     
     $pattern = "$docId*.low"; // Example: List all files with .txt extension
     
     // Check if the directory exists
     
     if (is_dir($poolFolder)) {
         // Get an array of files matching the pattern
         $files = glob($poolFolder . DIRECTORY_SEPARATOR . $pattern);
         //sort($files);
         usort($files, function($a, $b) {
             // Extract the numeric part from each string
             $aParts = explode('_', $a);
             $bParts = explode('_', $b);
             $aNumber = (int)$aParts[1];
             $bNumber = (int)$bParts[1];
             
             // Compare the numeric parts
             if ($aNumber == $bNumber) {
                 return strcmp($a, $b); // If numbers are equal, sort lexicographically
             }
             return $aNumber - $bNumber; // Otherwise, sort numerically
         });
         // Output each file name
         foreach ($files as $file) {
             $imagesList[] = $file;
         }
     } else {
         echo "The directory does not exist.";
     }
     
     return $imagesList;
     
}

function GetPoolFolderLowImagesOLD ($docId) {

     $docId = $docId;
     $poolFolder = getDocPoolFolder($docId, 1);
     $poolFolder = str_replace("\\", "/", $poolFolder);
     $poolFolderAkte = getDocPoolFolder($docId);
     $xmlFile = "$poolFolder/$docId.xml";
     $xmlFile = str_replace("\\", "/", $xmlFile); // Replace backslashes with slashes
     // Tiedoston polku
     //$xmlFile = '/mnt/akte/dw-pool/79/52/427952.xml';
     
     // Lataa XML-tiedosto SimpleXML-objektiin
     $xml = simplexml_load_file($xmlFile);
     $imagesList = [];

     $pattern = "$docId*.low"; // Example: List all files with .txt extension
     
     // Check if the directory exists
     
     if (is_dir($poolFolder)) {
         // Get an array of files matching the pattern
         $files = glob($poolFolder . DIRECTORY_SEPARATOR . $pattern);
         sort($files);
         // Output each file name
         foreach ($files as $file) {
             $imagesList[] = $file;
         }
     } else {
         echo "The directory does not exist.";
     }
     
     return $imagesList;
     
}

function validateDate($date, $format = 'Y-m-d') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

function GetNextDocidToProcess( $issn, $year ) {
    // TODO hae seuraava hyväksyttävä docid
    $query_get_next_inspection_doc = "select docid FROM [dw_nlf_custom].[dbo].[sl_status] 
    where [status] = 'QA'
    and dw_deletedate is null
    and issn = '$issn'
    and year(issuedate) = '$year'
    ORDER BY TRY_CAST(issuenum AS INT) ASC";
 
    $result = mssql_query($query_get_next_inspection_doc);
    $docidList = [];
    while($row = mssql_fetch_array($result)) {
        $docidList[] = $row["docid"];
    }
    mssql_free_result($result);
    
    return $docidList;
}

function findMissingNumber($numbers) {
    sort($numbers); // Järjestetään numerot

    $missingNumbers = array();

    $min = min($numbers);
    $max = max($numbers);

    for ($i = $min; $i <= $max; $i++) {
        if (!in_array($i, $numbers)) {
            $missingNumbers[] = $i;
        }
    }

    return $missingNumbers;
}

function checkYearMissingVolumes($volumeList) {
 
    foreach ($volumeList as $subarray) {
        $numbers[] = $subarray['number'];
    }
    
    // Get the maximum and minimum numbers
    $max_number = max($numbers);
    $min_number = min($numbers);
    
    for ($i = $min_number; $i <= $max_number; $i++) {
        if (!in_array($i, $numbers)) {
            $missingNumbers[] = $i;
            $volumeList[] = array("number" => $i, "details" => array("status" => 'missing',
                     "issuenum" => $i,
                     "QA_comment" => $row["QA_comment"],
                     "issuedate" => 'missing',
                     "shortageType" => "",
                     "shortageDescriptio" => "",
                     "docid" => 'missing',
                     "ID" => 'unknow'));
        }
    }
    sort($volumeList);
    return $volumeList;

}

function GetAllIssuesByYearAndIssn( $issn, $year ) {
 
    $query = "select sl.docid, sl.ID, sl.[status], sl.QA_error_comment, sl.issuenum, format(sl.issuedate,'yyyy-MM-dd') as issuedate, vo.[type], vo.[description]
              from dw_nlf_custom.dbo.sl_status sl
              LEFT JOIN dw_nlf_custom.dbo.volume_problems_log vo on sl.ID = vo.ID
              where sl.issn = '$issn'
              and year(sl.issuedate) = '$year'
              ORDER BY TRY_CAST(issuenum AS INT) ASC";
    //echo $query . '<br>';
    $result = mssql_query($query);
    $docidList = [];
    while($row = mssql_fetch_array($result)) {
        //echo $row["type"] . "<br>";
        //$docidList[] = [$row["issuenum"], $row["status"], $row["QA_comment"]];
        $docidList[] = array("number" => $row["issuenum"], "details" => array("status" => $row["status"],
                             "issuenum" => $row["issuenum"],
                             "QA_comment" => $row["QA_error_comment"],
                             "issuedate" => $row["issuedate"],
                             "shortageType" => $row["type"],
                             "shortageDescriptio" => $row["description"],
                             "docid" => $row["docid"],
                             "ID" => $row["ID"]));
    }
    mssql_free_result($result);
    sort($docidList);
    $newList = checkYearMissingVolumes($docidList);
    
    return $newList;
 
}

function checkIfDocumentIsAccepted($docId) {
    $sql = "select status, status_changed_by from dw_nlf_custom.dbo.sl_status where [docid] = $docId";
    $status = "";
    $acceptedBy = "";
    $result = mssql_query($sql) or die(mssql_errors());
    while($row = mssql_fetch_array($result)) {
        $status = $row["status"];
        $acceptedBy = $row["status_changed_by"];
    }
    mssql_free_result($result);

    if($status == "accepted") {
        return $acceptedBy;
    } else {
        return "";
    }
}

function getAllTitleFromSlStatus(){
 
    $query = "select distinct title from dw_nlf_custom.dbo.sl_status";
    $result = mssql_query($query);
    $titleArray = array($selectedTitle);
    
    while($row = mssql_fetch_array($result)) {
       array_push($titleArray, $row["title"]);
    }
    
    mssql_free_result($result);
    return $titleArray;

}

function getAllYearsFromSlStatus(){
    $query = "SELECT distinct SUBSTRING(CONVERT(VARCHAR(10), issuedate, 120), 1, 4) AS years from dw_nlf_custom.dbo.sl_status order by years";
    $result = mssql_query($query);
    $yearArray = array($selectedTitle);
    
    while($row = mssql_fetch_array($result)) {
       array_push($yearArray, $row["years"]);
    }
    
    mssql_free_result($result);
    return $yearArray;

}


function getAllYearsFromSlStatusNEW($title){
 
    $query = "SELECT distinct SUBSTRING(CONVERT(VARCHAR(10), issuedate, 120), 1, 4) AS years from dw_nlf_custom.dbo.sl_status where title = '$title' order by years";
    $result = mssql_query($query);
    $yearArray = array($selectedTitle);
    
    while($row = mssql_fetch_array($result)) {
       array_push($yearArray, $row["years"]);
    }
    
    mssql_free_result($result);
    
    return $yearArray;

}

function getIssnByTitle($title){
 
    $query = "select TOP(1) issn from dw_nlf_custom.dbo.sl_status where title =  '$title'";
    $result = mssql_query($query);
    
    while($row = mssql_fetch_array($result)) {
       $value = $row["issn"];
    }
    
    mssql_free_result($result);
    
    return $value;

}

function generateFormattedString($title, $desiredLength = 50) {
    // Etsi "+"-merkki ja jaa muuttuja sen perusteella
    $parts = explode("+", $title);

    // Otsikko on ensimmäinen osa
    $titlePart = $parts[0];

    // ISSN on toinen osa, jos sitä ei ole, käytä tyhjää merkkijonoa
    $issnPart = isset($parts[1]) ? $parts[1] : "";

    // Laske välilyöntien määrä otsikon ja ISSN-numeron välillä
    $spaceLength = $desiredLength - strlen($titlePart) - strlen($issnPart) - 1; // Yhden välilyönnin verran tilaa

    // Jos $formattedString on pidempi kuin $desiredLength, lyhennä $titlePart-muuttujaa
    if ($spaceLength < 1) {
        $titlePart = substr($titlePart, 0, $desiredLength - strlen($issnPart) - 1); // Lyhennä otsikkoa
        $spaceLength = $desiredLength - strlen($titlePart) - strlen($issnPart) - 1; // Päivitä välilyöntien määrä
    }

    // Luodaan välilyönnit
    $spaces = str_repeat("&nbsp;", max(0, $spaceLength));

    // Luodaan lopullinen muotoiltu merkkijono
    $formattedString = $titlePart . $spaces . " " . $issnPart;

    // Jos $formattedString on lyhyempi kuin $desiredLength, täytetään loput välilyönneillä
    if (strlen($formattedString) < $desiredLength) {
        $formattedString .= str_repeat("&nbsp;", $desiredLength - strlen($formattedString));
    }

    return $formattedString;
}

function translateStatus($status) {
    switch ($status) {
        case 'processing':
            return 'prosessoidaan';
        case 'hold':
            return 'odottamassa';
        case 'change':
            return 'muutettavana';
        case 'QA':
            return 'tarkastuksessa';
        case 'accepted':
            return 'hyväksytty';
        case 'rejected':
            return 'hylätty';
        case 'rescan':
            return 'rescan';
        case 'remove':
            return 'poistetaan';
        case 'digi_importing':
            return 'siirrossa digiin';
        case 'digi':
            return 'digissä';
        case 'completed':
            return 'valmis';
        default:
            return 'tuntematon tila';
    }
}

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
 
 function bypasstErrorJobs(numOfFiles) {
     
     var message = "Oletko varma että haluat OHITTAA " + numOfFiles + "kpl paketteja";
 
     if (confirm(message)) {         
         return true;         
     } else {
         return false;
     }
 }
 
 function restartErrorJobs(numOfFiles) {
     
     var message = "Oletko varma että haluat uudellen ajaa " + numOfFiles + "kpl paketteja";
 
     if (confirm(message)) {         
         return true;         
     } else {
         return false;
     }
 }
 
 function restartPassedJobs(comment) {
     
     var message = "Oletko varma että haluat uudellen ajaa ohitetut paketit commentilla " + comment;
 
     if (confirm(message)) {         
         return true;         
     } else {
         return false;
     }
 }
 
 function bypassPasZip(zip) {
    
    var message = "Otetaanko varmistus viesti pois, niin ei tarvi aina naksutella kaks kertaa?\n"
      + "Oletko varma että haluat ohittaa paketin " + zip;

    if (confirm(message)) {         
        return true;         
    } else {
        return false;
    }
}
   
function restartPasZip(zip) {
    
    var message = "Otetaanko varmistus viesti pois, niin ei tarvi aina naksutella kaks kertaa?\n"
      + "Oletko varma että haluat uudelleen käynnistää paketin " + zip;

    if (confirm(message)) {         
        return true;         
    } else {
        return false;
    }
}

function restartDoc(docId) {

    var message = "Oletko varma että haluat uudelleen käynnistää työn? \n"
                + "DOC_id : " + docId;

    if (confirm(message)) {
        return true;
    } else {
        return false;
    }
}

function deleteDoc(docId) {

    var message = "Oletko varma että haluat poistaa työn? \n"
                + "DOC_id : " + docId;

    if (confirm(message)) {
        return true;
    } else {
        return false;
    }
}

function deleteDocFromDebosit(docId) {

    var message = "Oletko varma että haluat poistaa työn Debositin tietokannasta? \n"
                + "DOC_id : " + docId;

    if (confirm(message)) {
        return true;
    } else {
        return false;
    }
}

function copyTextToClipboard(docId) {

   var div = "#DIV" + docId;
   var inputId = "#INPUT" + docId;

   <?php $_SESSION['message'] = "Kohde kopioitu leikepöydälle"; ?>

   var copyText = document.querySelector(inputId);

   copyText.select();
   document.execCommand("copy");
   $("#okMessage").fadeIn();
   $("#okMessage").fadeOut(6000);

}

function displayOkMessage() {

   $("#displayOkMessage").fadeIn();
   $("#displayOkMessage").fadeOut(10000);

}

function displayErrorMessage() {

   $("#displayErrorMessage").fadeIn();
   $("#displayErrorMessage").fadeOut(20000);

}

function copyRejectedFilePathToClipboard(idInput) {
  // Get the text field

  var copyText = document.getElementById(idInput);
  // Select the text field
  copyText.select();
  document.execCommand("copy");
  <?php $_SESSION['message'] = "Kohde kopioitu leikepöydälle"; ?>
  $("#okMessage").fadeIn();
  $("#okMessage").fadeOut(6000);  
  // Alert the copied text
}



</script>
